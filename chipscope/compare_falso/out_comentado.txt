;Salto para a main
0x0000     BRAI      0x21

;Prepara��o da stack
0x0021     MOVI      r14,32	//posi��o de inicio das variaveis globais
0x0022     PUSH      r0		//placeholder (guarda um espa�o na stack)
0x0023     PUSH      r16	//endere�o das variaveis locais main
0x0024     MOV       r16,r31	//stack pointer no framebase
0x0025     CALL      0x27	//salto para a main (coloca 0x0027 na stack)
0x0026     BRI       -0x0	//jump$

;fun��o main
0x0027     POP       r1		//tira da stack o valor do program counter
0x0028     SWI       r1,r16,2	//guarda o valor no final da stack (placeholder)

;Inicializa��o das vari�veis
0x0029     MOVI      r1,12	//inicializa��o da variavel [num1] com 12;
0x002A     PUSH      r1		//guarda valor na stack
0x002B     MOVI      r2,0	//(declara��o) inicializa��o de [res] com 0;
0x002C     PUSH      r2		//guarda valor na stack

;Context saving
0x002D     PUSH      r1		//faz segundo push porque � argumento (por valor)
0x002E     PUSH      r0		//placeholder (guarda um espa�o na stack)
0x002F     PUSH      r16	//endere�o das variaveis locais para a fun��o
0x0030     MOV       r2,r31	//move o stack pointer para r2

;Fun��o da main
0x0031     LWBI      r4,r16,-0	//load para r4 do endere�o das variaveis locais
0x0032     PUSH      r4		//guarda r4 na stack
0x0033     MOV       r16,r2	//move stack pointer (contido r2) para r16
0x0034     CALL      0x3B	//chama a fun��o (coloca 0x0035 na stack)

;Main ap�s chamada da fun��o
0x0035     POP       r1		//faz pop do r1 da subrotina
0x0036     SWBI      r15,r16,-1	//store com imediato negativo
0x0037     MOVI      r15,0	//move 0 para r15 (registo de return)
0x0038     MOV       r31,r16	//carrega novo valor do stack pointer
0x0039     POP       r16	//tira r16 da stack
0x003A     RET			//retorna (final da main)

;Fun��o compara
0x003B     POP       r1		//tira da stack o valor do program counter
0x003C     SWI       r1,r16,2	//guarda o valor na stack (placeholder)
0x003D     LWBI      r1,r16,-0	//load para r1 do endere�o das variaveis locais

;Compara�ao
0x003E     MOVI      r3,21	//move 21 para r3
0x003F     CMP       r2,r1,r3	//compara r1 e r3 e guarda resultado em r2
0x0040     MOVI      r3,3	//muda o valor do salto
0x0041     BEQ       r2,r3	//salta se igual (salta 3, para 0x0044)
0x0042     MOVI      r2,0	//move 0 (falso) para r2
0x0043     BRI       0x2	//salta a proxima instru��o
0x0044     MOVI      r2,1	//move 1 (verdadeiro) para r2

;If
0x0045     MOVI      r4,5	//muda o valor de salto
0x0046     CMP       r3,r2,r0	//compara r2 e 0 e guarda o resultado em r3
0x0047     BEQ       r3,r4	//salta se igual (salta 5, para 0x004c)
0x0048     MOVI      r15,1	//move 1 para r15 (registo de return)
0x0049     MOV       r31,r16	//move para o stack pointer (r31) o valor do PFB
0x004A     POP       r16	//tira r16 da stack
0x004B     RET			//retorna (para main, verdadeiro)
0x004C     MOVI      r15,0	//move 0 para r15 (registo de return)
0x004D     MOV       r31,r16	//move para o stack pointer (r31) o valor do PFB
0x004E     POP       r16	//tira r16 da stack
0x004F     RET			//retorna (para main, falso)
