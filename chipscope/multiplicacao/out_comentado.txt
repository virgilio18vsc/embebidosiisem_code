;Salto para a main
0x0000     BRAI      0x21

;Prepara��o da stack
0x0021     MOVI      r14,32	//posi��o de inicio das variaveis globais
0x0022     PUSH      r0		//placeholder (guarda um espa�o na stack)
0x0023     PUSH      r16	//endere�o das variaveis locais main
0x0024     MOV       r16,r31	//stack pointer no framebase
0x0025     CALL      0x27	//salto para a main (coloca 0x0027 na stack)
0x0026     BRI       -0x0	//jump$

;fun��o main
0x0027     POP       r1		//tira da stack o valor do program counter
0x0028     SWI       r1,r16,2	//guarda o valor no final da stack (placeholder)

;inicializa��o das variaveis para a multiplica��o
0x0029     MOVI      r1,2	//inicializa��o da variavel [num1] com 2;
0x002A     PUSH      r1		//guarda valor na stack
0x002B     MOVI      r2,3	//inicializa��o da variavel [num2] com 3;
0x002C     PUSH      r2		//guarda valor na stack
0x002D     MOVI      r3,0	//(declara��o) inicializa��o de [res] com 0;
0x002E     PUSH      r3		//guarda valor na stack

;context saving & chamada da fun��o
0x002F     PUSH      r1		//faz segundo push (??)
0x0030     PUSH      r0		//placeholder (guarda um espa�o na stack)
0x0031     PUSH      r16	//endere�o das variaveis locais para a fun��o
0x0032     MOV       r2,r31	//move o stack pointer para r2
0x0033     LWBI      r4,r16,-0	//load para r4 do endere�o das variaveis locais
0x0034     PUSH      r4		//guarda r4 na stack
0x0035     LWBI      r5,r16,-1	(?)
0x0036     PUSH      r5		//guarda r5 na stack
0x0037     MOV       r16,r2	//move o stack pointer (contido r2) para r16
0x0038     CALL      0x3F	//chama a fun��o (coloca 0x0039 na stack)

;main ap�s chamda da fun��o
0x0035     POP       r1		//faz pop do r1 (??)
0x0036     SWBI      r15,r16,-1	//store com imediato negativo
0x0037     MOVI      r15,0	//move 0 para r15 (registo de return)
0x0038     MOV       r31,r16	//carrega novo valor do stack pointer
0x0039     POP       r16	//tira r16 da stack
0x003A     RET			//retorna (final da main)

;fun��o de multiplica��o
0x003F     POP       r1		//tira da stack o valor do program counter
0x0040     SWI       r1,r16,2	//guarda o valor na stack (placeholder)
0x0041     LWBI      r1,r16,-0	//load para r1 do primeiro parametro
0x0042     LWBI      r2,r16,-1 	//load para r2 do segundo parametro
0x0043     MOVI      r3,0	//inicializa o resultado da multiplica��o a 0
0x0044     MOV       r4,r2	//move r2 (valor de mul) para auxiliar r4
0x0045     MOVI      r5,4	//altera o valor de salto
0x0046     CMP       r6,r4,r0	//compara r4 com zero e guarda resultado em r6
0x0047     BEQ       r6,r5	//salta se igual (salta 4, para 0x004b)
0x0048     ADD       r3,r3,r1	//aumenta o resultado da multiplica��o por 1
0x0049     SUBI      r4,r4,1	//subtrai 1 ao auxiliar r4 
0x004A     BRI       -0x4	//salto incondicional para o inicio do loop
0x004B     MOV       r15,r3	//move resultado para r15 (registo de retorno)
0x004C     MOV       r31,r16	//move para o stack pointer (r31) o valor do PFB
0x004D     POP       r16	//retira r16 da stack
0x004E     RET			//retorna (para main)
