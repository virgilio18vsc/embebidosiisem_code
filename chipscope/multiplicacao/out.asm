0x0000     BRAI      0x21		//Salto para o código de load

0x0021     MOVI      r14,32		//Endereço das variáveis globais 
0x0022     PUSH      r0		//Place Holder
0x0023     PUSH      r16		//Save do frame_base
0x0024     MOV       r16,r31		
0x0025     CALL      0x27		
0x0026     BRI       -0x0


0x0027     POP       r1
0x0028     SWI       r1,r16,2
0x0029     MOVI      r1,2
0x002A     PUSH      r1
0x002B     MOVI      r2,3
0x002C     PUSH      r2
0x002D     MOVI      r3,0
0x002E     PUSH      r3
0x002F     PUSH      r1
0x0030     PUSH      r0
0x0031     PUSH      r16
0x0032     MOV       r2,r31
0x0033     LWBI      r4,r16,-0
0x0034     PUSH      r4
0x0035     LWBI      r5,r16,-1
0x0036     PUSH      r5
0x0037     MOV       r16,r2
0x0038     CALL      0x3F
0x0039     POP       r1
0x003A     SWBI      r15,r16,-2
0x003B     MOVI      r15,0
0x003C     MOV       r31,r16
0x003D     POP       r16
0x003E     RET
0x003F     POP       r1
0x0040     SWI       r1,r16,2
0x0041     LWBI      r1,r16,-0
0x0042     LWBI      r2,r16,-1
0x0043     MOVI      r3,0
0x0044     MOV       r4,r2
0x0045     MOVI      r5,4
0x0046     CMP       r6,r4,r0
0x0047     BEQ       r6,r5
0x0048     ADD       r3,r3,r1
0x0049     SUBI      r4,r4,1
0x004A     BRI       -0x4
0x004B     MOV       r15,r3
0x004C     MOV       r31,r16
0x004D     POP       r16
0x004E     RET
