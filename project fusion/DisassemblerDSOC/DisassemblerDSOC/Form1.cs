﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace DisassemblerDSOC
{
    public partial class Form1 : Form
    {
        OpenFileDialog opFile;
        string FileName;
        int instNumber;

        public Form1()
        {
            InitializeComponent();
            opFile = new OpenFileDialog();
            FileName = "";
            instNumber = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            opFile.ShowDialog(); 
            FileName = opFile.FileName;
            string[] pathar = FileName.Split('\\');
            textBox1.Text = pathar.Last().ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            instNumber = 0;
            if (FileName != "")
            {
                try
                {
                    FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    StreamReader sr = new StreamReader(fs);
                    string instruction = "";

                    string line = sr.ReadLine();            //2 leituras pa passar o cabeçalho
                    line = sr.ReadLine();

                    while (!sr.EndOfStream)
                    {
                        instruction = "";
                        line = sr.ReadLine();
                        
                        int i = 0;
                        while (line[i] != ',')
                        {
                            instruction += line[i];
                            i++;
                        }

                        PrintAssembler1(GetOpcode(instruction), GetRd(instruction), GetRa(instruction), GetRb(instruction));
                        PrintAssembler2(GetOpcode(instruction),GetRd(instruction),GetRa(instruction),GetIMM(instruction));
                        PrintAssembler3(GetOpcode(instruction), GetRd(instruction), GetRa(instruction), GetRb(instruction), GetIMM(instruction));
                        instNumber++;
                    }
                    sr.Close();
                    sr.Dispose();
                    sr = null;
                    
                }
                catch { /*MessageBox.Show("erro ao ler ficheiro");*/ }
            }
            else
            {
                MessageBox.Show("E preciso escolher um ficheiro");
            }
        }

        private int GetOpcode(string inst)
        {
            uint instruction = uint.Parse(inst, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (int)((instruction >> 26) & 0x3F);
        }

        private int GetRd(string inst)
        {
            uint instruction = uint.Parse(inst, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (int)((instruction >> 21) & 0x1F);
        }

        private int GetRa(string inst)
        {
            uint instruction = uint.Parse(inst, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (int)((instruction >> 16) & 0x1F);
        }

        private int GetRb(string inst)
        {
            uint instruction = uint.Parse(inst, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (int)((instruction >> 11) & 0x1F);
        }

        private int GetIMM(string inst)
        {
            uint instruction = uint.Parse(inst, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (int)(instruction & 0xFFFF);
        }


        private void PrintAssembler1(int opcode, int rd, int ra, int rb)
        {
            string assembley = "0x" + instNumber.ToString("X4") + "     ";
            if (!checkBox1.Checked && opcode == 0) { assembley = ""; }
            switch (opcode)
            { 
                case 1:     assembley += "ADD       r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 2:     assembley += "SUB       r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 5:     assembley += "CMP       r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;

                case 18:    assembley += "PUSH      r" + rd.ToString(); break;
                case 19:    assembley += "POP       r" + rd.ToString(); break;

               

                case 21:    assembley += "RET"; break;
                case 22:    assembley += "RETI"; break;

                case 32:    assembley += "OR        r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 33:    assembley += "AND       r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 34:    assembley += "XOR       r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 35:    assembley += "NOT       r" + rd.ToString(); break;
                case 36:    assembley += "MOV       r" + rd.ToString() + ",r" + ra.ToString(); break;

                case 38:
                    if (ra == 8) { assembley += "BRA       r" + rb.ToString(); }
                    else { assembley += "BR        r" + rb.ToString(); } 
                    break;
                case 39:
                    if (rd == 0) { assembley += "BEQ       r" + ra.ToString() + ",r" + rb.ToString(); }
                    else if (rd == 1) { assembley += "BNE       r" + ra.ToString() + ",r" + rb.ToString(); }
                    else if (rd == 5) { assembley += "BGE       r" + ra.ToString() + ",r" + rb.ToString(); }
                    else if (rd == 4) { assembley += "BGT       r" + ra.ToString() + ",r" + rb.ToString(); }
                    else if (rd == 3) { assembley += "BLE       r" + ra.ToString() + ",r" + rb.ToString(); }
                    else if (rd == 2) { assembley += "BLT       r" + ra.ToString() + ",r" + rb.ToString(); }
                    break;

                case 48: assembley += "LB        r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 50: assembley += "LW        r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 52: assembley += "SB        r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;
                case 54: assembley += "SW        r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); break;

                default: assembley = ""; break;
            }
            if (assembley != "") { listBox1.Items.Add(assembley); }
        }


        private void PrintAssembler2(int opcode, int rd, int ra, int imm)
        {
            string assembley = "0x" + instNumber.ToString("X4") + "     ";
            if (!checkBox1.Checked && opcode == 0) { assembley = ""; }
            switch (opcode)
            {
                case 0: if (checkBox1.Checked) { assembley = "NOP"; } break;
                case 8:     assembley += "ADDI      r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;
                case 9:     assembley += "SUBI      r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break; 
                
                case 20:
                    int address = (rd << 21) + (ra << 16) + imm;
                    assembley += "CALL       0x" + address.ToString("X");
                    break;
                
                case 40:    assembley += "ORI       r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;
                case 41:    assembley += "ANDI      r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;
                case 42:    assembley += "XORI      r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;
                case 43:    assembley += "MOVI      r" + rd.ToString() + "," + imm.ToString(); break;
                case 44:    assembley += "IMM        " + imm.ToString(); break;

            
                    
                case 46:
                    if (ra == 8) { assembley += "BRAI      " + "0x" + imm.ToString("X"); }
                    else 
                    {
                        if (rd == 16)
                        {
                            assembley += "BRI       " + "-0x" + imm.ToString("X");
                        }
                        else
                        {
                            assembley += "BRI       " + "0x" + imm.ToString("X");
                        }
                    } 
                    break;
                case 47:
                    if (rd == 0) { assembley += "BEQI      r" + ra.ToString() + ", 0x" + imm.ToString("X"); }
                    else if (rd == 1) { assembley += "BNEI      r" + ra.ToString() + ", 0x" + imm.ToString("X"); }
                    else if (rd == 5) { assembley += "BGEI      r" + ra.ToString() + ", 0x" + imm.ToString("X"); }
                    else if (rd == 4) { assembley += "BGTI      r" + ra.ToString() + ", 0x" + imm.ToString("X"); }
                    else if (rd == 3) { assembley += "BLEI      r" + ra.ToString() + ", 0x" + imm.ToString("X"); }
                    else if (rd == 2) { assembley += "BLTI      r" + ra.ToString() + ", 0x" + imm.ToString("X"); }
                    break;

                case 58: assembley += "LWI       r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;
                case 59: assembley += "LWBI      r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;
                case 61: assembley += "SWBI      r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;
                case 62: assembley += "SWI       r" + rd.ToString() + ",r" + ra.ToString() + "," + imm.ToString(); break;

                default: assembley = ""; break;
                
            }
            if (assembley != "") { listBox1.Items.Add(assembley); }
        }

        private void PrintAssembler3(int opcode, int rd, int ra, int rb, int imm)
        {
            string assembley = "0x" + instNumber.ToString("X4") + "     ";
            if (!checkBox1.Checked && opcode == 0) { assembley = ""; }

            switch (opcode)
            { 
                case 17:
                    if (((imm >> 10) & 0x01) == 1)
                    { assembley += "BSL       r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); }
                    else
                    { assembley += "BSR       r" + rd.ToString() + ",r" + ra.ToString() + ",r" + rb.ToString(); }
                    break;

                default: assembley = ""; break;
            }
            if (assembley != "") { listBox1.Items.Add(assembley); }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog file = new SaveFileDialog();
            file.ShowDialog();
            StreamWriter sw = new StreamWriter(file.FileName);

            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                sw.WriteLine(listBox1.Items[i]);
            }
            sw.Flush();
            sw.Close();
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            this.TopMost = false;
            string[] FileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            FileName = FileList[0];
            listBox1.Items.Clear();
            if (FileName != "")
            {
                try
                {

                    StreamReader sr = new StreamReader(FileName);
                    string instruction = "";

                    string line = sr.ReadLine();            //2 leituras pa passar o cabeçalho
                    line = sr.ReadLine();

                    while (!sr.EndOfStream)
                    {
                        instruction = "";
                        line = sr.ReadLine();

                        int i = 0;
                        while (line[i] != ',')
                        {
                            instruction += line[i];
                            i++;
                        }

                        PrintAssembler1(GetOpcode(instruction), GetRd(instruction), GetRa(instruction), GetRb(instruction));
                        PrintAssembler2(GetOpcode(instruction), GetRd(instruction), GetRa(instruction), GetIMM(instruction));
                        PrintAssembler3(GetOpcode(instruction), GetRd(instruction), GetRa(instruction), GetRb(instruction), GetIMM(instruction));
                        instNumber++;
                    }
                    sr.Close();
                    instNumber = 0;

                }
                catch { /*MessageBox.Show("erro ao ler ficheiro");*/ }
            }
            
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
             e.Effect = e.AllowedEffect;

             this.TopMost = true;
            //this.TopLevel = true;
            //this.BringToFront();
        }


    }
}
