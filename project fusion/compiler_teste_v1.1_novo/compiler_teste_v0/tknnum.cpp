#include "stdafx.h"

void TNumberToken::Get(TTextInBuffer &buffer)
{
	const int maxIntegr = 42949672;						//se o micro for de 16bits se for mais mexer aqui
	const int maxExponent = 37;

	float numValue = 0.0;

	int wholePlaces=0;									//numero de digitos antes da virgula
	int decimalPlaces=0;								//numero de digitos depois da virgula

	char exponentSign = '+';
	float eValue = 0.0;					//valor do numero depois do 'E'
	int exponent=0;						//expoente final

	ch=buffer.Char();
	ps=string;
	digitCount=0;
	countErrorFlag=False;
	code=tcError;
	type=tyInteger;						//assumimos por defeito que sera um inteiro

	if(!AccumulateValue(buffer,numValue,errInvalidNumber)){return;}					//tira o num inteiro
	wholePlaces=digitCount;

	if(ch == '.')
	{
		type=tyReal;
		*ps++='.';
		ch=buffer.GetChar();
		
		if(!AccumulateValue(buffer,numValue,errInvalidNumber)){return;}				//tira a parte decimal do numero se tiver
		decimalPlaces=digitCount-wholePlaces;
	}

	if(ch == 'E' || ch=='e')
	{
		type=tyReal;
		*ps++=ch;
		ch=buffer.GetChar();

		if((ch=='+') || (ch=='-'))
		{
			*ps++=exponentSign=ch;
			ch=buffer.GetChar();
		}

		digitCount=0;
		if(!AccumulateValue(buffer,eValue,errInvalidNumber)){return;}			//tira o valor do expoente
		if(exponentSign == '-'){eValue = -eValue;}
	}

	if(countErrorFlag)
	{
		Error(errTooManyDigits);
		return;
	}

	exponent = int(eValue) - decimalPlaces;
	if((exponent + wholePlaces < -maxExponent) || (exponent + wholePlaces > maxExponent))
	{
		Error(errRealOutOfRange);
		return;
	}
	if(exponent != 0){numValue *= float(pow(10,exponent));}

	if(type == tyInteger)
	{
		if((numValue < -maxIntegr) || (numValue > maxIntegr))
		{
			Error(errIntegerOutOfRange);
			return;
		}
		value.integer=(int)numValue;
	}
	else
	{
		value.real=numValue;
	}

	*ps='\0';
	code=tcNumber;
}




int TNumberToken::AccumulateValue(TTextInBuffer &buffer,float &value,TErrorCode ec)
{
	const int maxDigitCount = 20;					//nuero maximo de digitos que aceita (se calhar 20 e muito)
	
	if(charCodeMap[ch] != ccDigit)
	{
		Error(ec);
		return False;
	}

	do
	{
		*ps++=ch;
		if(++digitCount <= maxDigitCount)
		{
			value = 10*value + (ch - '0');
		}
		else
		{
			countErrorFlag=True;
		}

		ch=buffer.GetChar();
	}while(charCodeMap[ch]==ccDigit);
	*ps='\0';

	return True;
}

void TNumberToken::Print(void) const
{
	if(type == tyInteger)
	{
		sprintf(list.text,"\t%-18s = %d",">> integer",value.integer);
	}
	else
	{
		sprintf(list.text,"\t%-18s = %g",">> integer",value.real);
	}

	list.PutLine();
}



