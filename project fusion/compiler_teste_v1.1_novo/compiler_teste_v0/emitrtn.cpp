#include "stdafx.h"

//--------------------------------------------------------------
//  EmitProgramPrologue         Emit the program prologue.
//--------------------------------------------------------------

void TCodeGenerator::EmitProgramPrologue(void)				//verificar
{
	//*****************************************************************************
	//		cabecalho da memoria
	PutLine("memory_initialization_radix=16;");
	PutLine("\n");
	PutLine("memory_initialization_vector=");
	PutLine("\n");

	//forca a saltar para a posicao
	Emit2(BRAI,0,8,33);		//para reutilizar opcode BRAI obriga a que Rb seja 8
    PC=33;					//da pos 1 ate a 32 sao espa�os para o vetor de interrupcoes
	
	//inicializa o vetor com o endereco das variaveis globais
	Emit2(MOVI,GLOBAL_VAR_REG,2,32);

	//INICIALIZA AS VARIAVEIS GLOBAIS
	int offset=0;
	TRegister *reg;
	for(int i=0;i<globalSymtab.NodeCount();i++)
	{
		if(globalSymtab.Get(i)->defn.how==dcVariable)
		{
			globalSymtab.Get(i)->defn.data.offset=offset;

			reg=regFile.GetRegister(globalSymtab.Get(i));
			Emit2(MOVI,reg->GetReg(),0,globalSymtab.Get(i)->defn.constant.value.integer);
			Emit2(SWI,reg->GetReg(),GLOBAL_VAR_REG,offset);
			offset += globalSymtab.Get(i)->pType->size;
		}
	}
	
	//Liberta os registos usados
	for(int i=0;i<globalSymtab.NodeCount();i++)
	{
		if(globalSymtab.Get(i)->defn.how==dcVariable || globalSymtab.Get(i)->defn.how==dcPointer)
		{
			regFile.FreeRegister(globalSymtab.Get(i));
		}
	}

	dependencyList->AddDependencyCall("main", PC);					//verificar o que se passa a fun�ao		PC???
	Emit2(CALL, 0,0,0);
	//dependencyList->AddDependencyCall("main");
	//devia de por aqui um call 
	//Emit4(CALL,0x2FF5686);							//para ja esta a simular o call

	//devia de por um while(1); ou halt
	//Emit2(BRI,16,0,0);							//se rd tiver 16 salta para tras se nao salta pa frente

}

//--------------------------------------------------------------
//  EmitProgramEpilogue         Emit the program epilogue.
//--------------------------------------------------------------

void TCodeGenerator::EmitProgramEpilogue(void)
{
	//Termina o programa
	Emit2(BRI,rd_BRI_back,0,1);
}

//--------------------------------------------------------------
//  EmitMain            Emit code for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunction(const TDefn *func)
{
	dependencyList->SetPosProlog(func);
	EmitFunctionPrologue(func);

	EmitStatmentList(tcEndOfFile);					//nao ta implementado o return nem call

	dependencyList->SetPosEpilog(func);
	EmitFunctionEpilogue();
}

//--------------------------------------------------------------
//  EmitMainPrologue    Emit the prologue for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunctionPrologue(const TDefn *func)
{
	int i = 0;

	int offset = -1;

	TSymtab * pSymtab;

	TSymtabNode *var;

	regFile.ClearRegisters();
	
	Emit1(PUSH, FRAME_BASE_REG, 0, 0);
	Emit2(ADDI, FRAME_BASE_REG, STACK_POINTER, 1);

	pSymtab = symtabStack.GetCurrentSymtab();
	
	for(i = 0; i < func->routine.parmCount; i++)
	{
		var = pSymtab->Get(i);
		regFile.GetParamRegister(var);
	}
	
	for(i = i; i < pSymtab->NodeCount(); i++)
	{
		var=pSymtab->Get(i);

		if(var->defn.how == dcVariable || var->defn.how == dcPointer)
		{
			Emit2(MOVI,1,0,var->defn.data.value.integer);							//CUIDADO
			Emit1(PUSH,1,0,0);

			var->defn.data.offset = offset;
			offset--;
		}
		
	}

}

//--------------------------------------------------------------
//  EmitMainEpilogue    Emit the epilogue for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunctionEpilogue(void)
{
	Emit1(MOV,STACK_POINTER,FRAME_BASE_REG,0);
	regFile.PopParamRegisters();
	Emit1(POP,FRAME_BASE_REG,0,0);
	Emit1(RET,0,0,0);
}


//--------------------------------------------------------------
//  EmitStatementList        emit a statement list until
//                              the terminator token.
//
//      terminator : the token that terminates the list
//--------------------------------------------------------------

void TCodeGenerator::EmitStatmentList(TTokenCode terminator)
{
	GetToken();
	do 
	{
		EmitStatement();
		while (token == tcSemicolon) GetToken();
    } while (token != terminator);
}

void TCodeGenerator::EmitFunctionCall(TSymtabNode *pFunc)
{
	int numParam = 0;
	ExprStackData param;

	GetToken();
	regFile.PushRegisters();
	
	GetToken();

	while(token != tcRParen)
	{
		if(token == tcComma) GetToken();

		numParam++;

		if(pFunc->defn.routine.parmCount < numParam)
		{
			ErrorBackend(errTooManyParameters);
		}
		else
		{
				EmitExpression();
				param = genStack.Pop();

				Emit1(PUSH, numParam-1 + MIN_PARAM_REG, 0, 0);

				if(token == tcNumber)
				{
					Emit2(MOVI, numParam-1 + MIN_PARAM_REG, 0, param.val.integer);
				}
				else
				{
					Emit2(MOV, numParam-1 + MIN_PARAM_REG, param.reg->GetReg(), 0);
				}
		}
	}
		
	if(pFunc->defn.routine.parmCount < numParam)
	{
		ErrorBackend(errTooManyParameters);
	}
	else
	{
		regFile.StoreRegisters();
		dependencyList->AddDependencyCall(pFunc->String(), PC);				
		Emit2(CALL, 0,0,0);
		regFile.PopAndLoadRegisters();
	}
}

//endfig