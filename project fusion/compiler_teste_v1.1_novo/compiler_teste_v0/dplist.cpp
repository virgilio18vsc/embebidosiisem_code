#include "stdafx.h"

List::List(){
	Next = 0;
}

List::~List()
{
	
}

//----------------DEPENDENCYLIST------------------------

DependencyList::DependencyList(char *pfunctionName)
{
	this->pfunctionName = new char[strlen(pfunctionName)];
	strcpy(this->pfunctionName,pfunctionName);
}

DependencyList::~DependencyList()
{
	delete[] this->pfunctionName;
	if(this->dependenciesCall != NULL)
		delete ((PosList *)this->dependenciesCall);
	if(this->dependenciesReturn != NULL)
		delete ((PosList *)this->dependenciesReturn);
	if(this->Next != NULL)
		delete ((DependencyList *)this->Next);
}

void DependencyList::SetPosEpilog(const TDefn *func)
{
	DependencyList *aux = this;
	TSymtabNode * ts = *(func->routine.vSymtab);
	while(aux != NULL && strcmp(aux->pfunctionName, ts->String()) != 0)
		aux = (DependencyList*)aux->Next;
	if(aux != NULL)
		aux->posEpilog = PC;
}

void DependencyList::SetPosProlog(const TDefn *func)
{
	DependencyList *aux = this;
	TSymtabNode * ts = *(func->routine.vSymtab);
	while(aux != NULL && strcmp(aux->pfunctionName, ts->String()) != 0)
	{
		aux = (DependencyList*)aux->Next;
	}
	if(aux != NULL)
		aux->posProlog = PC;
}

void DependencyList::AddDependencyReturn(char* functionName, int val)
{
	DependencyList *cur_func = this;

	while(cur_func != NULL && strcmp(cur_func->pfunctionName, functionName) != 0)
		cur_func = (DependencyList*)cur_func->Next;

	if(cur_func != NULL)
	{
		if(cur_func->dependenciesReturn != NULL)
			cur_func->dependenciesReturn->Add(&val);
		else
			cur_func->dependenciesReturn = new PosList(val);
	}
}

void DependencyList::AddDependencyCall(char* functionName, int val)
{
	DependencyList *cur_func = this;

	while(cur_func != NULL && strcmp(cur_func->pfunctionName, functionName) != 0)
		cur_func = (DependencyList*)cur_func->Next;

	if(cur_func != NULL)
	{
		if(cur_func->dependenciesCall != NULL)
			cur_func->dependenciesCall->Add(&val);
		else
			cur_func->dependenciesCall = new PosList(val);
	}
}

void DependencyList::ResolveDependencies()
{
	for(DependencyList * cur_func = this; cur_func != NULL; cur_func = (DependencyList *)cur_func->Get()) 
	{
		for(PosList * cur_call = cur_func->dependenciesCall; cur_call != NULL; cur_call = (PosList *)cur_call->Get())
		{
			PC = cur_call->getPos();
			Emit2(CALL, 0, 0, cur_func->posProlog)
		}
		for(PosList * cur_ret = cur_func->dependenciesReturn; cur_ret != NULL; cur_ret = (PosList *)cur_ret->Get())
		{
			PC = cur_ret->getPos();
			Emit2(BRI, 0, 0, (cur_func->posEpilog - PC))
		}
	}
}

void DependencyList::Add(void *arg)
{
	DependencyList * new_dp = new DependencyList((char*)arg);
	DependencyList *aux = this;

	while(aux->Next != NULL)
		aux=(DependencyList*)aux->Next;
	aux->Next = new_dp;
}

//------------POSLIST---------------------------------------

PosList::PosList(int val)
{
	this->pos = val;
}

PosList::~PosList()
{
	if(this->Next != NULL)
		delete (PosList * )this->Next;
}


void PosList::Add(void *arg){

	PosList *new_pl = new PosList(*(int*)arg);
	PosList *aux = this;

	while(aux->Next != NULL) 
		aux=(PosList*)aux->Next;
	aux->Next = new_pl;
}

List* PosList::Get()
{
	return Next;
}
int PosList::getPos() const
{
	return pos;
}