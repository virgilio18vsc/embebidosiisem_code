#ifndef __MISC_H__
#define __MISC_H__

#include "stdafx.h"


const int False=0;
const int True=1;

//carater codes
enum TCharCode
{
	ccLetter,
	ccDigit,
	ccSpecial,
	ccQuote,
	ccWhiteSpace,
	ccEndOfFile,
	ccError,
};

//token codes
enum TTokenCode
{
	tcDummy,
	tcWord, tcIdentifier, tcNumber, tcString, tcEndOfFile, tcError,
	
	tcPlus, tcMinus, tcStar, tcSlash, tcPercent,  tcPlusPlus, tcMinusMinus, tcPlusEqual, tcMinusEqual, tcEq,		//+  - *  / % ++ -- += -= =
	tcAndEqual, tcOrEqual, tcXorEqual, tcStarEqual, tcSlashEqual,													//&= |= ^= *= /=
	tcPeriod, tcComma, tcColon, tcSemicolon, tcDQuote, tcQuote, tcArrow,											//. , : ; " ' ->
	tcLParen, tcRParen, tcLBracket, tcRBracket, tcLSquareBracket, tcRSquareBraquet,									//( ) { } [ ]					
	tcLt, tcGt, tcLe, tcGe, tcNe, tcEqual,																			//<  > <= >= != ==
	tcLogicAnd, tcLogicOr,																							// && ||
	tcInterrogation, tcLShift, tcRShift,																			//? << >>
	tcAnd, tcOr, tcNot, tcXor, tcTil,																				// & | ! ^ ~

	tcDo, tcIf,
	tcFor,  tcInt, 
	tcAuto, tcCase, tcChar, tcElse, tcEnum, tcGoto, tcLong, tcVoid,
	tcBreak, tcConst, tcDouble, tcFloat, tcWhile, tcShort, tcUnion, 
	tcExtern, tcReturn, tcSigned, tcSizeof, tcStatic, tcStruct, tcSwitch,
	tcDefault, tcTypedef, 	 
	tcContinue, tcRegister, tcUnsigned, tcVolatile,

};

//data type
enum TDataType
{
	tyDummy,
	
	tyCharacter,
	tyUnsignedChar,

	tyShortInteger,
	tyUnsignedShortInteger,
	tyInteger,
	tyUnsignedInteger,
	tyLongInteger,
	tyUnsignedLongInteger,
	tyLongLongInteger,

	tyReal,
	tyString,

	tyVoid,
	tyStruct,
};

//data value
union TDataValue
{
	int		integer;
	float	real;
	char	character;
	char	*pString;
};

#endif