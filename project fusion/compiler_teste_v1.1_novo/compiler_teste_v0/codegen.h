#ifndef __CODEGEN_H__
#define __CODEGEN_H__

#include "stdafx.h"

//--------------------------------------------------------------
//  Runtime stack frame items
//--------------------------------------------------------------

#define STATIC_LINK             "$STATIC_LINK"
#define RETURN_VALUE            "$RETURN_VALUE"
#define HIGH_RETURN_VALUE       "$HIGH_RETURN_VALUE"

//--------------------------------------------------------------
//		TRegister		gere os registos usados pelo processador
//
//--------------------------------------------------------------
class TRegister
{
	int reg;
	TSymtabNode *symbol;

public:
	TRegister(){}
	TRegister(int Reg, TSymtabNode* Symbol);
	int GetReg(void);
	TSymtabNode* GetSymbolAssociated(void);
	void SetSymbol(TSymtabNode* Symbol);
};

class TRegisterFile
{
	TRegister *registos[REG_NUMBER];

public:
	TRegisterFile(void);
	TRegister* GetRegister(TSymtabNode* var);
	void FreeRegister(TSymtabNode* var);
	void FreeRegister(int reg);
	TRegister* FindRegister(TSymtabNode* var);
	void PrintRegFile(void);
	void StoreRegisters(void);
	void PopAndLoadRegisters(void);
	void PushRegisters(void);
	void ClearRegisters(void);
	TRegister* GetReturnRegister();
	void PopParamRegisters();
	TRegister * GetParamRegister (TSymtabNode * param);

};

class ExprStackData
{
	public:
		TTokenCode token;
		TRegister *reg;
		TDataValue val;
		TSymtabNode *identifier;
};

class TExpressionStack
{
	ExprStackData stack[64];
	ExprStackData *tos;

public:
	TExpressionStack(void){tos=&stack[0];}
	void Push(TTokenCode tk,TRegister* reg){tos->token=tk; tos->reg=reg; tos++; }
	void Push(TTokenCode tk,int value){tos->token=tk; tos->val.integer=value; tos++; }
	void Push(TTokenCode tk,float value){tos->token=tk; tos->val.real=value; tos++; }
	ExprStackData Pop(void){tos--; return *tos;}

	int Elements(void){return (tos-stack);}
};



//*****************************************************************
//esta classe e usada para resolver por ex a++ em que so deve ser feito o ++ a variavel depois de finalizada a expressao
class TPostOperations
{
	enum{StackMaxSize = 16};
	ExprStackData stack[StackMaxSize];
	ExprStackData *tos;

public:
	TPostOperations(void){tos=&stack[0];}
	void Push(TTokenCode token,TSymtabNode* symbol){tos->token=token; tos->identifier=symbol; tos++;}
	ExprStackData Pop(void){tos--; return *tos;}
	int Elements(void){return (tos-stack);}
};


//--------------------------------------------------------------
//  TAssemblyBuffer     Assembly language buffer subclass of
//                      TTextOutBuffer.
//--------------------------------------------------------------

class TAssemblyBuffer : public TTextOutBuffer {
    enum {maxLength = 72,};

    fstream  file;        // assembly output file
    char    *pText;       // assembly buffer pointer
    int      textLength;  // length of assembly comment

public:
    TAssemblyBuffer(const char *pAsmFileName, TAbortCode ac);

    char *Text (void) const { return pText; }

    void Reset(void)
    {
		pText = text;
		text[0] = '\0';
		textLength = 0;
    }

    void Put(char ch) { *pText++ = ch; *pText = '\0'; ++textLength; }
    virtual void PutLine(void) { file << endl; }

	void writeFile(char *pText){file<<pText;}

    void PutLine(const char *pText)
    {
		TTextOutBuffer::PutLine(pText);
    }

	void Put(int num)
	{
		char *pString=new char [10];
		_itoa(num,pString,10);
		strcpy(pText, pString);
		Advance();
	}

    void Advance(void);

    void Put(const char *pString)
    {
		strcpy(pText, pString);
		Advance();
    }

    void Reset(const char *pString) { Reset(); Put(pString); }

    int Fit(int length) const
    {
		return textLength + length < maxLength;
    }
};



/*************************************************************************************
Classes Auxiliares para o Switch

*ListaLabel
*ListaBreak
*************************************************************************************/
class ListaLabel
{
public:
	ListaLabel();
	~ListaLabel();
	ListaLabel *Insere(string valor, int endereco);
	void SetEndereco(int endereco);
	int GetEndereco(int endereco);
	int GetNumber();
	ListaLabel *GetSeguinte();
	void Deletelist();

private:
	int CaseNumber;
	int JumpLabel;
	ListaLabel *Seguinte;
};


class ListaBreak
{
public:
	ListaBreak();
	~ListaBreak();
	ListaBreak *Insere(int endereco);
	int GetPC();
	ListaBreak *GetSeguinte();
	void Deletelist();

private:
	int PC;
	ListaBreak * Seguinte;
};



//--------------------------------------------------------------
//  TCodeGenerator      Code generator subclass of TBackend.
//--------------------------------------------------------------

class TCodeGenerator : public TBackend 
{
	//teste
	TRegisterFile regFile;
	TExpressionStack genStack;
	TPostOperations postStack;


	string curFunction;
	

    TAssemblyBuffer *const pAsmBuffer;

    //--Pointers to the list of all the float and string literals
    //--used in the source program.
    TSymtabNode *pFloatLitList;
    TSymtabNode *pStringLitList;

	TSymtabNode tempReg; //declara��o do TSymtabNode para usar os registos temporarios
	TSymtabNode branchReg;


	DependencyList *dependencyList;

    //void Reg              (TRegister r);
    //void Operator         (TInstruction opcode);
    void Label            (const char *prefix, int index);
    void WordLabel        (const char *prefix, int index);
    void HighDWordLabel   (const char *prefix, int index);
    void Byte             (const TSymtabNode *pId);
    void Word             (const TSymtabNode *pId);
    void HighDWord        (const TSymtabNode *pId);
    //void ByteIndirect     (TRegister r);
    //void WordIndirect     (TRegister r);
    //void HighDWordIndirect(TRegister r);
    void TaggedName       (const TSymtabNode *pId);
    void NameLit          (const char *pName);
    void IntegerLit       (int n);
    void CharLit          (char ch);

    void EmitStatementLabel(int index);

    //--Program
    void EmitProgramPrologue	(void);
    void EmitProgramEpilogue	(void);
    void EmitFunction			(const TDefn *func);
    void EmitFunctionPrologue	(const TDefn *func);
    void EmitFunctionEpilogue	(void);
	void EmitStatmentList		(TTokenCode terminator);

    //--Routines
   /* void   EmitRoutine               (const TSymtabNode *pRoutineId);
    void   EmitRoutinePrologue       (const TSymtabNode *pRoutineId);
    void   EmitRoutineEpilogue       (const TSymtabNode *pRoutineId);
    TType *EmitSubroutineCall        (const TSymtabNode *pRoutineId);
    TType *EmitDeclaredSubroutineCall(const TSymtabNode *pRoutineId);
    TType *EmitStandardSubroutineCall(const TSymtabNode *pRoutineId);
    void   EmitActualParameters      (const TSymtabNode *pRoutineId);*/

    //--Standard routines
    TType *EmitReadReadlnCall  (const TSymtabNode *pRoutineId);
    TType *EmitWriteWritelnCall(const TSymtabNode *pRoutineId);
    TType *EmitEofEolnCall     (const TSymtabNode *pRoutineId);
    TType *EmitAbsSqrCall      (const TSymtabNode *pRoutineId);
    TType *EmitArctanCosExpLnSinSqrtCall(const TSymtabNode *pRoutineId);
    TType *EmitPredSuccCall    (const TSymtabNode *pRoutineId);
    TType *EmitChrCall         (void);
    TType *EmitOddCall         (void);
    TType *EmitOrdCall         (void);
    TType *EmitRoundTruncCall  (const TSymtabNode *pRoutineId);

    //--Declarations
    void EmitDeclarations     (const TSymtabNode *pRoutineId);
    void EmitStackOffsetEquate(const TSymtabNode *pId);

    //--Loads and pushes
    void EmitAdjustBP     (int level);
    void EmitRestoreBP    (int level);
    void EmitLoadValue    (const TSymtabNode *pId);
    void EmitLoadFloatLit (TSymtabNode *pNode);
    void EmitPushStringLit(TSymtabNode *pNode);
    void EmitPushOperand  (const TType *pType);
    void EmitPushAddress  (const TSymtabNode *pId);
    void EmitPushReturnValueAddress(const TSymtabNode *pId);
    void EmitPromoteToReal(const TType *pType1, const TType *pType2);

    //--Statements
    void EmitStatement    (void);
//    void EmitStatmentList (TTokenCode terminator);
	void EmitAssignPtr	  (void);
    void EmitAssignment   (const TSymtabNode *pTargetId);
    void EmitREPEAT       (void);
    void EmitWHILE        (void);
    void EmitIF           (void);
    void EmitFOR          (void);
	void EmitSWITCH       (void);
    void EmitCompound     (void);
	void EmitReturn		  (void);


	/*****************************************************************************
	*			FUNCOES AUXILIARES
	*****************************************************************************/
	void CorreCase(int chavetas);
	ListaBreak *CriarSaltos(TRegister *rd, TRegister *registo, ListaLabel *lista, ListaBreak *lista2, bool flag);
	void EscreveCases(TRegister *rd, TRegister *salto, ListaLabel *l1, ListaBreak *l2);
	void EscreveBreaks(ListaBreak *l);
	void StatementList3Arg(int terminator1, int terminator2, int terminator3);

    //--Expressions
    TType *EmitExpression(void);
	TType *EmitLogicExpression(void);
    TType *EmitSimpleExpression(void);
    TType *EmitTerm      (void);
    TType *EmitFactor    (void);
    TType *EmitConstant  (TSymtabNode *pId);
    TType *EmitVariable  (TSymtabNode *pId);
    TType *EmitSubscripts(const TType *pType);
    TType *EmitField     (void);


	void EmitFunctionCall(TSymtabNode *pFunc);


	TType *EmitMultiplication(ExprStackData op1, ExprStackData op2);
	TType *EmitDivision(ExprStackData op1, ExprStackData op2);

    //--Assembly buffer
    char *AsmText(void)          { return pAsmBuffer->Text();  }
    void  Reset  (void)          { pAsmBuffer->Reset();        }
    void  Put    (char ch)       { pAsmBuffer->Put(ch);        }
    void  Put    (char *pString) { pAsmBuffer->Put(pString);   }
    void  PutLine(void)          { pAsmBuffer->PutLine();      }
	void  PutLine(char *pText)   { pAsmBuffer->writeFile(pText); }
    void  Advance(void)          { pAsmBuffer->Advance();      }

    //--Comments
    void PutComment  (const char *pString);
    void StartComment(int n);

    void StartComment(void) { Reset(); PutComment("; "); }

    void StartComment(const char *pString)
    {
		StartComment();
		PutComment(pString);
    }

    void EmitProgramHeaderComment    (const TSymtabNode *pProgramId);
    void EmitSubroutineHeaderComment (const TSymtabNode *pRoutineId);
    void EmitSubroutineFormalsComment(const TSymtabNode *pParmId);
    void EmitVarDeclComment          (const TSymtabNode *pVarId);
    void EmitTypeSpecComment         (const TType *pType);
    void EmitStmtComment             (void);
    void EmitAsgnOrCallComment       (void);
    void EmitREPEATComment           (void);
    void EmitUNTILComment            (void);
    void EmitWHILEComment            (void);
    void EmitIFComment               (void);
    void EmitFORComment              (void);
    void EmitCASEComment             (void);
    void EmitExprComment             (void);
    void EmitIdComment               (void);

public:
	TCodeGenerator(const char *pAsmName) : pAsmBuffer(new TAssemblyBuffer(pAsmName, abortAssemblyFileOpenFailed)),
		tempReg(TEMP_REG, dcVariable), branchReg(BRANCH_REG, dcVariable)//acrescentou-se a gera��o da codifica��o dos branches e registos temporarios
	{
		pFloatLitList = pStringLitList = NULL;

		tempReg.defn.data.offset = 0;//inicializa-se a zero os valores
		branchReg.defn.data.offset = 0;
	}


	DependencyList* GetDependencyList(){
		return dependencyList;
	}

    //virtual void Go(const TSymtabNode *pProgramId);
	virtual void Go(const TSymtabNode *pRoutineId);

};


#endif