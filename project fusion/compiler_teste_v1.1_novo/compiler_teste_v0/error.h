#ifndef __ERROR_H__
#define __ERROR_H__

extern int errorCount;
extern int errorArrowFlag;
extern int errorArrowOffset;

enum TAbortCode
{
	abortInvalidCommandLineArgs = -1,
	abortSourceFileOpenFailed	= -2,
	abortIFormFileOpenFailed	= -3,
	abortAssemblyFileOpenFailed	= -4,
	abortTooManySintaxErrors	= -5,
	abortStackOverflow			= -6,
	abortCodeSegmentOverflow	= -7,
	abortNestingTooDeep			= -8,
	abortRuntimeError			= -9,
	abortUnimplementedFeature	= -10,
	abortBackendError           = -11,
};

void AbortTranslation(TAbortCode ac);

enum TErrorCode
{
	errNone,
	errUnrecognizable,
	errTooMany,
	errUnexpectedEndOfFile,
	errIdentifierNotFound,
	errInvalidNumber,
	errInvalidFraction,
	errInvalidExponent,
	errTooManyDigits,
	errRealOutOfRange,
	errIntegerOutOfRange,
	errMissingRightParen,
	errMissingLeftParen,
	errMissingRightBracket,
	errMissingLeftBracket,
	errMissingRightSquareBracket,
	errMissingLeftSquareBracket,
	errInvalidExpression,
	errInvalidAssignment,
	errMissingIdentifier,
	errUndefinedIdentifier,
	errStackOverflow,
	errInvalidStatment,
	errUnexpectedToken,
	errMissingSemicolon,
	errMissingComma,
	errMissingDo,
	errInvalidFORControl,
	errInvalidConstant,
	errMissingConstant,
	errRedefinedIdentifier,
	errMissingEqual,
	errInvalidType,
	errNotATypeIdentifier,
	errInvalidSubrangeType,
	errNotAConstantIdentifier,
	errIncompatibleTypes,
	errInvalidTarget,
	errInvalidIdentifierUsage,
	errIncompatibleAssignment,
	errMinGtMax,
	errInvalidIndexType,
	errMissingPeriod,
	errInvalidField,
	errNestingTooDeep,
	errAlreadyForwarded,
	errWrongNumberOfParms,
	errInvalidParm,
	errMissingVariable,
	errCodeSegmentOverflow,
	errMissingColon,
	errUnexpectedEndLine,
	errMissingEntryPoint,
	errUnimplementedFeature,
};

void Error(TErrorCode ec);

//--------------------------------------------------------------
//  Runtime error codes.
//--------------------------------------------------------------

enum TRuntimeErrorCode {
   rteNone,
   rteStackOverflow,
   rteValueOutOfRange,
   rteInvalidCaseValue,
   rteDivisionByZero,
   rteInvalidFunctionArgument,
   rteInvalidUserInput,
   rteUnimplementedRuntimeFeature,
};

void RuntimeError(TRuntimeErrorCode ec);

//--------------------------------------------------------------
//  Backend error codes.
//-------------------------------------------------------------

enum TErrorBackend {
	errTooManyParameters,
};

void ErrorBackend(TErrorBackend eb);

#endif