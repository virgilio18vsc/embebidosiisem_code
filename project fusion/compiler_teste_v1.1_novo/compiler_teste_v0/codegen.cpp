#include "stdafx.h"

TRegister::TRegister(int Reg, TSymtabNode* Symbol)
{
	reg=Reg;
	symbol=Symbol;
}

int TRegister::GetReg(void)
{
	return reg;
}

TSymtabNode* TRegister::GetSymbolAssociated(void)
{
	return symbol;
}

void TRegister::SetSymbol(TSymtabNode* Symbol)
{
	symbol=Symbol;
}


//**********************************************************************
//			register file
TRegisterFile::TRegisterFile(void)
{
	for(int i=0;i<REG_NUMBER;i++)
	{
		registos[i]=new TRegister(i,NULL);
	}
	registos[0]->SetSymbol((TSymtabNode*)0x12345);
	registos[GLOBAL_VAR_REG]->SetSymbol((TSymtabNode*)0x12345);
	registos[FUNCTION_RETURN_REG]->SetSymbol((TSymtabNode*)0x12345);
	registos[FRAME_BASE_REG]->SetSymbol((TSymtabNode*)0x12345);
	registos[STACK_POINTER]->SetSymbol((TSymtabNode*)0x12345);
}

void TRegisterFile::ClearRegisters(){
	int i;

	for(i=0;i<REG_NUMBER;i++){
		registos[i]->SetSymbol(NULL);
	}
}

TRegister* TRegisterFile::GetRegister(TSymtabNode* var)
{
	for (int i = 1; i < MIN_PARAM_REG; i++) {
		if (registos[i]->GetSymbolAssociated() == NULL) {

			if (var->defn.data.offset != 0 || var->defn.data.globalData) {

				if( var->defn.data.offset >= 0 ) {
					Emit2(LWI, registos[i]->GetReg(), GLOBAL_VAR_REG, var->defn.data.offset);
				}
				else {// carregar vari�vel da stack
					Emit2(LWBI, registos[i]->GetReg(), FRAME_BASE_REG, -(var->defn.data.offset));
				}
			}
			registos[i]->SetSymbol(var);
			return registos[i];
		}
	}

	// n�o h� registos livres, procurar pelo primeiro com 
	for (int i = 1; i < MIN_PARAM_REG; i++) {
		TSymtabNode* old_var = registos[i]->GetSymbolAssociated();

		if (old_var->defn.data.offset != 0 || old_var->defn.data.globalData) {
			// guardar vari�vel antiga


			if( var->defn.data.offset >= 0 ) 
			{
				Emit2(LWI, registos[i]->GetReg(), GLOBAL_VAR_REG, var->defn.data.offset);
			}
			else {// carregar vari�vel da stack
				Emit2(LWBI, registos[i]->GetReg(), FRAME_BASE_REG, -(var->defn.data.offset));
			}

			if( old_var->defn.data.offset >= 0 )
			{
				Emit2(SWI, registos[i]->GetReg(), GLOBAL_VAR_REG, -(old_var->defn.data.offset));
			}
			else {// carregar vari�vel da stack
				Emit2(SWBI, registos[i]->GetReg(), FRAME_BASE_REG, -(old_var->defn.data.offset));
			}


			registos[i]->SetSymbol(var);
			return registos[i];
		}
	}

	return NULL;
}

/********************************************/
TRegister * TRegisterFile::GetParamRegister (TSymtabNode * param)
{
	int i;
	bool stop = true;
	TRegister * reg = NULL;

	for ( i = MIN_PARAM_REG; i <= MAX_PARAM_REG && stop; ++i)
	{
		if (registos[i]->GetSymbolAssociated() == NULL)
		{
			registos[i]->SetSymbol(param);
			reg = registos[i];
			stop = false;
		}
	}

	if(stop)
	{
		ErrorBackend(errTooManyParameters);
	}

	return reg;
}

void TRegisterFile::PopParamRegisters(){
	int i;

	for(i = 0; i < NUM_PARAM_REG; i++){
		if(registos[MAX_PARAM_REG-i]->GetSymbolAssociated() != NULL)
			Emit2(LWI, MAX_PARAM_REG - i, FRAME_BASE_REG, i + 2);
	}
}

/********************************************/


void TRegisterFile::FreeRegister(TSymtabNode* var)
{
	for(int i=0;i<REG_NUMBER;i++)
	{
		if(registos[i]->GetSymbolAssociated()==var)
		{
			registos[i]->SetSymbol(NULL);
		}
	}
}

void TRegisterFile::FreeRegister(int reg)
{
	registos[reg]->SetSymbol(NULL);
}


TRegister* TRegisterFile::FindRegister(TSymtabNode* var)
{
	for(int i = 0; i < REG_NUMBER; i++)
	{
		if(registos[i]->GetSymbolAssociated() == var)
		{
			return registos[i];
		}
	}

	return NULL;
}



void TRegisterFile::PrintRegFile(void)
{
	for(int i = 1; i < REG_NUMBER; i++)
	{
		if(i != 14 && i != 15 && i != 16 && registos[i]->GetSymbolAssociated() != NULL)
		{
			cout<<"reg = r"<<registos[i]->GetReg();
			cout<<"\t\tvar = "<<registos[i]->GetSymbolAssociated()->String()<<endl;
		}
	}
}

void TRegisterFile::PushRegisters()
{
	int i;
	TSymtabNode * symbol;

	for(i = 1; i < MIN_PARAM_REG; i++)
	{	
		symbol = registos[i]->GetSymbolAssociated();

		if(symbol!=NULL)
		{
			if(symbol->defn.data.offset == 0)
			{
				Emit1(PUSH,i,0,0);
			}
		}	
	}
}

void TRegisterFile::StoreRegisters(void)
{
	int i;
	TSymtabNode * symbol;

	for(i = 1; i < MIN_PARAM_REG; i++)
	{
		symbol = registos[i]->GetSymbolAssociated();
		if(symbol != NULL)
		{
			if(symbol->defn.data.offset != 0)
				Emit2(SWBI, i, FRAME_BASE_REG, -(symbol->defn.data.offset));
		}
	}
}

void TRegisterFile::PopAndLoadRegisters()
{
	TSymtabNode * symbol;
	for(int i = MIN_PARAM_REG-1; i >= 0; i--)
	{
		symbol = registos[i]->GetSymbolAssociated();

		if(symbol!=NULL)
		{
			if(symbol->defn.data.offset == 0)
			{
				Emit1(POP,i,0,0);
			}
			else
			{
				Emit2(LWBI,i,FRAME_BASE_REG, -(symbol->defn.data.offset));
			}
		}
	}
}

TRegister* TRegisterFile::GetReturnRegister()
{
	return registos[FUNCTION_RETURN_REG];
}