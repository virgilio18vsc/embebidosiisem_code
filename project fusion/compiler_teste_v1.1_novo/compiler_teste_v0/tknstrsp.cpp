#include "stdafx.h"

void TStringToken::Get(TTextInBuffer &buffer)
{
	char ch;
	char *ps=string;

	*ps++='"';

	ch=buffer.GetChar();

	while(ch != eofChar && ch != '"' && ch != '\0')
	{
		*ps++=ch;
		ch=buffer.GetChar();
	}

	if(ch == eofChar){Error(errUnexpectedEndOfFile);}
	if(ch == '\0')	{Error(errUnexpectedEndLine);}

	*ps++='"';
	*ps='\0';
}


void TStringToken::Print(void)const
{
	sprintf(list.text,"\t%-18s %-s",">> string:",string);
	list.PutLine();
}



//special tokens

void TSpecialToken::Get(TTextInBuffer &buffer)
{
	char ch=buffer.Char();
	char *ps=string;

	*ps++=ch;

	switch(ch)
	{
	case '+': 
		ch = buffer.GetChar(); 
		if(ch == '+')
		{
			*ps++=ch;
			code=tcPlusPlus;
			buffer.GetChar();
		}
		else if(ch == '=')
		{
			*ps++=ch;
			code=tcPlusEqual;
			buffer.GetChar();
		}
		else
		{
			code=tcPlus;
		}
	break;

	case '-':
		ch = buffer.GetChar(); 
		if(ch == '-')
		{
			*ps++=ch;
			code=tcMinusMinus;
			buffer.GetChar();
		}
		else if(ch == '=')
		{
			*ps++=ch;
			code=tcMinusEqual;
			buffer.GetChar();
		}
		else if(ch == '>')
		{
			*ps++=ch;
			code=tcArrow;
			buffer.GetChar();
		}
		else
		{
			code=tcMinus;
		}
	break;

	case '*': 
		ch=buffer.GetChar();
		if(ch == '=')
		{
			*ps++=ch;
			code=tcStarEqual;
			buffer.GetChar();
		}
		else
		{
			code=tcStar;
		}
	break;
	
	case '/': 
		ch=buffer.GetChar();
		if(ch=='=')
		{
			*ps++=ch;
			code=tcSlashEqual;
			buffer.GetChar();
		}
		else
		{
			code=tcSlash;
		}
	break;
	
	case '%': code=tcPercent; buffer.GetChar(); break;
	
	case '=': 
		ch = buffer.GetChar();
		if(ch == '=')
		{
			*ps++='=';
			code=tcEqual;
			buffer.GetChar();
		}
		else
		{
			code = tcEq;
		}
	break;
	
	case '&': 
		ch = buffer.GetChar();
		if(ch == '=')
		{
			*ps++=ch;
			code=tcAndEqual;
			buffer.GetChar();
		}
		else if(ch == '&')
		{
			*ps++=ch;
			code=tcLogicAnd;
			buffer.GetChar();
		}
		else
		{
			code=tcAnd;
		}
	break;

	case '|': 
		ch=buffer.GetChar();
		if(ch=='=')
		{
			*ps++=ch;
			code=tcOrEqual;
			buffer.GetChar();
		}
		else if(ch == '|')
		{
			*ps++=ch;
			code=tcLogicOr;
			buffer.GetChar();
		}
		else
		{
			code=tcOr;
		}
	break;
	
	case '!': 
		ch=buffer.GetChar();
		if(ch == '=')
		{
			*ps++=ch;
			code=tcNe;
			buffer.GetChar();
		}
		else
		{
			code=tcNot;
		}
	break;
	
	case '^': 
		ch=buffer.GetChar();
		if(ch == '=')
		{
			*ps++=ch;
			code=tcXorEqual;
			buffer.GetChar();
		}
		else
		{
			code=tcXor;
		}
	break;
	
	case '~': code=tcTil; buffer.GetChar(); break;
	case '?': code=tcInterrogation; buffer.GetChar(); break;
	
	case '<': 
		ch=buffer.GetChar();
		if(ch == '=')
		{
			*ps++=ch;
			code=tcLe;
			buffer.GetChar();
		}
		else if(ch == '<')
		{
			*ps++=ch;
			code=tcLShift;
			buffer.GetChar();
		}
		else
		{
			code=tcLt;
		}
	break;
	
	case '>': 
		ch=buffer.GetChar();
		if(ch == '=')
		{
			*ps++=ch;
			code=tcGe;
			buffer.GetChar();
		}
		else if(ch == '>')
		{
			*ps++=ch;
			code=tcRShift;
			buffer.GetChar();
		}
		else
		{
			code=tcGt;
		}
	break;
	
	case '.': code=tcPeriod;			buffer.GetChar(); break;
	case ',': code=tcComma;				buffer.GetChar(); break;
	case ':': code=tcColon;				buffer.GetChar(); break;
	case ';': code=tcSemicolon;			buffer.GetChar(); break;
	case '"': code=tcDQuote;			buffer.GetChar(); break;
	case '\'': code=tcQuote;			buffer.GetChar(); break;
	case '{': code=tcLBracket;			buffer.GetChar(); break;
	case '}': code=tcRBracket;			buffer.GetChar(); break;
	case '(': code=tcLParen;			buffer.GetChar(); break;
	case ')': code=tcRParen;			buffer.GetChar(); break;
	case '[': code=tcLSquareBracket;	buffer.GetChar(); break;
	case ']': code=tcRSquareBraquet;	buffer.GetChar(); break;

	default:	
		code=tcError;
		buffer.GetChar();
		Error(errUnrecognizable);
	}
	*ps='\0';
}


void TSpecialToken::Print(void)const
{
	sprintf(list.text,"\t%-18s %-s",">> special:",string);
	list.PutLine();
}


//error token
void TErrorToken::Get(TTextInBuffer &buffer)
{
	string[0]=buffer.Char();
	string[1]='\0';
	buffer.GetChar();
	cout<<"entrou com >> "<<string<<endl;
	Error(errUnrecognizable);
}

void TErrorToken::Print(void)const
{

}

