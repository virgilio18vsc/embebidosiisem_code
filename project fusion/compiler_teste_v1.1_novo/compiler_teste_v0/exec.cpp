#include "stdafx.h"

TSymtabNode *pCurrentFunction;					//guarda o apontador para a fun�ao que esta a ser executada no momento
TStackItem functionReturnValue;					//guarda o valor de retorno das funcoes

//              *******************
//              *                 *
//              *  Runtime Stack  *
//              *                 *
//              *******************

//--------------------------------------------------------------
//  Constructor
//--------------------------------------------------------------

TRuntimeStack::TRuntimeStack(void)
{
    tos        = &stack[-1];  // point to just below bottom of stack
    pFrameBase = &stack[ 0];  // point to bottom of stack

    //--Initialize the program's stack frame at the bottom.
    Push(0);  // function return value
    Push(0);  // static  link
    Push(0);  // dynamic link
    Push(0);  // return address icode pointer
    Push(0);  // return address icode location
}

//--------------------------------------------------------------
//  PushFrameHeader     Push the callee subroutine's stack frame
//                      header onto the runtime stack.  (Leave
//                      it inactive.)
//
//      oldLevel : nesting level of the caller routine
//      newLevel : nesting level of the callee subroutine's
//                 formal parameters and local variables
//      pIcode   : ptr to caller's intermediate code
//
//  Return: ptr to the base of the callee's stack frame
//--------------------------------------------------------------

void TRuntimeStack::PushFrameHeader(void)
{
	pNewFrameBase=(tos+1);
	Push((void*)(pFrameBase));
}

//--------------------------------------------------------------
//  ActivateFrame       Activate the newly-allocated stack frame
//                      by pointing the frame base pointer to it
//                      and setting the return address location.
//
//      pNewFrameBase : ptr to the new stack frame base
//      location      : return address location
//--------------------------------------------------------------

void TRuntimeStack::ActivateFrame(void)
{
    pFrameBase = pNewFrameBase;
}

//--------------------------------------------------------------
//  PopFrame    Pop the current frame from the runtime stack.
//              If it's for a function, leave the return value
//              on the top of the stack.
//
//      pRoutineId : ptr to subroutine name's symbol table node
//      pIcode     : ref to ptr to caller's intermediate code
//--------------------------------------------------------------

void TRuntimeStack::PopFrame(void)
{
	tos=pFrameBase;
	pFrameBase=(TStackItem*)Pop()->address;
}


TStackItem *TRuntimeStack::GetVar(int offset)
{
	return (pFrameBase + offset);
}

//--------------------------------------------------------------
//  AllocateValue       Allocate a runtime stack item for the
//                      value of a formal parameter or a local
//                      variable.
//
//      pId : ptr to symbol table node of variable or parm
//--------------------------------------------------------------

void TRuntimeStack::AllocateValue(const TSymtabNode *pId)
{
    TType *pType = pId->pType->Base();  // ptr to type object of value

    if      (pType == pIntegerType) Push(0);
    else if (pType == pRealType)    Push(0.0f);
    else if (pType == pBooleanType) Push(0);
    else if (pType == pCharType)    Push(0);

    else if (pType->form == fcEnum) Push(0);
    else 
	{
		//--Array or record
		void *addr = new char[pType->size];
		Push(addr);
    }
}

//--------------------------------------------------------------
//  DeallocateValue     Deallocate the data area of an array or
//                      record value of a formal value parameter
//                      or a local variable.
//
//      pId : ptr to symbol table node of variable or parm
//--------------------------------------------------------------

void TRuntimeStack::DeallocateValue(const TSymtabNode *pId)
{
    if ((! pId->pType->IsScalar()) && (pId->defn.how != dcVarParm)) 
	{
		TStackItem *pValue = ((TStackItem *) pFrameBase) + frameHeaderSize + pId->defn.data.offset;
		delete[] pValue->address;
    }
}

//--------------------------------------------------------------
//  GetValueAddress     Get the address of the runtime stack
//                      item that contains the value of a formal
//                      parameter or a local variable.  If
//                      nonlocal, follow the static links to the
//                      appropriate stack frame.
//
//      pId : ptr to symbol table node of variable or parm
//
//  Return:  ptr to the runtime stack item containing the
//           variable, parameter, or function return value
//--------------------------------------------------------------

TStackItem *TRuntimeStack::GetValueAddress(const TSymtabNode *pId)
{
    int functionFlag = pId->defn.how == dcFunction;  // true if function
						     //   else false
    TFrameHeader *pHeader = (TFrameHeader *) pFrameBase;

    //--Compute the difference between the current nesting level
    //--and the level of the variable or parameter.  Treat a function
    //--value as if it were a local variable of the function.  (Local
    //--variables are one level higher than the function name.)
    int delta = currentNestingLevel - pId->level;
    if (functionFlag) --delta;

    //--Chase static links delta times.
    while (delta-- > 0) 
	{
		pHeader = (TFrameHeader *) pHeader->staticLink.address;
    }

    return functionFlag ? &pHeader->functionValue : ((TStackItem *) pHeader) + frameHeaderSize + pId->defn.data.offset;
}




//              **************
//              *            *
//              *  Executor  *
//              *            *
//              **************

//--------------------------------------------------------------
//  Go                  Start the executor.
//--------------------------------------------------------------

void TExecutor::Go(const TSymtabNode *pProgramId)
{
	TSymtabNode *func_main=symtabStack.SearchAll("main");
	if(!func_main){Error(errMissingEntryPoint); AbortTranslation(abortRuntimeError);}

	icode=*(func_main->defn.routine.pIcode);								//pomos o icode da main
	symtabStack.PushSymtab(func_main->defn.routine.pSymtab);				//pos a tabela de simolos da main na stak de tabelas de simbolos
	
	Push(0);															//push dum dummy valor para inicializar o apontador
	runStack.PushFrameHeader();											//alocamos memoria na runtime stack para a main
	runStack.ActivateFrame();

	icode.Reset();

	cout<<"Start interpreter\n\n";

	//InitializeMain();
	ExecuteFunction(func_main);
	
	//icode.Print();
    //--Print the executor's summary.
    //cout << endl;
    //cout << "Successful completion.  " << stmtCount << " statements executed." << endl;

	//teste
	//cout<<"return value = "<<functionReturnValue.integer<<endl;
	/*do
	{
		GetToken();
		cout<<"token = "<<pToken->String()<<"\t\tcode = "<<token<<endl;
	}while(token != tcEndOfFile);*/
}

//--------------------------------------------------------------
//  Initializa main      put local variable of main function in stack
//
//      pTargetType : ptr to target type object
//      value       : integer value to assign
//--------------------------------------------------------------

void TExecutor::InitializeMain(void)
{
	
}


//--------------------------------------------------------------
//  RangeCheck      Range check an assignment to a subrange.
//
//      pTargetType : ptr to target type object
//      value       : integer value to assign
//--------------------------------------------------------------

void TExecutor::RangeCheck(const TType *pTargetType, int value)
{

    if (   (pTargetType->form == fcSubrange) && (   (value < pTargetType->subrange.min) || (value > pTargetType->subrange.max))) 
	{
		RuntimeError(rteValueOutOfRange);
    }
}
//endfig





