#include "stdafx.h"

//--------------------------------------------------------------
//  EmitStatement       Emit code for a statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitStatement(void)
{
    switch (token) 
	{
		case tcStar:		EmitAssignPtr();		break;
		case tcIdentifier:	EmitAssignment(NULL);	break;
		case tcIf:			EmitIF();				break;
		case tcWhile:		EmitWHILE();			break;
		case tcReturn:		EmitReturn();			break;
		case tcFor:			EmitFOR();				break;
		case tcSwitch:		EmitSWITCH();			break;

    }
}


//--------------------------------------------------------------
//  EmitAssignment      Emit code for an assignment statement for a ptr
//--------------------------------------------------------------
void TCodeGenerator::EmitAssignPtr(void)
{

}


//--------------------------------------------------------------
//  EmitAssignment      Emit code for an assignment statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitAssignment(const TSymtabNode *pTargetId)
{	
	TSymtabNode *var=symtabStack.SearchAll(pToken->String());

	TRegister *reg=regFile.FindRegister(var);
	if(!reg){reg=regFile.GetRegister(var);}
	
	if (var->defn.how == dcFunction)
	{
		EmitFunctionCall(var);
	}
	else
	{
		GetToken();

		if (token == tcPlusPlus){																			//Opera��o do tipo a++ incrementa imediatamente o valor.
			Emit2(ADDI, reg->GetReg(), reg->GetReg(), 1);
			GetToken();
		}
		else if (token == tcMinusMinus){																		//Opera��o do tipo a-- decrementa imediatamente o valor.
			Emit2(SUBI, reg->GetReg(), reg->GetReg(), 1);
			GetToken();
		}
		else{
			TTokenCode op = token;

			while (TokenIn(token, tlAssignOps)){
				GetToken();																					//1� token da expression
				EmitExpression();

				ExprStackData result = genStack.Pop();														//Resultado da expression calculada.

				switch (op){
				case tcEq:																			//=
					if (result.token == tcNumber){														//Instru��o tipo B
						Emit2(MOVI, reg->GetReg(), 0, result.val.integer);
					}
					else																				//Instru��o tipo A
						if (reg->GetReg() != result.reg->GetReg())
							Emit1(MOV, reg->GetReg(), result.reg->GetReg(), 0);
					break;

				case tcPlusEqual:																		//+=
					if (result.token == tcNumber){														//Instru��o tipo B
						Emit2(ADDI, reg->GetReg(), reg->GetReg(), result.val.integer);
					}
					else																				//Instru��o tipo A
						Emit1(ADD, reg->GetReg(), reg->GetReg(), result.reg->GetReg());
					break;

				case tcMinusEqual:																		//-=
					if (result.token == tcNumber){														//Instru��o tipo B
						Emit2(SUBI, reg->GetReg(), reg->GetReg(), result.val.integer);
					}
					else																				//Instru��o tipo A
						Emit1(SUB, reg->GetReg(), reg->GetReg(), result.reg->GetReg());
					break;

				case tcXorEqual:																		//^=
					if (result.token == tcNumber){														//Instru��o tipo B
						Emit2(XORI, reg->GetReg(), reg->GetReg(), result.val.integer);
					}
					else																				//Instru��o tipo A
						Emit1(XOR, reg->GetReg(), reg->GetReg(), result.reg->GetReg());
					break;

				case tcOrEqual:																			//|=
					if (result.token == tcNumber){														//Instru��o tipo B
						Emit2(ORI, reg->GetReg(), reg->GetReg(), result.val.integer);
					}
					else																				//Instru��o tipo A
						Emit1(OR, reg->GetReg(), reg->GetReg(), result.reg->GetReg());
					break;

				case tcAndEqual:																		//&=
					if (result.token == tcNumber){														//Instru��o tipo B
						Emit2(ANDI, reg->GetReg(), reg->GetReg(), result.val.integer);
					}
					else																			//Instru��o tipo A
						Emit1(AND, reg->GetReg(), reg->GetReg(), result.reg->GetReg());
					break;

				case tcStarEqual:																		//*=

					genStack.Push(tcRegister, reg);
					if (result.token == tcNumber)genStack.Push(result.token, result.val.integer);
					else genStack.Push(result.token, result.reg);
					op = token;
					token = tcStarEqual;
					EmitTerm();
					token = op;
					result = genStack.Pop();
					Emit1(MOV, reg->GetReg(), result.reg->GetReg(), 0);

				case tcSlashEqual:
					genStack.Push(tcRegister, reg);
					if (result.token == tcNumber)genStack.Push(result.token, result.val.integer);
					else genStack.Push(result.token, result.reg);
					op = token;
					token = tcSlashEqual;
					EmitTerm();
					token = op;
					result = genStack.Pop();
					Emit1(MOV, reg->GetReg(), result.reg->GetReg(), 0);
					break;
				}
			}
		}
	}
}

//--------------------------------------------------------------
//  EmitIf      Emit code for an if statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitIF(void)
{
	ExprStackData resultado;
	int pc_branch_falso, offset_falso, actual_pc, pc_branch_apos_else, offset_apos_else;

	GetToken(); //"("
	GetToken(); //Primeiro token da express�o
	EmitExpression();//calcula a express�o do if
	//GetToken();//")"
	GetToken();//"{"

	resultado = genStack.Pop();
	TRegister *rd = regFile.GetRegister(&tempReg);
	Emit1(CMP, rd->GetReg(), resultado.reg->GetReg(), 0); //compara��o com o resultado com 0


	pc_branch_falso = PC;
	PC = PC + 2;

	EmitStatmentList(tcRBracket); //Gerado o c�digo dos statemantes dentro do bloco if

	//GetToken();	//"}"
	GetToken();

	if (token == tcElse){ //Testar se existe o bloco else depois do if

		pc_branch_apos_else = PC; //deixado espa�o em mem�ria para o salto incondicional (para n�o executal o bloco else depois do if) 	
		PC = PC + 2;
		actual_pc = PC;
		offset_falso = PC - pc_branch_falso - 1;//calculo do offset de salto, caso a condi��o do if seja falso

		//Preenche posi��o memoria aberta com branch para else se a condi��o for falsa.
		PC = pc_branch_falso;
		Emit2(MOVI, resultado.reg->GetReg(), 0, offset_falso);
		Emit1(BEQ, 0, rd->GetReg(), resultado.reg->GetReg());

		PC = actual_pc;//recuperar PC com a posi��o apos o c�digo correspondente


		GetToken();//"{"
		EmitStatmentList(tcRBracket);////gera��o de c�digo do statemant do else 
		GetToken();//"}"

		actual_pc = PC;
		offset_apos_else = PC - pc_branch_apos_else - 1;	//calcula offset para branch incondicional para fora do if se a condi��o for verdadeira 
		//(premitino n�o executar o c�digo correspondente ao else).
		//preenche a pos���o de mem�ria aberta com branch incondicional para fora do if se a condi��o for verdadeira
		PC = pc_branch_apos_else;
		Emit2(MOVI, resultado.reg->GetReg(), 0, offset_apos_else);
		Emit1(BRA, 0, 0, resultado.reg->GetReg());

		PC = actual_pc;		//Recupera PC com a posi��o apos o c�digo correspondente ao while
	}
	else{

		actual_pc = PC;
		offset_falso = PC - pc_branch_falso - 1;

		//Preencher posi��o de memoria aberta com branch para fora do if se a condi��o for falsa
		PC = pc_branch_falso;
		Emit2(MOVI, resultado.reg->GetReg(), 0, offset_falso);
		Emit1(BEQ, 0, rd->GetReg(), resultado.reg->GetReg());

		PC = actual_pc; //recuperar PC com a posi��o apos o c�digo correspondente

		icode.GoTo(icode.CurrentLocation() - 1); // devolvido o token consumido para verificar se era o else

	}
	regFile.FreeRegister(&tempReg); //Liberta registos auxiliares apenas utilizados dentro da expression

	//Libertados registos auxiliares apenas utilizados dentro da expression.
}


//--------------------------------------------------------------
//  EmitWhile      Emit code for an if statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitWHILE(void)
{
	int PC_while, pc_branch_falso, actual_pc, offset_falso;
	PC_while = PC;		// guarda a posi��o da condi��o para depois ser feito o salto para essa posi��o 

	GetToken();			//"("
	GetToken();			// 1� token da condi��o
	EmitExpression();

	GetToken(); //"{"
	ExprStackData reg_condicao = genStack.Pop();

	TRegister *rd = regFile.GetRegister(&tempReg);	//registo que fica com o valor da compara��o, usado para resolver o branch

	Emit1(CMP, rd->GetReg(), reg_condicao.reg->GetReg(), 0);	//R0 pode ser utilizado para compara��o da condi��o com o 0, pk r0 = 0

	pc_branch_falso = PC;	//Guarda a posi��o no c�digo gerado, para depois ser calculado o offset do branch ao fim dos statements do while
	PC = PC + 2;			//Deixa em vazio para que depois seja escrito o valor de salto 
	EmitStatmentList(tcRBracket);
	GetToken(); //"}"
	Emit2(MOVI, reg_condicao.reg->GetReg(), 0, PC_while);
	Emit1(BRA, 0, 8, reg_condicao.reg->GetReg());	//branch incondicional para a instru��o  de teste do while

	actual_pc = PC;		//guarda a posi��o do fim do while
	offset_falso = PC - pc_branch_falso - 1; //calcular o offset do branch para sair do while 

	//Preencher pos���o mem�ria aberta com branch para fora do ciclo se a condi��o for falsa
	PC = pc_branch_falso;
	Emit2(MOVI, reg_condicao.reg->GetReg(), 0, offset_falso);
	Emit1(BEQ, 0, rd->GetReg(), reg_condicao.reg->GetReg());

	PC = actual_pc;
	regFile.FreeRegister(&tempReg); //Liberta registos auxiliares apenas utilizados dentro da expression

}


//--------------------------------------------------------------
//  EmitReturn      Emit code for a return statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitReturn()
{
	ExprStackData param;
	TSymtab * tab = symtabStack.GetCurrentSymtab();
	TSymtabNode *var;
	TDefn *func = NULL;


	for(int i = 0; i < tab->NodeCount(); i++){
		var=tab->Get(i);

		if(var->defn.how == dcFunction)
			func = &(var->defn);
	}

	GetToken();
	EmitExpression();

	param = genStack.Pop();

	if(param.token == tcNumber)
	{
		Emit2(MOVI, FUNCTION_RETURN_REG, 0, param.val.integer);
	}
	else
	{
		Emit2(MOV, FUNCTION_RETURN_REG, param.reg->GetReg(), 0);
	}

	//dependencyList->AddDependencyReturn((*(func->routine.vSymtab))->String(), PC);	access func's name
	dependencyList->AddDependencyReturn((char *)curFunction.c_str(), PC); 
	Emit2(BR, 0, 0, 0);
}

void TCodeGenerator::EmitFOR(void)
{
	int pc_test, label_inc, pc_branch_falso, label_apos_for, offset_falso, actual_pc;
	ExprStackData resultado;
	GetToken();
	GetToken();
	EmitAssignment(NULL);
	//GetToken();
	pc_test = PC;
	//GetToken();
	EmitExpression();
	//GetToken();
	label_inc = icode.CurrentLocation();
	while (token != tcLBracket)
		GetToken();
	//resultado = genStack.Pop();
	//TRegister *reg_condicao = regFile.GetRegister(&tempReg);

	ExprStackData reg_condicao = genStack.Pop();

	TRegister *rd = regFile.GetRegister(&tempReg);
	//Emit2(MOVI, reg_condicao->GetReg(), 0, reg_condicao_s.val.integer);


	Emit1(CMP, rd->GetReg(), reg_condicao.reg->GetReg(), 0);
	pc_branch_falso = PC;
	PC = PC + 2;
	EmitStatmentList(tcRBracket);
	//	GetToken();
	label_apos_for = icode.CurrentLocation();
	icode.GoTo(label_inc);
	GetToken();
	EmitAssignment(NULL);
	Emit2(MOVI, reg_condicao.reg->GetReg(), 0, pc_test);
	//Emit1(BR, 0, 0, reg_condicao.reg->GetReg());
	Emit1(BRA, 0, 8, reg_condicao.reg->GetReg());// NOTA: alterei o Emit1 pk queremos fazer o salto para traz por isso tem que ser o BRA e nao o BR! ->Rapha
	actual_pc = PC;
	offset_falso = PC - pc_branch_falso - 1;
	PC = pc_branch_falso;
	pc_branch_falso = offset_falso;
	Emit2(MOVI, reg_condicao.reg->GetReg(), 0, offset_falso);
	Emit1(BEQ, rd_BEQ, rd->GetReg(), reg_condicao.reg->GetReg());
	PC = actual_pc;
	icode.GoTo(label_apos_for);
	regFile.FreeRegister(&tempReg);
	GetToken();
	//regFile.FreeRegister(reg_condicao->GetReg());
}


void TCodeGenerator::EmitSWITCH(void)
{


	int primeiro_case, chavetas = 0, pc_aux;

	bool flagDefault = false; //determinar se existe default
	GetToken(); //"("
	GetToken();//variavel de entrada

	//armazenar num registo a variavel de entrada

	TSymtabNode *var = symtabStack.SearchAll(pToken->String()); //buscar a variavel que ficou como argumento no switch
	TRegister *reg = regFile.FindRegister(var); //buscar o registo que ficou com o valor que est� como argumento no switch
	TRegister *rd = regFile.GetRegister(&tempReg); // registo que fica com o valor da CMP
	TRegister *salto = regFile.GetRegister(&tempReg); // registo que fica com o valor da CMP
	if (!reg){
		reg = regFile.GetRegister(var);
	}

	GetToken();//")"
	GetToken();// "{"
	GetToken();// "{"
	primeiro_case = icode.CurrentLocation(); //guardar o endere�o da linha de codigo onde come�a os cases do switch

	//chavetas = 1;

	ListaLabel *listaLabel = NULL;
	ListaBreak *listaBreaks = NULL;

	GetToken(); //l� "case" OU Default

	//--------------------1� ITERA��O: ler os cases que tem para comparar
	//								  e escreve as compara��es e guarda os endere�oes dos comandos  BEQs
	//								  para escrever no futuro
	pc_aux = PC;
	while (token != tcRBracket)	//corre at� encontrar a chaveta do switch
	{
		if (token == tcCase)
		{
			GetToken();	//l� o valor do case

			listaLabel = listaLabel->Insere(pToken->String(), PC);// insere na lista ligada
			PC = PC + 4;// para depois escrever os compares e valores de salto
		}
		else if (token == tcDefault) //default ????
		{
			listaLabel = listaLabel->Insere(pToken->String(), 0);
			flagDefault = true;

		}

		CorreCase(chavetas);	//ignora tudo que tem dentro do case at� encontrar tcbreak,tcCase ou tcDefault
	}

	PC = PC + 1;//guardar espa�o para salto do default ou fim de switch
	listaBreaks = CriarSaltos(rd, reg, listaLabel, listaBreaks, flagDefault);//escreve no vetor de codigo(CodeMeM) os compares e armazena os endere�os onde se ira por no futuro os BEQ's/BRA

	//---------------------2� ITERA��O: escrita dos cases e dos BEQS na sec�ao das compara�oes
	//									e guarda o endere�o dos breaks

	icode.GoTo(primeiro_case);//volta para o inicio dos cases

	GetToken(); //le o "Case" ou ou "Default"

	while (token != tcRBracket)//Corre ate chegar a chaveta de fim de switch
	{
		EscreveCases(rd, salto, listaLabel, listaBreaks);//???????????
	}

	//-------------------------3� ITERA��O: preencher os espa�os vazios dos breaks, com os 
	//										saltos para o fim do switch

	EscreveBreaks(listaBreaks);
	regFile.FreeRegister(&tempReg); //Liberta registos auxiliares apenas utilizados dentro da expression
	listaLabel->Deletelist();
	listaBreaks->Deletelist();
	GetToken();
	GetToken();

}


/*****************************************************************************
*			FUNCOES AUXILIARES
*****************************************************************************/
void  TCodeGenerator::CorreCase(int chavetas)
{
	//serve para saber se esta na estrutura de condicao que a chamou ou num outro ciclo/estrutura de condicao
	//sendo os valores impares a abertura de uma chaveta e os valores pares o fechar chaveta
	do
	{
		GetToken();
		if (token == tcLBracket) { chavetas++; }
		if (token == tcRBracket) { chavetas--; }

	} while (token != tcBreak && token != tcDefault && token != tcCase && (chavetas >= 0));
}

ListaBreak *TCodeGenerator::CriarSaltos(TRegister *rd, TRegister *registo, ListaLabel *lista, ListaBreak *lista2, bool flag)
{
	ListaLabel *aux = lista;
	//	TRegister *rb = regFile.GetRegister(&tempReg); // registo auxiliar
	while (aux != NULL)
	{
		Emit2(MOVI, rd->GetReg(), 0, aux->GetNumber());
		Emit1(CMP, rd->GetReg(), registo->GetReg(), rd->GetReg());
		aux->SetEndereco(PC);
		PC = PC + 2;						//para deixar um espaco para depois colocar o comando BEQ
		aux = aux->GetSeguinte();
	}
	if (flag)		//caso tenha default, salta so para o default
	{
		aux->SetEndereco(PC);
		PC = PC + 2;

	}
	else		//sem default, tera que fazer um salto incondicional para o final do switch
	{

		//	Emit2(MOVI, ra->GetReg(), 0, aux->GetNumber());
		//	Emit1(CMP,rd->GetReg(),registo->GetReg(), ra->GetReg());
		//	aux->SetEndereco(PC);
		//	PC = PC + 2;					//para deixar um espaco para depois colocar o comando BEQ
		lista2 = lista2->Insere(PC);
		PC = PC + 2;					//para deixar um espaco para depois colocar o comando BRA para o fim do switch
	}
	regFile.FreeRegister(&tempReg); //Liberta registos auxiliares apenas utilizados dentro da expression
	return lista2;
}

void TCodeGenerator::EscreveCases(TRegister *rd, TRegister *salto, ListaLabel *l1, ListaBreak *l2)
{
	int PC_aux = PC;
	int jump_value;
	string val;

	//escreve 1� o comando BEQ
	if (token == tcCase)
	{

		GetToken();		//le numero
		val = pToken->String();
		PC = l1->GetEndereco(atoi(val.c_str()));
		jump_value = PC_aux - PC - 1;	//calcular o valor do jump para a label

		Emit2(MOVI, salto->GetReg(), 0, jump_value);
		Emit1(BEQ, 0, rd->GetReg(), salto->GetReg());

	}
	else
	{
		//	PC = l1->GetEndereco(tcDEFAULT);
		//	Emit1(BRA, 0, PC_aux, 0);
	}

	PC = PC_aux;
	GetToken(); //:
	GetToken();
	if (token != tcRBracket){
		//escreve o conteudo do case para assembly
		StatementList3Arg(tcBreak, tcCase, tcDefault);
	}
	if (token == tcBreak)
	{
		l2 = l2->Insere(PC);
		PC++; // GUARDAR ESPA�O PARA O BRA
	}

}

void  TCodeGenerator::EscreveBreaks(ListaBreak *l)
{
	int finalLabel = PC;			//endereco on esta o fim da estrutura do switch
	ListaBreak * aux = l;

	while (aux != NULL)					//enquanto houver breaks, mais o caso do salto incodicional, caso nao haja default...
	{
		PC = aux->GetPC();
		if ((finalLabel - PC) == 1)
		{
			Emit1(0, 0, 0, 0);			//Escreve NOP, dado que na linha a seguir esta o fim do switch
		}
		else
		{
			Emit2(BRAI, 0, 8, finalLabel);		//faz o salto para o fim da estrutura do switch
		}
		aux = aux->GetSeguinte();
	}
}

void  TCodeGenerator::StatementList3Arg(int terminator1, int terminator2, int terminator3)
{
	//GetToken();
	do
	{
		EmitStatement();
		while (token == tcSemicolon) { GetToken(); }

	} while (token != terminator1 && token != terminator2 && token != terminator3);
}

ListaLabel::ListaLabel()
{

	Seguinte = NULL;
}

ListaLabel::~ListaLabel()
{
}

ListaLabel *ListaLabel::Insere(string valor, int endereco_PC)
{
	int val = atoi(valor.c_str());
	ListaLabel *aux = this, *anterior = this;
	ListaLabel *p = new ListaLabel;
	p->CaseNumber = val;
	p->JumpLabel = endereco_PC;
	if (aux != NULL)
	{
		while (aux != NULL)
		{
			anterior = aux;
			aux = aux->Seguinte;
		}

		anterior->Seguinte = p;
		p->Seguinte = aux;
		return this;
	}
	else
	{
		return p;

	}


}
void ListaLabel::SetEndereco(int endereco)
{
	this->JumpLabel = endereco;
}
int ListaLabel::GetEndereco(int numbercase)
{
	ListaLabel *p;
	p = this;
	while (p != NULL && (p->CaseNumber != numbercase))
	{
		p = p->GetSeguinte();
	}

	return p->JumpLabel;
}
int ListaLabel::GetNumber()
{
	return CaseNumber;
}
ListaLabel *ListaLabel::GetSeguinte()
{
	return this->Seguinte;
}

void ListaLabel::Deletelist()
{
	ListaLabel * p = this, *Anterior = this;
	while (p != NULL)
	{
		Anterior = p;
		p = p->GetSeguinte();
		delete Anterior;
	}
}

ListaBreak::ListaBreak()
{
	Seguinte = NULL;
}

ListaBreak::~ListaBreak()
{

}

ListaBreak *ListaBreak::Insere(int endereco)
{
	ListaBreak *aux = this, *base = this;

	ListaBreak *p = new ListaBreak;
	p->PC = endereco;
	p->Seguinte = NULL;
	if (aux != NULL)
	{
		while (aux->Seguinte != NULL)
		{
			aux = aux->Seguinte;
		}
		aux->Seguinte = p;
	}
	else
	{
		return p;

	}
	return 	base;
}
int ListaBreak::GetPC()
{
	return PC;
}

ListaBreak *ListaBreak::GetSeguinte()
{
	return this->Seguinte;
}

void ListaBreak::Deletelist()
{
	ListaBreak * p = this, *Anterior = this;
	while (p != NULL)
	{
		Anterior = p;
		p = p->GetSeguinte();
		delete Anterior;
	}
}