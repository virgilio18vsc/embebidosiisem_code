#include "stdafx.h"

const int minResWordLen = 2;
const int maxResWordLen = 8;

struct TResWord
{
	char *pString;
	TTokenCode code;
};


//tabela com as palavras reservadas ordenadas por numero de letras e depois por ordem alfabetica
static TResWord rw2[] = 
{
	{"do",tcDo},{"if",tcIf},{NULL},
};

static TResWord rw3[] =
{
	{"for",tcFor},{"int",tcInt},{NULL},
};

static TResWord rw4[] =
{
	{"auto",tcAuto},{"case",tcCase},{"char",tcChar},{"else",tcElse},{"enum",tcEnum},{"goto",tcGoto},{"long",tcLong},{"void",tcVoid},{NULL},
};

static TResWord rw5[] =
{
	{"break",tcBreak},{"const",tcConst},{"float",tcFloat},{"short",tcShort},{"union",tcUnion},{"while",tcWhile},{NULL},
};

static TResWord rw6[] =
{
	{"double",tcDouble},{"extern",tcExtern},{"return",tcReturn},{"signed",tcSigned},{"sizeof",tcSizeof},{"struct",tcStruct},{"switch",tcSwitch},{NULL},
};

static TResWord rw7[] =
{
	{"default",tcDefault},{"typedef",tcTypedef},{NULL},
};

static TResWord rw8[] =
{
	{"continue",tcContinue},{"register",tcRegister},{"unsigned",tcUnsigned},{"volatile",tcVolatile},{NULL},
};


static TResWord *rwTable[]=
{
	NULL,NULL,rw2,rw3,rw4,rw5,rw6,rw7,rw8,
};

//***********************************************************************************************************************************

void TWordToken::Get(TTextInBuffer &buffer)
{
	extern TCharCode charCodeMap[];

	char ch=buffer.Char();
	char *ps=string;

	do
	{
		*ps++=ch;
		ch=buffer.GetChar();
	}while((charCodeMap[ch] == ccLetter) || (charCodeMap[ch]==ccDigit) || (ch == '_'));

	*ps='\0';

	CheckForReservedWord();
}

void TWordToken::CheckForReservedWord(void)
{
	int len=strlen(string);
	TResWord *prw;

	code = tcIdentifier;										// a partida assumimos que e um identificador

	if((len >= minResWordLen) && (len <= maxResWordLen))
	{
		for(prw=rwTable[len];prw->pString;prw++)
		{
			if(strcmp(string,prw->pString) == 0)
			{
				code=prw->code;
				break;
			}
		}
	}
}

void TWordToken::Print(void)const
{
	if(code == tcIdentifier)
	{
		sprintf(list.text,"\t%-18s %-s",">> identifier",string);
	}
	else
	{
		sprintf(list.text,"\t%-18s %-s",">> reserved word",string);
	}

	list.PutLine();
}
