//short circuit para o caso a||1 e a&&0 adicionado e correcção no código quando os dois tokens são números;
TType *TCodeGenerator::EmitLogicExpression(void)
{
	TTokenCode op;

	EmitSimpleExpression();

	while (TokenIn(token, tlLogicOps)) //tlRelOps
	{
		op = token;
		GetToken();
		EmitSimpleExpression();

		ExprStackData op2 = genStack.Pop();
		ExprStackData op1 = genStack.Pop();

		//Criar registo temporário
		TSymtabNode *temp_symbol = symtabStack.SearchAll("temp_reg");
		if (!temp_symbol) {
			temp_symbol = globalSymtab.Enter("temp_reg");
		}
		TRegister *result = regFile.GetRegister(temp_symbol);
		TRegister *branch_reg = regFile.GetRegister(temp_symbol);					//Pedido registo auxiliar para fazer branch condicional.
		//
		if (op1.token == tcNumber) {
			if (op2.token == tcNumber) {
				if (op == tcLogicAnd)
				{
					if (op1.val.integer == 0 || op2.val.integer == 0) { genStack.Push(tcNumber, 0); }
					else { genStack.Push(tcNumber, 1); }
					regFile.FreeRegister(branch_reg->GetReg());
					return NULL;
				}
				else if (op == tcLogicOr)
				{
					if (op1.val.integer == 1 || op2.val.integer == 1) { genStack.Push(tcNumber, 1); }
					else { genStack.Push(tcNumber, 0); }
					regFile.FreeRegister(branch_reg->GetReg());
					return NULL;
				}
			}
			else if (op == tcLogicAnd && op1.val.integer == 0) {									//Se for uma operacao && com primeiro operador 0 faz short circuit.
				genStack.Push(tcNumber, 0);
				regFile.FreeRegister(branch_reg->GetReg());
				return NULL;
			}
			else if (op == tcLogicOr && op1.val.integer != 0) {								//Se for uma operacao || com primeiro operador 1 faz short circuit.
				genStack.Push(tcNumber, 1);
				regFile.FreeRegister(branch_reg->GetReg());
				return NULL;
			}
		}
		else{
			Emit1(CMP, result->GetReg(), op1.reg->GetReg(), 0);							//R0 pode ser utilizado para comparação  da condição com 0, porque R0 = 0.
			Emit2(MOVI, branch_reg->GetReg(), 0, 2);

			if (op == tcLogicAnd) {													//Se for uma operacao && com primeiro operador 0 faz short circuit.
				Emit1(BNE, rd_BNE, result->GetReg(), branch_reg->GetReg());
				Emit2(MOVI, result->GetReg(), 0, 0);
			}
			else if (op == tcLogicOr) {												//Se for uma operacao || com primeiro operador 1 faz short circuit.
				Emit1(BEQ, rd_BEQ, result->GetReg(), branch_reg->GetReg());
				Emit2(MOVI, result->GetReg(), 0, 1);
			}
		}
		if (op2.token == tcNumber) {											//É preciso testar quando as outras rotinas estiverem a funcionar
			if (op == tcLogicOr && op2.val.integer == 1){
				// Não faz nada
			}
			else if (op == tcLogicAnd && op2.val.integer == 0){
				// Não faz nada
			}
			else {
				Emit2(BRI, rd_BRI, 0, 1);
				if (op2.val.integer == 0) {
					Emit2(MOVI, result->GetReg(), 0, 0);
				}
				else {
					Emit2(MOVI, result->GetReg(), 0, 1);
				}
			}
		}
		else
		{
			if (op1.token == tcIdentifier) { Emit2(BRI, 0, 0, 6) }
			Emit1(CMP, result->GetReg(), op2.reg->GetReg(), 0);							//R0 pode ser utilizado para comparação  da condição com 0, porque R0 = 0.
			Emit2(MOVI, branch_reg->GetReg(), 0, 2);
			Emit1(BNE, rd_BNE, result->GetReg(), branch_reg->GetReg());
			Emit2(MOVI, result->GetReg(), 0, 0);
			Emit2(BRI, rd_BRI, 0, 1);
			Emit2(MOVI, result->GetReg(), 0, 1);
		}

		regFile.FreeRegister(branch_reg->GetReg());
		genStack.Push(tcIdentifier, result);
		
	}
	return NULL;
}