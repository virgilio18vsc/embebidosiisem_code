#ifndef __DPLIST_H__
#define __DPLIST_H__

#include "stdafx.h"
#include "symtab.h"

class List
{
protected:
	List *Next;
public:
	List();
	~List();
	virtual void Add(void *arg){};
	virtual List* Get(){return Next;};
	//virtual void Remove(){};
	
};
class PosList : public List
{
protected:
	int pos;
public:
	PosList(int);
	~PosList();
	void Add(void *arg);
	List* Get();
	int getPos() const;
};

class DependencyList : public List
{
private:
	char *pfunctionName;
	int posProlog;
	int posEpilog;
	PosList* dependenciesCall;
	PosList* dependenciesReturn;
public:
	DependencyList(char *pfunctionName);
	~DependencyList();
	void SetPosProlog(const TDefn *func);
	void SetPosEpilog(const TDefn *func);
	void AddDependencyReturn(char*, int); //Lista ligada de dependenciesReturn
	void AddDependencyCall(char*, int); //Lista ligada de dependenciesCall
	void ResolveDependencies();
	void Add(void *arg);

};







#endif