#include "stdafx.h"

TType *TCodeGenerator::EmitExpression(void)
{

	TTokenCode op;

	EmitLogicExpression();//vai buscar primeira variavel/numero da opera��o condicinal e faz push para a stack

	while(TokenIn(token,tlRelOps)) 
	{
		op=token; 
		GetToken(); 
		EmitLogicExpression();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		
		
		TRegister *result_reg = regFile.GetRegister(&tempReg);	//Pedido registo auxiliar para armazenar resultado e pushar para pilha.

	//Se dois number, compara o valor das constantes e resolve express�o sem gerar c�digo m�quina.
		if(op1.token == tcNumber && op2.token == tcNumber)
		{
			int operation_true = 0;
			switch(op)
			{   //number == number
				case tcEqual:
					if( op1.val.integer == op2.val.integer) operation_true = true;//temos um problema aqui, temos de declarar como inteiros e ver como no futuro por a premitir float para alem de int
					break;
				//number != number
				case tcNe:
					if(op1.val.integer != op2.val.integer) operation_true = true;
					break;
				//number <= number
				case tcLe:
					if(op1.val.integer <= op2.val.integer) operation_true = true;
					break;
				//number < number
				case tcLt:
					if(op1.val.integer < op2.val.integer) operation_true = true;
					break;
				//number >= number
				case tcGe:
					if(op1.val.integer >= op2.val.integer) operation_true = true;
					break;
				//number > number
				case tcGt:
					if(op1.val.integer > op2.val.integer) operation_true = true;
					break;

				default:
					break;

			}
			/* //nao � util para ja mas pode vir a ser
			if (operation_true) //aqui est� a dar barraco � melhor simplificar e fazer logo push ao valor para a stack(ver emitassignment)
			{
				Emit2(MOVI, result_reg->GetReg(), 0, 1); // gera codigo equivalente ao push 1, no fluxograma de compara��o dos valores
			}
			else 
			{
				Emit2(MOVI, result_reg->GetReg(), 0, 0); //gera codigo equivalente ao push 0, no fluxograma de compara��o dos valores
			}*/

			genStack.Push(tcNumber, operation_true); // so aqui � feito o push para a stack
			regFile.FreeRegister(result_reg->GetReg());//faz s� o free do registo
		} 
		//Se n�o forem duas constantes � gerado c�digo m�quina.
		else
		{

			TRegister *rd = regFile.GetRegister(&tempReg);	//Registo que fica com o resultado da compara��o

			//operador 1 e 2 s�o variaveis e nao numeros
			if( (op1.token != tcNumber) & (op2.token != tcNumber))
			{
				Emit1(CMP, rd->GetReg(), op1.reg->GetReg(), op2.reg->GetReg());
			}
			else if(op1.token != tcNumber && op2.token == tcNumber) //
			{
				
				TRegister *op2_t = regFile.GetRegister(&tempReg);	//Constante num�rica passada para registo para poder ser feita a compara��o

				Emit2(MOVI, op2_t->GetReg(), 0, op2.val.integer);
				Emit1(CMP, rd->GetReg(), op1.reg->GetReg(), op2_t->GetReg());

				regFile.FreeRegister(op2_t->GetReg());//free do registo

			}
			else if(op1.token == tcNumber && op2.token != tcNumber)
			{
				TRegister *op1_t = regFile.GetRegister(&tempReg);	//Constante num�rica passada para registo para poder ser feita a compara��o

				Emit2(MOVI, op1_t->GetReg(), 0, op1.val.integer);
				Emit1(CMP, rd->GetReg(), op1_t->GetReg(), op2.reg->GetReg());

				regFile.FreeRegister(op1_t->GetReg());

			}
			
			TRegister *branch_reg = regFile.GetRegister(&branchReg);	//Pedido registo auxiliar para fazer branch condicional.

			Emit2(MOVI, branch_reg->GetReg(), 0, 3); //ALTERADO ANDREIA/RAPAH----->>> VALOR DE SALTO TEM QUE SER 3!

			switch(op)   //Emitido branch condicional para cada token de compara��o.
			{
				case tcEqual:
					Emit1(BEQ, rd_BEQ, rd->GetReg(), branch_reg->GetReg()); //valores dos branchs foram buscados ao disassembler do Tiago
					break;

				case tcNe:
					Emit1(BNE, rd_BNE, rd->GetReg(), branch_reg->GetReg());
					break;

				case tcLe:
					Emit1(BLE, rd_BLE, rd->GetReg(), branch_reg->GetReg());
					break;	

				case tcLt:
					Emit1(BLT, rd_BLT, rd->GetReg(), branch_reg->GetReg());
					break;	

				case tcGt:
					Emit1(BGT, rd_BGT, rd->GetReg(), branch_reg->GetReg());
					break;	

				case tcGe:
					Emit1(BGE, rd_BGE, rd->GetReg(), branch_reg->GetReg());
					break;

				default:
					break;
			}
			
			regFile.FreeRegister(rd->GetReg());
			regFile.FreeRegister(branch_reg->GetReg());		//Remove registos temporarios locais 

			Emit2(MOVI, result_reg->GetReg(), 0, 0);
			Emit2(BRI, rd_BRI, 0, 2);	//ALTERADO ANDREIA/RAPAH----->>> VALOR DE SALTO TEM QUE SER 2!
			Emit2(MOVI, result_reg->GetReg(), 0, 1);

			genStack.Push(tcIdentifier, result_reg);



		}
//			result_type = pBooleanType; // nao sei para que serve, acho que � para os ciclos
	}

	//return result_type;
	return NULL;
}

//short circuit para o caso a||1 e a&&0 adicionado e correc��o no c�digo quando os dois tokens s�o n�meros;
TType *TCodeGenerator::EmitLogicExpression(void)
{
	TTokenCode op;

	EmitSimpleExpression();

	while (TokenIn(token, tlLogicOps)) //tlRelOps
	{
		op = token;
		GetToken();
		EmitSimpleExpression();

		ExprStackData op2 = genStack.Pop();
		ExprStackData op1 = genStack.Pop();

		//Criar registo tempor�rio
		TSymtabNode *temp_symbol = symtabStack.SearchAll("temp_reg");
		if (!temp_symbol) {
			temp_symbol = globalSymtab.Enter("temp_reg");
		}
		TRegister *result = regFile.GetRegister(temp_symbol);
		TRegister *branch_reg = regFile.GetRegister(temp_symbol);					//Pedido registo auxiliar para fazer branch condicional.
		//
		if (op1.token == tcNumber) {
			if (op2.token == tcNumber) {
				if (op == tcLogicAnd)
				{
					if (op1.val.integer == 0 || op2.val.integer == 0) { genStack.Push(tcNumber, 0); }
					else { genStack.Push(tcNumber, 1); }
					regFile.FreeRegister(branch_reg->GetReg());
					return NULL;
				}
				else if (op == tcLogicOr)
				{
					if (op1.val.integer == 1 || op2.val.integer == 1) { genStack.Push(tcNumber, 1); }
					else { genStack.Push(tcNumber, 0); }
					regFile.FreeRegister(branch_reg->GetReg());
					return NULL;
				}
			}
			else if (op == tcLogicAnd && op1.val.integer == 0) {									//Se for uma operacao && com primeiro operador 0 faz short circuit.
				genStack.Push(tcNumber, 0);
				regFile.FreeRegister(branch_reg->GetReg());
				return NULL;
			}
			else if (op == tcLogicOr && op1.val.integer != 0) {								//Se for uma operacao || com primeiro operador 1 faz short circuit.
				genStack.Push(tcNumber, 1);
				regFile.FreeRegister(branch_reg->GetReg());
				return NULL;
			}
		}
		else{
			Emit1(CMP, result->GetReg(), op1.reg->GetReg(), 0);							//R0 pode ser utilizado para compara��o  da condi��o com 0, porque R0 = 0.
			Emit2(MOVI, branch_reg->GetReg(), 0, 2);

			if (op == tcLogicAnd) {													//Se for uma operacao && com primeiro operador 0 faz short circuit.
				Emit1(BNE, rd_BNE, result->GetReg(), branch_reg->GetReg());
				Emit2(MOVI, result->GetReg(), 0, 0);
			}
			else if (op == tcLogicOr) {												//Se for uma operacao || com primeiro operador 1 faz short circuit.
				Emit1(BEQ, rd_BEQ, result->GetReg(), branch_reg->GetReg());
				Emit2(MOVI, result->GetReg(), 0, 1);
			}
		}
		if (op2.token == tcNumber) {											//� preciso testar quando as outras rotinas estiverem a funcionar
			if (op == tcLogicOr && op2.val.integer == 1){
				// N�o faz nada
			}
			else if (op == tcLogicAnd && op2.val.integer == 0){
				// N�o faz nada
			}
			else {
				Emit2(BRI, rd_BRI, 0, 1);
				if (op2.val.integer == 0) {
					Emit2(MOVI, result->GetReg(), 0, 0);
				}
				else {
					Emit2(MOVI, result->GetReg(), 0, 1);
				}
			}
		}
		else
		{
			if (op1.token == tcIdentifier) { Emit2(BRI, 0, 0, 6) }
			Emit1(CMP, result->GetReg(), op2.reg->GetReg(), 0);							//R0 pode ser utilizado para compara��o  da condi��o com 0, porque R0 = 0.
			Emit2(MOVI, branch_reg->GetReg(), 0, 2);
			Emit1(BNE, rd_BNE, result->GetReg(), branch_reg->GetReg());
			Emit2(MOVI, result->GetReg(), 0, 0);
			Emit2(BRI, rd_BRI, 0, 1);
			Emit2(MOVI, result->GetReg(), 0, 1);
		}

		regFile.FreeRegister(branch_reg->GetReg());
		genStack.Push(tcIdentifier, result);

	}
	return NULL;
}

TType *TCodeGenerator::EmitSimpleExpression(void)
{
	TTokenCode  op;                // operator
	TTokenCode  signal = tcDummy;            // sinal
	//Necess�rio criar s�mbolo gen�rico global para fazer com que se possa eliminar todos os s�mbolos temporarios.
	/*TSymtabNode *temp_symbol = symtabStack.SearchAll("temp_reg");

	if (!temp_symbol) {
		temp_symbol = globalSymtab.Enter("temp_reg");
	}
	TRegister *reg = regFile.GetRegister(temp_symbol);*/
	TRegister *reg = NULL;

	if (token == tcMinus || token == tcPlus){
		signal = token;
		GetToken();
	}
	EmitTerm();	

	//Verificar se o primeiro operando � negativo
	if (signal == tcMinus){
		ExprStackData op1 = genStack.Pop();
		//verifica se � um numero se sim multiplica por -1 senao faz o complemento para 2
		if (op1.token == tcNumber){
			op1.val.integer *= -1;
			genStack.Push(tcNumber, op1.val.integer);//push do resultado
		}
		else{
			reg = regFile.GetRegister(&tempReg);
			//complemento para 2
			Emit2(MOV, reg->GetReg(), op1.reg->GetReg(), 0);
			Emit2(NOT, reg->GetReg(), 0, 0);
			Emit2(ADDI, reg->GetReg(), reg->GetReg(), 1);
			genStack.Push(tcIdentifier, reg);//push do resultado
		}
	}
	
	while (TokenIn(token, tlAddOps))
	{

		op = token;
		GetToken();

		EmitTerm();

		ExprStackData op2 = genStack.Pop();
		ExprStackData op1 = genStack.Pop();
		
		if(!reg)
			reg = regFile.GetRegister(&tempReg);

		switch (op)
		{
		case tcPlus:
			// num + num;
			if (op1.token == tcNumber && op2.token == tcNumber){
				int resul;
				resul = op1.val.integer + op2.val.integer;
				genStack.Push(tcNumber, resul);//push do resultado
			}
			// var + num;
			else if (op1.token != tcNumber && op2.token == tcNumber){
				if (signal == tcMinus){
					Emit2(ADDI, reg->GetReg(), reg->GetReg(), op2.val.integer);
				}
				else
					Emit2(ADDI, reg->GetReg(), op1.reg->GetReg(), op2.val.integer);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// num + var;
			else if (op1.token == tcNumber && op2.token != tcNumber){
				Emit2(ADDI, reg->GetReg(), op2.reg->GetReg(), op1.val.integer);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// var + var;
			else if (op1.token != tcNumber && op2.token != tcNumber){
				Emit1(ADD, reg->GetReg(), op1.reg->GetReg(), op2.reg->GetReg());
				genStack.Push(tcIdentifier, reg);//push do resultado
			}

			//regFile.FreeRegister(temp_symbol); -> depois de assign apagar todos os temporarios.
			regFile.PrintRegFile(); //DEBUG
			break;

		case tcMinus:
			// num - num;
			if (op1.token == tcNumber && op2.token == tcNumber){
				int resul;
				resul = op1.val.integer - op2.val.integer;
				genStack.Push(tcNumber, resul);//push do resultado
			}
			// var - num;
			else if (op1.token != tcNumber && op2.token == tcNumber){
				if (signal == tcMinus){
					Emit2(SUBI, reg->GetReg(), reg->GetReg(), op2.val.integer);
				}
				else
					Emit2(SUBI, reg->GetReg(), op1.reg->GetReg(), op2.val.integer);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// num - var;
			else if (op1.token == tcNumber && op2.token != tcNumber){
				Emit2(MOVI, reg->GetReg(), 0, op1.val.integer);
				Emit1(SUB, reg->GetReg(), reg->GetReg(), op2.reg->GetReg());
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// var - var;
			else if (op1.token != tcNumber && op2.token != tcNumber){
				Emit1(SUB, reg->GetReg(), op1.reg->GetReg(), op2.reg->GetReg());
				genStack.Push(tcIdentifier, reg);//push do resultado
			}

			//regFile.FreeRegister(temp_symbol); -> depois de assign apagar todos os temporarios.
			regFile.PrintRegFile(); //DEBUG
			break;

		case tcLShift:
			// num << num;
			if (op1.token == tcNumber && op2.token == tcNumber){
				int resul;
				resul = op1.val.integer << op2.val.integer;
				genStack.Push(tcNumber, resul);//push do resultado
			}
			// var << num;
			else if (op1.token != tcNumber && op2.token == tcNumber){
				if (signal == tcMinus){//ja tem o reg alocado para a variavel precisa de outro registo temporario para tcNumber (tempReg)
					TRegister *regTemp = regFile.GetRegister(&tempReg);
					Emit2(MOVI, regTemp->GetReg(), 0, op2.val.integer);
					Emit3(BS, reg->GetReg(), op1.reg->GetReg(), regTemp->GetReg(), 1);
					regFile.FreeRegister(regTemp->GetReg());
				}
				else{
					Emit2(MOVI, reg->GetReg(), 0, op2.val.integer);
					Emit3(BS, reg->GetReg(), op1.reg->GetReg(), reg->GetReg(), 1);
				}
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// num << var;
			else if (op1.token == tcNumber && op2.token != tcNumber){
				Emit2(MOVI, reg->GetReg(), 0, op1.val.integer);
				Emit3(BS, reg->GetReg(), reg->GetReg(), op2.reg->GetReg(), 1);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// var << var;
			else if (op1.token != tcNumber && op2.token != tcNumber){
				Emit3(BS, reg->GetReg(), op1.reg->GetReg(), op2.reg->GetReg(), 1);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}

			//regFile.FreeRegister(temp_symbol); -> depois de assign apagar todos os temporarios.
			regFile.PrintRegFile(); //DEBUG
			break;

		case tcRShift:
			// num >> num;
			if (op1.token == tcNumber && op2.token == tcNumber){
				int resul;
				resul = op1.val.integer >> op2.val.integer;
				genStack.Push(tcNumber, resul);//push do resultado
			}
			// var >> num;
			else if (op1.token != tcNumber && op2.token == tcNumber){
				if (signal == tcMinus){//ja tem o reg alocado para a variavel precisa de outro registo temporario para tcNumber (tempReg)
					TRegister *regTemp = regFile.GetRegister(&tempReg);
					Emit2(MOVI, regTemp->GetReg(), 0, op2.val.integer);
					Emit3(BS, reg->GetReg(), op1.reg->GetReg(), regTemp->GetReg(), 0);
					regFile.FreeRegister(regTemp->GetReg());
				}
				else{
					Emit2(MOVI, reg->GetReg(), 0, op2.val.integer);
					Emit3(BS, reg->GetReg(), op1.reg->GetReg(), reg->GetReg(), 0);
				}
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// num >> var;
			else if (op1.token == tcNumber && op2.token != tcNumber){
				Emit2(MOVI, reg->GetReg(), 0, op1.val.integer);
				Emit3(BS, reg->GetReg(), reg->GetReg(), op2.reg->GetReg(), 0);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// var >> var;
			else if (op1.token != tcNumber && op2.token != tcNumber){
				Emit3(BS, reg->GetReg(), op1.reg->GetReg(), op2.reg->GetReg(), 0);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}

			//regFile.FreeRegister(temp_symbol); -> depois de assign apagar todos os temporarios.
			regFile.PrintRegFile(); //DEBUG
			break;

		case tcXor:
			// num ^ num;
			if (op1.token == tcNumber && op2.token == tcNumber){
				int resul;
				resul = op1.val.integer ^ op2.val.integer;
				genStack.Push(tcNumber, resul);//push do resultado
			}
			// var ^ num;
			else if (op1.token != tcNumber && op2.token == tcNumber){
				Emit2(XORI, reg->GetReg(), op1.reg->GetReg(), op2.val.integer);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// num ^ var;
			else if (op1.token == tcNumber && op2.token != tcNumber){
				Emit2(XORI, reg->GetReg(), op2.reg->GetReg(), op1.val.integer);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// var ^ var;
			else if (op1.token != tcNumber && op2.token != tcNumber){
				Emit1(XOR, reg->GetReg(), op1.reg->GetReg(), op2.reg->GetReg());
				genStack.Push(tcIdentifier, reg);//push do resultado
			}

			//regFile.FreeRegister(temp_symbol); -> depois de assign apagar todos os temporarios.
			regFile.PrintRegFile(); //DEBUG
			break;

		case tcOr:
			// num | num;
			if (op1.token == tcNumber && op2.token == tcNumber){
				int resul;
				resul = op1.val.integer | op2.val.integer;
				genStack.Push(tcNumber, resul);//push do resultado
			}
			// var | num;
			else if (op1.token != tcNumber && op2.token == tcNumber){
				Emit2(ORI, reg->GetReg(), op1.reg->GetReg(), op2.val.integer);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// num | var;
			else if (op1.token == tcNumber && op2.token != tcNumber){
				Emit2(ORI, reg->GetReg(), op2.reg->GetReg(), op1.val.integer);
				genStack.Push(tcIdentifier, reg);//push do resultado
			}
			// var | var;
			else if (op1.token != tcNumber && op2.token != tcNumber){
				Emit1(OR, reg->GetReg(), op1.reg->GetReg(), op2.reg->GetReg());
				genStack.Push(tcIdentifier, reg);//push do resultado
			}

			//regFile.FreeRegister(temp_symbol); -> depois de assign apagar todos os temporarios.
			regFile.PrintRegFile(); //DEBUG
			break;

		default:
			break;
		}
	}
	
	//if(reg) regFile.FreeRegister(reg->GetReg());

	cout << pToken->String() << endl;
	return NULL;
}

TType *TCodeGenerator::EmitTerm(void)
{
	EmitFactor();

	while (TokenIn(token, tlMulOps)){

		TTokenCode op;
		op = token;
		GetToken();

		EmitFactor();

		ExprStackData op2 = genStack.Pop();
		ExprStackData op1 = genStack.Pop();

		TRegister *reg_tmp;
		TRegister *reg_cmp;
		TRegister *reg_jump;
		TRegister *flag_neg;
		TRegister *result;

		TRegister *r_op;
		TRegister *l_op;
		TRegister *reg;

		bool zero = false;

		if (op1.token == tcNumber && op2.token == tcNumber)
			switch (op){
			case tcStar:
				genStack.Push(tcNumber, op1.val.integer * op2.val.integer);
				break;
			case tcSlash:
				genStack.Push(tcNumber, op1.val.integer / op2.val.integer);
				break;
			case tcPercent:
				genStack.Push(tcNumber, op1.val.integer % op2.val.integer);
				break;
			case tcAnd:
				genStack.Push(tcNumber, op1.val.integer & op2.val.integer);
				break;
		}
		else if (op == tcAnd){
			result = regFile.GetRegister(&tempReg);
			if (op1.token != tcNumber && op2.token == tcNumber){
				Emit2(ANDI, result->GetReg(), op1.reg->GetReg(), op2.val.integer);
				genStack.Push(tcIdentifier, result);
			}
			else if (op1.token == tcNumber && op2.token != tcNumber){
				Emit2(ANDI, result->GetReg(), op2.reg->GetReg(), op1.val.integer);
				genStack.Push(tcIdentifier, result);
			}
			else{
				Emit1(AND, result->GetReg(), op1.reg->GetReg(), op2.reg->GetReg());
				genStack.Push(tcIdentifier, result);
			}
		}
		else{

			reg_tmp = regFile.GetRegister(&tempReg);
			reg_cmp = regFile.GetRegister(&tempReg);
			reg_jump = regFile.GetRegister(&tempReg);
			flag_neg = regFile.GetRegister(&tempReg);
			result = regFile.GetRegister(&tempReg);

			if (op1.token != tcNumber && op2.token == tcNumber){
				r_op = op1.reg;
				reg = r_op;
				l_op = reg_tmp;
				if (op2.val.integer != 0){
					Emit1(MOV, result->GetReg(), 0, 0);
					Emit1(MOV, flag_neg->GetReg(), 0, 0);
					Emit1(PUSH, op1.reg->GetReg(), 0, 0);
					zero = false;
					if (op2.val.integer < 0){
						op2.val.integer *= -1;
						Emit1(NOT, flag_neg->GetReg(), 0, 0);
					}
					Emit2(MOVI, l_op->GetReg(), 0, op2.val.integer);
				}
				else zero = true;
			}
			else if (op1.token == tcNumber && op2.token != tcNumber){
				l_op = op2.reg;
				reg = l_op;
				r_op = reg_tmp;
				if (op2.val.integer != 0){
					Emit1(MOV, result->GetReg(), 0, 0);
					Emit1(MOV, flag_neg->GetReg(), 0, 0);
					zero = false;
					Emit1(PUSH, op2.reg->GetReg(), 0, 0);
					if (op1.val.integer < 0){
						op1.val.integer *= -1;
						Emit1(NOT, flag_neg->GetReg(), 0, 0);
					}
					Emit2(MOVI, r_op->GetReg(), 0, op1.val.integer);
				}
				else zero = true;
			}
			else{
				zero = false;
				r_op = op1.reg;
				reg = r_op;
				l_op = op2.reg;
				Emit1(MOV, result->GetReg(), 0, 0);
				Emit1(MOV, flag_neg->GetReg(), 0, 0);
				Emit1(PUSH, r_op->GetReg(), 0, 0);
				Emit1(PUSH, l_op->GetReg(), 0, 0);

				Emit1(CMP, reg_cmp->GetReg(), l_op->GetReg(), 0);
				Emit2(MOVI, reg_jump->GetReg(), 0, 25);
				Emit1(BEQ, 0, reg_cmp->GetReg(), reg_jump->GetReg());
				Emit2(MOVI, reg_jump->GetReg(), 0, 4);
				Emit1(BGT, rd_BGT, reg_cmp->GetReg(), reg_jump->GetReg());
				Emit2(SUBI, l_op->GetReg(), l_op->GetReg(), 1);
				Emit1(NOT, l_op->GetReg(), 0, 0);
				Emit1(NOT, flag_neg->GetReg(), 0, 0);
			}

			if (!zero){
				Emit1(CMP, reg_cmp->GetReg(), reg->GetReg(), 0);
				Emit2(MOVI, reg_jump->GetReg(), 0, 17);
				Emit1(BEQ, 0, reg_cmp->GetReg(), reg_jump->GetReg());
				Emit2(MOVI, reg_jump->GetReg(), 0, 4);
				Emit1(BGT, rd_BGT, reg_cmp->GetReg(), reg_jump->GetReg());
				Emit2(SUBI, reg->GetReg(), reg->GetReg(), 1);
				Emit1(NOT, reg->GetReg(), 0, 0);
				Emit1(NOT, flag_neg->GetReg(), 0, 0);

				switch(op){
				case tcStar:
					Emit1(CMP, reg_cmp->GetReg(), l_op->GetReg(), 0);
					Emit2(MOVI, reg_jump->GetReg(), 0, 4);
					Emit1(BEQ, rd_BEQ, reg_cmp->GetReg(), reg_jump->GetReg());
					Emit1(ADD, result->GetReg(), result->GetReg(), r_op->GetReg());
					Emit2(SUBI, l_op->GetReg(), l_op->GetReg(), 1);
					Emit2(BRI, 16, 0, 5);
					Emit1(CMP, reg_cmp->GetReg(), flag_neg->GetReg(), 0);
					Emit2(MOVI, reg_jump->GetReg(), 0, 3);
					Emit1(BEQ, 0, reg_cmp->GetReg(), reg_jump->GetReg());
					Emit1(NOT, result->GetReg(), 0, 0);
					Emit2(ADDI, result->GetReg(), result->GetReg(), 1);
					break;

				case tcSlash:
					Emit1(CMP, reg_cmp->GetReg(), r_op->GetReg(), l_op->GetReg());
					Emit2(MOVI, reg_jump->GetReg(), 0, 4);
					Emit1(BLT, rd_BLT, reg_cmp->GetReg(), reg_jump->GetReg());
					Emit1(SUB, r_op->GetReg(), r_op->GetReg(), l_op->GetReg());
					Emit2(ADDI, result->GetReg(), result->GetReg(), 1);
					Emit2(BRI, 16, 0, 5);
					Emit1(CMP, reg_cmp->GetReg(), flag_neg->GetReg(), 0);
					Emit2(MOVI, reg_jump->GetReg(), 0, 3);
					Emit1(BEQ, 0, reg_cmp->GetReg(), reg_jump->GetReg());
					Emit1(NOT, result->GetReg(), 0, 0);
					Emit2(ADDI, result->GetReg(), result->GetReg(), 1);
					break;

				case tcPercent:
					Emit1(CMP, reg_cmp->GetReg(), r_op->GetReg(), l_op->GetReg());
					Emit2(MOVI, reg_jump->GetReg(), 0, 3);
					Emit1(BLT, rd_BLT, reg_cmp->GetReg(), reg_jump->GetReg());
					Emit1(SUB, r_op->GetReg(), r_op->GetReg(), l_op->GetReg());
					Emit2(BRI, 16, 0, 4);
					Emit1(MOV, result->GetReg(), r_op->GetReg(), 0);
					Emit1(CMP, reg_cmp->GetReg(), flag_neg->GetReg(), 0);
					Emit2(MOVI, reg_jump->GetReg(), 0, 3);
					Emit1(BEQ, 0, reg_cmp->GetReg(), reg_jump->GetReg());
					Emit1(NOT, result->GetReg(), 0, 0);
					Emit2(ADDI, result->GetReg(), result->GetReg(), 1);
					break;
				case tcAnd:
					Emit1(AND, result->GetReg(), r_op->GetReg(), l_op->GetReg());
					break;
				}

				if (op1.token != tcNumber && op2.token == tcNumber){
					Emit1(POP, r_op->GetReg(), 0, 0);
				}
				else if (op1.token == tcNumber && op2.token != tcNumber){
					Emit1(POP, l_op->GetReg(), 0, 0);
				}
				else{
					Emit1(POP, l_op->GetReg(), 0, 0);
					Emit1(POP, r_op->GetReg(), 0, 0);
				}
				genStack.Push(tcIdentifier, result);
			}
			else genStack.Push(tcNumber, op1.val.integer & op2.val.integer);

			regFile.FreeRegister(reg_tmp->GetReg());
			regFile.FreeRegister(reg_cmp->GetReg());
			regFile.FreeRegister(reg_jump->GetReg());
			regFile.FreeRegister(flag_neg->GetReg());
		}
	}

	return NULL;



}

TType *TCodeGenerator::EmitFactor(void)
{

/*** Registos tempor�rios ***/
	TRegister *result = NULL;		//Registo auxiliar para retorno
	TRegister *branch_reg = NULL;	//Registo auxiliar para os branches
/********************************************************/

	TSymtabNode *var = NULL;
	ExprStackData oper;

	switch(token)
	{
	case tcIdentifier:
		var = symtabStack.SearchAll(pToken->String());
		if (var->defn.how == dcFunction)
		{
			EmitFunctionCall(var);
			genStack.Push(tcReturn, regFile.GetReturnRegister());
		}
		else
		{
			EmitVariable(var);
			GetToken();
			if (token == tcPlusPlus || token == tcMinusMinus) {
				postStack.Push(token, var);
				GetToken();
			}
		}
		break;

	case tcNumber:
		var = symtabStack.SearchAll(pToken->String());
		EmitConstant(var);
		GetToken();
		break;

	case tcLParen:
		GetToken();
		EmitExpression();
		GetToken();
		break;

	case tcPlusPlus:
		GetToken();
		if(token == tcIdentifier) {
			var = symtabStack.SearchAll(pToken->String());
			//ve se a variavel ja esta nos registos
			TRegister *reg=regFile.FindRegister(var);
			//adiciona se nao estiver	
			if(!reg){reg=regFile.GetRegister(var); }
			Emit2(ADDI,reg->GetReg(),reg->GetReg(),1);
			EmitVariable(var);
			GetToken();
		}
		break;

	case tcMinusMinus:
		GetToken();
		if(token == tcIdentifier) {
			var = symtabStack.SearchAll(pToken->String());
			//ve se a variavel ja esta nos registos
			TRegister *reg=regFile.FindRegister(var);
			//adiciona se nao estiver	
			if(!reg){reg=regFile.GetRegister(var); }
			Emit2(SUBI,reg->GetReg(),reg->GetReg(),1);
			EmitVariable(var);
			GetToken();
		}
		break;


	case tcStar:
		GetToken();
		if (token == tcIdentifier) {
			result = regFile.GetRegister(&tempReg);
			var = symtabStack.SearchAll(pToken->String());

			// determinar a base para o offset consuante a vari�vel � global ou local
			int base = var->defn.data.globalData ? GLOBAL_VAR_REG : FRAME_BASE_REG;
			Emit2(LWI, result->GetReg(), base, var->defn.data.offset);

			genStack.Push(tcNumber, result);
		}
		break;

	case tcAnd:				//passar endere�o de uma variavel
		GetToken();
		if (token == tcIdentifier) {
			result = regFile.GetRegister(&tempReg);
			var = symtabStack.SearchAll(pToken->String());

			// determinar a base para o offset consuante a vari�vel � global ou local
			int base = var->defn.data.globalData ? GLOBAL_VAR_REG : FRAME_BASE_REG;
			Emit2(ADDI, result->GetReg(), base, var->defn.data.offset);

			genStack.Push(tcNumber, result);
		}
		break;

	
	case tcNot:
		GetToken();
		EmitFactor();

		oper = genStack.Pop();
		if(oper.token != tcNumber) {
			result = regFile.GetRegister(&tempReg);
			branch_reg = regFile.GetRegister(&branchReg);

			Emit1(CMP, result->GetReg(), oper.reg->GetReg(), 0);
			Emit2(MOVI, branch_reg->GetReg(), 0, 2);
			Emit1(BEQ, rd_BEQ, result->GetReg(), branch_reg->GetReg());
			Emit2(MOVI, result->GetReg(), 0, 0);
			Emit2(BRI, rd_BRI, 0, 1);
			Emit2(MOVI, result->GetReg(), 0, 1);

			regFile.FreeRegister(branch_reg->GetReg());

			genStack.Push(tcIdentifier, result);
		}
		else
			genStack.Push(tcNumber, !oper.val.integer);
		break;
	
	case tcTil:
		GetToken();
		EmitFactor();
		
		oper=genStack.Pop();
		if(oper.token != tcNumber) {
			result = regFile.GetRegister(&tempReg);
		
			Emit1(MOV, result->GetReg(), oper.reg->GetReg(), 0);//Para n�o afectar o valor do registo, � copiado o valor para um registo auxiliar
			Emit1(NOT, result->GetReg(), 0, 0);
			
			genStack.Push(tcIdentifier, result);
		}
		else
			genStack.Push(tcNumber, ~oper.val.integer);
		break;
	}

	return NULL;
}


TType *TCodeGenerator::EmitVariable(TSymtabNode *pId)//� preciso ser revisto
{
	//ve se a variavel ja esta nos registos
	TRegister *reg=regFile.FindRegister(pId);
	//adiciona se nao estiver	
	if(!reg){reg=regFile.GetRegister(pId); }			

	genStack.Push(token,reg);
	
	return NULL;
}

TType *TCodeGenerator::EmitConstant(TSymtabNode *pId)
{	
	int val;
	
	sscanf(pId->String(), "%d", &val);		//maneira de retirar o valor do n�, uma vez que o pId->defn.constant.value.integer tinah lixo
	//genStack.Push(token,pId->defn.constant.value.integer); deu merda
	genStack.Push(token, val);

	return NULL;
}






TType *TCodeGenerator::EmitMultiplication(ExprStackData op1, ExprStackData op2)
{
	

			return NULL;
}


TType *TCodeGenerator::EmitDivision(ExprStackData op1, ExprStackData op2)
{
	

				return NULL;
}









