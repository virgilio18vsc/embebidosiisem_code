// ittool.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//#include "itest.h"

using namespace std;


#define _TMP_FILE_SOURCE_LIST_  "source_list.ittool"


int main(int argc, char **argv)
{
	cout << "ittool v.0.2\n" << endl;

	/*
	if(argc < 2)
	{
		cerr<<"Usage: " << argv[0] << " [-w] -dTESTS_DIR | -fTEST_FILE"<<endl;
		system("pause");
		return 1;
	}
	*/

	//std::string path;
	
	string line;
	string name;
	stringstream cmd;
	stringstream path_model;
	stringstream path_compiler;
	stringstream path_source;

	std::string sceneries_folder = ".\\ittool_sceneries";
	std::string complier_path = "";

	bool is_folder = false;

	bool watcher = false;
	bool pause = true;

	

	// cfg parse
	//
	std::string key;
	std::string value;
	std::size_t found;
	ifstream cfg_file (".\\ittool.cfg");
	if (cfg_file.is_open())
	{
		while ( getline (cfg_file,line) )
		{
			found = line.find_first_of("=");
			key = string(line.begin() , line.begin()+found);
			value = string(line.begin()+found+1 , line.end());
			//cout << found << " " << key << " = " << value << endl;
			if(key == "compiler_path"){
				complier_path = value;
			}
			else if(key == "sceneries_folder"){
				sceneries_folder = value;
			}
			else if(key == "watcher" && value == "true"){
				watcher = value == "true" ? true : false;
			}
			else if(key == "pause"){
				pause = value == "true" ? true : false;
			}
		}

	}

	//


	// command params parse
	//
	for(int i=1; i<argc; i++){
		if(argv[i][0]=='-' && argv[i][1]=='w'){
			watcher = true;
		}
		else if(argv[i][0]=='-' && argv[i][1]=='s'){
			sceneries_folder = argv[i];
			sceneries_folder = string(sceneries_folder.begin()+2, sceneries_folder.end());
		}
		else if(argv[i][0]=='-' && argv[i][1]=='c'){
			complier_path = argv[i];
			complier_path = string(complier_path.begin()+2, complier_path.end());
		}
		else if(argv[i][0]=='-' && argv[i][1]=='n' && argv[i][2]=='p'){
			pause = false;
		}

		else{
			// help
		}


	}
	//

	if(watcher) cout << "Watcher folder\"" << sceneries_folder << "\". {not implement}" << endl;
	//if(is_folder) cout << "Integration Testing, start tests in folder \"" << path << "\"." << endl;
	//else cout << "Integration Testing, test file \"" << path << "\"." << endl;
	
	ITest * it = new ITest();

	if(complier_path==""){
		cout << "The compiler path should be configured, example: 'ittool -cPATH_COMPILER'." << endl;
		return 2;
	} else {
		cout << "The compiler path is '" << complier_path << "'." << endl;
	}
	

		 
	
	cout << "Test in folder '" << sceneries_folder << "'..." << endl;
	
	cmd.str("");
	cmd << "dir " << sceneries_folder << "\\*.c /b > " _TMP_FILE_SOURCE_LIST_;

	system(cmd.str().c_str());



	//



  


  //file_path << "";

  //cout << file_path.str();

  

  //cout << file_path.str();


	

	cout << endl << endl;

	stringstream file_path;
	ifstream myfile (".\\"  _TMP_FILE_SOURCE_LIST_);
	if (myfile.is_open())
	{
		while ( getline (myfile,line) )
		{
				//cout << "SIZE: " << line.length() << "\n\n";
				file_path.str("");
				cmd.str("");
				path_model.str("");
				path_compiler.str("");
				path_source.str("");;

				int cmd_error = 0;

				file_path << sceneries_folder << "\\" << line;
				
				name = file_path.str();
				name = string(name.begin(), name.end()-2);

				path_source << file_path.str();
				path_compiler << name << ".asm.T";
				path_model << name << ".asm.M";


				cmd << complier_path << " \"" << 
					path_source.str() << "\" \"" << path_compiler.str() << "\" > NUL" ; 
				

				//cout << "\n[COMPILER] => " << endl;
				//cout << "\t" << "Source: " << path_source.str()		<< endl;
				//cout << "\t" << "Model: "  << path_model.str()		<< endl;
				//cout << "\t" << "Test: "   << path_compiler.str()	<< endl << endl;

				// compiler
				cmd_error =  system(cmd.str().c_str());
				if(cmd_error)
				{
					cout << "[COMPILER ERROR ID:" << cmd_error << "] " << cmd.str() << endl << endl;
				} else
				{
					// exec test
					it->test_file(path_model.str(), path_compiler.str());

					
				}
				
				
				//file_path.str(string(file_path.str().begin(), file_path.str().end()-2));
				//cout << file_path.str();
		}
		myfile.close();
		// result tests
		it->test_result();
	}

	else cout << "Unable to open file";

	//




	system("del " _TMP_FILE_SOURCE_LIST_);
	if(pause) system("pause");
	return 0;
}

