========================================================================
    ittool Project Overview
========================================================================

A ferramenta ittool (Integration Test Tool) foi desenvolvida para garantir a consistência do software do compilador, uma vez que o software do compilador não tem Testes Unitários se torna essencial promover alguma forma de garantir que o desenvolvimento dos vários grupos não quebrem o que já esta desenvolvido.

O ittool funciona de forma a automatizar o processo de testes executando as tarefas de compilação dos códigos fontes e posteriormente a sua comparação com os modelos especificados no design.

De forma a agilizar ainda mais os teste o ittool foi contruindo tendo em mente o conceito de "Convention over configuration - CoC", neste sentido o aplicativo tem algumas convenções, que são elas:
    * os ficheiros a serem compilados tem que ter a extensão ".c";
    * os ficheiros gerados pelo compilador através da ferramenta ittools, possuem a extensão ".asm.T";
    * os ficheiros modelos que são usados para a comparação possuem a extensão ".asm.T";
    * a pasta usada por defeito para guardar os testes é ".\ittool_sceneries";
    * é usado o ficheiro de configuração localizado em ".\ittool.cfg".
    
A ferramenta suporta via paramentros as seguintes configurações:
    "-s SCENERIES_FOLDER" - directório onde estão os testes;
    "-c COMPLIER_PATH" - caminho para o compilador a ser usado;
    "-w" - monitora alterações/adições no directório de teste ou no compilador e executa os teste automaticamente [POR IMPLEMENTAR];
    "-np" - elimina a pause ao fim da execução da ferramenta, sendo útil esta opção para encadeamento de comando.
    
Através do ficheiro de configuração ".\ittool.cfg" podem ser definidas as seguintes variáveis:
    compiler_path=COMPLIER_PATH
    sceneries_folder=SCENERIES_FOLDER
    watcher=true|false
    pause=true|false
     
Futuramente se se justificar podemos adicionar suporte  para regexp nos ficheiros modelos, tornando os testes mais dinâmicos.

Exemplo de Utilização:

/////////////////////////////////////////////////////////////////////////////
> itool
/////////////////////////////////////////////////////////////////////////////

ittool v.0.2

Test in folder '.\ittool_sceneries\'...



[Test Number 3 Fail] => 
	Model: .\ittool_sceneries\test 03.scn.asm.M
	Test : .\ittool_sceneries\test 03.scn.asm.T

	Line number: 4
		M. mem[0]=8'b01001000;
		T. mem[0]=8'b01101000;

	Line number: 6
		M. mem[0]=8'b01010000;
		T. mem[0]=8'b01110000;

	=> Total Lines: M[12] = T[12] 
	=> Test Fail in 2 line(s).

[Test Number 4 Fail] => 
	Model: .\ittool_sceneries\test 04.scn.asm.M
	Test : .\ittool_sceneries\test 04.scn.asm.T

	=> Total Lines: M[1] < T[12] 

		Size of M file not match with T file.

Sceneries Results
=================

	Total Sceneries Fail: 2
	Total Sceneries Test: 4

/////////////////////////////////////////////////////////////////////////////




Após os ficheiros T serem gerados pelo compilador o directório '.\ittool_sceneries' fica assim.

/////////////////////////////////////////////////////////////////////////////
> dir ittool_sceneries 
/////////////////////////////////////////////////////////////////////////////

 Directory of W:\ittool_sceneries

 03/23/2014  23:02    <DIR>          .
 03/23/2014  23:03    <DIR>          ..
 03/22/2014  22:54               231 test 01.scn.asm.M
 03/23/2014  23:02               231 test 01.scn.asm.T
 03/17/2014  12:54               284 test 01.scn.c
 03/22/2014  22:54               231 test 02.scn.asm.M
 03/23/2014  23:02               231 test 02.scn.asm.T
 03/17/2014  12:54               284 test 02.scn.c
 03/23/2014  21:15               231 test 03.scn.asm.M
 03/23/2014  23:02               231 test 03.scn.asm.T
 03/17/2014  12:54               284 test 03.scn.c
 03/23/2014  20:34                19 test 04.scn.asm.M
 03/23/2014  23:02               231 test 04.scn.asm.T
 03/17/2014  12:54               284 test 04.scn.c
               12 File(s)          2,772 bytes
                2 Dir(s)  11,314,364,416 bytes free
               
/////////////////////////////////////////////////////////////////////////////


