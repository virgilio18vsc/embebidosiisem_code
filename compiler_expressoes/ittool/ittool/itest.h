
#ifndef __ITEST_H__
#define __ITEST_H__

#include "stdafx.h"

class ITest{
private:
	std::string folder_path;
	std::string file_path;

	int total_tests;
	int total_tests_error;
public:
	ITest();
	~ITest(void);

	bool test_file(std::string path_model, std::string path_compiler);
	bool test_folder(void);

	void test_result(void);


};

#endif