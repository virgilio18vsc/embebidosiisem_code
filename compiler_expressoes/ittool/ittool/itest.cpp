#include "stdafx.h"



ITest::ITest(){

	total_tests = 0;
	total_tests_error = 0;

	//if(is_folder){
	//	folder_path = path;
	//	test_folder();
	//} else {
	//	file_path = path;
	//	//test_file();
	//}
		
	//std::cout << "ITest construct " << folder_path << std::endl;
}

ITest::~ITest(void){

}

bool ITest::test_file(std::string path_model, std::string path_compiler){
	//std::cout << path_model << " - " << path_compiler << std::endl;

	std::ifstream model_file (path_model);
	std::string model_line;

	std::ifstream compiler_file (path_compiler);
	std::string compiler_line;

	std::stringstream errors;

	int model_total_line = 0;
	int compiler_total_line = 0;
	int current_line = 0;
	int error_counter = 0;
	bool show_header = true;

	if (model_file.is_open())
	{
		total_tests++;

		while (!model_file.eof() || !compiler_file.eof())
		{
			if(!model_file.eof()){
				model_total_line++;
				getline (model_file, model_line);
			}

			if(!compiler_file.eof()){
				compiler_total_line++;
				getline (compiler_file, compiler_line);
			}

			current_line++;

			if( (model_line.compare(compiler_line) != 0) && !model_file.eof() && !compiler_file.eof() )
			{

				errors << "\tLine number: " << current_line << std::endl;
				errors << "\t\tM. " << model_line << std::endl;
				errors << "\t\tT. " << compiler_line << std::endl << std::endl;
				error_counter++;
		
			}
		}

		if(model_total_line != compiler_total_line) error_counter++;
			
			//std::cout << "\t=> Size of M file not match with T file." << std::endl;
		

		if(error_counter > 0){

			
			std::cout << "\n[Test Number " << total_tests << " Fail] => " << std::endl;
			std::cout << "\t" << "Model: "  << path_model		<< std::endl;
			std::cout << "\t" << "Test : "   << path_compiler	<< std::endl << std::endl;

			std::cout << errors.str();

			std::cout << "\t=> Total Lines: M[" << model_total_line << "] ";
			if(model_total_line > compiler_total_line){
				std::cout << ">";
			}
			else if(model_total_line < compiler_total_line){
				std::cout << "<";
			} else {
				std::cout << "=";
			}
			std::cout << " T[" << compiler_total_line << "] " << std::endl;
			if(model_total_line != compiler_total_line)
				std::cout << "\n\t\t" << "Size of M file not match with T file." << std::endl;
			else
				std::cout << "\t=> Test Fail in " << error_counter << " line(s)."  << std::endl;
			
			total_tests_error++;
		}
	}

	model_file.close();
	compiler_file.close();

	return true;
}


void ITest::test_result(void){
	std::cout << std::endl << 
		"Sceneries Results" << std::endl << 
		"=================" << std::endl << std::endl;

	std::cout << "\tTotal Sceneries Fail: " << total_tests_error << std::endl;
	std::cout << "\tTotal Sceneries Test: " << total_tests << std::endl << std::endl;

	

	
}


bool ITest::test_folder(void){
	std::cout << file_path << std::endl;
	return true;
}