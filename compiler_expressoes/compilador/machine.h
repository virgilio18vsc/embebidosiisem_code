#ifndef __MACHINE_H__
#define __MACHINE_H__

#define MEM_CODE_SIZE	1024
#define REG_NUMBER		32



//ABI
#define GLOBAL_VAR_REG		14
#define FUNCTION_RETURN_REG	15
#define FRAME_BASE_REG		16
#define STACK_POINTER		31


//variaveis externas pa geracao de codigo
extern unsigned long int CodeMem[];
extern int PC;


//**************************************************************
//		Emit1			Emit opcode,rd,ra,rb
#define Emit1(opcode,rd,ra,rb)					\
{												\
	CodeMem[PC] |= ((opcode & 0x3F)<<26);		\
	CodeMem[PC] |= ((rd & 0x1F)<<21);			\
	CodeMem[PC] |= ((ra & 0x1F)<<16);			\
	CodeMem[PC] |= ((rb & 0x1F)<<11);			\
	PC++;										\
}

//**************************************************************
//		Emit2			Emit opcode,rd,ra,imm
#define Emit2(opcode,rd,ra,imm)					\
{												\
	CodeMem[PC] |= ((opcode & 0x3F)<<26);		\
	CodeMem[PC] |= ((rd & 0x1F)<<21);			\
	CodeMem[PC] |= ((ra & 0x1F)<<16);			\
	CodeMem[PC] |= ((imm & 0xFFFF)<<0);			\
	PC++;										\
}

//**************************************************************
//		Emit3			Emit opcode,rd,ra,rb,bit
//	usar este para os logical shift
#define Emit3(opcode,rd,ra,rb,bit)				\
{												\
	CodeMem[PC] |= ((opcode & 0x3F)<<26);		\
	CodeMem[PC] |= ((rd & 0x1F)<<21);			\
	CodeMem[PC] |= ((ra & 0x1F)<<16);			\
	CodeMem[PC] |= ((rb & 0x1F)<<11);			\
	CodeMem[PC] |= ((bit & 0x01)<<10);			\
	PC++;										\
}
//**************************************************************
//		Instrucoes

//ALU
const short ADD=1;
const short ADDI=8;
const short SUB=2;
const short SUBI=9;
const short OR=32;
const short ORI=40;
const short AND=33;
const short ANDI=41;
const short XOR=34;
const short XORI=42;
const short NOT=35;
const short CMP=5;

const short BS=17;	
const short ShiftLeft=1;
const short ShiftRight=0;

//SALTOS
const short BR=38;
const short BRI=46;
const short BRA=38;				//DIFERENCIA O RA MAS TEM DE SER CONFIRADO
const short BRAI=46;
const short BEQ=39;
const short BEQI=47;
const short BNE=39;				//DIFERENCIA RD MAS TEM DE SER CONFIRMADO
const short BNEI=47;

const short BGE=39;
const short BGEI=47;			//DIFERENCIA RD
const short BGT=39;
const short BGTI=47;
const short BLE=39;
const short BLEI=47;
const short BLT=39;
const short BLTI=47;

const short CALL=20;


//DATA TRANSFER
const short LW=50;
const short LB=48;
const short LWI=58;
const short SW=54;
const short SB=52;
const short SWI=62;
const short MOV=36;
const short MOVI=43;
const short IMM=44;

const short RET=21;
const short RETI=22;
const short PUSH=18;
const short POP=19;

//OTHER
const short NOP=0;

#endif