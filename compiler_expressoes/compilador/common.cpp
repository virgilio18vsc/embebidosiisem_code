#include "stdafx.h"

int currentNestingLevel=0;
int currentLineNumber=0;

TSymtab globalSymtab;						//tabela de simbolos global
int cntSymtabs=0;
TSymtab *pSymtabList=NULL;
TSymtab **vpSymtabs;

TIcode icode;




//--------------------------------------------------------------
//  Token lists
//--------------------------------------------------------------

//--Tokens that can start a declaration.
extern const TTokenCode tlDeclarationStart[] = {
	tcConst, tcInt, tcFloat, tcDouble, tcChar, tcVolatile, tcLong, tcUnsigned, tcStruct, tcEnum, tcTypedef, tcVoid, tcDummy
};

//--Tokens that can follow a declaration.
extern const TTokenCode tlDeclarationFollow[] = {
	tcIdentifier, tcStruct, tcInt, tcFloat, tcDouble, tcChar, tcLong, tcUnsigned, tcDummy
};

//--Tokens that can start an identifier or field.
extern const TTokenCode tlIdentifierStart[] = {
    tcIdentifier, tcDummy
};

//--Tokens that can follow an identifier or field.
extern const TTokenCode tlIdentifierFollow[] = {
	tcComma, tcIdentifier, tcPeriod, tcSemicolon, tcDummy
};

//--Tokens that can follow an identifier or field sublist.
extern const TTokenCode tlSublistFollow[] = {
    tcColon, tcDummy
};

//--Tokens that can follow a field declaration.
extern const TTokenCode tlFieldDeclFollow[] = {
	tcSemicolon, tcIdentifier, tcColon, tcDummy
};

//--Tokens that can start an enumeration constant.
extern const TTokenCode tlEnumConstStart[] = {
	tcEnum, tcDummy
};

//--Tokens that can follow an enumeration constant.
extern const TTokenCode tlEnumConstFollow[] = {
    tcComma, tcIdentifier, tcRParen, tcSemicolon, tcDummy
};

//--Tokens that can follow a subrange limit.
extern const TTokenCode tlSubrangeLimitFollow[] = {
    tcIdentifier, tcPlus, tcMinus, tcString,
    tcRBracket, tcComma, tcSemicolon, tcDummy
};

//--Tokens that can start an index type.
extern const TTokenCode tlIndexStart[] = {
    tcIdentifier, tcNumber, tcString, tcLParen, tcPlus, tcMinus,
    tcDummy
};

//--Tokens that can follow an index type.
extern const TTokenCode tlIndexFollow[] = {
    tcComma, tcRBracket, tcSemicolon, tcDummy
};

//--Tokens that can follow the index type list.
extern const TTokenCode tlIndexListFollow[] = {
    tcIdentifier, tcLParen,
    tcPlus, tcMinus, tcNumber, tcString, tcSemicolon, tcDummy
};

//--Tokens that can start a subscript or field.
extern const TTokenCode tlSubscriptOrFieldStart[] = {
    tcLBracket, tcPeriod, tcDummy
};






//***********************************************************************************************************************************
//--Tokens that can start a statement.
extern const TTokenCode tlStatementStart[] = {
	tcCase, tcFor, tcIf, tcWhile, tcDo, tcIdentifier, tcStar,tcLBracket, tcSwitch, tcReturn, tcStar,
    tcDummy
};

//--Tokens that can follow a statement.
extern const TTokenCode tlStatementFollow[] = {
    tcSemicolon, tcElse, tcEq, tcDummy
};

//--Tokens that can start a CASE label.
extern const TTokenCode tlCaseLabelStart[] = {
    tcIdentifier, tcNumber, tcPlus, tcMinus, tcString, tcDummy
};

//--Tokens that can start an expression.
extern const TTokenCode tlExpressionStart[] = {
    tcPlus, tcMinus, tcIdentifier, tcNumber, tcString,
    tcNot, tcLParen, tcDummy
};

//--Tokens that can follow an expression.
extern const TTokenCode tlExpressionFollow[] = {
    tcComma, tcRParen, tcRBracket,
    tcDo, tcDummy
};

//--Relational operators.
extern const TTokenCode tlRelOps[] = {
	tcEqual, tcNe, tcLt, tcGt, tcLe, tcGe, tcDummy
};

//Logical operators && e ||
extern const TTokenCode tlLogicOps[] = {
	tcLogicAnd, tcLogicOr,tcDummy
};

//--Unary + and - operators.
extern const TTokenCode tlUnaryOps[] = {
    tcPlus, tcMinus, tcAnd, tcOr, tcNot, tcXor, tcDummy
};

//--Additive operators.
extern const TTokenCode tlAddOps[] = {
	tcPlus, tcMinus, tcOr, tcLShift, tcRShift, tcXor, tcDummy
};

//--Multiplicative operators.
extern const TTokenCode tlMulOps[] = {
    tcStar, tcSlash,tcPercent, tcAnd, tcDummy
};

//--Tokens that can end a program.
extern const TTokenCode tlProgramEnd[] = {
	tcEndOfFile, tcDummy
};

//--Individual tokens.
extern const TTokenCode tlEq[]         = {tcEq,           tcDummy};
extern const TTokenCode tlLBracket[]   = {tcLBracket,	  tcDummy};
extern const TTokenCode tlRBracket[]   = {tcRBracket,	  tcDummy};
extern const TTokenCode tlEqual[]	   = {tcEq,			  tcDummy};
extern const TTokenCode tlDO[]         = {tcDo,           tcDummy};
extern const TTokenCode tlColon[]      = {tcColon,        tcDummy};

//--------------------------------------------------------------
//  TokenIn     Check if a token code is in the token list.
//
//      tc    : token code
//      pList : ptr to tcDummy-terminated token list
//
//  Return:  true if in list, false if not or empty list
//--------------------------------------------------------------

int TokenIn(TTokenCode tc, const TTokenCode *pList)
{
    const TTokenCode *pCode;   // ptr to token code in list

    if (!pList) return false;  // empty list

    for (pCode = pList; *pCode; ++pCode) {
	if (*pCode == tc) return true;  // in list
    }

    return false;  // not in list
}



