#include "stdafx.h"


int execFlag = true;	// true for excutor back end,
			//   false for code generator back end

TDataType GetType(void);
extern TType *pStructs[1024];
//--------------------------------------------------------------
//  ParseDeclarations   Parse any constant definitions, type
//                      definitions, and variable declarations,
//                      in that order.
//--------------------------------------------------------------

void TParser::ParseDeclarations(TSymtabNode *pRoutineId)
{
    if (token == tcConst) 
	{
		GetToken();
		ParseConstantDefinitions(pRoutineId);
    }

	else if(token == tcEnum)
	{
		GetToken();

	}

	else if (TokenIn(token,tlDeclarationStart)) 
	{
		ParseVariableDeclarations(pRoutineId);
    }
}

//              **************************
//              *                        *
//              *  Constant Definitions  *
//              *                        *
//              **************************

//--------------------------------------------------------------
//  ParseConstDefinitions   Parse a list of constant definitions
//                          separated by semicolons:
//
//                              <id> = <constant>
//
//      pRoutineId : ptr to symbol table node of program,
//                   procedure, or function identifier
//--------------------------------------------------------------

void TParser::ParseConstantDefinitions(TSymtabNode *pRoutineId)
{
	//guarda o tipo
	TDataType type;
	switch (token)
	{
	case tcInt:
		type=tyInteger;
		break;

	case tcFloat:
	case tcDouble:
		type=tyReal;
		break;

	case tcChar:
		type=tyCharacter;
		break;

	case tcUnsigned:
		GetToken(); 
		switch(token)
		{
		case tcChar:
			type=tyUnsignedChar;
			break;

		case tcInt:
			type=tyUnsignedInteger;
			break;

		case tcLong:
			GetToken();
			if(token == tcInt){type=tyUnsignedLongInteger;}
			else
			{
				Error(errUnexpectedToken);
			}
			break;

		case tcShort:
			GetToken();
			if(token == tcInt){type=tyUnsignedShortInteger;}
			else
			{
				Error(errUnexpectedToken);
			}
			break;
		}
		break;

	case tcShort:
		GetToken();
		if(token == tcInt){type=tyShortInteger;}
		else
		{
			Error(errUnexpectedToken);
		}
		break;

	case tcLong:
		GetToken();
		switch(token)
		{
		case tcInt:
			type=tyLongInteger;
			break;

		case tcLong:
			GetToken();
			if(token == tcInt){type=tyLongLongInteger;}
			else
			{
				Error(errUnexpectedToken);
			}
			break;
		}
		break;

	case tcVoid:
		type=tyVoid;
		break;

	default:
		type=tyDummy;
		break;
	}

	TSymtabNode *pLastId = NULL;  // ptr to last constant id node

	do
	{
		GetToken();
		TSymtabNode *pConstId = EnterNewLocal(pToken->String());

	//--Link the routine's local constant id nodes together.
	if (!pRoutineId->defn.routine.locals.pConstantIds) {
	    pRoutineId->defn.routine.locals.pConstantIds = pConstId;
	}
	else {
		pLastId=pRoutineId->defn.routine.locals.pConstantIds;
	    pLastId->next = pConstId;
	}
	pLastId = pConstId;

	//-- =
	GetToken();
	CondGetToken(tcEq, errMissingEqual);

	//atribuir o tipo
		switch(type)
		{
//*******************************************************************************************************
		case tyInteger:
				SetType(pConstId->pType, pIntegerType);
		break;

		case tyUnsignedInteger:
				SetType(pConstId->pType, pUnsIntegerType);
		break;

		case tyShortInteger:
				SetType(pConstId->pType, pShortIntegerType);
		break;

		case tyUnsignedShortInteger:
				SetType(pConstId->pType,pUnsShortIntegerType);
		break;

		case tyLongInteger:
				SetType(pConstId->pType,pLongIntegerType);
		break;

		case tyUnsignedLongInteger:
				SetType(pConstId->pType,pUnsLongIntegerType);
		break;
//*******************************************************************************************************
		case tyCharacter:
				SetType(pConstId->pType, pCharType);
		break;

//*******************************************************************************************************
		
		case tyReal:
				SetType(pConstId->pType, pRealType);
		break;

		default:
				SetType(pConstId->pType, pDummyType);
		break;
		}
	
	//--<constant>
	ParseConstant(pConstId);
	pConstId->defn.how = dcConstant;

	}while(token == tcComma);
	
	CondGetToken(tcSemicolon, errMissingSemicolon);
	//--Skip extra semicolons.
	while (token == tcSemicolon) {GetToken();}
}

//--------------------------------------------------------------
//  ParseConstant       Parse a constant.
//
//      pConstId : ptr to symbol table node of the identifier
//                 being defined
//--------------------------------------------------------------

void TParser::ParseConstant(TSymtabNode *pConstId)
{
    TTokenCode sign = tcDummy;  // unary + or - sign, or none
	
    //--Unary + or -
    if (TokenIn(token, tlUnaryOps)) 
	{
		if (token == tcMinus) sign = tcMinus;
		GetToken();
    }
	
    switch (token) {

	//--Numeric constant:  Integer or real type.
	case tcNumber:
	    if (pToken->Type() == tyInteger) {
			pConstId->defn.constant.value.integer = sign == tcMinus ? -pToken->Value().integer :  pToken->Value().integer;
		//SetType(pConstId->pType, pIntegerType);
	    }
	    else {
		pConstId->defn.constant.value.real = sign == tcMinus ? -pToken->Value().real :  pToken->Value().real;
		//SetType(pConstId->pType, pRealType);
	    }
		
	    GetToken();
	    break;	    
    }
}



//      ********************************************
//      *                                          *
//      *  Variable and Record Field Declarations  *
//      *                                          *
//      ********************************************

//--------------------------------------------------------------
//  ParseVariableDeclarations   Parse variable declarations.
//
//      pRoutineId : ptr to symbol table node of program,
//                   procedure, or function identifier
//--------------------------------------------------------------

void TParser::ParseVariableDeclarations(TSymtabNode *pRoutineId)
{
    if (execFlag) {
	ParseVarOrFieldDecls(pRoutineId, NULL, 0);
    }
}

//--------------------------------------------------------------
//  ParseFieldDeclarations      Parse a list record field
//                              declarations.
//
//      pRecordType : ptr to record type object
//      offet       : byte offset within record
//--------------------------------------------------------------

void TParser::ParseFieldDeclarations(TType *pRecordType, int offset)
{
    ParseVarOrFieldDecls(NULL, pRecordType, offset);
}






void TParser::ParseArraySize(TSymtabNode *pConstId)
{
	if(token == tcNumber)
	{
		if(pToken->Type() == tyInteger)
		{
			pConstId->pType->array.elmtCount=pToken->Value().integer;
			pConstId->pType->array.minIndex=0;
			pConstId->pType->array.maxIndex=pToken->Value().integer-1;
		}
		else
		{
			Error(errIncompatibleTypes);
		}
	}
	else
	{
		Error(errUnexpectedToken);
	}

	GetToken();
	CondGetToken(tcRSquareBraquet,errMissingRightSquareBracket);
}

//--------------------------------------------------------------
//  ParseVarOrFieldDecls        Parse a list of variable or
//                              record field declarations
//                              separated by semicolons:
//
//                                  <id-sublist> : <type>
//
//      pRoutineId  : ptr to symbol table node of program,
//                    procedure, or function identifier, or NULL
//      pRecordType : ptr to record type object, or NULL
//      offset      : variable: runtime stack offset
//                    field: byte offset within record
//--------------------------------------------------------------

void TParser::ParseVarOrFieldDecls(TSymtabNode *pRoutineId, TType *pRecordType, int offset)
{
	//para ajudar com structs
	TSymtabNode *aux=NULL;
	int ptrLevel=0;

	//novo identificador
	TSymtabNode *newIdentifier;

	//pode ser volatile
	if(token == tcVolatile)
	{
		//tratar o volatile

		GetToken();
	}

	//guarda o tipo
	TDataType type;
	switch (token)
	{
	case tcInt:
		type=tyInteger;
		break;

	case tcFloat:
	case tcDouble:
		type=tyReal;
		break;

	case tcChar:
		type=tyCharacter;
		break;

	case tcUnsigned:
		GetToken(); 
		switch(token)
		{
		case tcChar:
			type=tyUnsignedChar;
			break;

		case tcInt:
			type=tyUnsignedInteger;
			break;

		case tcLong:
			GetToken();
			if(token == tcInt){type=tyUnsignedLongInteger;}
			else
			{
				Error(errUnexpectedToken);
			}
			break;

		case tcShort:
			GetToken();
			if(token == tcInt){type=tyUnsignedShortInteger;}
			else
			{
				Error(errUnexpectedToken);
			}
			break;
		}
		break;

	case tcShort:
		GetToken();
		if(token == tcInt){type=tyShortInteger;}
		else
		{
			Error(errUnexpectedToken);
		}
		break;

	case tcLong:
		GetToken();
		switch(token)
		{
		case tcInt:
			type=tyLongInteger;
			break;

		case tcLong:
			GetToken();
			if(token == tcInt){type=tyLongLongInteger;}
			else
			{
				Error(errUnexpectedToken);
			}
			break;
		}
		break;

	case tcVoid:
		type=tyVoid;
		break;

	case tcStruct:
		GetToken();
		aux = globalSymtab.Search(pToken->String());
		if(!aux)
		{
			ParseStruct(pRoutineId);
			return;
		}
		type = tyStruct;
		break;

	default:
		type=tyDummy;
		break;
	}

	//tratar o identificador
	do
	{
		GetToken();

		//tratar do apontador
		ptrLevel=0;
		while(token == tcStar){ptrLevel++; GetToken();}

		newIdentifier = EnterNewLocal(pToken->String());
		if(!newIdentifier)
		{
			Error(errRedefinedIdentifier);
		}

		GetToken();
		
		//atribuir o tipo
		switch(type)
		{
//*******************************************************************************************************
		case tyInteger:
				SetType(newIdentifier->pType, pIntegerType);
		break;

		case tyUnsignedInteger:
				SetType(newIdentifier->pType, pUnsIntegerType);
		break;

		case tyShortInteger:
				SetType(newIdentifier->pType, pShortIntegerType);
		break;

		case tyUnsignedShortInteger:
				SetType(newIdentifier->pType,pUnsShortIntegerType);
		break;

		case tyLongInteger:
				SetType(newIdentifier->pType,pLongIntegerType);
		break;

		case tyUnsignedLongInteger:
				SetType(newIdentifier->pType,pUnsLongIntegerType);
		break;
//*******************************************************************************************************
		case tyCharacter:
				SetType(newIdentifier->pType, pCharType);
		break;

//*******************************************************************************************************
		
		case tyReal:
				SetType(newIdentifier->pType, pRealType);
		break;

		case tyStruct:
			SetType(newIdentifier->pType,pStructs[aux->defn.Struct.id]);
		break;

		default:
				SetType(newIdentifier->pType, pDummyType);
		break;
		}

		//valor inicial para as variaveis
		newIdentifier->defn.constant.value.integer=0;
		newIdentifier->defn.constant.value.real=0;
		newIdentifier->defn.constant.value.character='\0';

		if(token == tcEq)			//tem valor inicial
		{
			GetToken();
			ParseConstant(newIdentifier);
			newIdentifier->defn.data.value=newIdentifier->defn.constant.value;
		}
		
		if(ptrLevel)				//testa se e variavel normal ou apontador
		{
			newIdentifier->defn.how=dcPointer;
			newIdentifier->defn.data.pointerLevel=ptrLevel;
		}
		else{newIdentifier->defn.how=dcVariable;}

		if(token == tcLParen)		//e funcao
		{
			symtabStack.EnterScope();
			newIdentifier->defn.routine.returnType=type;
			ParseFuncDecls(newIdentifier,NULL,0);

			CondGetToken(tcRParen, errMissingRightParen);
			newIdentifier->defn.how=dcFunction;
			newIdentifier->defn.routine.pSymtab=symtabStack.ExitScope();
			newIdentifier->defn.routine.vSymtab=NULL;							//isto e para o interpretador
		}

		if(token == tcLSquareBracket)		//e vector
		{
			GetToken();
			ParseArraySize(newIdentifier);
			newIdentifier->defn.how=dcArray;
		}

	}while(token == tcComma);

	//tem de testar se e uma chaveta e fa�o o parse do que esta dentro da fun�ao ou se e apenas a declara�ao
	if(token == tcLBracket)
	{
		symtabStack.PushSymtab(newIdentifier->defn.routine.pSymtab);					//colocamos a stack da fun�ao como principal
		newIdentifier->defn.routine.pIcode=new TIcode();								//criamos espa�o no codigo intermedio para a fun�ao
		icode=*(newIdentifier->defn.routine.pIcode);

		GetToken();
		do
		{
			
			if (TokenIn(token, tlStatementStart)) 
			{
				ParseStatement();
				
			}else if (TokenIn(token, tlDeclarationStart)) 
			{
				ParseDeclarations(newIdentifier);
			}
			else
			{
				AbortTranslation(abortUnimplementedFeature);
			}
		}while(token != tcRBracket);

		icode.Put(tcEndOfFile);								//reutilizamos este token para identificar fim de funcao
		*(newIdentifier->defn.routine.pIcode)=icode;
		symtabStack.PopSymtab();
		CondGetToken(tcRBracket,errMissingRightBracket);
		
	}
	else
	{
		//se nao for fun�ao tem de ter ;
		CondGetToken(tcSemicolon, errMissingSemicolon);
		//--Skip extra semicolons.
		while (token == tcSemicolon) {GetToken();}
	}
	
}



void TParser::ParseFuncDecls(TSymtabNode *pRoutineId, TType *pRecordType, int offset)
{
	int paramPosition=1;
	do
	{
		GetToken();
		//guarda o tipo 
		TDataType type;
		switch (token)
		{
		case tcInt:
			type=tyInteger;
			break;

		case tcFloat:
		case tcDouble:
			type=tyReal;
			break;

		case tcChar:
			type=tyCharacter;
		break;

		case tcUnsigned:
			GetToken(); 
			switch(token)
			{
			case tcChar:
				type=tyUnsignedChar;
			break;

			case tcInt:
				type=tyUnsignedInteger;
			break;
			}
		break;

		case tcShort:
			GetToken();
			if(token == tcInt){type=tyShortInteger;}
			else
			{
				Error(errUnexpectedToken);
			}
		break;

		case tcLong:
			GetToken();
			switch(token)
			{
			case tcInt:
				type=tyLongInteger;
			break;

			case tcLong:
				GetToken();
				if(token == tcInt){type=tyLongLongInteger;}
				else
				{
					Error(errUnexpectedToken);
				}
			break;
			}
		break;

		case tcVoid:
			type=tyVoid;
		break;
		default:
			type=tyDummy;
			pRoutineId->defn.routine.parmCount=0;
			return;
		break;
	}
		int ptr_level=-1;
		do
		{
			ptr_level++;
			GetToken();
		}while(token == tcStar);

		//TSymtabNode *newIdentifier = EnterNewLocal(pToken->String());
		TSymtabNode *newIdentifier= EnterNewLocal(pToken->String());
		newIdentifier->defn.data.offset=paramPosition++;
		if(!newIdentifier)
		{
			Error(errRedefinedIdentifier);
		}
		
		newIdentifier->defn.data.pointerLevel=ptr_level;
		//atribuir o tipo
		switch(type)
		{
		case tyInteger:
				SetType(newIdentifier->pType, pIntegerType);
		break;

		case tyReal:
				SetType(newIdentifier->pType, pRealType);
		break;

		case tyCharacter:
				SetType(newIdentifier->pType, pCharType);
		break;

		case tyStruct:
			//SetType(newIdentifier->pType,);
		break;

		default:
				SetType(newIdentifier->pType, pDummyType);
		break;
		}
		newIdentifier->defn.how=dcField;
		GetToken();

	}while(token == tcComma);

	pRoutineId->defn.routine.parmCount=paramPosition-1;						//guarda o numero de parametros e util para validar a chamada da funcao
}




void TParser::ParseStruct(TSymtabNode *pRoutineId)
{
	static int id_gen=0;

	TSymtabNode *newType = EnterNewLocal(pToken->String(),dcType);
	newType->defn.how=dcType;
	if(!newType){Error(errInvalidType);}

	GetToken();
	CondGetToken(tcLBracket,errMissingLeftBracket);

	symtabStack.EnterScope();
	//nao esta acabado so pa nao dar erro
	while(token != tcRBracket)
	{
		ParseVarOrFieldDecls(newType,NULL,0);
		//GetToken();
	}
	newType->defn.Struct.id=id_gen++;

	//criar e ligar o tipo
	SetType(pStructs[newType->defn.Struct.id], new TType(fcStruct, sizeof(unsigned int), newType));
	SetType(newType->pType ,pStructs[newType->defn.Struct.id]);

	newType->defn.Struct.pSymtab=symtabStack.ExitScope();

	GetToken();
	CondGetToken(tcSemicolon,errMissingSemicolon);
}


void TParser::ParseEnum(TSymtabNode *pRoutineId)
{
	
}








