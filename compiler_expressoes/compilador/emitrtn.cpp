#include "stdafx.h"

//--------------------------------------------------------------
//  EmitProgramPrologue         Emit the program prologue.
//--------------------------------------------------------------

void TCodeGenerator::EmitProgramPrologue(void)
{
	//*****************************************************************************
	//		cabecalho da memoria
	PutLine("memory_initialization_radix=16;");
	PutLine("\n");
	PutLine("memory_initialization_vector=");
	PutLine("\n");

	//forca a saltar para a posicao
	Emit2(BRAI,0,8,33);		//para reutilizar opcode BRAI obriga a que Rb seja 8
    PC=33;					//da pos 1 ate a 32 sao espa�os para o vetor de interrupcoes
	
	//inicializa o vetor com o endereco das variaveis globais
	Emit2(MOVI,GLOBAL_VAR_REG,2,32);

	//INICIALIZA AS VARIAVEIS GLOBAIS
	int offset=0;
	TRegister *reg;
	for(int i=0;i<globalSymtab.NodeCount();i++)
	{
		if(globalSymtab.Get(i)->defn.how==dcVariable)
		{
			globalSymtab.Get(i)->defn.data.offset=offset;

			reg=regFile.GetRegister(globalSymtab.Get(i));
			Emit2(MOVI,reg->GetReg(),0,globalSymtab.Get(i)->defn.constant.value.integer);
			Emit2(SWI,reg->GetReg(),GLOBAL_VAR_REG,offset);
			offset += globalSymtab.Get(i)->pType->size;
		}
	}
	
	//Liberta os registos usados
	for(int i=0;i<globalSymtab.NodeCount();i++)
	{
		if(globalSymtab.Get(i)->defn.how==dcVariable || globalSymtab.Get(i)->defn.how==dcPointer)
		{
			regFile.FreeRegister(globalSymtab.Get(i));
		}
	}

	//devia de por aqui um call 
	//Emit2(BRI,0,0,2);							//para ja esta a simular o call

	//devia de por um while(1); ou halt
	//Emit2(BRI,16,0,0);							//se rd tiver 16 salta para tras se nao salta pa frente

}

//--------------------------------------------------------------
//  EmitProgramEpilogue         Emit the program epilogue.
//--------------------------------------------------------------

void TCodeGenerator::EmitProgramEpilogue(void)
{
	
}

//--------------------------------------------------------------
//  EmitMain            Emit code for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunction(const TSymtabNode *pMainId)
{
	EmitFunctionPrologue();

	EmitStatmentList(tcEndOfFile);

	EmitFunctionEpilogue();
}

//--------------------------------------------------------------
//  EmitMainPrologue    Emit the prologue for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunctionPrologue(void)
{
	//ciclo para contar o numero de variaveis locais para ver se todas cabem nos registos
	int num_variaveis=0;
	for(int i=0;i < symtabStack.GetCurrentSymtab()->NodeCount();i++)
	{
		if(symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcVariable || symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcPointer)
		{
			num_variaveis++;
		}
	}

	int rx=1;								//variavel para atribuir um novo registo caso todos estejam ocupdos
	if(num_variaveis < REG_NUMBER - 4)		//se todas as variaveis couberem em registos...
	{
		for(int i=0;i < symtabStack.GetCurrentSymtab()->NodeCount();i++)
		{
			if(symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcVariable || symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcPointer)
			{
				TRegister *reg=regFile.GetRegister(symtabStack.GetCurrentSymtab()->Get(i));
				if(!reg)
				{
					//emit push rx
					regFile.FreeRegister(rx++);
					reg=regFile.GetRegister(symtabStack.GetCurrentSymtab()->Get(i));
				}
				
				Emit2(MOVI,reg->GetReg(),0,symtabStack.GetCurrentSymtab()->Get(i)->defn.constant.value.integer);
			}
		}
	}

	//debug
	//regFile.PrintRegFile();
}

//--------------------------------------------------------------
//  EmitMainEpilogue    Emit the epilogue for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunctionEpilogue(void)
{
   
}


//--------------------------------------------------------------
//  EmitStatementList        emit a statement list until
//                              the terminator token.
//
//      terminator : the token that terminates the list
//--------------------------------------------------------------

void TCodeGenerator::EmitStatmentList(TTokenCode terminator)
{
	GetToken();
	do 
	{
		EmitStatement();
		while (token == tcSemicolon) {GetToken();}
    } while (token != terminator);
}
//endfig