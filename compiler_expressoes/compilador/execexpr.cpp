#include "stdafx.h"


TType *TExecutor::ExecuteExpression(void)
{
	TType      *pOperand1Type;  // ptr to first  operand's type
    TType      *pOperand2Type;  // ptr to second operand's type
    TType      *pResultType;    // ptr to result type
    TTokenCode op;

	//--Execute the first simple expression.
	pResultType = ExecuteSimpleExpression();
	
	
    //--If we now see a relational operator,
    //--execute the second simple expression.
    if (TokenIn(token, tlRelOps)) 
	{
		op            = token;
		pOperand1Type = pResultType->Base();
		pResultType   = pBooleanType;

		GetToken();
		pOperand2Type = ExecuteSimpleExpression()->Base();

	//--Perform the operation, and push the resulting value
	//--onto the stack.
	if (((pOperand1Type == pIntegerType) && (pOperand2Type == pIntegerType)) || ((pOperand1Type == pCharType) && (pOperand2Type == pCharType)) || (pOperand1Type->form == fcEnum)) 
	{
	    //--integer <op> integer
	    //--boolean <op> boolean
	    //--char    <op> char
	    //--enum    <op> enum
	    int value1, value2;
	    if (pOperand1Type == pCharType) 
		{
			value2 = Pop()->character;
			value1 = Pop()->character;
	    }
	    else 
		{
			value2 = Pop()->integer;
			value1 = Pop()->integer;
	    }

	    switch (op) 
		{
			case tcEqual:	Push(value1 == value2);	break;
			case tcNe:		Push(value1 != value2); break;
			case tcLt:	    Push(value1 <  value2); break;
			case tcGt:	    Push(value1 >  value2); break;
			case tcLe:	    Push(value1 <= value2); break;
			case tcGe:	    Push(value1 >= value2); break;
	    }
	}
	else if ((pOperand1Type == pRealType) || (pOperand2Type == pRealType)) 
	{

	    //--real    <op> real
	    //--real    <op> integer
	    //--integer <op> real
	    float value2 = pOperand2Type == pRealType ? Pop()->real : Pop()->integer;
	    float value1 = pOperand1Type == pRealType ? Pop()->real : Pop()->integer;

	    switch (op) 
		{
			case tcEqual:	Push(value1 == value2); break;
			case tcNe:	    Push(value1 != value2); break;
			case tcLt:	    Push(value1 <  value2); break;
			case tcGt:	    Push(value1 >  value2); break;
			case tcLe:	    Push(value1 <= value2); break;
			case tcGe:	    Push(value1 >= value2); break;
	    }
	}
    }
	
    return pResultType;
}

TType *TExecutor::ExecuteSimpleExpression(void)
{
    TType      *pOperandType;      // ptr to operand's type
    TType      *pResultType;       // ptr to result type
    TTokenCode  op;                // operator
    TTokenCode  unaryOp = tcPlus;  // unary operator

    //--Unary + or -
	if (token == tcPlus || token == tcMinus) 
	{
		unaryOp = token;
		GetToken();
    }

    //--Execute the first term.
    pResultType = ExecuteTerm();

    //--If there was a unary -, negate the first operand value.
    if (unaryOp == tcMinus) 
	{
		if (pResultType == pRealType) Push(-Pop()->real);
		else                          Push(-Pop()->integer);
    }
	
    //--Loop to execute subsequent additive operators and terms.
    while (TokenIn(token, tlAddOps)) 
	{
		op = token;
		pResultType = pResultType->Base();

		GetToken();
		pOperandType = ExecuteTerm()/*->Base()*/;

		//--Perform the operation, and push the resulting value
		//--onto the stack.
		if (op == tcOr) 
		{
			//--boolean OR boolean
			int value2 = Pop()->integer;
			int value1 = Pop()->integer;

			Push(value1 || value2);
			pResultType = pBooleanType;
		}
		else if ((pResultType  == pIntegerType) && (pOperandType == pIntegerType)) 
		{
			//--integer +|- integer
			int value2 = Pop()->integer;
			int value1 = Pop()->integer;
			Push(op == tcPlus ? value1 + value2 : value1 - value2);
			pResultType = pIntegerType;
		}
		else 
		{
			//--real    +|- real
			//--real    +|- integer
			//--integer +|- real
			float value2 = pOperandType == pRealType ? Pop()->real : Pop()->integer;
			float value1 = pResultType  == pRealType ? Pop()->real : Pop()->integer;

			Push(op == tcPlus ? value1 + value2 : value1 - value2);
			pResultType = pRealType;
		}
    }
	
    return pResultType;
}

TType *TExecutor::ExecuteTerm(void)
{
    TType      *pOperandType;  // ptr to operand's type
    TType      *pResultType;   // ptr to result type
    TTokenCode  op;            // operator

    //--Execute the first factor.
    pResultType = ExecuteFactor();

    //--Loop to execute subsequent multiplicative operators and factors.
    while (TokenIn(token, tlMulOps)) 
	{
		op = token;
		pResultType = pResultType->Base();

		GetToken();
		pOperandType = ExecuteFactor()->Base();

	//--Perform the operation, and push the resulting value
	//--onto the stack.
		
		switch (op) 
		{
			case tcAnd: 
			{
			//--boolean AND boolean
			int value2 = Pop()->integer;
			int value1 = Pop()->integer;

			Push(value1 && value2);
			pResultType = pBooleanType;
			break;
			}
			case tcStar:

				if ((pResultType  == pIntegerType) && (pOperandType == pIntegerType)) 
				{

					//--integer * integer
					int value2 = Pop()->integer;
					int value1 = Pop()->integer;

					Push(value1*value2);
					pResultType = pIntegerType;
				}
				else 
				{
					//--real    * real
					//--real    * integer
					//--integer * real
					float value2 = pOperandType == pRealType ? Pop()->real : Pop()->integer;
					float value1 = pResultType == pRealType ? Pop()->real : Pop()->integer;

					Push(value1*value2);
					pResultType = pRealType;
				}
			break;

			case tcSlash: 
				//--real    / real
				//--real    / integer
				//--integer / real
				//--integer / integer
				float value2 = pOperandType == pRealType ? Pop()->real : Pop()->integer;
				float value1 = pResultType == pRealType ? Pop()->real : Pop()->integer;

				if (value2 == 0.0f) RuntimeError(rteDivisionByZero);

				Push(value1/value2);
				pResultType = pRealType;
			break;
	    
		}
    }

    return pResultType;
}



TType *TExecutor::ExecuteFactor(void)
{
    TType *pResultType=NULL;  // ptr to result type
	
    switch (token) 
	{
		case tcIdentifier:
			switch (pNode->defn.how) 
			{
				case dcConstant:
					pResultType = ExecuteConstant(pNode);
				break;

				case dcFunction:
					{
						TSymtabNode *pFunc=symtabStack.SearchAll(pToken->String());
						ExecuteAssignment(pFunc);
						if(pFunc->defn.routine.returnType == tyReal)
						{
							Push(functionReturnValue.real);
							pResultType=pRealType;
						}
						else if(pFunc->defn.routine.returnType == tyInteger)
						{
							Push(functionReturnValue.integer);
							pResultType=pIntegerType;
						}
						
						//remendo para compensar gettokens perdidos
						icode.GoTo(icode.CurrentLocation()-6);
						GetToken();
						
					}
					break;

				default:
					pResultType = ExecuteVariable(pNode, false);
				break;
			}
	    break;
	

	case tcNumber: 
	    //--Push the number's integer or real value onto the stack.
	    if (pNode->pType == pIntegerType) 
		{
			Push(pNode->defn.constant.value.integer);
	    }
	    else 
		{
			Push(pNode->defn.constant.value.real);
	    }
	    pResultType = pNode->pType;

	    GetToken();
	    break;
	
	case tcNot:

	    //--Execute boolean factor and invert its value.
	    GetToken();
	    ExecuteFactor();

	    Push(1 - Pop()->integer);
	    pResultType = pBooleanType;
	    break;

	case tcLParen:

	    //--Parenthesized subexpression:  Call ExecuteExpression
	    //--                              recursively.
	    GetToken();  // first token after (

	    pResultType = ExecuteExpression();

	    GetToken();  // first token after )
	    break;

	case tcAnd:{
		//obter endere�o
		GetToken();
		TSymtabNode *pPtr=symtabStack.SearchAll(pToken->String());
		Push((void*)runStack.GetVar(pPtr->defn.data.offset));
		pResultType=pIntegerType;
		GetToken();
		}
		break;

	case tcStar:{
		//obter apontado
		int ptr_pointer=0;
		do
		{
			ptr_pointer++;
			GetToken();			
		}while(token == tcStar);

		TSymtabNode *pPtr=symtabStack.SearchAll(pToken->String());

		while(ptr_pointer > 0)
		{
			pPtr=(TSymtabNode*)pPtr->defn.data.value.integer;
			ptr_pointer--;
		}
		
		if(pPtr->pType == pRealType){Push(runStack.GetVar(pPtr->defn.data.offset));}
		else{ Push(runStack.GetVar(pPtr->defn.data.offset)); }

		pResultType=pPtr->pType;
		GetToken();

		}
		break;
    }

    return pResultType;
}




TType *TExecutor::ExecuteConstant(const TSymtabNode *pId)
{
    TType      *pType = pId->pType;
    TDataValue  value = pId->defn.constant.value;

    if      (pType == pRealType)     Push(value.real);
    else if (pType == pCharType)     Push(value.character);
    else if (pType->form == fcArray) Push(value.pString);
    else                             Push(value.integer);

    GetToken();
    //TraceDataFetch(pId, TOS(), pType);
    return pType;
}


TType *TExecutor::ExecuteVariable(const TSymtabNode *pId, int addressFlag)
{
    TType *pType = pId->pType;
    //pomos o valor da variavel na stack para usar nos calculos
	if(pId->pType == pRealType){ Push(runStack.GetVar(pId->defn.data.offset)->real); }
	else { Push(runStack.GetVar(pId->defn.data.offset)->integer); }

	//cout<<"val = "<<runStack.GetVar(pId->defn.data.offset)->integer<<"\t\tvar = "<<pId->String()<<endl;

	GetToken();

    return pType;
}




TType *TExecutor::ExecuteField(void)
{
    GetToken();
    TSymtabNode *pFieldId = pNode;

    Push(((char *) (Pop()->address)) + pFieldId->defn.data.offset);

    GetToken();
    return pFieldId->pType;
}