#include "stdafx.h"

char *symbolStrings[]=
{
	NULL,
	NULL,NULL,NULL,NULL,NULL,NULL,

	"+","-","*","/","%","++","--","+=","-=","=",
	"&=","|=","^=","*=","/=",
	".",",",":",";","\"","'","->",
	"(",")","{","}","[","]",
	"<",">","<=",">=","!=","==",
	"&&","||",
	"?","<<",">>",
	"&","|","!","^","~",

	"do","if","for","int","auto","case",
	"char","else","enum","goto","long","void",
	"break","const","double","float","while","short","union",
	"extern","return","signed","sizeof","static","struct",
	"switch","default","typedef",
	"continue","register","unsigned",
	"volatile",
};




TIcode::TIcode(const TIcode &icode)
{
	int length = int (icode.cursor - icode.pCode);

	pCode=cursor = new char [length];
	memcpy(pCode,icode.pCode,length);
}


void TIcode::CheckBounds(int size)
{
	if(cursor + size >= &pCode[codeSegmentSize])
	{
		Error(errCodeSegmentOverflow);
		AbortTranslation(abortCodeSegmentOverflow);
	}
}

void TIcode::Put(TTokenCode tc)
{
	if(errorCount > 0) return;
	char code = tc;
	CheckBounds(sizeof(char));
	memcpy((void*)cursor,(const void*)&code,sizeof(char)); //cout<<"put token = "<<(int)*cursor<<endl;
	cursor += sizeof(char);
}

void TIcode::Put(const TSymtabNode *pNode)
{
	if(errorCount > 0) return;

	short xSymtab = pNode->SymtabIndex();
	short xNode= pNode->NodeIndex();

	CheckBounds(2*sizeof(short));
	memcpy((void*)cursor,(const void*)&xSymtab,sizeof(short));							//cout<<"put xsymtab ="<<(int)*cursor<<endl;
	memcpy((void*)(cursor + sizeof(short)),(const void*)&xNode,sizeof(short));			//cout<<"put xnode = "<<(int)*(cursor + sizeof(short))<<endl;
	cursor += 2*sizeof(short);
}



TToken *TIcode::Get(void)
{
	TToken *pToken;
	char code;
	TTokenCode token;

	do
	{
		memcpy((void*)&code,(const void*)cursor,sizeof(char));
		cursor += sizeof(char);
		token = (TTokenCode)code;
		if(token == mcLineMarker)
		{
			short number;

			memcpy((void*)&number,(const void*)cursor,sizeof(short));
			currentLineNumber=number;
			cursor += sizeof(short);
		}
	
	}while(token == mcLineMarker);

	switch (token)
	{
	case tcNumber:
		pToken=&numberToken;
		break;

	case tcString:
		pToken=&stringToken;
		break;

	case tcIdentifier:
		pToken=&wordToken;
		pToken->code=tcIdentifier;
		break;

	default:
		if(token <= tcTil)
		{
			pToken = &specialToken;
			pToken->code=token;
		}
		else
		{
			pToken=&wordToken;
			pToken->code=token;
		}
		break;
	}
	
	switch(token)
	{
	case tcIdentifier:
	case tcNumber:
	case tcString:
		pNode=GetSymtabNode();
		strcpy(pToken->string,pNode->String());
		break;

	default:
		pNode=NULL;
		if(token != tcEndOfFile)
		{
			strcpy(pToken->string,symbolStrings[code]);
		}
		break;
	}
	return pToken;
}


TSymtabNode *TIcode::GetSymtabNode(void)
{
	extern TSymtab **vpSymtabs;
	short xSymtab, xNode;

	memcpy((void*)&xSymtab,(const void*)cursor,sizeof(short));
	memcpy((void*)&xNode,(const void*)(cursor + sizeof(short)),sizeof(short));
	cursor += 2*sizeof(short);

	//cout<<"xsymtab = "<<xSymtab<<endl;

	return vpSymtabs[xSymtab]->Get(xNode);
}

void TIcode::InsertLineMarker(void)
{
	if(errorCount > 0) return;

	char lastCode;
	cursor -= sizeof(char);

	memcpy((void*)&lastCode,(const void *)cursor,sizeof(char));

	char code=mcLineMarker;
	short number=currentLineNumber;
	CheckBounds(sizeof(char)+sizeof(short));
	memcpy((void*)cursor,(const void*)&code,sizeof(char));
	cursor += sizeof(char);
	memcpy((void*)cursor,(const void*)&number,sizeof(short));
	cursor += sizeof(short);

	memcpy((void*)cursor,(const void*)&lastCode,sizeof(char));
	cursor += sizeof(char);
}

void TIcode::Print(void)
{
	for(cursor=pCode;cursor < (pCode + 50);cursor++)
	{
		cout<<(int)*cursor<<endl;
	}
}


