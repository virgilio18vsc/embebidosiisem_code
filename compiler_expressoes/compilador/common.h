#ifndef __COMMON_H__
#define __COMMON_H__

#include "stdafx.h"
#include "misc.h"

extern int currentLineNumber;
extern int currentNestingLevel;

extern TSymtab globalSymtab;

extern int cntSymtabs;
extern TSymtab *pSymtabList;
extern TSymtab **vpSymtabs;

//extern TIcode icode;

//--------------------------------------------------------------
//  Token lists
//--------------------------------------------------------------

extern const TTokenCode tlDeclarationStart[], tlDeclarationFollow[],
			tlIdentifierStart[],  tlIdentifierFollow[],
			tlSublistFollow[],    tlFieldDeclFollow[];

extern const TTokenCode tlEnumConstStart[], tlEnumConstFollow[],
			tlSubrangeLimitFollow[];

extern const TTokenCode tlIndexStart[], tlIndexFollow[],
			tlIndexListFollow[];

extern const TTokenCode tlSubscriptOrFieldStart[];

//******************************************************************************
extern const TTokenCode tlStatementStart[], tlStatementFollow[];
extern const TTokenCode tlCaseLabelStart[];

extern const TTokenCode tlExpressionStart[], tlExpressionFollow[];
extern const TTokenCode tlRelOps[], tlLogicOps[], tlUnaryOps[],
			tlAddOps[], tlMulOps[];

extern const TTokenCode tlProgramEnd[];

extern const TTokenCode tlEq[];
extern const TTokenCode tlLBracket[];
extern const TTokenCode tlRBracket[];
extern const TTokenCode tlEqual[];
extern const TTokenCode tlDO[];
extern const TTokenCode tlColon[];


int TokenIn(TTokenCode tc, const TTokenCode *pList);

#endif