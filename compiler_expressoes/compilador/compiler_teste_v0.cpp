// compiler_teste_v0.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace std;


int main(int argc, char **argv)
{
	
	if(argc != 3)
	{
		cerr<<"Usage: list <source file>"<<endl;
		AbortTranslation(abortInvalidCommandLineArgs);
		system("pause");
		return 1;
	}

	TParser *pParser=new TParser(new TSourceBuffer(argv[1]));
	pParser->Parse();
	delete pParser;
	
	if(errorCount == 0)
	{
		vpSymtabs=new TSymtab *[cntSymtabs];
		for(TSymtab *pst=pSymtabList;pst;pst=pst->Next())
		{
			pst->Convert(vpSymtabs);
		}
	}
	else
	{
		system("pause");
		return 1;
	}

	//TSymtab *sym=pSymtabList;
	//sym->Print();

	if (errorCount == 0) {
	vpSymtabs = new TSymtab *[cntSymtabs];
	for (TSymtab *pSt = pSymtabList; pSt; pSt = pSt->Next()) 
	{
		//cout<<"**************************     symtab separator      *************************************"<<endl;
	    pSt->Convert(vpSymtabs);
		//pSt->Print();
	}

	//TBackend *pBackend=new TExecutor;
	//pBackend->Go(NULL);

	//TSymtabNode *pProgramId = NULL;
	TBackend *pBackend = new TCodeGenerator(argv[2]);
	pBackend->Go(NULL);
	}

	system("pause");
	return 0;
}

