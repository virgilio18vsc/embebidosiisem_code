#include "stdafx.h"

void TParser::ParseStatement(void)
{
	//InsertLineMarker();
    //--Call the appropriate parsing function based on
    //--the statement's first token.
    switch (token) 
	{
		case tcStar :
			while(token == tcStar) 
			{ 
				icode.Put(token);
				GetToken();
			}
		case tcIdentifier:  
			ParseAssignment();
			CondGetToken(tcSemicolon,errMissingSemicolon);
			break;
		case tcWhile:       ParseWHILE();       break;
		case tcIf:          ParseIF();          break;
		case tcFor:         ParseFOR();         break;
		case tcSwitch:      ParseSWITCH();      break;
		case tcReturn:		ParseRETURN();		break;
    }
}


void TParser::ParseStatementList(TTokenCode terminator)
{
    //--Loop to parse statements and to check for and skip semicolons.
    do {
	ParseStatement();

	if (TokenIn(token, tlStatementStart)) {
	    Error(errMissingSemicolon);
	}
	else while (token == tcSemicolon) GetTokenAppend();
    } while ((token != terminator) && (token != tcEndOfFile));
}


void TParser::ParseAssignment(void)
{
	int parametro=0;													//variavel usada para testar os parametros de chamadas de funcoes
	TSymtabNode *pTargetNode=SearchAll(pToken->String());
	if(!pTargetNode){pTargetNode=EnterLocal(pToken->String());}
	
	icode.Put(tcIdentifier);
	icode.Put(pTargetNode);
    GetTokenAppend();
	
    switch(token)
	{
	case tcEq:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcPlusPlus:
			GetTokenAppend();
			if(token != tcSemicolon){ParseExpression();}
		break;

	case tcPlusEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcMinusMinus:
			GetTokenAppend();
			if(token != tcSemicolon){ParseExpression();}
		break;

	case tcMinusEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcStarEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcSlashEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcOrEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcAndEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcXorEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcLParen:
			TSymtabNode *Param;
			TSymtabNode **ListParam;
			ListParam=new TSymtabNode *[pTargetNode->defn.routine.pSymtab->NodeCount()];
			pTargetNode->defn.routine.pSymtab->Root()->Convert(ListParam);						//transforma a arvore em lista para procurar pelos parametros

			do
			{
				GetTokenAppend();
				switch (token)
				{
				case tcIdentifier: 
					
					Param=symtabStack.SearchAll(pToken->String());
					if(!Param){ Error(errIdentifierNotFound); }
					else{ icode.Put(symtabStack.SearchAll(pToken->String())); }
					
					break;
					
				case tcNumber:
					
					Param=symtabStack.SearchLocal(pToken->String());
					if(!Param){symtabStack.EnterLocal(pToken->String());}
					
					break;
				}

				GetTokenAppend();

			}
			while(token == tcComma);

			CondGetTokenAppend(tcRParen,errMissingRightParen);

		break;

	default:
		Error(errInvalidAssignment);
	}
}



//--------------------------------------------------------------
//  ParseWHILE      Parse a WHILE statement.:
//
//                      WHILE <expr> DO <stmt>
//--------------------------------------------------------------

void TParser::ParseWHILE(void)
{
	icode.Put(tcWhile);
	GetTokenAppend();
	
	//espera o (
	CondGetTokenAppend(tcLParen,errMissingLeftParen);

	//--<expr>
    ParseExpression();

	//espera )
	CondGetTokenAppend(tcRParen,errMissingRightParen);

	//espera {
	CondGetToken(tcLBracket,errMissingLeftBracket);
	
	//processa o que esta dentro do while
	while (token != tcRBracket)
	{
		ParseStatement();
		
	}
	
	icode.Put(tcRBracket); 
	//GetTokenAppend();
	if(token != tcRBracket)
	{
		Error(errMissingRightBracket);
	}
	GetToken();
}



void TParser::ParseIF(void)
{
	icode.Put(tcIf);
	GetTokenAppend();
	
	//espera o (
	CondGetTokenAppend(tcLParen,errMissingLeftParen);

	//--<expr>
    ParseExpression();

	//espera )
	CondGetTokenAppend(tcRParen,errMissingRightParen);

	//espera {
	CondGetToken(tcLBracket,errMissingLeftBracket);

	//processa o que esta dentro do if
	while(token != tcRBracket)
	{
		ParseStatement();
	}
	
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket,errMissingRightBracket);
	
	if(token != tcElse){ return;}			//se o if nao tiver else entao devemos sair
	
	icode.Put(tcElse);
	GetTokenAppend();

	//espera {
	CondGetToken(tcLBracket,errMissingLeftBracket);

	//processa o que esta dentro do else
	while(token != tcRBracket)
	{
		ParseStatement();
	}
	
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket,errMissingRightBracket);
}


void TParser::ParseFOR(void)
{
	icode.Put(tcFor);
    GetTokenAppend();
	//espera o (
	CondGetToken(tcLParen, errMissingLeftParen);
	ParseAssignment();
	CondGetTokenAppend(tcSemicolon, errMissingSemicolon);
	ParseExpression();
	CondGetToken(tcSemicolon, errMissingSemicolon);
	ParseAssignment();

	//espera o )
	CondGetTokenAppend(tcRParen, errMissingRightParen);
	CondGetToken(tcLBracket, errMissingLeftBracket);

	while(token != tcRBracket)
	{
		ParseStatement();
	}
	
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket, errMissingRightBracket);
}


//--------------------------------------------------------------
//  ParseCASE       Parse a CASE statement:
//
//                      CASE <expr> OF
//                          <case-branch> ;
//                          ...
//                      END
//--------------------------------------------------------------

void TParser::ParseSWITCH(void)
{
	icode.Put(tcSwitch); 
	GetTokenAppend();
	
	//espera (
	CondGetTokenAppend(tcLParen,errMissingLeftParen);
	
	//--<expr>
    ParseExpression();
	//espera )
	icode.Put(tcRParen);
	CondGetTokenAppend(tcRParen,errMissingRightParen);
	
	//espera {
	CondGetTokenAppend(tcLBracket,errMissingLeftBracket);
	
	do
	{
		ParseCase();
	}while(token == tcCase);

	if(token == tcDefault)
	{
		//espera :
		GetTokenAppend();
		CondGetToken(tcColon,errMissingColon);
		do
		{
			ParseStatement();
		}while(token != tcRBracket && token != tcBreak);
	}
	
	//espera }
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket,errMissingRightBracket);
}


//--------------------------------------------------------------
//  ParseCase     Parse a CASE branch:
//
//                          <case-label-list> : <stmt>
//--------------------------------------------------------------

void TParser::ParseCase(void)
{	
	GetTokenAppend();
	//espera um numero
	if(token != tcNumber){Error(errInvalidParm);}
	TSymtabNode *pNode = symtabStack.EnterLocal(pToken->String());
	pNode->value = (pToken->Type() == tyInteger ? (float) pToken->Value().integer : pToken->Value().real);
	icode.Put(pNode);

	GetTokenAppend();
	//espera :
	CondGetToken(tcColon,errMissingColon);

	//executa o case
	while(token != tcRBracket && token != tcCase && token != tcBreak)
	{
		ParseStatement();	
	}
	
	//verifica se tem break;
	if(token == tcBreak)
	{
		icode.Put(tcBreak);
		GetTokenAppend();
		CondGetTokenAppend(tcSemicolon,errMissingSemicolon);
	}
}



//--------------------------------------------------------------
//  Parse Return     
//
//                          return identifier
//--------------------------------------------------------------

void TParser::ParseRETURN(void)
{
	icode.Put(tcReturn);
	GetToken();

	TSymtabNode *retValue=symtabStack.SearchAll(pToken->String());
	if(!retValue)
	{
		if(token == tcIdentifier)
		{Error(errUndefinedIdentifier);}
		else
		{
			retValue = symtabStack.EnterLocal(pToken->String()); 
			retValue->defn.data.value=pToken->Value();
			
			//atribuir o tipo
			switch(pToken->Type())
			{
//*******************************************************************************************************
			case tyInteger:				SetType(retValue->pType, pIntegerType); 			break;
			case tyUnsignedInteger:		SetType(retValue->pType, pUnsIntegerType);		break;
			case tyShortInteger:		SetType(retValue->pType, pShortIntegerType);		break;
			case tyUnsignedShortInteger:SetType(retValue->pType,pUnsShortIntegerType);	break;
			case tyLongInteger:			SetType(retValue->pType,pLongIntegerType);		break;
			case tyUnsignedLongInteger:	SetType(retValue->pType,pUnsLongIntegerType);	break;
//*******************************************************************************************************
			case tyCharacter:			SetType(retValue->pType, pCharType);				break;
//*******************************************************************************************************
			case tyReal:				SetType(retValue->pType, pRealType);				break;
			default:					SetType(retValue->pType, pDummyType);			break;
			}
		}
	}
	icode.Put(token);
	icode.Put(retValue);

	GetTokenAppend();
	CondGetToken(tcSemicolon,errMissingRightBracket);
}



