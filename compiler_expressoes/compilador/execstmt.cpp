#include "stdafx.h"

void TExecutor::ExecuteStatement(void)
{
    stmtCount++;  
    switch(token)
	{
		case tcIdentifier:	{
			TSymtabNode *pIdentifier= symtabStack.SearchAll(pToken->String());
			if(!pIdentifier){AbortTranslation(abortRuntimeError);}
			ExecuteAssignment(pIdentifier);
			
			break;}
		case tcIf:			ExecuteIF();			break;
		case tcWhile:		ExecuteWHILE();			break;
		case tcFor:			ExecuteFOR();			break;
		case tcSwitch:		ExecuteSWITCH();		break;
		case tcReturn:		ExecuteRETURN();		break;
		case tcStar:		ExecuteAssignmentPtr();	break;
	}
}


void TExecutor::ExecuteAssignmentPtr(void)
{
	int			ptr_level=0;
	TType      *pExprType;    // ptr to expression type object
	TType      *pTargetType = NULL;  // ptr to target type object

	while(token == tcStar)
	{
		ptr_level++;
		GetToken();
	}

	TSymtabNode *pPtr=symtabStack.SearchAll(pToken->String());
	TStackItem *item=(TStackItem*)runStack.GetVar(pPtr->defn.data.offset)->integer;

	while(ptr_level > 1)
	{
		item=(TStackItem*)item->integer;
		ptr_level--;
	}

	GetToken();									//obtem o primeiro token da expressao
	GetToken();

	pTargetType=pPtr->pType;

	pExprType = ExecuteExpression();

	//--Do the assignment.
		if (pTargetType == pRealType) 
		{
			item->real = Pop()->real;
		}
		else if ((pTargetType->Base() == pIntegerType) || (pTargetType->Base()->form == fcEnum)) 
		{
			int value = Pop()->integer;
			RangeCheck(pTargetType, value);

			item->integer=value;
		}
		else if (pTargetType->Base() == pCharType) 
		{
			char value = Pop()->character;
			RangeCheck(pTargetType, value);

			//--character := character
			pPtr->defn.data.value.character = value;
		}
		else 
		{
			void *pSource = Pop()->address;

			//--array  := array
			//--record := record
			memcpy(pPtr, pSource, pTargetType->size);
		}
		cout<<"no ptr = "<<pPtr->String()<<"\t\tvalue = "<<item->integer<<endl;
		//TraceDataStore(pTargetId, pTarget, pTargetType);			//para debug depois pode sair
}

void TExecutor::ExecuteAssignment(const TSymtabNode *pTargetId)
{
	TStackItem *pTarget = NULL;      // runtime stack address of target
	TType      *pTargetType = NULL;  // ptr to target type object
	TType      *pExprType;    // ptr to expression type object
	
	//procura o identificador
	TSymtabNode *pVar=symtabStack.SearchAll(pToken->String());
	pTargetType = pVar->pType;
	//--Execute the expression and leave its value
	//--on top of the runtime stack.
	GetToken();									//obtem = ou (

	if(token == tcEq)
	{
		GetToken();									//obtem o primeiro token da expressao
		pExprType = ExecuteExpression();
	
		//--Do the assignment.
		if (pTargetType == pRealType) 
		{
			runStack.GetVar(pVar->defn.data.offset)->real = Pop()->real;
		}
		else if ((pTargetType->Base() == pIntegerType) || (pTargetType->Base()->form == fcEnum)) 
		{
			int value = Pop()->integer;
			RangeCheck(pTargetType, value);

			//--integer     := integer
			//--enumeration := enumeration
			//pTarget->integer = value;

			runStack.GetVar(pVar->defn.data.offset)->integer=value;
		}
		else if (pTargetType->Base() == pCharType) 
		{
			char value = Pop()->character;
			RangeCheck(pTargetType, value);

			//--character := character
			pTarget->character = value;
		}
		else 
		{
			void *pSource = Pop()->address;

			//--array  := array
			//--record := record
			memcpy(pTarget, pSource, pTargetType->size);
		}
		cout<<"var = "<<pVar->String()<<"\t\tvalue = "<<runStack.GetVar(pVar->defn.data.offset)->integer<<endl;
		//TraceDataStore(pTargetId, pTarget, pTargetType);			//para debug depois pode sair
	}
	else
	{
		//current location
		int *pCurrentLocation= new int;
		Push((void*)pCurrentLocation);

		//guardamos o ptr para a funcao a retornar
		Push((void*)pCurrentFunction);
		
		runStack.PushFrameHeader();							//criamos uma frame para a nova funcao
		
		GetToken();											//primeiro token da expressao dos parametros
		while (token != tcRParen)
		{
			TType *pExprType=ExecuteExpression();
			if(pExprType == pRealType){Push(Pop()->real);}
			else{Push(Pop()->integer);}
			if(token == tcComma){GetToken();}
		}

		runStack.ActivateFrame();					//ja esta tudo a postos ativamos a frame

		GetToken();
		
		//guardamos a localizašao do icode
		*pCurrentLocation=icode.CurrentLocation();
		//chama a nova funcao
		ExecuteFunction(pVar);
	}
}

void TExecutor::ExecuteIF(void)
{
	GetToken();		//(
	GetToken();		//primeiro token da expressao
	ExecuteExpression();
	GetToken();		
	GetToken();	

	if(runStack.Pop()->integer)
	{
		while(token != tcRBracket)
		{
			ExecuteStatement();
			if(token != tcRBracket){GetToken();}				//para resolver chamadas de funcao
		} 
		GetToken();
		if(token == tcElse)
		{
			int level=1;
			GetToken();		//{
			while(level>0)
			{
				GetToken();
				if(token == tcLBracket){level++;}
				if(token == tcRBracket){level--;}
			}
			GetToken();
		}
	}
	else
	{
		int level=1;
		while(level>0)
		{
			GetToken();
			if(token == tcLBracket){level++;}
			if(token == tcRBracket){level--;}
		}
		GetToken();			//else
		if(token == tcElse)
		{
			GetToken();			//{
			GetToken();			//primeiro statment

			while(token != tcRBracket)
			{
				ExecuteStatement();
				GetToken();
			}
			GetToken();
		}
	}
}


void TExecutor::ExecuteWHILE(void)
{
	int label=icode.CurrentLocation();
	int label2=-1;

	GetToken();		//(
	GetToken();		//primeiro token da expressao
	ExecuteExpression();
	
	while (runStack.Pop()->integer)
	{
		GetToken();		//)
		GetToken();		//{
		//GetToken();		//primeiro token de statments

		while(token != tcRBracket)
		{
			ExecuteStatement();
			GetToken();
		}
		label2=icode.CurrentLocation();
		icode.GoTo(label);

		GetToken();		//(
		GetToken();		//primeiro token da expressao
		ExecuteExpression();
	}

	if(label2 > 0)
	{
		icode.GoTo(label2);
		GetToken();
	}
	else
	{
		int level=1;
		GetToken();		//)
		GetToken();		//{
		while(level>0)
		{
			GetToken();
			if(token == tcLBracket){level++;}
			if(token == tcRBracket){level--;}
		}
		GetToken();
	}
}





void TExecutor::ExecuteFOR(void)
{
	
	int label_inicio;
	int label_inc;
	int label_fim=-1;
	GetToken();
	GetToken();
	
	label_inicio=icode.CurrentLocation();
	ExecuteStatement();
	GetToken();
	ExecuteExpression();
	
	label_inc=icode.CurrentLocation();
	
	while (token != tcLBracket)
	{
		GetToken();
	}
	GetToken();
	
	while (Pop()->integer)
	{
		while (token != tcRBracket)
		{
			ExecuteStatement();
			GetToken();
		}
		
		label_fim=icode.CurrentLocation();

		icode.GoTo(label_inc);
		GetToken();
		
		ExecuteAssignment(symtabStack.SearchAll(pToken->String()));
		
		icode.GoTo(label_inicio);
		while(token != tcSemicolon)
		{
			GetToken();
		}
		GetToken();
		ExecuteExpression();

		while (token != tcLBracket)
		{
			GetToken();
		}
		GetToken();
	}

	if(label_fim > 0)
	{
		icode.GoTo(label_fim);
		GetToken();
	}
	else
	{
		int level=1;
		
		icode.GoTo(label_inc);
		while (token != tcLBracket)
		{
			GetToken();
		}
		GetToken();

		while(level>0)
		{
			GetToken();
			if(token == tcLBracket){level++;}
			if(token == tcRBracket){level--;}
		}
		GetToken();
	}
}




void TExecutor::ExecuteSWITCH(void)
{
	int acabou=1;
	GetToken();
	GetToken();
	
	ExecuteExpression();
	int caso=Pop()->integer;
	
	while(token != tcLBracket){GetToken();}
	GetToken();
	
	do
	{
		GetToken();
		ExecuteExpression();
		GetToken();
		if(caso == Pop()->integer)
		{
			while(token == tcCase){GetToken(); GetToken(); GetToken();}
			while(token != tcBreak && token != tcRBracket && token != tcCase)
			{
				ExecuteStatement();
				GetToken();
				acabou=0;
			}
		}
		else
		{
			//fazer saltar o case
			int caase=1;
	
			while(caase > 0 && (token != tcRBracket && token != tcDefault))
			{
				GetToken();
				if(token == tcCase){caase--;}
				if(token == tcSwitch)
				{
					while(token != tcLBracket){GetToken();}
					GetToken();
					int level=1;
					while(level>0)
					{
						GetToken();
						if(token == tcLBracket){level++;}
						if(token == tcRBracket){level--;}
					}
					GetToken();
				}
			}
		}
		
	}while(acabou  && (token != tcRBracket  && token != tcDefault));
	
	if(token == tcDefault)
	{
		GetToken();
		GetToken(); 
		while (token != tcRBracket)
		{
			ExecuteStatement();
			GetToken();
		}
		
	}
	
	if(token != tcRBracket)
	{
		int level=1;
		while(level>0)
		{
			GetToken();
			if(token == tcLBracket){level++;}
			if(token == tcRBracket){level--;}
		}
		GetToken();
	}
	else
	{
		GetToken();
	}
}




//--------------------------------------------------------------
//  ExecuteStatementList        Execute a statement list until
//                              the terminator token.
//
//      terminator : the token that terminates the list
//--------------------------------------------------------------

void TExecutor::ExecuteStatementList(TTokenCode terminator)
{
    //--Loop to execute statements and skip semicolons.
    GetToken();
	do 
	{
		ExecuteStatement();
		while (token == tcSemicolon) {GetToken();}
    } while (token != terminator);
}


void TExecutor::ExecuteRETURN(void)
{
	GetToken();
	if(token != tcSemicolon)
	{
		TSymtabNode *retValue=symtabStack.SearchAll(pToken->String());
		if(retValue->pType == pRealType){functionReturnValue.real=retValue->defn.data.value.real;}
		else if(retValue->pType == pIntegerType)
		{
			functionReturnValue.integer=retValue->defn.data.value.integer;
		}
		GetToken();
	}

	while(token != tcEndOfFile){GetToken();}
}