#ifndef __TYPES_H__
#define __TYPES_H__

#include "stdafx.h"


//--Pointers to predefined types.
extern TType *pIntegerType;
extern TType *pUnsIntegerType;
extern TType *pShortIntegerType;
extern TType *pUnsShortIntegerType;
extern TType *pLongIntegerType;
extern TType *pUnsLongIntegerType;

extern TType *pRealType;
extern TType *pBooleanType;
extern TType *pVoidType;

extern TType *pCharType;
extern TType *pUnsCharType;

extern TType *pDummyType;

extern TType *pStructs[1024];

//--------------------------------------------------------------
//  TFormCode           Form code: What form of data structure.
//--------------------------------------------------------------

enum TFormCode {
    fcNone, fcScalar, fcEnum, fcSubrange, fcArray, fcRecord,fcStruct
};

extern char *formStrings[];

//--------------------------------------------------------------
//  TType               Type class.
//--------------------------------------------------------------

class TType {
    int refCount;           // reference count

public:
    TFormCode    form;      // form code
    int          size;      // byte size of type
    TSymtabNode *pTypeId;   // ptr to symtab node of type identifier

    union {

	//--Enumeration
	struct {
	    TSymtabNode *pConstIds;  // ptr to list of const id nodes
	    int          max;        // max constant value
	} enumeration;

	//--Subrange
	struct {
	    TType *pBaseType;  // ptr to base type object
	    int    min, max;   // min and max subrange limit values
	} subrange;

	//--Array
	struct {
	    TType *pIndexType;          // ptr to index type object
	    TType *pElmtType;           // ptr to elmt  type object
	    int    minIndex, maxIndex;  // min and max index values
	    int    elmtCount;           // count of array elmts
	} array;

	//--Record
	struct {
	    TSymtab *pSymtab;  // ptr to record fields symtab
	} record;
    };

    //--General and string type constructors.
    TType(TFormCode fc, int s, TSymtabNode *pId);
    TType(int length);

   ~TType(void);

    int IsScalar(void) const { return (form != fcArray) &&
    				      (form != fcRecord); }

    TType *Base(void) const
    {
		return form == fcSubrange ? subrange.pBaseType : (TType *) this;
    }

    enum TVerbosityCode {vcVerbose, vcTerse};

    void PrintTypeSpec    (TVerbosityCode vc) const;
    void PrintEnumType    (TVerbosityCode vc) const;
    void PrintSubrangeType(TVerbosityCode vc) const;
    void PrintArrayType   (TVerbosityCode vc) const;
    void PrintRecordType  (TVerbosityCode vc) const;
	void PrintStructType  (TVerbosityCode vc) const;

    friend TType *SetType   (TType *&pTargetType, TType *pSourceType);
    friend void   RemoveType(TType *&pType);

    friend void CheckRelOpOperands(const TType *pType1, const TType *pType2);
    friend void CheckIntegerOrReal(const TType *pType1, const TType *pType2 = NULL);
    friend void CheckBoolean      (const TType *pType1, const TType *pType2 = NULL);
    friend void CheckAssignmentTypeCompatible(const TType *pTargetType, const TType *pValueType, TErrorCode ec);

    friend int  IntegerOperands(const TType *pType1, const TType *pType2);
    friend int  RealOperands   (const TType *pType1, const TType *pType2);
};

void InitializePredefinedTypes(TSymtab *pSymtab);
void RemovePredefinedTypes(void);




#endif