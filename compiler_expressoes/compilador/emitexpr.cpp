#include "stdafx.h"


TType *TCodeGenerator::EmitExpression(void)
{
	TTokenCode op;

	EmitLogicExpression();

	while(TokenIn(token,tlLogicOps))
	{
		op=token;
		GetToken();
		EmitLogicExpression();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();
		
	}

	return NULL;
}

TType *TCodeGenerator::EmitLogicExpression(void)
{
	TTokenCode op;

	EmitSimpleExpression();

	while(TokenIn(token,tlRelOps))
	{
		op=token;
		GetToken();
		EmitSimpleExpression();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		
	}

	return NULL;
}

TType *TCodeGenerator::EmitSimpleExpression(void)
{
	TTokenCode  op;                // operator
	
	EmitTerm();
	
	while(TokenIn(token,tlAddOps))
	{
		op=token;
		GetToken();
		
		EmitTerm();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		switch (op)
		{
		case tcPlus:
			{
				//Necess�rio criar s�mbolo gen�rico global para fazer com que se possa eliminar todos os s�mbolos temporarios.
			TSymtabNode *temp_symbol = symtabStack.SearchAll("temp_reg");
			if(!temp_symbol) {
				temp_symbol = globalSymtab.Enter("temp_reg");
			}
			TRegister *reg = regFile.GetRegister(temp_symbol);

			//assumindo que 2 variaveis... (depois testar op1.token == tcIdentifier)) - existirao 4 aqui: var-num, num-var, num-num, var-var
			Emit1(ADD, reg->GetReg(), op1.reg->GetReg(), op2.reg->GetReg());

			//registo � tcIdentifier, numero � tcnumber
			genStack.Push(tcIdentifier, reg);

			//regFile.FreeRegister(temp_symbol); -> depois de assign apagar todos os temporarios.
			regFile.PrintRegFile(); //DEBUG
			}
			break;

		default:
			break;
		}
		cout << pToken->String() << endl;
	}

	return NULL;
}

TType *TCodeGenerator::EmitTerm(void)
{
	TTokenCode op;

	EmitFactor();

	while(TokenIn(token,tlMulOps))
	{
		//nao pode chegar aqui pk o processador nao tem instru�oes para 
		//resolver operadores multiplica�ao divisao
		op=token;
		GetToken();

		EmitFactor();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		

	}


	return NULL;
}

TType *TCodeGenerator::EmitFactor(void)
{
	switch(token)
	{
	case tcIdentifier:{
		TSymtabNode *var = symtabStack.SearchAll(pToken->String());
		GetToken();
		if(token == tcPlusPlus) {
			postStack.Push(tcPlusPlus, var);
			GetToken();
		}else if(token == tcMinusMinus) {
			postStack.Push(tcMinusMinus, var);
			GetToken();
		}
		EmitVariable(var);
		
		}break;

	case tcNumber:{
		
		}break;

	case tcLParen:
		
		break;

	case tcPlusPlus:{
		GetToken();
		if(token == tcIdentifier) {
			TSymtabNode *var = symtabStack.SearchAll(pToken->String());
			//ve se a variavel ja esta nos registos
			TRegister *reg=regFile.FindRegister(var);
			//adiciona se nao estiver	
			if(!reg){reg=regFile.GetRegister(var); }
			Emit2(ADDI,reg->GetReg(),reg->GetReg(),1);
			EmitVariable(var);
			GetToken();
		}
		}break;

	case tcMinusMinus:{
		GetToken();
		if(token == tcIdentifier) {
			TSymtabNode *var = symtabStack.SearchAll(pToken->String());
			//ve se a variavel ja esta nos registos
			TRegister *reg=regFile.FindRegister(var);
			//adiciona se nao estiver	
			if(!reg){reg=regFile.GetRegister(var); }
			Emit2(SUBI,reg->GetReg(),reg->GetReg(),1);
			EmitVariable(var);
			GetToken();
		}
		}break;

	case tcStar:{
		
		}break;

	case tcAnd:				//passar endere�o de uma variavel

		break;

	case tcNot:{
		

		}break;

	case tcTil:{
			
		}break;
	}

	return NULL;
}


TType *TCodeGenerator::EmitVariable(TSymtabNode *pId)
{
	//ve se a variavel ja esta nos registos
	TRegister *reg=regFile.FindRegister(pId);
	//adiciona se nao estiver	
	if(!reg){reg=regFile.GetRegister(pId); }			

	genStack.Push(token,reg);
	
	return NULL;
}

TType *TCodeGenerator::EmitConstant(TSymtabNode *pId)
{	
	genStack.Push(token,pId->defn.constant.value.integer);

	return NULL;
}






TType *TCodeGenerator::EmitMultiplication(ExprStackData op1, ExprStackData op2)
{
	

			return NULL;
}


TType *TCodeGenerator::EmitDivision(ExprStackData op1, ExprStackData op2)
{
	

				return NULL;
}







