#include "stdafx.h"


//--------------------------------------------------------------
//  EmitStatement       Emit code for a statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitStatement(void)
{
    switch (token) 
	{
		case tcStar:		EmitAssignPtr();		break;
		case tcIdentifier:	EmitAssignment(NULL);	break;
		case tcIf:			EmitIF();				break;
		case tcWhile:		EmitWHILE();			break;
		case tcReturn:		EmitReturn();			break;
    }
}


//--------------------------------------------------------------
//  EmitAssignment      Emit code for an assignment statement for a ptr
//--------------------------------------------------------------
void TCodeGenerator::EmitAssignPtr(void)
{

}


//--------------------------------------------------------------
//  EmitAssignment      Emit code for an assignment statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitAssignment(const TSymtabNode *pTargetId)
{	
	TSymtabNode *var=symtabStack.SearchAll(pToken->String());
	
	TRegister *reg=regFile.FindRegister(var);
	if(!reg){reg=regFile.GetRegister(var);}
	
	GetToken();

	switch (token)
	{
		case tcEq:{
			GetToken();
			EmitExpression();
			
			ExprStackData result=genStack.Pop();

			if(result.token == tcNumber)														//se for a atribuicao direta de um numero
			{
				Emit2(MOVI,reg->GetReg(),0,result.val.integer);
			}
			else																				//sempre que haja uma expressao entra aqui
			{
				if(reg->GetReg() != result.reg->GetReg())
				{
					Emit1(MOV,reg->GetReg(),result.reg->GetReg(),0);
				}
			}

			}break;

		case tcPlusEqual:{
			GetToken();
			EmitExpression();
			
			ExprStackData result=genStack.Pop();

			if(result.token == tcNumber)														//se for a atribuicao direta de um numero
			{
				Emit2(ADDI,reg->GetReg(),reg->GetReg(),result.val.integer);
			}
			else																				//sempre que haja uma expressao entra aqui
			{
				if(reg->GetReg() != result.reg->GetReg())
				{
					Emit1(ADD,reg->GetReg(),reg->GetReg(),result.reg->GetReg());
				}
			}
			}break;

		case tcMinusEqual:{
			
			}break;

		case tcOrEqual:{
			
			}break;

		case tcAndEqual:{
			
			}break;

		case tcXorEqual:{

			}break;

		case tcStarEqual:																	//*=
			{
			}
			break;

		case tcSlashEqual:{
			
			}break;

		case tcPlusPlus:{
			Emit2(ADDI,reg->GetReg(),reg->GetReg(),1);
			GetToken();
			}break;

		case tcMinusMinus:
			Emit2(SUBI,reg->GetReg(),reg->GetReg(),1);
			GetToken();
			break;
	}

	while(postStack.Elements()) {
		ExprStackData post = postStack.Pop();
		TRegister *reg_post=regFile.FindRegister(post.identifier);
		if(!reg_post){
			reg_post=regFile.GetRegister(post.identifier);
		}
		
		if(post.token == tcPlusPlus) {
			Emit2(ADDI,reg_post->GetReg(),reg_post->GetReg(),1);
		} else if(post.token == tcMinusMinus) {
			Emit2(SUBI,reg_post->GetReg(),reg_post->GetReg(),1);
		}
	}
	GetToken();
}


//--------------------------------------------------------------
//  EmitIf      Emit code for an if statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitIF(void)
{
	
}


//--------------------------------------------------------------
//  EmitIf      Emit code for an if statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitWHILE(void)
{
	
}


//--------------------------------------------------------------
//  EmitReturn      Emit code for a return statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitReturn(void)
{
	//para teste ha mais cenas a fazer aqui
	while(token != tcEndOfFile){GetToken();}
}