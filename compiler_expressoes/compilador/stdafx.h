// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <iostream>
#include <fstream>

//
#include "defines.h"

#include "symtab.h"
#include "error.h"
#include "types.h"

#include "buffer.h"
#include "misc.h"
#include "common.h"

#include "token.h"
#include "scanner.h"

#include "complist.h"
#include "icode.h"
#include "parser.h"

#include "machine.h"

#include "backend.h"
#include "exec.h"
#include "codegen.h"


// TODO: reference additional headers your program requires here
