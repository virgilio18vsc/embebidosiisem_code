#include "stdafx.h"


//***********************************************************************
//		vetor que guarda as instruçoes geradas sem o saltos resolidos
unsigned long int CodeMem[MEM_CODE_SIZE];
int PC=0;

void TJmpList::Push(int location,TSymtabNode* Func)
{
	tos->setPos(location);
	tos->setFunc(Func);
	tos++;
}

void TJmpList::ResolveJumps(void)
{
	tos--;
	while(tos != &stack[-1])
	{
		CodeMem[tos->getPos()] += tos->getFunc()->defn.routine.startAddr; 
		tos--;
	}
}

void TJmpList::ResolveInterrupts(void)
{
	TSymtabNode *ISR;
	PC=1;

	ISR=globalSymtab.Search("TIMER_ISR_vect");
	if(ISR){Emit2(BRAI,0,8,ISR->defn.routine.startAddr);}else{PC++;}

	ISR=globalSymtab.Search("DUMMY_ISR_vect");
	if(ISR){Emit2(BRAI,0,8,ISR->defn.routine.startAddr);}else{PC++;}

	ISR=globalSymtab.Search("EXTINT_ISR_vect");
	if(ISR){Emit2(BRAI,0,8,ISR->defn.routine.startAddr);}else{PC++;}
	
}

TSymtabNode* TJmpList::Find(char* str)
{
	TJmps *aux=tos-1;
	TSymtabNode *lib=globalSymtab.Search(str);
	while(aux != &stack[-1])
	{
		if(aux->getFunc() == lib){return lib;}
		aux--;
	}
	return NULL;
}


