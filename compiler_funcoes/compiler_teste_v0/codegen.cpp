#include "stdafx.h"

TRegister::TRegister(int Reg, TSymtabNode* Symbol)
{
	reg=Reg;
	symbol=Symbol;
}

int TRegister::GetReg(void)
{
	return reg;
}

TSymtabNode* TRegister::GetSymbolAssociated(void)
{
	return symbol;
}

void TRegister::SetSymbol(TSymtabNode* Symbol)
{
	symbol=Symbol;
}


//**********************************************************************
//			register file
TRegisterFile::TRegisterFile(void)
{
	for(int i=0;i<REG_NUMBER;i++)
	{
		registos[i]=new TRegister(i,NULL);
	}
	registos[0]->SetSymbol((TSymtabNode*)0x12345);
	registos[GLOBAL_VAR_REG]->SetSymbol((TSymtabNode*)0x12345);
	registos[FUNCTION_RETURN_REG]->SetSymbol((TSymtabNode*)0x12345);
	registos[FRAME_BASE_REG]->SetSymbol((TSymtabNode*)0x12345);
	registos[STACK_POINTER]->SetSymbol((TSymtabNode*)0x12345);
}

TRegister* TRegisterFile::GetRegister(TSymtabNode* var)
{
	for(int i=1;i<REG_NUMBER;i++)
	{
		if(registos[i]->GetSymbolAssociated()==NULL)
		{
			registos[i]->SetSymbol(var);
			return registos[i];
		}
	}

	return NULL;
}

void TRegisterFile::FreeRegister(TSymtabNode* var)
{
	for(int i=0;i<REG_NUMBER;i++)
	{
		if(registos[i]->GetSymbolAssociated()==var)
		{
			registos[i]->SetSymbol(NULL);
		}
	}
}

void TRegisterFile::FreeRegister(int reg)
{
	registos[reg]->SetSymbol(NULL);
}


void TRegisterFile::CleanRegFile(void)
{
	for(int i=1;i<REG_NUMBER-1;i++)
	{
		if(i != GLOBAL_VAR_REG && i != FUNCTION_RETURN_REG && i != FRAME_BASE_REG)
		{
			registos[i]->SetSymbol(NULL);
		}
	}
}


TRegister* TRegisterFile::FindRegister(TSymtabNode* var)
{
	for(int i=0;i<REG_NUMBER;i++)
	{
		if(registos[i]->GetSymbolAssociated()==var)
		{
			return registos[i];
		}
	}

	return NULL;
}

TRegister* TRegisterFile::FindRegister(int reg)
{
	return registos[reg];
}



void TRegisterFile::PrintRegFile(void)
{
	for(int i=1;i<REG_NUMBER-1;i++)
	{
		if(i != 14 && i != 15 && i != 16 && registos[i]->GetSymbolAssociated() != NULL)
		{
			cout<<"reg = r"<<registos[i]->GetReg();
			cout<<"\t\tvar = "<<registos[i]->GetSymbolAssociated()->String()<<endl;
		}
	}
}





bool TCodeGenerator::CheckForISR(TSymtabNode *pId)
{
	TISRName *ptr;

	for(ptr=ISRList;ptr->Name;ptr++)
	{
		if(strcmp(pId->String(),ptr->Name) == 0)
		{
			return true;
		}
	}

	return false;
}