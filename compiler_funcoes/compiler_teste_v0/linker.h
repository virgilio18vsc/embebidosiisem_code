#ifndef __LINKER_H__
#define __LINKER_H__

#include "stdafx.h"

extern unsigned long int CodeMem[];
extern int PC;

class TJmps
{
	int position;
	TSymtabNode *func;

public:
	void setPos(int pos){position=pos;}
	void setFunc(TSymtabNode* Func){func=Func;}

	int getPos(void){return position;}
	TSymtabNode* getFunc(void){return func;}
};

class TJmpList
{
	TJmps stack[128];
	TJmps *tos;

public:
	TJmpList(void){tos=&stack[0];}
	~TJmpList(void){delete[] stack;}

	void Push(int location,TSymtabNode* Func);
	TSymtabNode* Find(char* str);
	void ResolveJumps(void);
	void ResolveInterrupts(void);
};

#endif

