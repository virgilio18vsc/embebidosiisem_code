#ifndef __COMPLIST_H__
#define __COMPLIST_H__

#include "stdafx.h"

const int maxCompactTextLength = 72;

class TCompactListBuffer : public TTextOutBuffer
{
	int textLength;
	char *pText;

public:
	TCompactListBuffer(void)
	{
		pText=text;
		*pText='\0';
		textLength=0;
	}

	void PutBlank(void);
	void Put(const char *pString);
	void PutLine(void);
};


#endif