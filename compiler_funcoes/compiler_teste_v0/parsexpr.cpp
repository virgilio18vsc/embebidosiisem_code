#include "stdafx.h"

void TParser::ParseExpression(void)
{
	ParseLogicalExpression();

	while(TokenIn(token,tlRelOps))
	{
		GetTokenAppend();
		ParseLogicalExpression();
	}
}

void TParser::ParseLogicalExpression(void)
{
	ParseSimpleExpression();

	while(TokenIn(token,tlLogicOps))
	{
		GetTokenAppend();
		ParseLogicalExpression();
	}
}

void TParser::ParseSimpleExpression(void)
{
	//--Unary + or -
	if (token == tcPlus || token == tcMinus) {GetTokenAppend();}

    //--Parse the first term.
    ParseTerm();
    //--Loop to parse subsequent additive operators and terms.
    while (TokenIn(token, tlAddOps)) 
	{
		GetTokenAppend();
		ParseTerm();
    }
}

void TParser::ParseTerm(void)
{
	//--Parse the first factor.
    ParseFactor();
    //--Loop to parse subsequent multiplicative operators and factors.
	while (TokenIn(token, tlMulOps)) 
	{
		GetTokenAppend();
		ParseFactor();
    }
}


void TParser::ParseFactor(void)
{
	TSymtabNode *pNode;
	switch(token)
	{
	case tcAnd:
		GetTokenAppend();

	case tcIdentifier:
		pNode = SearchAll(pToken->String());
	    if (pNode) 
		{
			if(pNode->defn.how == dcVariable || pNode->defn.how == dcField || pNode->defn.how == dcPointer)
			{
				icode.Put(pNode); 
				GetTokenAppend();
				if(token == tcPlusPlus || token == tcMinusMinus)
				{
					GetTokenAppend();
				}

				if(token == tcPeriod || token == tcArrow)
				{
					GetTokenAppend();
					TSymtabNode *campo=pNode->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
					icode.Put(campo);
					if(!campo){ Error(errIdentifierNotFound); }
					GetTokenAppend();
				}
			}
			else if (pNode->defn.how == dcFunction)
			{
				icode.Put(pNode);
				GetTokenAppend(); GetTokenAppend();
				while (token != tcRParen)
				{
					ParseExpression();
					if(token != tcRParen){GetTokenAppend();}				
				}
				GetTokenAppend();
			}
			else if(pNode->defn.how == dcArray)
			{
				icode.Put(pNode);
				GetTokenAppend();
				if(token == tcLSquareBracket)
				{
					GetTokenAppend();
					ParseExpression();
					GetTokenAppend();
				}
			}
			else{Error(errUnexpectedToken);}
		}
		else 
		{ Error(errUndefinedIdentifier); }

		break;

	case tcStar:
				do
				{
					GetTokenAppend();
				}while(token == tcStar);

				pNode = SearchAll(pToken->String());
				icode.Put(pNode);
				GetTokenAppend();
		break;

	case tcNumber:
		 pNode = SearchAll(pToken->String());
	    if (!pNode) 
		{
			pNode = EnterLocal(pToken->String());
			pToken->Type() == tyInteger ? pNode->defn.constant.value.integer= pToken->Value().integer : pNode->defn.constant.value.real=pToken->Value().real;
			pNode->pType = (pToken->Type() == tyInteger ? pIntegerType : pRealType);
	    }

	    icode.Put(pNode);

	    GetTokenAppend();
		break;

	case tcString:
	    GetTokenAppend();
	    break;

	case tcNot:
	    GetTokenAppend();
	    ParseFactor();
	    break;

	case tcTil:
		GetTokenAppend();
		ParseFactor();
		break;

	case tcPlusPlus:
		GetTokenAppend();
		ParseFactor();
		break;

	case tcMinusMinus:
		GetTokenAppend();
		ParseFactor();
		break;

	case tcLParen:
		GetTokenAppend();
		ParseExpression();

		if(token == tcRParen){GetTokenAppend();}
		else
		{
			Error(errMissingRightParen);
		}
		break;

	default:
		Error(errInvalidExpression);
		break;
	}
}

