#include "stdafx.h"

//**************************************************************************************************************************************************
//		LOGIC compare ops
void TCodeGenerator::EmitIntLogicAnd(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntLogicOr(ExprStackData op1, ExprStackData op2)
{
	
}

//**************************************************************************************************************************************************
//		COMPARE ops

void TCodeGenerator::EmitIntEqual(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntNotEqual(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntGreatThan(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntGreatEqual(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntLessThan(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntLessEqual(ExprStackData op1, ExprStackData op2)
{
	
}

//**************************************************************************************************************************************************
//		ADD ops
void TCodeGenerator::EmitIntADD(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntSUB(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntLShift(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntRShift(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntOR(ExprStackData op1, ExprStackData op2)
{
	
}



void TCodeGenerator::EmitIntXOR(ExprStackData op1, ExprStackData op2)
{
	
}


//**********************************************************************************************************************************************
//		MUL ops

void TCodeGenerator::EmitIntAND(ExprStackData op1, ExprStackData op2)
{
	
}



void TCodeGenerator::EmitIntMultiplication(ExprStackData op1, ExprStackData op2)
{

}


void TCodeGenerator::EmitIntDivision(ExprStackData op1, ExprStackData op2)
{
	
}


void TCodeGenerator::EmitIntDivisionModule(ExprStackData op1, ExprStackData op2)
{
	
}


//*******************************************************************************************************************************************
//		UNARY ops
void TCodeGenerator::EmitIntLogicNot(void)
{
	
}

void TCodeGenerator::EmitIntBitNot(void)
{
	
}



