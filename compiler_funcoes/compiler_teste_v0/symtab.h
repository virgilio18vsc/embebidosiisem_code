#ifndef __SYMTAB_H__
#define __SYMTAB_H__

#include "stdafx.h"
#include "misc.h"

extern int currentNestingLevel;
extern int xrefFlag;
extern int currentLineNumber;
extern int asmLabelIndex;

class TLineNumList;

class TSymtab;
class TSymtabNode;
class TIcode;
class TType;



//***************************************************************************************************************
//--------------------------------------------------------------
//  TDefnCode           Definition code: How an identifier
//                                       is defined.
//--------------------------------------------------------------

enum TDefnCode {
    dcUndefined,
	dcArray,
    dcConstant, dcType, dcVariable, dcField,
    dcValueParm, dcVarParm,
    dcProgram, dcProcedure, dcFunction,
	dcPointer,

	dcRegister,
};

//--------------------------------------------------------------
//  TRoutineCode        Routine code: For procedures, functions,
//                                    and standard routines.
//--------------------------------------------------------------

enum TRoutineCode {
    rcDeclared, rcForward,
    rcRead, rcReadln, rcWrite, rcWriteln,
    rcAbs, rcArctan, rcChr, rcCos, rcEof, rcEoln,
    rcExp, rcLn, rcOdd, rcOrd, rcPred, rcRound,
    rcSin, rcSqr, rcSqrt, rcSucc, rcTrunc,
};

//--------------------------------------------------------------
//  TLocalIds           Local identifier lists structure.
//--------------------------------------------------------------

struct TLocalIds {
    TSymtabNode *pParmIds;      // ptr to local parm id list
    TSymtabNode *pConstantIds;  // ptr to local constant id list
    TSymtabNode *pTypeIds;      // ptr to local type id list
    TSymtabNode *pVariableIds;  // ptr to local variable id list
    TSymtabNode *pRoutineIds;   // ptr to local proc and func id list
};

//--------------------------------------------------------------
//  TDefn               Definition class.
//--------------------------------------------------------------

class TDefn {

public:
    TDefnCode how;  // the identifier was defined

	struct{
		int			  id;				//id to map with type
		int           parmCount;       // count of parameters
		int           totalParmSize;   // total byte size of parms
		TSymtab      *pSymtab;         // ptr to local symtab
		int			  ptrIsGlobal;		//1 se a variavel para a qual estamos a apontar e global
	}Struct;

	//--Constant
	struct {
		TDataValue value;  // value of constant
	} constant;

	//--Procedure, function, or standard routine
	struct {
	    int           parmCount;       // count of parameters
	    int           totalParmSize;   // total byte size of parms
	    int           totalLocalSize;  // total byte size of locals
	    TLocalIds     locals;          // local identifiers
	    TSymtab      *pSymtab;         // ptr to local symtab
	    TIcode       *pIcode;          // ptr to routine's icode
		TType		 *returnType;	   // ptr to return type
		TSymtabNode  **vSymtab;		   // symtab in vector form
		int			  startAddr;	   // endere�o de inicio da routina, este valor vai ser usado pelo linker para resolver os saltos
	} routine;

	//--Variable, record field, parameter, pointer
	struct {
		TDataValue value;		//valor atual da variavel/apontador util para o interpretador
	    int offset;  // vars and parms: sequence count
			 // fields: byte offset in record
		int pointerLevel;
	} data;
   
	struct{
		int offset;
		int elemCount;
	}vetor;

    TDefn(TDefnCode dc) { how = dc; }
   ~TDefn(void);
};
//***************************************************************************************************************

//tabela de simbolos no
class TSymtabNode
{
	TSymtabNode *left, *right;			//apontadores da arvore
	char *pString;						//string do simbolo
	short xSymtab;						//indice da tabela de simbolos	(talvez para usar tipo hash table)
	short xNode;						//indice do no
	TLineNumList *pLineNumList;			//apontador para a lista com os numeros das linhas

	friend class TSymtab;

public:
	float value; //temporary

	TSymtabNode *next;   // ptr to next sibling in chain
    TType       *pType;  // ptr to type info

    TDefn defn;          // definition info
    int   level;         // nesting level
    int   labelIndex;    // index for code label

	TSymtabNode(const char *pString, TDefnCode dc = dcUndefined);
   ~TSymtabNode(void);

	TSymtabNode *LeftSubTree	(void)const {return left;	}
	TSymtabNode *RightSubTree	(void)const	{return right;	}
	char		*String			(void)const	{return pString;}
	short		SymtabIndex		(void)const	{return xSymtab;}
	short		NodeIndex		(void)const {return xNode;	}
	void Convert(TSymtabNode *vpNodes[]);

	void Print(void)const;
	void PrintIdentifier(void) const;
    void PrintConstant  (void) const;
    void PrintVarOrField(void) const;
    void PrintType      (void) const;
	void PrintFunc		(void) const;
	void PrintArray		(void) const;
};


//tabela de simbolos arvore
class TSymtab
{
	TSymtabNode *root;				//apontador para a raiz da arvore
	TSymtabNode **vpNodes;
	short		cntNodes;			//contem o numero de nos
	short		xSymtab;			//indice da actual tabela de symbolos
	TSymtab		*next;

public:
	TSymtab()
	{
		extern int cntSymtabs;
		extern TSymtab *pSymtabList;

		root=NULL;
		vpNodes=NULL;
		cntNodes=0;
		xSymtab=cntSymtabs++;

		next=pSymtabList;
		pSymtabList=this;
	}

	~TSymtab()
	{
		delete root;
		delete[] vpNodes;
	}

	TSymtabNode  *Search(const char *pString)const;
	TSymtabNode  *SearchParam(int pos);
	TSymtabNode  *Enter   (const char *pString, TDefnCode dc = dcUndefined);
    TSymtabNode  *EnterNew(const char *pString, TDefnCode dc = dcUndefined);

	TSymtabNode *Root(void) const{return root;}
	TSymtabNode *Get(short xNode) const {return vpNodes[xNode];}
	TSymtab *Next(void)const {return next;}
	TSymtabNode **NodeVector(void) const {return vpNodes;}

	int NodeCount(void)const{return cntNodes;}
	void Print()const{root->Print();}

	void Convert(TSymtab *vpSymtabs[]);
};


//--------------------------------------------------------------
//  TSymtabStack      Symbol table stack class.
//--------------------------------------------------------------

class TSymtabStack {
    enum {maxNestingLevel = 8};

    TSymtab *pSymtabs[maxNestingLevel];  // stack of symbol table ptrs

public:
    TSymtabStack(void);
   ~TSymtabStack(void);

    TSymtabNode *SearchLocal(const char *pString)
    {
		return pSymtabs[currentNestingLevel]->Search(pString);
    }

    TSymtabNode *EnterLocal(const char *pString, TDefnCode dc = dcUndefined)
    {
		return pSymtabs[currentNestingLevel]->Enter(pString, dc);
    }

    TSymtabNode *EnterNewLocal(const char *pString, TDefnCode dc = dcUndefined)
    {
		return pSymtabs[currentNestingLevel]->EnterNew(pString, dc);
    }

    TSymtab *GetCurrentSymtab(void) const
    {
		return pSymtabs[currentNestingLevel];
    }

    void SetCurrentSymtab(TSymtab *pSymtab)
    {
		pSymtabs[currentNestingLevel] = pSymtab;
    }

	void PushSymtab(TSymtab *pSymtab)
	{
		pSymtabs[++currentNestingLevel] = pSymtab;
	}

	TSymtab *PopSymtab(void)
	{
		return pSymtabs[currentNestingLevel--];
	}

    TSymtabNode *SearchAll (const char *pString) const;
    TSymtabNode *Find      (const char *pString) const;
    void         EnterScope(void);
    TSymtab     *ExitScope (void);
};


//class para o numero de linhas
class TLineNumNode
{
	TLineNumNode *next;		//ptr para o proximo no
	const int number;		//numero da linha

	friend class TLineNumList;

public:
	TLineNumNode(void):number(currentLineNumber){next=NULL;}

};

//lista 
class TLineNumList
{
	TLineNumNode *head,*tail;

public:
	TLineNumList(void){head=tail=new TLineNumNode;}

	virtual ~TLineNumList(void);

	void Update(void);
	void Print(int newLineFlag,int indent)const;
};

#endif

