#include "stdafx.h"

int asmLabelIndex = 0;      // assembly label index
int xrefFlag = False;

//*****************************************************************************************************
//tdefine class

TDefn::~TDefn(void)
{
    switch (how) {

	case dcProgram:
	case dcProcedure:
	case dcFunction:

	    /*if (routine.which == rcDeclared) {
		delete routine.pSymtab;
		delete routine.pIcode;
	    }*/
	    break;

	default:  break;
    }
}

//*****************************************************************************************************

TSymtabNode::TSymtabNode(const char *pStr, TDefnCode dc) : defn(dc)
{
	left = right = next = NULL;
    pLineNumList = NULL;
    pType	 = NULL;
    xNode	 = 0;
    level	 = currentNestingLevel;
    labelIndex	 = ++asmLabelIndex;

    //--Allocate and copy the symbol string.
    pString = new char[strlen(pStr) + 1];
    strcpy(pString, pStr);

    //--If cross-referencing, update the line number list.
    if (xrefFlag) pLineNumList = new TLineNumList;
}

TSymtabNode::~TSymtabNode(void)
{
	void RemoveType(TType *&pType);

	delete left;
	delete right;

	delete[] pString;
	delete pLineNumList;

	RemoveType(pType);
}

void TSymtabNode::Convert(TSymtabNode *vpNodes[])
{
	if(left) left->Convert(vpNodes);
	vpNodes[xNode]=this;
	if(right)right->Convert(vpNodes);
}

void TSymtabNode::Print(void)const
{
	const int maxNamePrintWidth = 16;

	if(left){left->Print();}

	sprintf(list.text,"%*s",maxNamePrintWidth,pString);
	PrintIdentifier();
	if(pLineNumList)
	{
		pLineNumList->Print(strlen(pString) > maxNamePrintWidth,maxNamePrintWidth);
	}
	else
	{
		list.PutLine();
	}

	if(right)
	{
		right->Print();
	}
}


//--------------------------------------------------------------
//  PrintIdentifier         Print information about an
//                          identifier's definition and type.
//--------------------------------------------------------------

void TSymtabNode::PrintIdentifier(void) const
{
    switch (defn.how) {
	case dcConstant:    PrintConstant();    break;
	case dcType:        PrintType();        break;

	case dcFunction:
						PrintFunc();		break;
	case dcPointer:
	case dcVariable:
	case dcField:       PrintVarOrField();  break;

	case dcArray:		PrintArray();		break;
    }
}

//--------------------------------------------------------------
//  PrintConstant       Print information about a constant
//                      identifier for the cross-reference.
//--------------------------------------------------------------

void TSymtabNode::PrintConstant(void) const
{
    extern TListBuffer list;

    list.PutLine();
    list.PutLine("Defined constant");

    //--Value
    if ((pType == pIntegerType) || (pType->form == fcEnum)) 
	{
		sprintf(list.text, "Value = %d", defn.constant.value.integer);
    }
    else if (pType == pRealType) 
	{
		sprintf(list.text, "Value = %g", defn.constant.value.real);
    }
    else if (pType == pCharType) 
	{
		sprintf(list.text, "Value = '%c'", defn.constant.value.character);
    }
    else if (pType->form == fcArray) 
	{
		sprintf(list.text, "Value = '%s'", defn.constant.value.pString);
    }
    list.PutLine();

    //--Type information
    if (pType) pType->PrintTypeSpec(TType::vcTerse);
    list.PutLine();
}

//--------------------------------------------------------------
//  PrintVarOrField         Print information about a variable
//                          or record field identifier for the
//                          cross-reference.
//--------------------------------------------------------------

void TSymtabNode::PrintVarOrField(void) const
{
    extern TListBuffer list;

    list.PutLine();
	list.PutLine(defn.how == dcVariable ? "Declared variable": (defn.how == dcField ? "Declared record field" : "Declared pointer"));

    //--Type information
    if (pType) pType->PrintTypeSpec(TType::vcTerse);
    if ((defn.how == dcVariable) || (this->next)) list.PutLine();

	if(defn.how == dcVariable)
	{
		cout<<"Initial Value = "<<defn.constant.value.integer<<endl;
	}

	if(defn.how == dcField)
	{
		cout<<"Field position = "<<defn.data.offset<<endl;
	}

	if(defn.how == dcPointer)
	{
		cout<<"Pointer "<<endl;
		cout<<"Pointer level = "<<defn.data.pointerLevel<<endl;
		cout<<"Address = "<<defn.constant.value.integer<<endl;
	}
}

//--------------------------------------------------------------
//  PrintType           Print information about a type
//                      identifier for the cross-reference.
//--------------------------------------------------------------

void TSymtabNode::PrintType(void) const
{
    list.PutLine();
    list.PutLine("Defined type");
	
    if (pType) pType->PrintTypeSpec(TType::vcVerbose);
    list.PutLine();
}

void TSymtabNode::PrintFunc(void) const
{
	cout<<"**********************************************************************"<<endl;
	cout<<"FUNC INFO"<<endl;
	list.PutLine();
    list.PutLine("Function");

	sprintf(list.text, "Name = '%s'", pString);
	list.PutLine();
	sprintf(list.text, "Return type = '%d'", defn.routine.returnType);
	list.PutLine();
	defn.routine.pSymtab->Print();
	
	cout<<"**********************************************************************"<<endl;
}

void TSymtabNode::PrintArray(void) const
{
	cout<<"**********************************************************************"<<endl;
	cout<<"\t\tARRAY INFO"<<endl;
	sprintf(list.text, "Name = '%s'", pString);
	list.PutLine();

	cout<<"Array size = "<<pType->array.elmtCount<<endl;
	cout<<"**********************************************************************"<<endl;
}


TSymtabNode *TSymtab::Search(const char *pString) const
{
	TSymtabNode *pNode=root;
	int comp;

	while(pNode)
	{
		comp = strcmp(pString,pNode->pString);
		if(comp==0)break;		//encontrou

		pNode = comp < 0 ? pNode->left : pNode->right;
	}

	if(xrefFlag && (comp == 0)){pNode->pLineNumList->Update();}

	return pNode;
}



TSymtabNode *TSymtab::Enter(const char *pString, TDefnCode dc)
{
	TSymtabNode  *pNode;           // ptr to node
    TSymtabNode **ppNode = &root;  // ptr to ptr to node

    //--Loop to search table for insertion point.
    while ((pNode = *ppNode) != NULL) {
	int comp = strcmp(pString, pNode->pString);  // compare strings
	if (comp == 0) return pNode;                 // found!

	//--Not yet found:  next search left or right subtree.
	ppNode = comp < 0 ? &(pNode->left) : &(pNode->right);
    }

    //--Create and insert a new node.
    pNode = new TSymtabNode(pString, dc);  // create a new node,
    pNode->xSymtab = xSymtab;              // set its symtab and
    pNode->xNode   = cntNodes++;           // node indexes,
    *ppNode        = pNode;                // insert it, and
    return pNode;                          // return a ptr to it
}

TSymtabNode *TSymtab::EnterNew(const char *pString, TDefnCode dc)
{
    TSymtabNode *pNode = Search(pString);

    if (!pNode)  pNode = Enter(pString, dc);
    else         Error(errRedefinedIdentifier);

    return pNode;
}

void TSymtab::Convert(TSymtab *vpSymtabs[])
{
	vpSymtabs[xSymtab]=this;
	vpNodes=new TSymtabNode *[cntNodes];
	root->Convert(vpNodes);
}



//              ************************
//              *		       *
//              *  Symbol Table Stack  *
//              *		       *
//              ************************

//fig 8-6
//--------------------------------------------------------------
//  Constructor	    Initialize the global (level 0) symbol
//		    table, and set the others to NULL.
//--------------------------------------------------------------

TSymtabStack::TSymtabStack(void)
{
    extern TSymtab globalSymtab;
    void InitializeStandardRoutines(TSymtab *pSymtab);

    currentNestingLevel = 0;
    for (int i = 1; i < maxNestingLevel; ++i) pSymtabs[i] = NULL;

    //--Initialize the global nesting level.
    pSymtabs[0] = &globalSymtab;
    InitializePredefinedTypes (pSymtabs[0]);
}

//--------------------------------------------------------------
//  Destructor	    Remove the predefined types.
//--------------------------------------------------------------

TSymtabStack::~TSymtabStack(void)
{
    RemovePredefinedTypes();      
}

//--------------------------------------------------------------
//  SearchAll   Search the symbol table stack for the given
//              name string.
//
//      pString : ptr to name string to find
//
//  Return: ptr to symbol table node if found, else NULL
//--------------------------------------------------------------

TSymtabNode *TSymtabStack::SearchAll(const char *pString) const
{
    for (int i = currentNestingLevel; i >= 0; --i) 
	{
		TSymtabNode *pNode = pSymtabs[i]->Search(pString);
		if (pNode) return pNode;
    }

    return NULL;
}

//--------------------------------------------------------------
//  Find        Search the symbol table stack for the given
//              name string.  If the name is not already in
//              there, flag the undefined identifier error,
//		and then enter the name into the local symbol
//		table.
//
//      pString : ptr to name string to find
//
//  Return: ptr to symbol table node
//--------------------------------------------------------------

TSymtabNode *TSymtabStack::Find(const char *pString) const
{
    TSymtabNode *pNode = SearchAll(pString);

    if (!pNode) 
	{
		Error(errUndefinedIdentifier);
		pNode = pSymtabs[currentNestingLevel]->Enter(pString);
    }

    return pNode;
}

//--------------------------------------------------------------
//  EnterScope	Enter a new nested scope.  Increment the nesting
//		level.  Push new scope's symbol table onto the
//		stack.
//
//      pSymtab : ptr to scope's symbol table
//--------------------------------------------------------------

void TSymtabStack::EnterScope(void)
{
    if (++currentNestingLevel > maxNestingLevel) 
	{
		Error(errNestingTooDeep);
		AbortTranslation(abortNestingTooDeep);
    }

    SetCurrentSymtab(new TSymtab);
}

//--------------------------------------------------------------
//  ExitScope	Exit the current scope and return to the
//		enclosing scope.  Decrement the nesting level.
//		Pop the closed scope's symbol table off the
//		stack and return a pointer to it.
//
//  Return: ptr to closed scope's symbol table
//--------------------------------------------------------------

TSymtab *TSymtabStack::ExitScope(void)
{
    return pSymtabs[currentNestingLevel--];
}



//destrutor da lista com o numero das linhas
TLineNumList::~TLineNumList(void)
{
	while(head)
	{
		TLineNumNode *pNode=head;
		head=head->next;
		delete pNode;
	}
}


void TLineNumList::Update(void)
{
	if(tail && (tail->number == currentLineNumber))return;

	tail->next=new TLineNumNode;
	tail=tail->next;
}


void TLineNumList::Print(int newLineFlag,int indent) const
{
	const int maxLineNumberPrintWidth=4;
	const int maxLineNumbersPerLine=10;

	int n;
	TLineNumNode *pNode;
	char *plt=&list.text[strlen(list.text)];

	n=newLineFlag ? 0:maxLineNumbersPerLine;

	for(pNode=head;pNode;pNode=pNode->next)
	{
		if(n==0)
		{
			list.PutLine();
			sprintf(list.text,"%*s",indent,"  ");
			plt=&list.text[indent];
			n=maxLineNumbersPerLine;
		}

		sprintf(plt,"%*d",maxLineNumberPrintWidth,pNode->number);
		plt += maxLineNumberPrintWidth;
		--n;
	}

	list.PutLine();
}