#include "stdafx.h"

void TNumberToken::Get(TTextInBuffer &buffer)
{
	const int maxIntegr = 42949672;						//se o micro for de 16bits se for mais mexer aqui
	const int maxExponent = 37;

	float numValue = 0.0;

	int wholePlaces=0;									//numero de digitos antes da virgula
	int decimalPlaces=0;								//numero de digitos depois da virgula

	char exponentSign = '+';
	float eValue = 0.0;					//valor do numero depois do 'E'
	int exponent=0;						//expoente final

	ch=buffer.Char();
	ps=string;
	digitCount=0;
	countErrorFlag=False;
	code=tcError;
	type=tyInteger;						//assumimos por defeito que sera um inteiro


	if(!AccumulateValue(buffer,numValue,errInvalidNumber)){return;}					//tira o num inteiro
	wholePlaces=digitCount;

	if(ch == 'x')
	{
		*ps++='x';
		value.integer=HexaConverter(buffer);
		code=tcNumber;
		return;
	}

	if(ch == 'c')
	{
		*ps++='c';
		value.integer=OctalConverter(buffer);
		code=tcNumber;
		return;
	}

	if(ch == 'b')
	{
		*ps++='b';
		value.integer=BinaryConverter(buffer);
		code=tcNumber;
		return;
	}

	if(ch == '.')
	{
		type=tyReal;
		*ps++='.';
		ch=buffer.GetChar();
		
		if(!AccumulateValue(buffer,numValue,errInvalidNumber)){return;}				//tira a parte decimal do numero se tiver
		decimalPlaces=digitCount-wholePlaces;
	}

	if(ch == 'E' || ch=='e')
	{
		type=tyReal;
		*ps++=ch;
		ch=buffer.GetChar();

		if((ch=='+') || (ch=='-'))
		{
			*ps++=exponentSign=ch;
			ch=buffer.GetChar();
		}

		digitCount=0;
		if(!AccumulateValue(buffer,eValue,errInvalidNumber)){return;}			//tira o valor do expoente
		if(exponentSign == '-'){eValue = -eValue;}
	}

	if(countErrorFlag)
	{
		Error(errTooManyDigits);
		return;
	}

	exponent = int(eValue) - decimalPlaces;
	if((exponent + wholePlaces < -maxExponent) || (exponent + wholePlaces > maxExponent))
	{
		Error(errRealOutOfRange);
		return;
	}
	if(exponent != 0){numValue *= float(pow(10,exponent));}

	if(type == tyInteger)
	{
		if((numValue < -maxIntegr) || (numValue > maxIntegr))
		{
			Error(errIntegerOutOfRange);
			return;
		}
		value.integer=(int)numValue;
	}
	else
	{
		value.real=numValue;
	}

	*ps='\0';
	code=tcNumber;
}


int TNumberToken::AccumulateValue(TTextInBuffer &buffer,float &value,TErrorCode ec)
{
	const int maxDigitCount = 20;					//nuero maximo de digitos que aceita (se calhar 20 e muito)
	
	if(charCodeMap[ch] != ccDigit)
	{
		Error(ec);
		return False;
	}

	do
	{
		*ps++=ch;
		if(++digitCount <= maxDigitCount)
		{
			value = 10*value + (ch - '0');
		}
		else
		{
			countErrorFlag=True;
		}

		ch=buffer.GetChar();
	}while(charCodeMap[ch]==ccDigit);
	*ps='\0';

	return True;
}

int TNumberToken::HexaConverter(TTextInBuffer &buffer)
{
	int value=0;

	do
	{
		ch=buffer.GetChar();
		*ps++=ch;
		switch (ch)
		{
		case '1': value += 1; break;
		case '2': value += 2; break;
		case '3': value += 3; break;
		case '4': value += 4; break;
		case '5': value += 5; break;
		case '6': value += 6; break;
		case '7': value += 7; break;
		case '8': value += 8; break;
		case '9': value += 9; break;

		case 'a':
		case 'A': value += 10; break;

		case 'b':
		case 'B': value += 11; break;

		case 'c':
		case 'C': value += 12; break;

		case 'd':
		case 'D': value += 13; break;

		case 'e':
		case 'E': value += 14; break;

		case 'f':
		case 'F': value += 15; break;
		
		default:
			break;
		}

		value = value << 4;

	}while(charCodeMap[ch] == ccDigit || ch == 'a' || ch == 'A' || ch == 'b' || ch == 'B' || ch == 'c' || ch == 'C' || ch == 'd' || ch == 'D' || ch == 'e' || ch == 'E' || ch == 'F' || ch == 'f');
	
	value = value >> 8;

	*ps='\0';
	return value;
}

int TNumberToken::OctalConverter(TTextInBuffer &buffer)
{
	int value=0;

	do
	{
		ch=buffer.GetChar();
		*ps++=ch;
		switch (ch)
		{
		case '1': value += 1; break;
		case '2': value += 2; break;
		case '3': value += 3; break;
		case '4': value += 4; break;
		case '5': value += 5; break;
		case '6': value += 6; break;
		case '7': value += 7; break;
		
		default:
			break;
		}

		value = value << 3;

	}while(charCodeMap[ch] == ccDigit);
	
	value = value >> 6;

	*ps='\0';

	return value;
}

int TNumberToken::BinaryConverter(TTextInBuffer &buffer)
{
	int value=0;

	do
	{
		ch=buffer.GetChar();
		*ps++=ch;
		if(ch == '1'){value++;}
		value = value << 1;
	}while (ch == '0' || ch == '1');
	*ps++='\0';
	value = value >> 2;

	return value;
}

void TNumberToken::Print(void) const
{
	if(type == tyInteger)
	{
		sprintf(list.text,"\t%-18s = %d",">> integer",value.integer);
	}
	else
	{
		sprintf(list.text,"\t%-18s = %g",">> integer",value.real);
	}

	list.PutLine();
}



