#ifndef __EXEC_H__
#define __EXEC_H__

#include "stdafx.h"
#include "backend.h"

extern TSymtab globalSymtab;
extern TSymtabNode *pCurrentFunction;

//--------------------------------------------------------------
//  TStackItem          Item pushed onto the runtime stack.
//--------------------------------------------------------------

union TStackItem {
    int    integer;
    float  real;
    char   character;
    void  *address;
};



extern TStackItem functionReturnValue;

//--------------------------------------------------------------
//  TRuntimeStack       Runtime stack class.
//--------------------------------------------------------------

class TRuntimeStack {
    enum {
	stackSize       = 1024,
	frameHeaderSize =   5,
    };

    //--Stack frame header
    struct TFrameHeader {
	TStackItem functionValue;
	TStackItem staticLink;
	TStackItem dynamicLink;

	struct {
	    TStackItem icode;
	    TStackItem location;
	} returnAddress;
    };

    TStackItem  stack[stackSize];  // stack items
    TStackItem *tos;               // ptr to the top of the stack
    TStackItem *pFrameBase;        // ptr to current stack frame base
	TStackItem *pNewFrameBase;		//ptr para um auxiliar da frame base

public:
    TRuntimeStack(void);

    void Push(int value)
    {
		if (tos < &stack[stackSize-1]) (++tos)->integer = value;
		else RuntimeError(rteStackOverflow);
    }

    void Push(float value)
    {
		if (tos < &stack[stackSize-1]) (++tos)->real = value;
		else RuntimeError(rteStackOverflow);
    }

    void Push(char value)
    {
		if (tos < &stack[stackSize-1]) (++tos)->character = value;
		else RuntimeError(rteStackOverflow);
    }

    void Push(void *addr)
    {
		if (tos < &stack[stackSize-1]) (++tos)->address = addr;
		else RuntimeError(rteStackOverflow);
    }

    void PushFrameHeader	(void);
    void ActivateFrame		(void);
    void PopFrame			(void);
	TStackItem *GetVar		(int offset);

    TStackItem *Pop(void)       { return tos--; }
    TStackItem *TOS(void) const { return tos;   }

    void AllocateValue  (const TSymtabNode *pId);
    void DeallocateValue(const TSymtabNode *pId);

    TStackItem *GetValueAddress(const TSymtabNode *pId);
};




class TExecutor : public TBackend {
    long          stmtCount;  // count of executed statements
    TRuntimeStack runStack;   // the runtime stack

	int eofFlag;  // true if at end of file, else false

    //--Pointers to the special "input" and "output"
    //--symbol table nodes entered by the parser.
    //TSymtabNode *const pInputNode;
    //TSymtabNode *const pOutputNode;

	//--Trace flags
    int traceRoutineFlag;    // true to trace routine entry/exit
    int traceStatementFlag;  // true to trace statements
    int traceStoreFlag;      // true to trace data stores
    int traceFetchFlag;      // true to trace data fetches

	//--Routines
    void   ExecuteFunction	(TSymtabNode *pRoutineId);
    void   EnterFunction	(TSymtabNode *pRoutineId);
    void   ExitFunction		(TSymtabNode *pRoutineId);
	void   SaveFunctionState(TSymtabNode *pRoutineId);

    //--Statements.
    void ExecuteStatement		(void);
    void ExecuteAssignment		(const TSymtabNode *pTargetId);
	void ExecuteAssignmentPtr	(void);
	void ExecuteIF				(void);
	void ExecuteWHILE			(void);
	void ExecuteFOR				(void);
	void ExecuteSWITCH			(void);
	void ExecuteRETURN			(void);
	void ExecuteStatementList	(TTokenCode terminator);

    //--Expressions.
    TType *ExecuteExpression(void);
    TType *ExecuteSimpleExpression(void);
    TType *ExecuteTerm(void);
    TType *ExecuteFactor(void);
    TType *ExecuteConstant  (const TSymtabNode *pId);
    TType *ExecuteVariable  (const TSymtabNode *pId, int addressFlag);
    TType *ExecuteField(void);


	//--Tracing
    void TraceRoutineEntry(const TSymtabNode *pRoutineId);
    void TraceRoutineExit (const TSymtabNode *pRoutineId);
    void TraceStatement(void);
    void TraceDataStore(const TSymtabNode *pTargetId, const void *pDataValue, const TType *pDataType);
    void TraceDataFetch(const TSymtabNode *pId, const void *pDataValue, const TType *pDataType);
    void TraceDataValue(const void *pDataValue, const TType *pDataType);

	//--Initialization
	void InitializeMain(void);



    void RangeCheck(const TType *pTargetType, int value);

    void Push(int    value) { runStack.Push(value); }
    void Push(float  value) { runStack.Push(value); }
    void Push(char   value) { runStack.Push(value); }
    void Push(void  *addr)  { runStack.Push(addr);  }

    TStackItem *Pop(void)       { return runStack.Pop(); }
    TStackItem *TOS(void) const { return runStack.TOS(); }

public:
    TExecutor(void)
    {
		stmtCount = 0;

		traceRoutineFlag   = true;
		traceStatementFlag = true;
		traceStoreFlag     = true;
		traceFetchFlag     = true;
    }

    virtual void Go(const TSymtabNode *pProgramId);

};

#endif