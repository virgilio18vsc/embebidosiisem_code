
//declaraçao da struct

//variaveis globais

//funcoes locais

//main
int main()
{	
	asm(						
		ADD r1,r2,r3	
		ADDI r1,r1,50
		SUB r3,r2,r13
		SUBI r1,r1,0x32
		CMP r1,r4,r16
		OR r1,r2,r4
		ORI r1,r5,0x55
		AND r1,r2,r4
		ANDI r1,r5,0x55
		XOR r1,r2,r4
		XORI r1,r5,0x55
		NOT r6
		
		BR r1
		BRI 0x10
		BRA R2
		BRAI 30
		
		LW r2,r3,r6
		LWI r2,r14,5
		LWBI r1,r16,12
		SW r2,r3,r6
		SWI r2,r14,5
		SWBI r1,r16,12
		
		BSL r1,r5,r17
		BSR r23,r26,r1
		
		IMM 0x21F4
		
		BEQ r1,r2
		BNE r3,r4
		BGE r5,r6
		BGT r7,r8
		BLE r9,r10
		BLT r11,r12
		
		MOV r4,r30
		MOVI r1,33
		CALL 0x23
		
		RET
		RETI
		
		PUSH r16
		POP r12
	);
	
	return 0;
}



//interrupcoes

/*void TIMER_ISR_vect(void)
{
	tempo++;
}

void EXTINT_ISR_vect(void)
{
	tempo++;
}*/


