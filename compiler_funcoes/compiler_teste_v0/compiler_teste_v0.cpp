// compiler_teste_v0.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace std;


int main(int argc, char **argv)
{
	
	if(argc < 3)
	{
		cerr<<"Usage: list <source file>"<<endl;
		AbortTranslation(abortInvalidCommandLineArgs);
		system("pause");
		return 1;
	}

	for(int i=2;i<argc;i++)
	{
		TParser *pParser=new TParser(new TSourceBuffer(argv[i]));
		pParser->Parse();
		//delete pParser;									//deveria de limpar, mas como o VS limpa por mim se eu limpar ele da erro (estupido!!!)
	}

	if (errorCount == 0) 
	{

		//TBackend *pBackend=new TExecutor;
		//pBackend->Go(NULL);

		TBackend *pBackend = new TCodeGenerator(argv[1]);
		pBackend->Go(NULL);
	}

	system("pause");
	return 0;
}

