#include "stdafx.h"

//--------------------------------------------------------------
//  EmitStatement       Emit code for a statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitStatement(void)
{
    switch (token) 
	{
		case tcStar:		EmitAssignPtr();		break;
		case tcIdentifier:{	
			TSymtabNode *var=symtabStack.SearchAll(pToken->String());
			if(var->defn.how == dcVariable || var->defn.how == dcPointer){ EmitAssignment(var);}
			else if(var->defn.how == dcArray){EmitAssignArray(var);}
			else{ EmitFunctionCall(var); }
			
			regFile.CleanRegFile();
		}break;
		case tcAsm:			EmitAsmStatement();		break;
		case tcIf:			EmitIF();				break;
		case tcWhile:		EmitWHILE();			break;
		case tcFor:			EmitFOR();				break;
		case tcSwitch:		EmitSWITCH();			break;
		case tcReturn:		EmitReturn();			break;
    }
}


//--------------------------------------------------------------
//  EmitAssignment      Emit code for an assignment statement for a ptr
//--------------------------------------------------------------
void TCodeGenerator::EmitAssignPtr(void)
{
	

}


//--------------------------------------------------------------
//  EmitAssignment      Emit code for an assignment statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitAssignment(TSymtabNode *pTargetId)
{	
	

}


//--------------------------------------------------------------
//  EmitAssignArray      Emit code for an assignment for an array.
//--------------------------------------------------------------

void TCodeGenerator::EmitAssignArray(TSymtabNode *pTargetId)
{
	
}


//--------------------------------------------------------------
//  EmitIf      Emit code for an if statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitIF(void)
{
	
}


//--------------------------------------------------------------
//  EmitWhile      Emit code for an while statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitWHILE(void)
{
	

}



//--------------------------------------------------------------
//  EmitFOR      Emit code for an for statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitFOR(void)
{
										//obtem primeiro token do proximo statment
}


//--------------------------------------------------------------
//  EmitSWITCH      Emit code for a switch statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitSWITCH(void)
{
	
}


//--------------------------------------------------------------
//  EmitCASE      Emit code for a case statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitCASE(void)
{
	
}


//--------------------------------------------------------------
//  EmitReturn      Emit code for a return statement.
//--------------------------------------------------------------

void TCodeGenerator::EmitReturn(void)
{
	GetToken();
	EmitExpression();
	ExprStackData result=genStack.Pop();
	if(result.token == tcNumber){Emit2(MOVI,FUNCTION_RETURN_REG,0,result.val.integer);}
	else{Emit1(MOV,FUNCTION_RETURN_REG,result.reg->GetReg(),0);}
	
	Emit1(MOV,31,FRAME_BASE_REG,0);
	Emit1(POP,FRAME_BASE_REG,0,0);
	Emit1(RET,0,0,0);

	GetToken();
}


void TCodeGenerator::EmitFunctionCall(TSymtabNode *pTargetId)
{

}


//***************************************************

void TCodeGenerator::type1(int* rd,int* ra, int* rb)
{
		GetToken();
		*rd=atoi((pToken->String() + 1));
		GetToken();
		*ra=atoi((pToken->String() + 1));
		GetToken();
		*rb=atoi((pToken->String() + 1));
		GetToken();
}

void TCodeGenerator::type2(int* rd,int* ra, int* imm)
{
		GetToken();
		*rd=atoi((pToken->String() + 1));
		GetToken();
		*ra=atoi((pToken->String() + 1));
		GetToken();
		*imm=symtabStack.SearchAll(pToken->String())->defn.constant.value.integer;
		GetToken();
}

void TCodeGenerator::type3(int* ra, int* rb)
{
		GetToken();
		*ra=atoi((pToken->String() + 1));
		GetToken();
		*rb=atoi((pToken->String() + 1));
		GetToken();
}


void TCodeGenerator::EmitAsmStatement(void)
{
	int rd,ra,rb,imm;

	GetToken();
	GetToken();

	while(token != tcRParen)
	{
		switch (token)
		{
//***************************************************
			//ALU INSTRUCTIONS
		case tcADD:{
			type1(&rd,&ra,&rb);
			Emit1(ADD,rd,ra,rb);
			}break;
		case tcSUB:{
			type1(&rd,&ra,&rb);
			Emit1(SUB,rd,ra,rb);
			}break;

		case tcADDI:{
			type2(&rd,&ra,&imm);
			Emit2(ADDI,rd,ra,imm);
			}break;
		case tcSUBI:{
			type2(&rd,&ra,&imm);
			Emit2(SUBI,rd,ra,imm);
			}break;
		case tcCMP:
			type1(&rd,&ra,&rb);
			Emit1(CMP,rd,ra,rb);
			break;
		case tcOR:
			type1(&rd,&ra,&rb);
			Emit1(OR,rd,ra,rb);
			break;
		case tcORI:{
			type2(&rd,&ra,&imm);
			Emit2(ORI,rd,ra,imm);
			}break;
		case tcAND:
			type1(&rd,&ra,&rb);
			Emit1(AND,rd,ra,rb);
			break;
		case tcANDI:{
			type2(&rd,&ra,&imm);
			Emit2(ANDI,rd,ra,imm);
			}break;
		case tcXOR:
			type1(&rd,&ra,&rb);
			Emit1(XOR,rd,ra,rb);
			break;
		case tcXORI:{
			type2(&rd,&ra,&imm);
			Emit2(XORI,rd,ra,imm);
			}break;
		case tcNOT:
			GetToken();
			Emit1(NOT,atoi(pToken->String() + 1),0,0);
			GetToken();
			break;

		case tcBSL:
			type1(&rd,&ra,&rb);
			Emit3(BS,rd,ra,rb,ShiftLeft);
			break;
		case tcBSR:
			type1(&rd,&ra,&rb);
			Emit3(BS,rd,ra,rb,ShiftRight);
			break;

		case tcMUL:
			type1(&rd,&ra,&rb);
			Emit1(MUL,rd,ra,rb);
			break;

//**********************************************************************
		//brunch instructions
		case tcBR:
			GetToken();
			Emit1(BR,0,0,atoi(pToken->String() + 1));
			GetToken();
			break;
		case tcBRI:
			GetToken();
			Emit2(BRI,0,0,symtabStack.SearchAll(pToken->String())->defn.constant.value.integer);
			GetToken();
			break;
		case tcBRA:
			GetToken();
			Emit1(BRA,0,8,atoi(pToken->String() + 1));
			GetToken();
			break;
		case tcBRAI:
			GetToken();
			Emit2(BRAI,0,8,symtabStack.SearchAll(pToken->String())->defn.constant.value.integer);
			GetToken();
			break;

		//BRUNCHS CONDICIONAIS
		case tcBEQ:
			type3(&ra,&rb);
			Emit1(BEQ,0,ra,rb);
			break;
		case tcBNE:
			type3(&ra,&rb);
			Emit1(BNE,1,ra,rb);
			break;
		case tcBGE:
			type3(&ra,&rb);
			Emit1(BGE,5,ra,rb);
			break;
		case tcBGT:
			type3(&ra,&rb);
			Emit1(BGT,4,ra,rb);
			break;
		case tcBLE:
			type3(&ra,&rb);
			Emit1(BLE,3,ra,rb);
			break;
		case tcBLT:
			type3(&ra,&rb);
			Emit1(BLT,2,ra,rb);
			break;
		case tcCALL:
			GetToken();
			EmitCALL(symtabStack.SearchAll(pToken->String())->defn.constant.value.integer);
			GetToken();
			break;

//***********************************************************************
		//other instructions
		case tcIMM:
			GetToken();
			Emit2(IMM,0,0,symtabStack.SearchAll(pToken->String())->defn.constant.value.integer);
			GetToken();
			break;

		case tcRET:
			Emit1(RET,0,0,0);
			GetToken();
			break;

		case tcRETI:
			GetToken();
			Emit1(RETI,0,0,0);
			break;

		case tcPUSH:
			GetToken();
			rd=atoi((pToken->String() + 1));
			GetToken();
			Emit1(PUSH,rd,0,0);
			break;

		case tcPOP:
			GetToken();
			rd=atoi((pToken->String() + 1));
			GetToken();
			Emit1(POP,rd,0,0);
			break;

//***********************************************************************
		//data transfer instructions
		case tcLW:{
			type1(&rd,&ra,&rb);
			Emit1(LW,rd,ra,rb);
			}break;
		case tcLWI:{
			type2(&rd,&ra,&imm);
			Emit2(LWI,rd,ra,imm);
			}break;
		case tcLWBI:{
			type2(&rd,&ra,&imm);
			Emit2(LWBI,rd,ra,imm);
			}break;
		case tcSW:{
			type1(&rd,&ra,&rb);
			Emit1(SW,rd,ra,rb);
			}break;
		case tcSWI:{
			type2(&rd,&ra,&imm);
			Emit2(SWI,rd,ra,imm);
			}break;
		case tcSWBI:{
			type2(&rd,&ra,&imm);
			Emit2(SWBI,rd,ra,imm);
			}break;

		case tcMOV:
			type3(&rd,&ra);
			Emit1(MOV,rd,ra,0);
			break;

		case tcMOVI:
			GetToken();
			ra=atoi((pToken->String() + 1));
			GetToken();
			imm=symtabStack.SearchAll(pToken->String())->defn.constant.value.integer;
			GetToken();
			Emit2(MOVI,rd,0,imm);
			break;

		}//chaveta do switch
	}//chaveta do while
	GetToken();
	GetToken();
}


