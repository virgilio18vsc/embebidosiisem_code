#include "stdafx.h"


TType *TCodeGenerator::EmitExpression(void)
{
	TType        *pOperand1Type;  // ptr to first  operand's type
    TType        *pOperand2Type;  // ptr to second operand's type
	TType		 *pResultType = NULL;
	TTokenCode op;

	pResultType=EmitLogicExpression();

	while(TokenIn(token,tlLogicOps))
	{
		op=token;
		GetToken();
		pOperand1Type=pResultType->Base();
		pOperand2Type=EmitLogicExpression();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		switch (op)
		{
		case tcLogicAnd:
			EmitIntLogicAnd(op1,op2);
			break;

		case tcLogicOr:
			EmitIntLogicOr(op1,op2);
			break;
		}
		
	}

	//finalizado a resolucao da expressao verificar se e necessario atulizar ++ ou -- 
	if(postStack.Elements())
	{
		while(postStack.Elements() > 0)
		{
			ExprStackData aux=postStack.Pop();
			TTokenCode op=aux.token;
			TRegister *reg=regFile.FindRegister(aux.identifier);

			switch (op)
			{
				case tcPlusPlus:	
					Emit2(ADDI,reg->GetReg(),reg->GetReg(),1);	
					if(!globalSymtab.Search(reg->GetSymbolAssociated()->String()))
					{
						Emit2(SWBI,reg->GetReg(),FRAME_BASE_REG,reg->GetSymbolAssociated()->defn.data.offset);
					}
					else
					{
						Emit2(SWI,reg->GetReg(),GLOBAL_VAR_REG,reg->GetSymbolAssociated()->defn.data.offset);
					}
				break;
				case tcMinusMinus:	
					Emit2(SUBI,reg->GetReg(),reg->GetReg(),1);	
					if(!globalSymtab.Search(reg->GetSymbolAssociated()->String()))
					{
						Emit2(SWBI,reg->GetReg(),FRAME_BASE_REG,reg->GetSymbolAssociated()->defn.data.offset);
					}
					else
					{
						Emit2(SWI,reg->GetReg(),GLOBAL_VAR_REG,reg->GetSymbolAssociated()->defn.data.offset);
					}
				break;
			}
		}
	}
	return pResultType;
}

TType *TCodeGenerator::EmitLogicExpression(void)
{
	TType        *pOperand1Type;  // ptr to first  operand's type
    TType        *pOperand2Type;  // ptr to second operand's type
	TType		 *pResultType = NULL;
	TTokenCode op;

	pResultType=EmitSimpleExpression();

	while(TokenIn(token,tlRelOps))
	{
		op=token;
		GetToken();
		pOperand1Type=pResultType->Base();
		pOperand2Type=EmitSimpleExpression();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		switch(op)
		{
		case tcEqual:
			EmitIntEqual(op1,op2);
			break;

		case tcNe:
			EmitIntNotEqual(op1,op2);
			break;

		case tcGt:
			EmitIntGreatThan(op1,op2);
			break;

		case tcGe:
			EmitIntGreatEqual(op1,op2);
			break;

		case tcLt:
			EmitIntLessThan(op1,op2);
			break;

		case tcLe:
			EmitIntLessEqual(op1,op2);
			break;
		}
	}
	return pResultType;
}

TType *TCodeGenerator::EmitSimpleExpression(void)
{
	TType        *pOperand1Type;  // ptr to first  operand's type
    TType        *pOperand2Type;  // ptr to second operand's type
	TType		 *pResultType = NULL;
	TTokenCode  op;                // operator
	
	pResultType=EmitTerm();
	
	while(TokenIn(token,tlAddOps))
	{
		op=token;
		GetToken();
		
		pOperand1Type=pResultType->Base();
		pOperand2Type=EmitTerm();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		switch (op)
		{
		case tcPlus:
			if(pOperand1Type == pIntegerType && pOperand2Type == pIntegerType)
			{
				EmitIntADD(op1,op2);
			}
			else if (pOperand1Type == pIntegerType && pOperand2Type == pRealType)
			{
				callFloatOperation(op1,"intToFloat");
				op1=genStack.Pop();
				callFloatOperation(op1,op2,"EmitFloatADD");
				pResultType=pRealType;
			}
			else if(pOperand1Type == pRealType && pOperand2Type == pIntegerType)
			{
				callFloatOperation(op2,"intToFloat");
				op2=genStack.Pop();
				callFloatOperation(op1,op2,"EmitFloatADD");
				pResultType=pRealType;
			}
			else
			{	
				callFloatOperation(op1,op2,"EmitFloatADD");
			}
			break;

		case tcMinus:
			if(pOperand1Type == pIntegerType && pOperand2Type == pIntegerType)
			{
				EmitIntSUB(op1,op2);
			}
			else if (pOperand1Type == pIntegerType && pOperand2Type == pRealType)
			{
				callFloatOperation(op1,"intToFloat");
				op1=genStack.Pop();

				TSymtabNode *temporary_symbol=globalSymtab.Search("temporary_symbol");
				if(!temporary_symbol){temporary_symbol=globalSymtab.Enter("temporary_symbol");}

				if(op1.token == tcNumber)
				{
					TRegister *mask=regFile.GetRegister(temporary_symbol);
					TRegister *operand=regFile.GetRegister(temporary_symbol);
					if(op2.val.integer > 0xFFFF){Emit2(IMM,0,0,(op2.val.integer >> 16) & 0xFFFF);}
					Emit2(MOVI,operand->GetReg(),0,op2.val.integer);
					Emit2(IMM,0,0,0x8000);
					Emit2(MOVI,mask->GetReg(),0,0);
					Emit1(XOR,operand->GetReg(),operand->GetReg(),mask->GetReg());
					regFile.FindRegister(mask->GetReg());
					op2.token=tcIdentifier;
					op2.reg=operand;
				}
				else
				{
					TRegister *mask=regFile.GetRegister(temporary_symbol);
					Emit2(IMM,0,0,0x8000);
					Emit2(MOVI,mask->GetReg(),0,0);
					Emit1(XOR,op2.reg->GetReg(),op2.reg->GetReg(),mask->GetReg());
					regFile.FindRegister(mask->GetReg());
				}
				callFloatOperation(op1,op2,"EmitFloatADD");
			}
			else if(pOperand1Type == pRealType && pOperand2Type == pIntegerType)
			{
				callFloatOperation(op2,"intToFloat");
				op2=genStack.Pop();

				TSymtabNode *temporary_symbol=globalSymtab.Search("temporary_symbol");
				if(!temporary_symbol){temporary_symbol=globalSymtab.Enter("temporary_symbol");}

				if(op1.token == tcNumber)
				{
					TRegister *mask=regFile.GetRegister(temporary_symbol);
					TRegister *operand=regFile.GetRegister(temporary_symbol);
					if(op2.val.integer > 0xFFFF){Emit2(IMM,0,0,(op2.val.integer >> 16) & 0xFFFF);}
					Emit2(MOVI,operand->GetReg(),0,op2.val.integer);
					Emit2(IMM,0,0,0x8000);
					Emit2(MOVI,mask->GetReg(),0,0);
					Emit1(XOR,operand->GetReg(),operand->GetReg(),mask->GetReg());
					regFile.FindRegister(mask->GetReg());
					op2.token=tcIdentifier;
					op2.reg=operand;
				}
				else
				{
					TRegister *mask=regFile.GetRegister(temporary_symbol);
					Emit2(IMM,0,0,0x8000);
					Emit2(MOVI,mask->GetReg(),0,0);
					Emit1(XOR,op2.reg->GetReg(),op2.reg->GetReg(),mask->GetReg());
					regFile.FindRegister(mask->GetReg());
				}
				callFloatOperation(op1,op2,"EmitFloatADD");
			}
			else
			{	
				TSymtabNode *temporary_symbol=globalSymtab.Search("temporary_symbol");
				if(!temporary_symbol){temporary_symbol=globalSymtab.Enter("temporary_symbol");}

				if(op1.token == tcNumber)
				{
					TRegister *mask=regFile.GetRegister(temporary_symbol);
					TRegister *operand=regFile.GetRegister(temporary_symbol);
					if(op2.val.integer > 0xFFFF){Emit2(IMM,0,0,(op2.val.integer >> 16) & 0xFFFF);}
					Emit2(MOVI,operand->GetReg(),0,op2.val.integer);
					Emit2(IMM,0,0,0x8000);
					Emit2(MOVI,mask->GetReg(),0,0);
					Emit1(XOR,operand->GetReg(),operand->GetReg(),mask->GetReg());
					regFile.FindRegister(mask->GetReg());
					op2.token=tcIdentifier;
					op2.reg=operand;
				}
				else
				{
					TRegister *mask=regFile.GetRegister(temporary_symbol);
					Emit2(IMM,0,0,0x8000);
					Emit2(MOVI,mask->GetReg(),0,0);
					Emit1(XOR,op2.reg->GetReg(),op2.reg->GetReg(),mask->GetReg());
					regFile.FindRegister(mask->GetReg());
				}
				callFloatOperation(op1,op2,"EmitFloatADD");
			}
			break;

		case tcLShift:
			EmitIntLShift(op1,op2);
			break;

		case tcRShift:
			EmitIntRShift(op1,op2);
			break;

		case tcOr:
			EmitIntOR(op1,op2);
			break;

		case tcXor:
			EmitIntXOR(op1,op2);
			break;
		}
	}
	return pResultType;
}

TType *TCodeGenerator::EmitTerm(void)
{
	TType        *pOperand1Type;  // ptr to first  operand's type
    TType        *pOperand2Type;  // ptr to second operand's type
	TType		 *pResultType = NULL;
	TTokenCode op;

	pResultType=EmitFactor();

	while(TokenIn(token,tlMulOps))
	{
		//nao pode chegar aqui pk o processador nao tem instru�oes para 
		//resolver operadores multiplica�ao divisao
		op=token;
		GetToken();

		pOperand1Type=pResultType->Base();
		pOperand2Type=EmitFactor();

		ExprStackData op2=genStack.Pop();
		ExprStackData op1=genStack.Pop();

		switch(op)
		{
		case tcAnd:				// & operacao ao bit
			EmitIntAND(op1,op2);
			break;

		case tcStar:
			if(pOperand1Type == pIntegerType && pOperand2Type == pIntegerType)
			{
				EmitIntMultiplication(op1,op2);
			}
			else if (pOperand1Type == pIntegerType && pOperand2Type == pRealType)
			{
				
			}
			else if(pOperand1Type == pRealType && pOperand2Type == pIntegerType)
			{
				
			}
			else
			{		
				callFloatOperation(op1,op2,"EmitFloatMUL");
			}

			break;

		case tcSlash:{
			if(pOperand1Type == pIntegerType && pOperand2Type == pIntegerType)
			{
				EmitIntDivision(op1,op2);
			}
			else if (pOperand1Type == pIntegerType && pOperand2Type == pRealType)
			{
				
			}
			else if(pOperand1Type == pRealType && pOperand2Type == pIntegerType)
			{
				
			}
			else
			{	
				callFloatOperation(op1,op2,"EmitFloatDIV");
			}

			}break;

		case tcPercent:	
			EmitIntDivisionModule(op1,op2);
			break;
		}
	}
	return pResultType;
}

TType *TCodeGenerator::EmitFactor(void)
{
	TType		 *pResultType = NULL;

	switch(token)
	{
	case tcIdentifier:{
		TSymtabNode *var=symtabStack.SearchAll(pToken->String());
		if(var->defn.how == dcFunction)
		{
			pResultType=var->defn.routine.returnType;
			EmitFunctionCall(var);																	//emito a function call que trata de colocar os parametros na stack
			genStack.Push(tcIdentifier,regFile.FindRegister(FUNCTION_RETURN_REG));					//por fim colocamos na stack o registo de retorno da fun�ao para usar para o calculo da expressao
			GetToken();
		}
		else if(var->defn.how == dcArray)
		{
			GetToken();
			if(token == tcLSquareBracket)
			{
				//vai buscar o valor
				GetToken();
				pResultType=EmitArray(var);			

				GetToken();			// ]
			}
			else
			{

				pResultType=EmitVariable(var);
			}
		}
		else
		{
			pResultType=EmitVariable(var);
			GetToken();

			if(token == tcPlusPlus || token == tcMinusMinus)
			{
				postStack.Push(token,var);
				GetToken();
			}
		}
		}break;

	case tcNumber:{
		TSymtabNode *number=symtabStack.SearchAll(pToken->String());
		pResultType=EmitConstant(number);
		GetToken();
		}break;

	case tcLParen:
		GetToken();
		pResultType=EmitExpression();
		GetToken();
		break;

	case tcPlusPlus:{
		GetToken();
		TSymtabNode *var=symtabStack.SearchAll(pToken->String());
		
		pResultType=EmitVariable(var);

		ExprStackData reg=genStack.Pop();
		Emit2(ADDI,reg.reg->GetReg(),reg.reg->GetReg(),1);
		genStack.Push(tcIdentifier,reg.reg->GetReg());

		GetToken(); 
		}break;

	case tcMinusMinus:{
		GetToken();
		TSymtabNode *var=symtabStack.SearchAll(pToken->String());
		
		pResultType=EmitVariable(var);

		ExprStackData reg=genStack.Pop();
		Emit2(SUBI,reg.reg->GetReg(),reg.reg->GetReg(),1);
		genStack.Push(tcIdentifier,reg.reg->GetReg());

		GetToken(); 			  
		}break;

	case tcStar:{
		TSymtabNode *temporary_symbol=globalSymtab.Search("temporary_symbol");
		if(!temporary_symbol){temporary_symbol=globalSymtab.Enter("temporary_symbol");}		
		TRegister *reg=regFile.GetRegister(temporary_symbol);

		GetToken();
		pResultType=EmitFactor();

		Emit1(LW,reg->GetReg(),genStack.Pop().reg->GetReg(),0);
		genStack.Push(tcIdentifier,reg);

		}break;

	case tcAnd:{			
		//passar endere�o de uma variavel
		TSymtabNode *temporary_symbol=globalSymtab.Search("temporary_symbol");
		if(!temporary_symbol){temporary_symbol=globalSymtab.Enter("temporary_symbol");}		
		TRegister *reg=regFile.GetRegister(temporary_symbol);

		GetToken();
		TSymtabNode *var=symtabStack.SearchAll(pToken->String());

		if(var->defn.how == dcVariable || var->defn.how == dcPointer)
		{
			if(!globalSymtab.Search(var->String()))
			{
				//e variavel local
				var=symtabStack.GetCurrentSymtab()->Search(pToken->String());
				Emit2(SUBI,reg->GetReg(),FRAME_BASE_REG,var->defn.data.offset);
				reg->GetSymbolAssociated()->defn.Struct.ptrIsGlobal=0;
			}
			else
			{
				//e variavel global
				Emit2(ADDI,reg->GetReg(),GLOBAL_VAR_REG,var->defn.data.offset);
				reg->GetSymbolAssociated()->defn.Struct.ptrIsGlobal=1;
			}
		}

		if(var->defn.how == dcArray)
		{
			GetToken();
			if(token == tcLSquareBracket)
			{
				GetToken();
				
				EmitExpression();

				if(!globalSymtab.Search(var->String()))
				{
					//nao e variavel global
					Emit2(LWBI,reg->GetReg(),FRAME_BASE_REG,var->defn.data.offset);
				}
				else
				{
					Emit2(LWI,reg->GetReg(),GLOBAL_VAR_REG,var->defn.data.offset);
				}
				
				ExprStackData result=genStack.Pop();
				if(result.token == tcNumber)
				{
					Emit2(ADDI,reg->GetReg(),reg->GetReg(),result.val.integer);
				}
				else
				{
					Emit1(ADD,reg->GetReg(),reg->GetReg(),result.reg->GetReg());
				}

				Emit1(ADD,reg->GetReg(),reg->GetReg(),GLOBAL_VAR_REG);

				GetToken();
			}
			else
			{
				if(!globalSymtab.Search(var->String()))
				{
					//nao e variavel global
					Emit2(LWBI,reg->GetReg(),FRAME_BASE_REG,var->defn.data.offset);
				}
				else
				{
					Emit2(LWI,reg->GetReg(),GLOBAL_VAR_REG,var->defn.data.offset);
				}
			}
		}

		GetToken();

		pResultType=pIntegerType;
		genStack.Push(tcIdentifier,reg);

		}break;

	case tcNot:
		GetToken();
		EmitFactor();
		EmitIntLogicNot();	
		break;

		case tcTil:
		GetToken();
		EmitFactor();
		EmitIntBitNot();
		break;
	}
	return pResultType;
}


TType *TCodeGenerator::EmitVariable(TSymtabNode *pId)
{
	TRegister *reg=NULL;
	//ve se a variavel ja esta nos registos
	if(pId->pType == pIntegerType || pId->pType == pRealType || pId->pType == pCharType){reg=regFile.FindRegister(pId);}
	//adiciona se nao estiver	
	if(!reg)
	{

		reg=regFile.GetRegister(pId); 
		if(!globalSymtab.Search(pId->String()))
		{
			//se for null e variavel local
			if(pId->pType == pIntegerType || pId->pType == pRealType)
			{
				Emit2(LWBI,reg->GetReg(),FRAME_BASE_REG,pId->defn.data.offset);
			}
			else
			{
				GetToken();
				if(token == tcPeriod)
				{
					GetToken();
					TSymtabNode *campo=pId->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
					Emit2(LWBI,reg->GetReg(),FRAME_BASE_REG,pId->defn.data.offset + campo->NodeIndex());
				}
				else
				{
					GetToken();
					TSymtabNode *campo=pId->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
					TRegister *addr=regFile.GetRegister(pId);
					Emit2(LWBI,reg->GetReg(),FRAME_BASE_REG,pId->defn.data.offset);
					if(campo->NodeIndex() != 0){Emit2(SUBI,reg->GetReg(),reg->GetReg(),campo->NodeIndex());}
					Emit1(LW,reg->GetReg(),reg->GetReg(),0);
				}
			}
		}
		else
		{
			//se existir e variavel global
			if(pId->pType == pIntegerType || pId->pType == pRealType)
			{
				Emit2(LWI,reg->GetReg(),GLOBAL_VAR_REG,pId->defn.data.offset);
			}
			else
			{
				GetToken();
				if(token == tcPeriod)
				{
					GetToken();
					TSymtabNode *campo=pId->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
					Emit2(LWI,reg->GetReg(),GLOBAL_VAR_REG,pId->defn.data.offset + campo->NodeIndex());
				}
				else
				{
					GetToken();
					TSymtabNode *campo=pId->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
					TRegister *addr=regFile.GetRegister(pId);
					Emit2(LWBI,reg->GetReg(),GLOBAL_VAR_REG,pId->defn.data.offset);
					if(campo->NodeIndex() != 0){Emit2(ADDI,reg->GetReg(),reg->GetReg(),campo->NodeIndex());}
					Emit1(LW,reg->GetReg(),reg->GetReg(),0);
				}
			}
		}
	}			

	genStack.Push(token,reg);
	
	return pId->pType;
}

TType *TCodeGenerator::EmitConstant(TSymtabNode *pId)
{	
	if(pId->pType == pIntegerType)
	{
		genStack.Push(token,pId->defn.constant.value.integer);
	}
	else if(pId->pType == pRealType)
	{
		genStack.Push(token,floatToRepresentation(pId->defn.constant.value.real));
	}
	return pId->pType;
}



TType *TCodeGenerator::EmitArray(TSymtabNode *pId)
{
	TSymtabNode *temporary_symbol=globalSymtab.Search("temporary_symbol");
	if(!temporary_symbol){temporary_symbol=globalSymtab.Enter("temporary_symbol");}	
	
	EmitExpression();

	ExprStackData result=genStack.Pop();
	if(result.token == tcNumber)
	{
		TRegister *reg=regFile.GetRegister(temporary_symbol);
		TRegister *addr=regFile.GetRegister(temporary_symbol);
		Emit2(MOVI,reg->GetReg(),0,result.val.integer);
		
		if(!globalSymtab.Search(pId->String()))
		{
			//var local
			Emit2(LWBI,addr->GetReg(),FRAME_BASE_REG,pId->defn.data.offset);
		}
		else
		{
			//var global
			Emit2(LWI,addr->GetReg(),GLOBAL_VAR_REG,pId->defn.data.offset);
		}
				
		Emit1(ADD,reg->GetReg(),reg->GetReg(),addr->GetReg());
		Emit1(LW,reg->GetReg(),GLOBAL_VAR_REG,reg->GetReg());

		regFile.FreeRegister(addr->GetReg());
		genStack.Push(tcIdentifier,reg);
	}

	return pId->pType;
}













