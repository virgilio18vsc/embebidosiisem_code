#ifndef __BACKEND_H__
#define __BACKEND_H__

#include "stdafx.h"

extern TIcode icode;

class TBackend {

protected:
    TToken      *pToken;		// ptr to the current token
    TTokenCode   token;			// code of current token
	TIcode		*pIcode;		//apontador para o codigo intermedio
    TSymtabNode *pNode;			// ptr to symtab node
	TSymtabStack symtabStack;					// the symbol table stack

    void GetToken(void)
    {
		pToken = icode.Get();
		token  = pToken->Code();
		pNode  = icode.SymtabNode();
    }

    void GoTo(int location) { icode.GoTo(location); }

    int CurrentLocation(void) const 
	{
		return icode.CurrentLocation();
    }

public:
    virtual ~TBackend(void) {}

    virtual void Go(const TSymtabNode *pRoutineId) = 0;
};

#endif