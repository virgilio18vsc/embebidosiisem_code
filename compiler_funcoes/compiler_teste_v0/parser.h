#ifndef __PARSER_H__
#define __PARSER_H__

#include "stdafx.h"
#include "icode.h"

extern TIcode *pIcode;
extern TSymtab globalSymtab;
extern TIcode icode;

class TParser
{
	TTextScanner *const pScanner;				//apontador para o scanner
	TToken *pToken;								//apontador para o token atual
	TTokenCode token;							//codigo do token atual
	TSymtabStack  symtabStack;					// the symbol table stack

	//declarations
	void ParseDeclarations       (TSymtabNode *pRoutineId);
    void ParseConstantDefinitions(TSymtabNode *pRoutineId);
    void ParseConstant           (TSymtabNode *pConstId);
	void ParseArraySize          (TSymtabNode *pConstId);
    void ParseIdentifierConstant (TSymtabNode *pId1, TTokenCode sign);

    void   ParseTypeDefinitions(TSymtabNode *pRoutineId);
    TType *ParseTypeSpec       (void);

    TType *ParseIdentifierType (const TSymtabNode *pId2);
    TType *ParseEnumerationType(void);

    TType *ParseSubrangeType (TSymtabNode *pMinId);
    TType *ParseSubrangeLimit(TSymtabNode *pLimitId, int &limit);

    TType *ParseArrayType (void);
    void   ParseIndexType (TType *pArrayType);
    int    ArraySize      (TType *pArrayType);
    TType *ParseRecordType(void);

    void ParseVariableDeclarations(TSymtabNode *pRoutineId);
    void ParseFieldDeclarations   (TType       *pRecordType, int offset);
    void ParseVarOrFieldDecls     (TSymtabNode *pRoutineId, TType *pRecordType, int offset);
	void ParseFuncDecls			  (TSymtabNode *pRoutineId, TType *pRecordType, int offset);
	void ParseStruct			  (TSymtabNode *pRoutineId);
	void ParseEnum				  (TSymtabNode *pRoutineId);
    TSymtabNode *ParseIdSublist   (const TSymtabNode *pRoutineId, const TType *pRecordType, TSymtabNode *&pLastId);

	//statments
	void ParseStatement    (void);
    void ParseStatementList(TTokenCode terminator);

    void ParseAssignment   (void);
	void ParseAsmStatement (void);
    void ParseWHILE        (void);
    void ParseIF           (void);
    void ParseFOR          (void);

    void ParseSWITCH       (void);
	void ParseCase		   (void);
	void ParseRETURN	   (void);

    //void ParseCaseLabel    (void);

	//Expressoes
	void ParseExpression(void);
	void ParseLogicalExpression(void);
	void ParseSimpleExpression(void);
	void ParseTerm(void);
	void ParseFactor(void);

	void GetToken(void)
	{
		pToken=pScanner->Get();
		token =pToken->Code();
	}

	void GetTokenAppend(void)
    {
		GetToken();
		icode.Put(token);  // append token code to icode
    }

	void CondGetToken(TTokenCode tc, TErrorCode ec)
    {
	//--Get another token only if the current one matches tc.
	if (tc == token) GetToken();
	else             Error(ec);  // error if no match
    }

    void CondGetTokenAppend(TTokenCode tc, TErrorCode ec)
    {
	//--Get another token only if the current one matches tc.
	if (tc == token) GetTokenAppend();
	else             Error(ec);  // error if no match
    }

    void Resync(const TTokenCode *pList1, const TTokenCode *pList2 = NULL, const TTokenCode *pList3 = NULL);

	void InsertLineMarker(void) { icode.InsertLineMarker(); }

	TSymtabNode *SearchLocal(const char *pString)
    {
		return symtabStack.SearchLocal(pString);
    }

	TSymtabNode *SearchAll(const char *pString) const
    {
		return symtabStack.SearchAll(pString);
    }

    TSymtabNode *EnterLocal(const char *pString, TDefnCode dc = dcUndefined)
    {
		return symtabStack.EnterLocal(pString, dc);
    }

    TSymtabNode *EnterNewLocal(const char *pString, TDefnCode dc = dcUndefined)
    {
		return symtabStack.EnterNewLocal(pString, dc);
    }

    TSymtabNode *Find(const char *pString) const
    {
		return symtabStack.Find(pString);
    }

    void CopyQuotedString(char *pString,
			  const char *pQuotedString) const
    {
	int length = strlen(pQuotedString) - 2;  // don't count quotes
	strncpy(pString, &pQuotedString[1], length);
	pString[length] = '\0';
    }

public:
	TParser(TTextInBuffer *pBuffer) : pScanner(new TTextScanner(pBuffer))/*,pCompact(new TCompactListBuffer)*/
	{
		InitializePredefinedTypes(&globalSymtab);
    }
	~TParser(void)
	{
		delete pScanner;
		RemovePredefinedTypes();
	}

	void Parse(void);
};



#endif