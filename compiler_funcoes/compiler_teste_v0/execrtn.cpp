#include "stdafx.h"


void TExecutor::ExecuteFunction(TSymtabNode *pRoutineId)
{
    EnterFunction(pRoutineId);

	pCurrentFunction=pRoutineId; 
    //--Execute the routine's compound statement.
	ExecuteStatementList(tcEndOfFile);
	
    ExitFunction(pRoutineId);
}


//--------------------------------------------------------------
//  EnterRoutine        Enter a routine:  Switch to its
//                      intermediate code and allocate its
//                      local variables on the runtime stack.
//
//      pRoutineId : ptr to routine name's symbol table node
//--------------------------------------------------------------

void TExecutor::EnterFunction(TSymtabNode *pRoutineId)
{
	//debug
	cout<<"\n***** entrou "<<pRoutineId->String()<<" *****"<<endl;

	//se for a primeira vez que a funcao corre, transformamos a tabela de simbolos num vetor
	if(!pRoutineId->defn.routine.vSymtab)
	{
		pRoutineId->defn.routine.vSymtab=new TSymtabNode *[pRoutineId->defn.routine.pSymtab->NodeCount()];
		pRoutineId->defn.routine.pSymtab->Root()->Convert(pRoutineId->defn.routine.vSymtab);

		TSymtabNode **vSymtab=pRoutineId->defn.routine.vSymtab;

		int offset_gen=pRoutineId->defn.routine.parmCount+1;
		for(int i=0;i<pRoutineId->defn.routine.pSymtab->NodeCount();i++)
		{
			if(vSymtab[i]->defn.how == dcVariable || vSymtab[i]->defn.how == dcPointer)
			{
				vSymtab[i]->defn.data.offset=offset_gen++;
			}
		}
	}

	TSymtabNode **vSymtab=pRoutineId->defn.routine.vSymtab;
	for(int i=0;i<pRoutineId->defn.routine.pSymtab->NodeCount();i++)
	{
		if(vSymtab[i]->defn.how == dcVariable || vSymtab[i]->defn.how == dcPointer)
		{
			if(vSymtab[i]->pType == pRealType){Push(vSymtab[i]->defn.data.value.real);}
			else{Push(vSymtab[i]->defn.data.value.integer);}
		}
	}
	
	//retiro a stack da funcao anterior e coloco da nova
	symtabStack.PopSymtab();
	symtabStack.PushSymtab(pRoutineId->defn.routine.pSymtab);

	icode=*(pRoutineId->defn.routine.pIcode);
	icode.Reset();
}




//--------------------------------------------------------------
//  ExitRoutine         Exit a routine:  Deallocate its local
//                      parameters and variables, pop its frame
//                      off the runtime stack, and return to the
//                      caller's intermediate code.
//
//      pRoutineId : ptr to routine name's symbol table node
//--------------------------------------------------------------

void TExecutor::ExitFunction(TSymtabNode *pRoutineId)
{
	//debug
	cout<<"\n***** saiu "<<pRoutineId->String()<<" *****"<<endl;

	runStack.PopFrame();
	
	TSymtabNode *main=symtabStack.SearchAll("main");
	
	if(pRoutineId != main)
	{
		TSymtabNode *pCaller=(TSymtabNode*)Pop()->address;
		icode=*(pCaller->defn.routine.pIcode);
		//retiramos o endere�o de retorno
		int *pCurrentLocation=(int*)Pop()->address;
		
		icode.GoTo(*pCurrentLocation);
		delete pCurrentLocation;
		
		//retiro a stack da funcao anterior e coloco da nova
		symtabStack.PopSymtab();
		symtabStack.PushSymtab(pCaller->defn.routine.pSymtab);
		
		GetToken();
		pCurrentFunction=pCaller;
	}
}




