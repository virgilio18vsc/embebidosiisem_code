#include "stdafx.h"

int errorCount=0;					//conta o numero de erros de syntax
int errorArrowFlag=False;
int errorArrowOffset=8;

static char *abortMsg[]={
	NULL,
	"Invalid command line arguments",
	"Failed to open source file",
	"Failed to open intermediate form file",
	"Failed to open assembly file",
	"Too many syntax errors",
	"Stack overflow",
	"Code segment overflow",
	"Nestling too deep",
	"Runtime error",
	"Unimplemented feature",
};

void AbortTranslation(TAbortCode ac)
{
	cerr<<"**** Fatal translation error: "<<abortMsg[-ac]<<endl; system("pause");
	exit(ac);
}

static char *errorMessages[]=
{
	"No Error",
	"Unrecognizable input",
	"Too many syntax errors",
	"Unexpected end of file",
	"Identifer not found",
	"Invalid number",
	"Invalid fraction",
	"Invalid exponent",
	"Too many digits",
	"Real literal out of range",
	"Integer literal out of range",
	"Missing )",
	"Missing (",
	"Missing }",
	"Missing {",
	"Missing ]",
	"Missing [",
	"Invalid expression",
	"Invalid assignment statement",
	"Missing identifier",
	"Undefined identifier",
	"Stack overflow",
	"Invalid statment",
	"Unexpected token",
	"Missing ;",
	"Missing ,",
	"Missing do",
	"Invalid for control variable",
	"Invalid constant",
	"Missing constant",
	"Redifined identifier",
	"Missing =",
	"Invalid type",
	"Not a type identifier",
	"Invalid subrange type",
	"Not a constant identifier",
	"Inconpatible types",
	"Invalid assignement target",
	"Invalid identifier usage",
	"Incompatible assignment",
	"Min limit greater than max limit",
	"Invalid index type",
	"Missing .",
	"Invalid field",
	"Nesting too deep",
	"Already specified in forward",
	"Wrong number parameters",
	"Invalid parameter",
	"Missing variable",
	"Code segment overflow",
	"Missing :",
	"Unexpected end line",
	"Missing Entry Function main",
	"Unimplemented feature",
};


void Error(TErrorCode ec)
{
	const int maxSyntaxErrors=25;

	int errorPosition=errorArrowOffset + inputPosition -1;

	if(errorArrowFlag)
	{
		sprintf(list.text,"%*s^",errorPosition,"");
		list.PutLine();
	}
	
	sprintf(list.text,"*** In line %d ERROR: %s",currentLineNumber, errorMessages[ec]);
	list.PutLine();

	if(++errorCount > maxSyntaxErrors)
	{
		list.PutLine("Too many syntax errors. Translation aborted.");
		AbortTranslation(abortTooManySintaxErrors);
	}
}


//--------------------------------------------------------------
//  Runtime error messages      Keyed to enumeration type
//                              TRuntimeErrorCode.
//--------------------------------------------------------------

char *runtimeErrorMessages[] = {
    "No runtime error",
    "Runtime stack overflow",
    "Value out of range",
    "Invalid CASE expression value",
    "Division by zero",
    "Invalid standard function argument",
    "Invalid user input",
    "Unimplemented runtime feature",
};

//--------------------------------------------------------------
//  RuntimeError        Print the runtime error message and then
//                      abort the program.
//
//	ec : error code
//--------------------------------------------------------------

void RuntimeError(TRuntimeErrorCode ec)
{
    extern int currentLineNumber;

    cout << endl;
    cout << "*** RUNTIME ERROR in line " << currentLineNumber << ": "
	 << runtimeErrorMessages[ec] << endl;

    exit(abortRuntimeError);
}





