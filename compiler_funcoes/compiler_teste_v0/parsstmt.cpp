#include "stdafx.h"

void TParser::ParseStatement(void)
{
	//InsertLineMarker();
    //--Call the appropriate parsing function based on
    //--the statement's first token.
    switch (token) 
	{
		case tcStar :
			while(token == tcStar) 
			{ 
				icode.Put(token);
				GetToken();
			}
		case tcIdentifier:  
			ParseAssignment();
			CondGetToken(tcSemicolon,errMissingSemicolon);
			break;
		case tcAsm:			ParseAsmStatement();break;
		case tcWhile:       ParseWHILE();       break;
		case tcIf:          ParseIF();          break;
		case tcFor:         ParseFOR();         break;
		case tcSwitch:      ParseSWITCH();      break;
		case tcReturn:		ParseRETURN();		break;
    }
}


void TParser::ParseStatementList(TTokenCode terminator)
{
    //--Loop to parse statements and to check for and skip semicolons.
    do {
	ParseStatement();

	if (TokenIn(token, tlStatementStart)) {
	    Error(errMissingSemicolon);
	}
	else while (token == tcSemicolon) GetTokenAppend();
    } while ((token != terminator) && (token != tcEndOfFile));
}


void TParser::ParseAssignment(void)
{
	int parametro=0;													//variavel usada para testar os parametros de chamadas de funcoes
	TSymtabNode *pTargetNode=symtabStack.SearchAll(pToken->String());
	if(!pTargetNode){pTargetNode=EnterLocal(pToken->String());}
	
	icode.Put(tcIdentifier);
	icode.Put(pTargetNode);
    GetTokenAppend();
	
    switch(token)
	{
	case tcEq:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcPlusPlus:
			GetTokenAppend();
			if(token == tcEq)
			{
				GetTokenAppend();
				ParseExpression();
			}
		break;

	case tcPlusEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcMinusMinus:
			GetTokenAppend();
			if(token == tcEq)
			{
				GetTokenAppend();
				ParseExpression();
			}
		break;

	case tcMinusEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcStarEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcSlashEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcOrEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcAndEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcXorEqual:
			GetTokenAppend();
			ParseExpression();
		break;

	case tcLParen:{
			TSymtabNode **ListParam;
			ListParam=new TSymtabNode *[pTargetNode->defn.routine.pSymtab->NodeCount()];
			pTargetNode->defn.routine.pSymtab->Root()->Convert(ListParam);						//transforma a arvore em lista para procurar pelos parametros

			do
			{
				GetTokenAppend();
				if(token != tcRParen){ParseExpression();}

			}while(token == tcComma);

			CondGetTokenAppend(tcRParen,errMissingRightParen);

		}break;

	case tcLSquareBracket:
		GetTokenAppend();
		ParseExpression();
		GetTokenAppend();
		if(token == tcPeriod)
		{
			GetTokenAppend();
			TSymtabNode *campo=pTargetNode->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
			icode.Put(campo);
			if(!campo){Error(errInvalidParm);}
			GetTokenAppend();
		}
		GetTokenAppend();
		ParseExpression();
		break;

	case tcPeriod:{
		GetTokenAppend();
		TSymtabNode *campo=pTargetNode->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
		if(!campo){Error(errIdentifierNotFound);}
		icode.Put(campo);
		GetTokenAppend();
		GetTokenAppend();
		ParseExpression();
		}break;

	case tcArrow:{
		GetTokenAppend();
		TSymtabNode *campo=pTargetNode->pType->pTypeId->defn.Struct.pSymtab->Search(pToken->String());
		if(!campo){Error(errIdentifierNotFound);}
		icode.Put(campo);
		GetTokenAppend();
		GetTokenAppend();
		ParseExpression();
		}break;

	default:
		Error(errInvalidAssignment);
	}
}



//--------------------------------------------------------------
//  ParseWHILE      Parse a WHILE statement.:
//
//                      WHILE <expr> DO <stmt>
//--------------------------------------------------------------

void TParser::ParseWHILE(void)
{
	icode.Put(tcWhile);
	GetTokenAppend();
	
	//espera o (
	CondGetTokenAppend(tcLParen,errMissingLeftParen);

	//--<expr>
    ParseExpression();

	//espera )
	CondGetTokenAppend(tcRParen,errMissingRightParen);

	//espera {
	CondGetToken(tcLBracket,errMissingLeftBracket);
	
	//processa o que esta dentro do while
	while (token != tcRBracket)
	{
		ParseStatement();
		
	}
	
	icode.Put(tcRBracket); 
	//GetTokenAppend();
	if(token != tcRBracket)
	{
		Error(errMissingRightBracket);
	}
	GetToken();
}



void TParser::ParseIF(void)
{
	icode.Put(tcIf);
	GetTokenAppend();
	
	//espera o (
	CondGetTokenAppend(tcLParen,errMissingLeftParen);

	//--<expr>
    ParseExpression();

	//espera )
	CondGetTokenAppend(tcRParen,errMissingRightParen);

	//espera {
	CondGetToken(tcLBracket,errMissingLeftBracket);

	//processa o que esta dentro do if
	while(token != tcRBracket)
	{
		ParseStatement();
	}
	
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket,errMissingRightBracket);
	
	if(token != tcElse){ return;}			//se o if nao tiver else entao devemos sair
	
	icode.Put(tcElse);
	GetTokenAppend();

	//espera {
	CondGetToken(tcLBracket,errMissingLeftBracket);

	//processa o que esta dentro do else
	while(token != tcRBracket)
	{
		ParseStatement();
	}
	
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket,errMissingRightBracket);
}


void TParser::ParseFOR(void)
{
	icode.Put(tcFor);
    GetTokenAppend();
	//espera o (
	CondGetToken(tcLParen, errMissingLeftParen);
	ParseAssignment();
	CondGetTokenAppend(tcSemicolon, errMissingSemicolon);
	ParseExpression();
	CondGetToken(tcSemicolon, errMissingSemicolon);
	ParseAssignment();

	//espera o )
	CondGetTokenAppend(tcRParen, errMissingRightParen);
	CondGetToken(tcLBracket, errMissingLeftBracket);

	while(token != tcRBracket)
	{
		ParseStatement();
	}
	
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket, errMissingRightBracket);
}


//--------------------------------------------------------------
//  ParseCASE       Parse a CASE statement:
//
//                      CASE <expr> OF
//                          <case-branch> ;
//                          ...
//                      END
//--------------------------------------------------------------

void TParser::ParseSWITCH(void)
{
	icode.Put(tcSwitch); 
	GetTokenAppend();
	
	//espera (
	CondGetTokenAppend(tcLParen,errMissingLeftParen);
	
	//--<expr>
    ParseExpression();
	//espera )
	icode.Put(tcRParen);
	CondGetTokenAppend(tcRParen,errMissingRightParen);
	
	//espera {
	CondGetTokenAppend(tcLBracket,errMissingLeftBracket);
	
	do
	{
		ParseCase();
	}while(token == tcCase);

	if(token == tcDefault)
	{
		//espera :
		GetTokenAppend();
		CondGetToken(tcColon,errMissingColon);
		do
		{
			ParseStatement();
		}while(token != tcRBracket && token != tcBreak);

		if(token == tcBreak)
		{
			icode.Put(token);
			GetTokenAppend();
			GetToken();
		}
	}
	
	//espera }
	icode.Put(tcRBracket);
	CondGetToken(tcRBracket,errMissingRightBracket);
}


//--------------------------------------------------------------
//  ParseCase     Parse a CASE branch:
//
//                          <case-label-list> : <stmt>
//--------------------------------------------------------------

void TParser::ParseCase(void)
{	
	GetTokenAppend();
	//espera um numero
	if(token != tcNumber){Error(errInvalidParm);}
	TSymtabNode *pNode = symtabStack.EnterLocal(pToken->String());
	pNode->defn.constant.value = pToken->Value();
	icode.Put(pNode);

	GetTokenAppend();
	//espera :
	CondGetToken(tcColon,errMissingColon);

	//executa o case
	while(token != tcRBracket && token != tcCase && token != tcBreak)
	{
		ParseStatement();	
	}

	if(token == tcCase){icode.Put(tcCase);}
	
	//verifica se tem break;
	if(token == tcBreak)
	{
		icode.Put(tcBreak);
		GetTokenAppend();
		CondGetTokenAppend(tcSemicolon,errMissingSemicolon);
	}
}



//--------------------------------------------------------------
//  Parse Return     
//
//                          return identifier
//--------------------------------------------------------------

void TParser::ParseRETURN(void)
{
	icode.Put(tcReturn);
	GetTokenAppend();
	ParseExpression();

	CondGetToken(tcSemicolon,errMissingRightBracket);
}



void TParser::ParseAsmStatement(void)
{
	icode.Put(tcAsm);
	GetTokenAppend();
	GetTokenAppend();
	TSymtabNode *registo;

	while (token != tcRParen)
	{
		switch (token)
		{
		case tcNOP:
			GetTokenAppend();
			break;

		case tcMUL:

		case tcADDI:
		case tcADD:
		case tcSUB:
		case tcSUBI:
		case tcCMP:
		case tcOR:
		case tcORI:
		case tcAND:
		case tcANDI:
		case tcXOR:
		case tcXORI:

		case tcLW:
		case tcLWI:
		case tcLWBI:
		case tcSW:
		case tcSWI:
		case tcSWBI:

		case tcBSL:
		case tcBSR:

			GetTokenAppend();
			registo=symtabStack.EnterLocal(pToken->String(),dcRegister);
			icode.Put(registo);
			GetToken();

			CondGetTokenAppend(tcComma,errMissingComma); 
			registo=symtabStack.EnterLocal(pToken->String(),dcRegister);
			icode.Put(registo);
			GetToken();

			CondGetTokenAppend(tcComma,errMissingComma); 
			registo=symtabStack.EnterLocal(pToken->String(),dcRegister);
			if(token == tcNumber){registo->defn.constant.value.integer = pToken->Value().integer;}
			icode.Put(registo);
			GetTokenAppend(); 
			break;

		case tcNOT:
		case tcBR:
		case tcBRI:
		case tcBRA:
		case tcBRAI:
		case tcIMM:
		case tcCALL:
		case tcPUSH:
		case tcPOP:
			GetTokenAppend();
			registo=symtabStack.EnterLocal(pToken->String(),dcRegister);
			if(token == tcNumber){registo->defn.constant.value.integer = pToken->Value().integer;}
			icode.Put(registo);
			GetTokenAppend();
			break;

		case tcBEQ:
		case tcBNE:
		case tcBGE:
		case tcBGT:
		case tcBLE:
		case tcBLT:
		case tcMOV:
		case tcMOVI:
			GetTokenAppend();
			registo=symtabStack.EnterLocal(pToken->String(),dcRegister);
			icode.Put(registo);
			GetToken();

			CondGetTokenAppend(tcComma,errMissingComma); 
			registo=symtabStack.EnterLocal(pToken->String(),dcRegister);
			if(token == tcNumber){registo->defn.constant.value.integer = pToken->Value().integer;}
			icode.Put(registo);
			GetTokenAppend();
			break;

		case tcRET:
		case tcRETI:
			GetTokenAppend();
			break;

		default:
			break;
		}
	}
	GetTokenAppend();
	CondGetToken(tcSemicolon,errMissingSemicolon);
}



