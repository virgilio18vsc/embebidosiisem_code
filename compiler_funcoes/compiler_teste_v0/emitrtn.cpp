#include "stdafx.h"

//--------------------------------------------------------------
//  EmitProgramPrologue         Emit the program prologue.
//--------------------------------------------------------------
int heapOffset;
void TCodeGenerator::EmitProgramPrologue(void)
{
	//*****************************************************************************
	//		cabecalho da memoria
	PutLine("memory_initialization_radix=16;");
	PutLine("\n");
	PutLine("memory_initialization_vector=");
	PutLine("\n");

	//forca a saltar para a posicao
	Emit2(BRAI,0,8,33);		//para reutilizar opcode BRAI obriga a que Ra seja 8
    PC=33;					//da pos 1 ate a 32 sao espa�os para o vetor de interrupcoes
	
	//inicializa o vetor com o endereco das variaveis globais
	Emit2(MOVI,GLOBAL_VAR_REG,2,32);

	//INICIALIZA AS VARIAVEIS GLOBAIS
	heapOffset=0;
	TRegister *reg;
	for(int i=0;i<globalSymtab.NodeCount();i++)
	{
		if(globalSymtab.Get(i)->defn.how==dcVariable || globalSymtab.Get(i)->defn.how==dcPointer)
		{
			if(globalSymtab.Get(i)->pType == pRealType || globalSymtab.Get(i)->pType == pIntegerType || globalSymtab.Get(i)->pType == pCharType)
			{
				globalSymtab.Get(i)->defn.data.offset=heapOffset;
				reg=regFile.GetRegister(globalSymtab.Get(i));
				if(globalSymtab.Get(i)->defn.constant.value.integer > 0xFFFF){Emit2(IMM,0,0,(globalSymtab.Get(i)->defn.constant.value.integer >> 16) & 0xFFFF)}
				Emit2(MOVI,reg->GetReg(),0,globalSymtab.Get(i)->defn.constant.value.integer);
				Emit2(SWI,reg->GetReg(),GLOBAL_VAR_REG,heapOffset);
				heapOffset++;
			}
			else
			{
				globalSymtab.Get(i)->defn.data.offset=heapOffset;
				heapOffset += globalSymtab.Get(i)->pType->pTypeId->defn.Struct.pSymtab->NodeCount();
			}
		}

		if(globalSymtab.Get(i)->defn.how==dcArray)
		{
			globalSymtab.Get(i)->defn.data.offset=heapOffset;
			heapOffset++;
			reg=regFile.GetRegister(globalSymtab.Get(i));
			Emit2(MOVI,reg->GetReg(),0,heapOffset);
			Emit2(SWI,reg->GetReg(),GLOBAL_VAR_REG,heapOffset-1);
			heapOffset += globalSymtab.Get(i)->defn.vetor.elemCount;
		}

	}
	
	//Liberta os registos usados
	for(int i=0;i<globalSymtab.NodeCount();i++)
	{
		if(globalSymtab.Get(i)->defn.how==dcVariable || globalSymtab.Get(i)->defn.how==dcPointer)
		{
			regFile.FreeRegister(globalSymtab.Get(i));
		}
	}

	Emit1(PUSH,0,0,0);												//dummy PUSH, para depois colocar aqui o endere�o de retorno
	Emit1(PUSH,FRAME_BASE_REG,0,0);
	Emit1(MOV,FRAME_BASE_REG,STACK_POINTER,0);	

	//devia de por aqui um call 
	TSymtabNode *func_main=globalSymtab.Search("main");
	list.Push(PC,func_main);
	EmitCALL(0x00);											

	//devia de por um while(1); ou halt
	Emit2(BRI,16,0,0);										//se rd tiver 16 salta para tras se nao salta pa frente
	
}

//--------------------------------------------------------------
//  EmitProgramEpilogue         Emit the program epilogue.
//--------------------------------------------------------------

void TCodeGenerator::EmitProgramEpilogue(void)
{
	
}

//--------------------------------------------------------------
//  EmitMain            Emit code for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunction(TSymtabNode *pMainId)
{
	EmitFunctionPrologue(pMainId);

	EmitStatmentList(tcEndOfFile);

	EmitFunctionEpilogue(pMainId);
}

//--------------------------------------------------------------
//  EmitFunctionPrologue    Emit the prologue for a function.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunctionPrologue(TSymtabNode *pMainId)
{
	//aqui e o ponto de inicio de uma funcao, aqui tenho de atualizar o campo defn.routine.startAddr
	pMainId->defn.routine.startAddr=PC;
	//limpa o register file
	regFile.CleanRegFile();

	if(!CheckForISR(pMainId))
	{
		TSymtabNode *temporary_symbol=globalSymtab.Search("temporary_symbol");
		if(!temporary_symbol){temporary_symbol=globalSymtab.Enter("temporary_symbol");}		
		TRegister *reg=regFile.GetRegister(temporary_symbol);
		//TRegister *reg2=regFile.GetRegister(temporary_symbol);

		Emit1(POP,reg->GetReg(),0,0);													//coloca o return addr no sitio correto
		Emit2(SWI,reg->GetReg(),FRAME_BASE_REG,2);

		//liberta os registos temporarios
		regFile.FreeRegister(temporary_symbol);

		//ciclo para contar o numero de variaveis locais para ver se todas cabem nos registos
		int num_variaveis=0;
		for(int i=0;i < symtabStack.GetCurrentSymtab()->NodeCount();i++)
		{
			if(symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcVariable || symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcPointer)
			{
				num_variaveis++;
			}

			if(symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcArray)
			{
				if(symtabStack.GetCurrentSymtab()->Get(i)->pType == pIntegerType || symtabStack.GetCurrentSymtab()->Get(i)->pType == pRealType || symtabStack.GetCurrentSymtab()->Get(i)->pType == pCharType)
				{
					symtabStack.GetCurrentSymtab()->Get(i)->defn.vetor.offset=heapOffset;
					symtabStack.GetCurrentSymtab()->Get(i)->defn.constant.value.integer=heapOffset;
					heapOffset += symtabStack.GetCurrentSymtab()->Get(i)->defn.vetor.elemCount;
					}
				else
				{
					symtabStack.GetCurrentSymtab()->Get(i)->defn.vetor.offset=heapOffset;
					symtabStack.GetCurrentSymtab()->Get(i)->defn.constant.value.integer=heapOffset;
					heapOffset += symtabStack.GetCurrentSymtab()->Get(i)->defn.vetor.elemCount * symtabStack.GetCurrentSymtab()->Get(i)->pType->pTypeId->defn.Struct.pSymtab->NodeCount();
				}
			}
		}

		int rx=1;	int offset=pMainId->defn.routine.parmCount;				//variavel para atribuir um novo registo caso todos estejam ocupdos
		if(num_variaveis < REG_NUMBER - 4)		//se todas as variaveis couberem em registos...
		{
			TRegister *reg=NULL;
			for(int i=0;i < symtabStack.GetCurrentSymtab()->NodeCount();i++)
			{
				if(symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcVariable || symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcPointer || symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcArray)
				{
					if(symtabStack.GetCurrentSymtab()->Get(i)->pType == pRealType)
					{
						reg=regFile.GetRegister(symtabStack.GetCurrentSymtab()->Get(i));
						//converter para ieee 754
						unsigned int representation=floatToRepresentation(symtabStack.GetCurrentSymtab()->Get(i)->defn.constant.value.real);
						if(representation > 0xFFFF)
						{
							Emit2(IMM,0,0,((representation >> 16) & 0xFFFF));
						}
						Emit2(MOVI,reg->GetReg(),0,representation & 0xFFFF);
						Emit1(PUSH,reg->GetReg(),0,0);
						symtabStack.GetCurrentSymtab()->Get(i)->defn.data.offset=offset++;
					}
					else if(symtabStack.GetCurrentSymtab()->Get(i)->pType == pIntegerType)
					{
						reg=regFile.GetRegister(symtabStack.GetCurrentSymtab()->Get(i));
						if(!reg)
						{
							//emit push rx
							regFile.FreeRegister(rx++);
							reg=regFile.GetRegister(symtabStack.GetCurrentSymtab()->Get(i));
						}
			
						Emit2(MOVI,reg->GetReg(),0,symtabStack.GetCurrentSymtab()->Get(i)->defn.constant.value.integer);
						Emit1(PUSH,reg->GetReg(),0,0);
						symtabStack.GetCurrentSymtab()->Get(i)->defn.data.offset=offset++;
					}
					else
					{
						symtabStack.GetCurrentSymtab()->Get(i)->defn.data.offset=offset++;
						if(symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcArray)
						{
							reg=regFile.GetRegister(temporary_symbol);
							Emit2(MOVI,reg->GetReg(),0,symtabStack.GetCurrentSymtab()->Get(i)->defn.constant.value.integer);
							Emit1(PUSH,reg->GetReg(),0,0);
						}
						else if(symtabStack.GetCurrentSymtab()->Get(i)->defn.how == dcPointer)
						{
							reg=regFile.GetRegister(temporary_symbol);
							Emit2(MOVI,reg->GetReg(),0,symtabStack.GetCurrentSymtab()->Get(i)->defn.data.offset);
							Emit1(PUSH,reg->GetReg(),0,0);
						}
						else
						{
							for(int j=0;j<symtabStack.GetCurrentSymtab()->Get(i)->pType->pTypeId->defn.Struct.pSymtab->NodeCount();j++)
							{
								reg=regFile.GetRegister(symtabStack.GetCurrentSymtab()->Get(i));
								Emit1(PUSH,reg->GetReg(),0,0);
								offset++;
							}
							offset--;
							regFile.FreeRegister(reg->GetReg());
							}
					}
					
				}
			}
			regFile.CleanRegFile();
		}
	}
}

//--------------------------------------------------------------
//  EmitMainEpilogue    Emit the epilogue for the main routine.
//--------------------------------------------------------------

void TCodeGenerator::EmitFunctionEpilogue(TSymtabNode *pMainId)
{
	if(CheckForISR(pMainId))
	{
		//quando descobrir os registos que uso, tenho que restabelecer o valor original do momento da chamada
		Emit1(RETI,0,0,0);

		//ve os registos que a ISR usou
		int usados[32]; int especieDeApontador=0;
		int opcode; int rd; 

		int codeMemPtr=pMainId->defn.routine.startAddr;
		//verifica os registos usados na ISR
		do
		{
			opcode = (CodeMem[codeMemPtr] >> 26) & 0x3F;
			rd = (CodeMem[codeMemPtr] >> 21) & 0x1F;

			int i;
			for(i=0;i<especieDeApontador;i++)
			{
				if(usados[i] == rd){break;}
			}

			if(i == especieDeApontador && rd != 0)
			{
				usados[especieDeApontador++]=rd;
			}

			codeMemPtr++;
		}while(opcode != RETI);

		//desloca o codigo na memoria para colocar os push dos registos usados
		codeMemPtr=PC;
		while(codeMemPtr >= pMainId->defn.routine.startAddr)
		{
			CodeMem[codeMemPtr + especieDeApontador]=CodeMem[codeMemPtr];
			CodeMem[codeMemPtr]=0;
			codeMemPtr--;
		}

		PC += especieDeApontador;

		//faz push aos registos usados
		int PCholder=PC;
		PC=pMainId->defn.routine.startAddr;
		
		int iter=especieDeApontador;
		while(iter > 0)
		{
			iter--;
			Emit1(PUSH,usados[iter],0,0);
		}

		//desloca o RETI para colocar os POP
		PC=PCholder;
		CodeMem[PC + especieDeApontador - 1]=CodeMem[PC - 1];
		CodeMem[PC - 1]=0;

		PC=PC + especieDeApontador;
		PCholder=PC;

		PC = PC - especieDeApontador - 1;
	
		//coloca os pops na ordem inversa dos push
		iter=0;
		while(iter < especieDeApontador)
		{
			Emit1(POP,usados[iter++],0,0);
		}

		PC=PCholder;										//recupera o PC
	}
	else
	{
		//ver se foi posto algum return se nao por
		int aux=pMainId->defn.routine.startAddr;
		int opcode=0;
		while(aux < PC && opcode != RET)
		{
			opcode = (CodeMem[aux] >> 26 ) & 0x3F;
			if(opcode != RET){aux++;}
		}
		if(aux >= PC)
		{
			Emit1(MOV,31,FRAME_BASE_REG,0);
			Emit1(POP,FRAME_BASE_REG,0,0);
			Emit1(RET,0,0,0);
		}
	}
}


//--------------------------------------------------------------
//  EmitStatementList        emit a statement list until
//                              the terminator token.
//
//      terminator : the token that terminates the list
//--------------------------------------------------------------

void TCodeGenerator::EmitStatmentList(TTokenCode terminator)
{
	GetToken();
	do 
	{
		EmitStatement();
		while (token == tcSemicolon) {GetToken();}
    } while (token != terminator);
}
//endfig


