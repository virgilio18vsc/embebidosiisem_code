#ifndef __CODEGEN_H__
#define __CODEGEN_H__

#include "stdafx.h"

//--------------------------------------------------------------
//  Runtime stack frame items
//--------------------------------------------------------------

#define STATIC_LINK             "$STATIC_LINK"
#define RETURN_VALUE            "$RETURN_VALUE"
#define HIGH_RETURN_VALUE       "$HIGH_RETURN_VALUE"

//--------------------------------------------------------------
//		TRegister		gere os registos usados pelo processador
//
//--------------------------------------------------------------
class TRegister
{
	int reg;
	TSymtabNode *symbol;

public:
	TRegister(){}
	TRegister(int Reg, TSymtabNode* Symbol);
	int GetReg(void);
	TSymtabNode* GetSymbolAssociated(void);
	void SetSymbol(TSymtabNode* Symbol);
};

class TRegisterFile
{
	TRegister *registos[REG_NUMBER];

public:
	TRegisterFile(void);
	TRegister* GetRegister(TSymtabNode* var);
	void FreeRegister(TSymtabNode* var);
	void FreeRegister(int reg);
	void CleanRegFile(void);
	TRegister* FindRegister(TSymtabNode* var);
	TRegister* FindRegister(int reg);

	void PrintRegFile(void);
};

class ExprStackData
{
	public:
		TTokenCode token;
		TRegister *reg;
		TDataValue val;
		TSymtabNode *identifier;
};

class TExpressionStack
{
	ExprStackData stack[64];
	ExprStackData *tos;

public:
	TExpressionStack(void){tos=&stack[0];}
	void Push(TTokenCode tk,TRegister* reg){tos->token=tk; tos->reg=reg; tos++; }
	void Push(TTokenCode tk,int value){tos->token=tk; tos->val.integer=value; tos++; }
	void Push(TTokenCode tk,float value){tos->token=tk; tos->val.real=value; tos++; }
	ExprStackData Pop(void){tos--; return *tos;}

	void Print(void)
	{
		for(ExprStackData* aux=&stack[0];aux<tos;aux++ )
		{
			if(aux->token == tcNumber)
			{
				cout<<"num = "<<aux->val.integer<<endl;
			}
			else
			{
				cout<<"reg = r"<<aux->reg->GetReg()<<endl;
			}
		}
	}

	int Elements(void){return (tos-stack);}
};



//*****************************************************************
//esta classe e usada para resolver por ex a++ em que so deve ser feito o ++ a variavel depois de finalizada a expressao
class TPostOperations
{
	enum{StackMaxSize = 16};
	ExprStackData stack[StackMaxSize];
	ExprStackData *tos;

public:
	TPostOperations(void){tos=&stack[0];}
	void Push(TTokenCode token,TSymtabNode* symbol){tos->token=token; tos->identifier=symbol; tos++;}
	ExprStackData Pop(void){tos--; return *tos;}
	int Elements(void){return (tos-stack);}
};


//--------------------------------------------------------------
//  TAssemblyBuffer     Assembly language buffer subclass of
//                      TTextOutBuffer.
//--------------------------------------------------------------

class TAssemblyBuffer : public TTextOutBuffer {
    enum {maxLength = 72,};

    fstream  file;        // assembly output file
    char    *pText;       // assembly buffer pointer
    int      textLength;  // length of assembly comment

public:
    TAssemblyBuffer(const char *pAsmFileName, TAbortCode ac);

    char *Text (void) const { return pText; }

    void Reset(void)
    {
		pText = text;
		text[0] = '\0';
		textLength = 0;
    }

    void Put(char ch) { *pText++ = ch; *pText = '\0'; ++textLength; }
    virtual void PutLine(void) { file << endl; }

	void writeFile(char *pText){file<<pText;}

    void PutLine(const char *pText)
    {
		TTextOutBuffer::PutLine(pText);
    }

	void Put(int num)
	{
		char *pString=new char [10];
		_itoa(num,pString,10);
		strcpy(pText, pString);
		Advance();
	}

    void Advance(void);

    void Put(const char *pString)
    {
		strcpy(pText, pString);
		Advance();
    }

    void Reset(const char *pString) { Reset(); Put(pString); }

    int Fit(int length) const
    {
		return textLength + length < maxLength;
    }
};



//--------------------------------------------------------------
//  TCodeGenerator      Code generator subclass of TBackend.
//--------------------------------------------------------------

class TCodeGenerator : public TBackend 
{
	//teste
	TRegisterFile		regFile;
	TRegisterFile		backupRegFile;

	TExpressionStack	genStack;
	TPostOperations		postStack;
	TJmpList			list;

    TAssemblyBuffer *const pAsmBuffer;

    //--Pointers to the list of all the float and string literals
    //--used in the source program.
    TSymtabNode *pFloatLitList;
    TSymtabNode *pStringLitList;


	bool CheckForISR(TSymtabNode *pId);

    //void Reg              (TRegister r);
    //void Operator         (TInstruction opcode);
    void Label            (const char *prefix, int index);
    void WordLabel        (const char *prefix, int index);
    void HighDWordLabel   (const char *prefix, int index);
    void Byte             (const TSymtabNode *pId);
    void Word             (const TSymtabNode *pId);
    void HighDWord        (const TSymtabNode *pId);
    //void ByteIndirect     (TRegister r);
    //void WordIndirect     (TRegister r);
    //void HighDWordIndirect(TRegister r);
    void TaggedName       (const TSymtabNode *pId);
    void NameLit          (const char *pName);
    void IntegerLit       (int n);
    void CharLit          (char ch);

    void EmitStatementLabel(int index);

	//metodos para gerir os inteiros
	void EmitIntLogicAnd		(ExprStackData op1, ExprStackData op2);
	void EmitIntLogicOr			(ExprStackData op1, ExprStackData op2);

	void EmitIntEqual			(ExprStackData op1, ExprStackData op2);
	void EmitIntNotEqual		(ExprStackData op1, ExprStackData op2);
	void EmitIntGreatThan		(ExprStackData op1, ExprStackData op2);
	void EmitIntGreatEqual		(ExprStackData op1, ExprStackData op2);
	void EmitIntLessThan		(ExprStackData op1, ExprStackData op2);
	void EmitIntLessEqual		(ExprStackData op1, ExprStackData op2);

	void EmitIntADD				(ExprStackData op1, ExprStackData op2);
	void EmitIntSUB				(ExprStackData op1, ExprStackData op2);
	void EmitIntLShift			(ExprStackData op1, ExprStackData op2);
	void EmitIntRShift			(ExprStackData op1, ExprStackData op2);
	void EmitIntOR				(ExprStackData op1, ExprStackData op2);
	void EmitIntXOR				(ExprStackData op1, ExprStackData op2);

	void EmitIntAND				(ExprStackData op1, ExprStackData op2);
	void EmitIntMultiplication	(ExprStackData op1, ExprStackData op2);
	void EmitIntDivision		(ExprStackData op1, ExprStackData op2);
	void EmitIntDivisionModule	(ExprStackData op1, ExprStackData op2);

	void EmitIntLogicNot		(void);
	void EmitIntBitNot			(void);

	//metodos para gerir os floats
	int floatToRepresentation	(float num);
	int floatToInt				(float num);
	void floatToInt				(void);
	void intToFloat				(void);

	void EmitFloatADD(void);
	void EmitFloatMUL(void);
	void EmitFloatDIV(void);
	void callFloatOperation(ExprStackData op1, ExprStackData op2,char* str);
	void callFloatOperation(ExprStackData op1, char* str);

    //--Program
    void EmitProgramPrologue	(void);
    void EmitProgramEpilogue	(void);
    void EmitFunction			(TSymtabNode *pMainId);
    void EmitFunctionPrologue	(TSymtabNode *pMainId);
    void EmitFunctionEpilogue	(TSymtabNode *pMainId);
	void EmitStatmentList		(TTokenCode terminator);

    //--Routines
	void InitializeInternalLibs(void);
	void PrintInternalLibs(void);

    //--Standard routines
    TType *EmitReadReadlnCall  (const TSymtabNode *pRoutineId);
    TType *EmitWriteWritelnCall(const TSymtabNode *pRoutineId);
    TType *EmitEofEolnCall     (const TSymtabNode *pRoutineId);
    TType *EmitAbsSqrCall      (const TSymtabNode *pRoutineId);
    TType *EmitArctanCosExpLnSinSqrtCall(const TSymtabNode *pRoutineId);
    TType *EmitPredSuccCall    (const TSymtabNode *pRoutineId);
    TType *EmitChrCall         (void);
    TType *EmitOddCall         (void);
    TType *EmitOrdCall         (void);
    TType *EmitRoundTruncCall  (const TSymtabNode *pRoutineId);

    //--Declarations
    void EmitDeclarations     (const TSymtabNode *pRoutineId);
    void EmitStackOffsetEquate(const TSymtabNode *pId);

    //--Loads and pushes
    void EmitAdjustBP     (int level);
    void EmitRestoreBP    (int level);
    void EmitLoadValue    (const TSymtabNode *pId);
    void EmitLoadFloatLit (TSymtabNode *pNode);
    void EmitPushStringLit(TSymtabNode *pNode);
    void EmitPushOperand  (const TType *pType);
    void EmitPushAddress  (const TSymtabNode *pId);
    void EmitPushReturnValueAddress(const TSymtabNode *pId);
    void EmitPromoteToReal(const TType *pType1, const TType *pType2);

    //--Statements
    void EmitStatement    (void);
//    void EmitStatmentList (TTokenCode terminator);
	void EmitAssignPtr	  (void);
    void EmitAssignment   (TSymtabNode *pTargetId);
	void EmitAssignArray  (TSymtabNode *pTargetId);
	void EmitFunctionCall (TSymtabNode *pTargetId);

	void type1(int* rd,int* ra, int* rb);
	void type2(int* rd,int* ra, int* imm);
	void type3(int* ra, int* rb);

	void EmitAsmStatement (void);
    void EmitREPEAT       (void);
    void EmitWHILE        (void);
    void EmitIF           (void);
    void EmitFOR          (void);
	void EmitSWITCH		  (void);
    void EmitCASE         (void);
    void EmitCompound     (void);
	void EmitReturn		  (void);

    //--Expressions
    TType *EmitExpression(void);
	TType *EmitLogicExpression(void);
    TType *EmitSimpleExpression(void);
    TType *EmitTerm      (void);
    TType *EmitFactor    (void);
    TType *EmitConstant  (TSymtabNode *pId);
    TType *EmitVariable  (TSymtabNode *pId);
	TType *EmitArray	 (TSymtabNode *pId);
    TType *EmitSubscripts(const TType *pType);
    TType *EmitField     (void);

	

    //--Assembly buffer
    char *AsmText(void)          { return pAsmBuffer->Text();  }
    void  Reset  (void)          { pAsmBuffer->Reset();        }
    void  Put    (char ch)       { pAsmBuffer->Put(ch);        }
    void  Put    (char *pString) { pAsmBuffer->Put(pString);   }
    void  PutLine(void)          { pAsmBuffer->PutLine();      }
	void  PutLine(char *pText)   { pAsmBuffer->writeFile(pText); }
    void  Advance(void)          { pAsmBuffer->Advance();      }

    //--Comments
    void PutComment  (const char *pString);
    void StartComment(int n);

    void StartComment(void) { Reset(); PutComment("; "); }

    void StartComment(const char *pString)
    {
		StartComment();
		PutComment(pString);
    }

    void EmitProgramHeaderComment    (const TSymtabNode *pProgramId);
    void EmitSubroutineHeaderComment (const TSymtabNode *pRoutineId);
    void EmitSubroutineFormalsComment(const TSymtabNode *pParmId);
    void EmitVarDeclComment          (const TSymtabNode *pVarId);
    void EmitTypeSpecComment         (const TType *pType);
    void EmitStmtComment             (void);
    void EmitAsgnOrCallComment       (void);
    void EmitREPEATComment           (void);
    void EmitUNTILComment            (void);
    void EmitWHILEComment            (void);
    void EmitIFComment               (void);
    void EmitFORComment              (void);
    void EmitCASEComment             (void);
    void EmitExprComment             (void);
    void EmitIdComment               (void);

public:
    TCodeGenerator(const char *pAsmName) : pAsmBuffer(new TAssemblyBuffer(pAsmName, abortAssemblyFileOpenFailed))
    {
		pFloatLitList = pStringLitList = NULL;
    }

    //virtual void Go(const TSymtabNode *pProgramId);
	virtual void Go(const TSymtabNode *pRoutineId);
};




#endif