#ifndef __ICODE_H__
#define __ICODE_H__

#include "stdafx.h"

using namespace std;

const TTokenCode mcLineMarker = ((TTokenCode)127);

class TSymtabNode;

class TIcode : public TScanner
{
	enum{codeSegmentSize=4096};
	//fstream file;							//ficheiro para o codigo intermedio
	//const char *const pFileName;			//nome do ficheiro do codigo intermedio
	//int size;								//tamanho do ficheiro

	//char **symtabStrings;					//vector of symbol table strings
	//int symtabCount;						//numero de strings no vector

	char *pCode;							//apontador para o segmento de codigo
	char *cursor;							//apontador para a localizacao actual
	TSymtabNode *pNode;						//apontador para o simbolo extraido da tabela de simbolos

	void CheckBounds(int size);
	TSymtabNode *GetSymtabNode(void);

public:
	//enum TMode{input,output};

	TIcode(void){pCode=cursor=new char[codeSegmentSize];}
	TIcode(const TIcode &icode);
	~TIcode(void){delete[] pCode;}

	void Put(TTokenCode tc);
	void Put(const TSymtabNode *pNode);
	void InsertLineMarker(void);

	void Reset(void){cursor=pCode;}
	void GoTo(int location){cursor=pCode+location;}

	int CurrentLocation(void) const {return cursor - pCode;}
	TSymtabNode* SymtabNode(void) const {return pNode;}

	virtual TToken *Get(void);

	void Print(void);
};

#endif