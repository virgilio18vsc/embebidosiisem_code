`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:33:19 04/20/2014 
// Design Name: 
// Module Name:    T0_T1_implementation 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module T0_T1_implementation(
	TxAINS,	TxBINS,	TxCINS,	TxDINS,
	TxAREL,	TxBREL,	TxCREL,	TxDREL,
	TxINC,
	T01IN0,	T01IN1,
	IN0, IN1, OUV_T2A, OUV_T2B,
	TxARUN, TxBRUN, TxCRUN, TxDRUN,
	TxAOV, TxBOV, 	TxCOV, TxDOV,
	RL_TxA, TNxDOV,
	TxASel, TxBSel, TxCSel, TxDSel,
	TxRASel, TxRBSel, TxRCSel, TxRDSel,
	data_in, clk
	);
	

/******************************************Inputs and outputs******************************************/

/*****Timer run signals***/
input TxARUN; 								//0 8-bit timer is not running, 1 8-bit timer is running.
input TxBRUN;								//0 8-bit timer is not running, 1 8-bit timer is running.
input TxCRUN; 								//0 8-bit timer is not running, 1 8-bit timer is running.
input TxDRUN;								//0 8-bit timer is not running, 1 8-bit timer is running.


/*****Timer configurations***/
input [1:0] TxAINS; 						//00 Clock input
input [1:0] TxBINS;						//01 CNT0
input [1:0] TxCINS;						//10 CNT1
input [1:0] TxDINS;						//11 Counter on overflow of timer (concatenation)

/****External signal selection CNT0 CNT1**/
input [1:0] T01IN0; 						// 00 Overflow T2x 01 Positive Edge
input [1:0] T01IN1; 						// 10 Negative Edge, 11 Both Edges(INx)

/****External clock entries that can be source in CNT0 and CNT1**/
input IN0;									// External clock entry.
input IN1;									// External clock entry.
input OUV_T2A;								// External clock entry (overflow subtimer A of timer 2)
input OUV_T2B;								// External clock entry (overflow subtimer B of timer 2)



/*****Reload Configurations***/
input TxAREL; 								// 0 Reload on TA, 1 Concatenation with TB
input TxBREL; 								// 0 Reload on TB, 1 Concatenation with TC
input TxCREL; 								// 0 Reload on TC, 1 Concatenation with TD
input TxDREL; 								// 0 Reload on TD, 1 Concatenation with TA

/****Concatenation selection of subtimer A**/
input TxINC;  								//0 TA Concatenates with own TD, 1 Concatenates with other TD of the other timer



/***Overflow signals****/
//T~# overflow values as input
input RL_TxA; 								//Overflow signal necessary in both timer instances if 64-bit timer is selected.
input TNxDOV;								//Overflow signal necessary in both timer instances if 64-bit timer is selected.
//T# overflow value as input and output
output reg TxAOV;							//Overflow signal necessary in both timer instances if 64-bit timer is selected.
output reg TxBOV;							//Overflow signal necessary in both timer instances if 64-bit timer is selected.
output reg TxCOV;							//Overflow signal necessary in both timer instances if 64-bit timer is selected.
output reg TxDOV;							//Overflow signal necessary in both timer instances if 64-bit timer is selected.


/*****Timer write selection***/
input TxASel;								//Write available to the register TxDCBA, subtimer A.
input TxBSel;								//Write available to the register TxDCBA, subtimer B.
input TxCSel;								//Write available to the register TxDCBA, subtimer C.
input TxDSel;								//Write available to the register TxDCBA, subtimer D.

input TxRASel;								//Write available to the register TxRDCBA, subtimer A.
input TxRBSel;								//Write available to the register TxRDCBA, subtimer B.
input TxRCSel;								//Write available to the register TxRDCBA, subtimer C.
input TxRDSel;								//Write available to the register TxRDCBA, subtimer D.


/*****Data connection with the exterior***/
input [31:0] data_in;					//"Data bus" of the timer.


/*****Global clock signal***/
input clk;


/*************************************Wires && registers of module*************************************/

/*****Clock of each 8-bit timer***/
wire TxAclk;								//Clock signal to 8-bit subtimer A.
wire TxBclk;								//Clock signal to 8-bit subtimer B.
wire TxCclk;								//Clock signal to 8-bit subtimer C.
wire TxDclk;								//Clock signal to 8-bit subtimer D.

wire TxAclk_both;							//Clock signal to 8-bit subtimer A when both edges (posedge and negedge) are wanted.
wire TxBclk_both;							//Clock signal to 8-bit subtimer B when both edges (posedge and negedge) are wanted.
wire TxCclk_both;							//Clock signal to 8-bit subtimer C when both edges (posedge and negedge) are wanted.
wire TxDclk_both;							//Clock signal to 8-bit subtimer D when both edges (posedge and negedge) are wanted.

/***External signals****/
reg CNT0;									//Registers with the external signal used in clock.
reg CNT1;									//Registers with the external signal used in clock.

/******Data Registers******/
reg [7:0] TxA, TxB, TxC, TxD;
reg [7:0] TxRA, TxRB, TxRC, TxRD;


/******Debug wires******/
wire [31:0] TxDCBA;					//Debug wire to control the counting registers.
wire [31:0] TxRDCBA;					//Debug wire to control the reload registers.




/******************************************* Implementation *******************************************/

/******Initialization and assign of data values******/
assign TxDCBA = {TxD, TxC, TxB, TxA};
assign TxRDCBA = {TxRD, TxRC, TxRB, TxRA};

initial
begin
	TxA = 0;
	TxB = 0;
	TxC = 0;
	TxD = 0;
	
	TxRA = 0;
	TxRB = 0;
	TxRC = 0;
	TxRD = 0;
	
	TxAOV = 0;
	TxBOV = 0;
	TxCOV = 0;
	TxDOV = 0;
end


/******Everytime that an edge of the external signal clock occurs the register is updated.******/
always @(IN0)
begin
	CNT0 = 	(T01IN0 == 2'b00) ? 	OUV_T2A 	:
				(T01IN0 == 2'b01) ? 	IN0 		:
				(T01IN0 == 2'b10) ? 	~IN0 		:
											1;
end

always @(IN1)
begin
	CNT1 = 	(T01IN1 == 2'b00) ? 	OUV_T2B	:
				(T01IN1 == 2'b01) ? 	IN1 		:
				(T01IN1 == 2'b10) ? 	~IN1 		:
											1;
end


/******If both edges of the external signal IN0 or IN1 are the clock of subtimer, the register is updated with it-s value.******/
assign TxAclk_both = ( (T01IN0 == 2'b11) && (TxAINS == 2'b01) ) ? IN0 :
							( (T01IN1 == 2'b11) && (TxAINS == 2'b10) ) ? IN1 :
							0;
							
assign TxBclk_both = ( (T01IN0 == 2'b11) && (TxBINS == 2'b01) ) ? IN0 :
							( (T01IN1 == 2'b11) && (TxBINS == 2'b10) ) ? IN1 :
							0;
							
assign TxCclk_both = ( (T01IN0 == 2'b11) && (TxCINS == 2'b01) ) ? IN0 :
							( (T01IN1 == 2'b11) && (TxCINS == 2'b10) ) ? IN1 :
							0;
							
assign TxDclk_both = ( (T01IN0 == 2'b11) && (TxDINS == 2'b01) ) ? IN0 :
							( (T01IN1 == 2'b11) && (TxDINS == 2'b10) ) ? IN1 :
							0;


/***clk selection - according to TxyINS it is selected the clock source...***/
assign TxAclk = (TxAINS == 2'b00)								?	clk	: 		//Global clock
					 (TxAINS == 2'b01)								?	CNT0	:		//External clock source 0
					 (TxAINS == 2'b10)								? 	CNT1	:		//External clock source 1
					 (TxAINS == 2'b11) && ( TxINC == 1'b0 ) 	? 	TxDOV	:		//Overflow of the next subtimer (controlled internally)
																				TNxDOV;		//Overflow of the next subtimer of the other timer. (particular case of subtimer A)
					 

assign TxBclk = (TxBINS == 2'b00)	?	clk	:
					 (TxBINS == 2'b01)	?	CNT0	:
					 (TxBINS == 2'b10)	? 	CNT1	:
													TxAOV;


assign TxCclk = (TxCINS == 2'b00)	?	clk	:
					 (TxCINS == 2'b01)	?	CNT0	:
					 (TxCINS == 2'b10)	? 	CNT1	:
													TxBOV;


assign TxDclk = (TxDINS == 2'b00)	?	clk	:
					 (TxDINS == 2'b01)	?	CNT0	:
					 (TxDINS == 2'b10)	? 	CNT1	:
													TxCOV;


/***Reload register is updated if reload selection is active***/
always @(posedge clk)
begin
	if ( TxRASel )
		TxRA = data_in[7:0];
		
	if ( TxRBSel )
		TxRB = data_in[15:8];

	if ( TxRCSel )
		TxRC = data_in[23:16];

	if ( TxRDSel )
		TxRD = data_in[31:24];		
end


reg	ReloadA_l;
reg	ReloadB_l;
reg	ReloadC_l;
reg	ReloadD_l;


wire	ReloadTNx = TxINC && RL_TxA;


wire	ReloadA = 	TxINC ? ReloadTNx && ( ( !TxAREL && TxAOV ) || ( !TxBREL && TxAREL && TxBOV ) || ( !TxCREL && TxBREL && TxAREL && TxCOV ) || ( TxCREL && TxBREL && TxAREL && TxDOV ) ) :
						( ( !TxAREL && TxAOV ) || ( !TxBREL && TxAREL && TxBOV ) || ( !TxCREL && TxBREL && TxAREL && TxCOV ) || ( !TxDREL && TxCREL && TxBREL && TxAREL && TxDOV && ( TxAINS != 3 ) ) );
						
wire	ReloadB = 	( TxINC && TxAREL ) ? ReloadTNx && ( ( !TxBREL && TxBOV ) || ( !TxCREL && TxBREL && TxCOV ) || ( TxCREL && TxBREL && TxDOV ) || ( TxAREL && TxDREL && TxCREL && TxBREL && TxAOV && ( TxBINS != 3 ) ) ) :
						( ( !TxBREL && TxBOV ) || ( !TxCREL && TxBREL && TxCOV ) || ( !TxDREL && TxCREL && TxBREL && TxDOV ) || ( TxAREL && TxDREL && TxCREL && TxBREL && TxAOV && ( TxBINS != 3 ) ) );
						
wire	ReloadC = 	( TxINC && TxAREL && TxBREL ) ? ReloadTNx && ( ( !TxCREL && TxCOV ) || ( TxCREL && TxDOV ) || ( !TxAREL && TxDREL && TxCREL && TxAOV ) || ( TxAREL && TxDREL && TxCREL && TxBREL && TxBOV && ( TxCINS != 3 ) ) ) :
						( ( !TxCREL && TxCOV ) || ( !TxDREL && TxCREL && TxDOV ) || ( !TxAREL && TxDREL && TxCREL && TxAOV ) || ( TxAREL && TxDREL && TxCREL && TxBREL && TxBOV && ( TxCINS != 3 ) ) );
						
wire	ReloadD = 	( TxINC && TxAREL && TxBREL && TxCREL ) ? ReloadTNx && ( ( !TxDREL && TxDOV ) || ( !TxAREL && TxDREL && TxAOV ) || ( !TxBREL && TxAREL && TxDREL && TxBOV ) || ( !TxCREL && TxDREL && TxAREL && TxBREL && TxBOV && ( TxDINS != 3 ) ) ) :
						( ( !TxDREL && TxDOV ) || ( !TxAREL && TxDREL && TxAOV ) || ( !TxBREL && TxAREL && TxDREL && TxBOV ) || ( !TxCREL && TxDREL && TxAREL && TxBREL && TxBOV && ( TxDINS != 3 ) ) );

/*
always @( clk )
begin
	if ( TxAOV )
		TxAOV = 0;

	if ( TxBOV )
		TxBOV = 0;
		
	if ( TxCOV )
		TxCOV = 0;
		
	if ( TxDOV )
		TxDOV = 0;
end
*/

/***All the clock counting management of the 4 subtimers is done in this always block***/
always @( TxAclk or TxAclk_both or ReloadA )
begin
	if ( TxARUN )
	begin
		if ( ReloadA && !ReloadA_l )
			TxA = TxRA;
		
		else if (!( !ReloadA && ReloadA_l ))
		begin
			if ( TxASel )
				TxA = data_in[7:0];
		
			else if ( TxAclk || TxAclk_both )
				{TxAOV, TxA} = TxA + 1;
		end
	end

	ReloadA_l = ReloadA;
end


			
always @( TxBclk or TxBclk_both or ReloadB )
begin
	if ( TxBRUN )
	begin
		if ( ReloadB && !ReloadB_l )
			TxB = TxRB;
		
		else if (!( !ReloadB && ReloadB_l ))
		begin
			if ( TxBSel )
				TxB = data_in[15:8];
		
			else if ( TxBclk || TxBclk_both )
				{TxBOV, TxB} = TxB + 1;
		end
	end

	ReloadB_l = ReloadB;
end


always @( TxCclk or TxCclk_both or ReloadC )
begin
	if ( TxCRUN )
	begin
		if ( ReloadC && !ReloadC_l )
			TxC = TxRC;	
			
		else if (!( !ReloadC && ReloadC_l ))
		begin
			if ( TxCSel )
				TxC = data_in[23:16];
		
			else if ( TxCclk || TxCclk_both )
				{TxCOV, TxC} = TxC + 1;
		end
	end

	ReloadC_l = ReloadC;
end


always @( TxDclk or TxDclk_both or ReloadD )
begin
	if ( TxDRUN )
	begin
		if ( ReloadD && !ReloadD_l )
			TxD = TxRD;
		
		else if (!( !ReloadD && ReloadD_l ))
		begin
			if ( TxDSel )
				TxD = data_in[31:24];
		
			else if ( TxDclk || TxDclk_both )
				{TxDOV, TxD} = TxD + 1;
		end
	end

	ReloadD_l = ReloadD;
end

endmodule
