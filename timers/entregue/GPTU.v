`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:26:49 04/23/2014 
// Design Name: 
// Module Name:    timer_tricore 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module GPTU(
	T01IRS,						//Registo de controlo do modo de opera��o e de recarga 
									//dos timers
	T01OTS,						//Registo que possibilita a selec��o do output, service
									//request e tiggers dos timers T0 e T1
	timer_write_sel,			//Escrita dispon�vel para os registos de reload (Provis�rio)
	timer_reload_write_sel,	//Escrita dispon�vel para os registos de reload (Provis�rio)
	IN0, 							//Input modo contador 0
	IN1,							//Input modo contador 1
	data_in,						//Barramento de dados
	T012RUN,						//Registo de controlo da execu��o/Paragem dos timers
	OSEL,							//Registo de sele��o da sa�da de output a ser alterada a 
									//quando o overflow de um timer
	OUT,							//Registo que contem o valor dos outputs e permite fazer 
									//clear/set aos outputs
	SRSEL,
	OUTX,							//Outputs
	SR,								//array do ISR
	clk							//Clock geral
);

/***********************************************************************************************************************
/*********** Registo T01IRS 
/***********************************************************************************************************************/
input [31:0] T01IRS;	//Controlo do modo de opera��o e de recarga dos timers

/*Selec��o das entradas de clock*/
`define	TO_INS_CLK	2'b00	//Input do clk
`define	TO_INS_CNT0	2'b01	//Input do CNT0
`define	TO_INS_CNT1	2'b10	//Input do CNT1
`define	TO_INS_CCT	2'b11	//Entrada de carry (concatena��o)

/*****Configura��o do timer***/
`define 	T0AINS 	1:0 	//[R/W] Sele��o de entrada T0A
`define 	T0BINS 	3:2	//[R/W] Sele��o de entrada T0B
`define	T0CINS 	5:4 	//[R/W] Sele��o de entrada T0C
`define	T0DINS 	7:6 	//[R/W] Sele��o de entrada T0D
`define	T1AINS 	9:8 	//[R/W] Sele��o de entrada T1A
`define	T1BINS 	11:10	//[R/W] Sele��o de entrada T1B
`define	T1CINS 	13:12	//[R/W] Sele��o de entrada T1C
`define	T1DINS 	15:14 //[R/W] Sele��o de entrada T1D


/*Selec��o doc reload*/
`define	TO_REL_RLD	1'b0	//Reload com overflow
`define	TO_REL_CCT	1'b1	//Concatena��o do overflow com o timer seguinte

/*****Configura��es de reload***/
`define 	T0AREL 	16	//[R/W] 0: Reload, 1: Concatena��o com TB
`define	T0BREL 	17 //[R/W] 0: Reload, 1: Concatena��o com TC
`define 	T0CREL	18 //[R/W] 0: Reload, 1: Concatena��o com TD
`define	T0DREL 	19	//[R/W] 0: Reload, 1: Concatena��o com TA
`define 	T1AREL 	20 //[R/W] 0: Reload, 1: Concatena��o com TB
`define	T1BREL 	21 //[R/W] 0: Reload, 1: Concatena��o com TC
`define	T1CREL 	22	//[R/W] 0: Reload, 1: Concatena��o com TD
`define	T1DREL 	23	//[R/W] 0: Reload, 1: Concatena��o com TA


/****Selec��o de concatena��o**/
`define 	T0INC 	24  	//[R/W] 0: O carry do T0A � o carry de sa�da do T0D, 
										//1: O carry do T0A � o carry de sa�da do T0D
`define	T1INC 	25 	//[R/W] 0: O carry do T1A � o carry de sa�da do T1D, 
										//1: O carry do T1A � o carry de sa�da do T0D

/****Sele��o dos sinais externos para CNT0 e CNT1**/
`define 	T01IN0 	27:26	//[R/W] 00: Overflow/underflow do T2A (OUV_T2A), 
										//01: Transi��o ascendente de IN0, 
										//10: Transi��o descendente de IN0, 
										//11: Transi��o ascendente de IN0
`define 	T01IN1 	29:28	//[R/W] 00: Overflow/underflow do T2B (OUV_T2B), 
										//01: Transi��o ascendente de IN1, 
										//10: Transi��o descendente de IN1, 
										//11: Transi��o ascendente de IN1


/***********************************************************************************************************************
/*********** Registo T01OTS
/***********************************************************************************************************************/
input [31:0] T01OTS;//Registo que possibilita a selec��o do output, service request e tiggers dos timers T0 e T1

/*Sele��o do pedido de servi�o SSRxy*/
`define 	SSR_TXA_OVF		2'b00	//Overflow timer A
`define 	SSR_TXB_OVF		2'b01	//Overflow timer B
`define 	SSR_TXC_OVF		2'b10	//Overflow timer C
`define 	SSR_TXD_OVF		2'b11	//Overflow timer D

/*Sele��o do sinal de sa�da STRGxy*/
`define 	STRG_TXA_OVF	2'b00	//Overflow timer A
`define 	STRG_TXB_OVF	2'b01	//Overflow timer B
`define 	STRG_TXC_OVF	2'b10	//Overflow timer C
`define 	STRG_TXD_OVF	2'b11	//Overflow timer D

/*Sele��o da sa�da SOUTxy*/
`define 	SOUT_TXA_OVF	2'b00	//Overflow timer A
`define 	SOUT_TXB_OVF	2'b01	//Overflow timer B
`define 	SOUT_TXC_OVF	2'b10	//Overflow timer C
`define 	SOUT_TXD_OVF	2'b11	//Overflow timer D

`define 	SOUT00	1:0		//[R/W]Seleciona da fonte da sa�da 0 de T0
`define 	SOUT01	3:2		//[R/W]Seleciona da fonte da sa�da 1 de T0
`define 	STRG00	5:4		//[R/W]Seleciona o trigger da fonte da sa�da 0 de T0
`define 	STRG01	7:6		//[R/W]Seleciona o trigger da fonte da sa�da 1 de T0
`define 	SSR00		9:8		//[R/W]Seleciona do pedido de servi�o 0 do T0
`define 	SSR01		11:10		//[R/W]Seleciona do pedido de servi�o 1 do T0
//[15:12] -> reservado(l�-se '0'; deve ser escrito com '0')
`define 	SOUT10	17:16		//[R/W]Seleciona da fonte da sa�da 0 de T1
`define 	SOUT11	19:18		//[R/W]Seleciona da fonte da sa�da 1 de T1
`define 	STRG10	21:20		//[R/W]Seleciona o trigger da fonte da sa�da 0 de T1
`define 	STRG11	23:22		//[R/W]Seleciona o trigger da fonte da sa�da 1 de T1
`define 	SSR10		25:24		//[R/W]Seleciona do pedido de servi�o 0 do T1
`define 	SSR11		27:26		//[R/W]Seleciona do pedido de servi�o 1 do T1
//[12] -> reservado(l�-se '0'; deve ser escrito com '0')


/***********************************************************************************************************************
/*********** Sinal de controlo timer_write_sel
/***********************************************************************************************************************/
input [9:0] timer_write_sel;//Escrita dispon�vel para os registos de contagem (Provis�rio)

`define 	T0ASel 	0
`define 	T0BSel 	1
`define 	T0CSel  	2
`define	T0DSel 	3
`define 	T1ASel 	4
`define 	T1BSel 	5
`define 	T1CSel 	6
`define 	T1DSel 	7
`define 	T2ASel 	8
`define 	T2BSel 	9

/***********************************************************************************************************************
/*********** Sinal de controlo timer_reload_write_sel
/***********************************************************************************************************************/
input [9:0] timer_reload_write_sel;//Escrita dispon�vel para os registos de reload (Provis�rio)

`define 	T0RASel	0
`define	T0RBSel 	1
`define	T0RCSel 	2
`define	T0RDSel 	3
`define	T1RASel 	4
`define	T1RBSel 	5
`define	T1RCSel 	6
`define 	T1RDSel	7
`define	T2RASel 	8
`define 	T2RBSel	9

/***********************************************************************************************************************
/*********** Registo TO12RUN 
/***********************************************************************************************************************/
input [31:0] T012RUN;//Controla a execu��o/Paragem dos timers

`define	T0ARUN	0 	//[RW] Controla a execu��o de T0A (0 - Para, 1 - Come�a)
`define	T0BRUN	1 	//[RW] Controla a execu��o de T0B (0 - Para, 1 - Come�a)
`define	T0CRUN 	2	//[RW] Controla a execu��o de T0C (0 - Para, 1 - Come�a)
`define	T0DRUN 	3 	//[RW] Controla a execu��o de T0C (0 - Para, 1 - Come�a)
`define	T1ARUN 	4 	//[RW] Controla a execu��o de T1A (0 - Para, 1 - Come�a)
`define	T1BRUN  	5 	//[RW] Controla a execu��o de T1B (0 - Para, 1 - Come�a)
`define	T1CRUN  	6 	//[RW] Controla a execu��o de T1C (0 - Para, 1 - Come�a)
`define	T1DRUN 	7 	//[RW] Controla a execu��o de T1D (0 - Para, 1 - Come�a)
`define	T2ARUN 	8	//[R] Cont�m o estado de execu��o de T2A (0 - Parado 1 - Em execu��o), s� alterado por Hardware
`define	T2ASETR 	9	//[W] Controla a execu��o de T2A (1 - Come�a, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
`define	T2ACLRR  10	//[W] Controla a paragem de T2A (1 - Para, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
//11 -> N�o usado
`define	T2BRUN  	12	//[R] Cont�m o estado de execu��o de T2B, (0 - Parado 1 - Em execu��o), s� alterado por Hardware
`define	T2BSETR  13	//[W] Controla a execu��o de T2B (1 - Come�a, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
`define	T2BCLRR  14	//[W] Controla a paragem de T2B (1 - Para, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
//15 - 31 -> N�o usado

/***********************************************************************************************************************
/*********** Registo OSEL 
/***********************************************************************************************************************/
input [31:0] OSEL; //Seleccionar sa�da de output a ser alterada a quando o overflow de um timer

/*Escolha dos overflows que afectam a sa�da*/
`define	SO_OUT00		3'b000	//1� Overflow do timer 0
`define	SO_OUT01		3'b001	//2� Overflow do timer 0
`define	SO_OUT10		3'b010	//1� Overflow do timer 1
`define	SO_OUT11		3'b011	//2� Overflow do timer 1
`define	SO_OUV_T2A	3'b100	//Overflow do timer 2 A
`define	SO_OUV_T2B	3'b101	//Overflow do timer 2 B
//110 - Reservado
//111 - Reservado

/*Localiza��o dos bits de configura��o de cada sa�da*/
`define 	SO0		2:0		//[R/W] Seleciona sa�da 0
`define 	SO1		6:4		//[R/W] Seleciona sa�da 1
`define 	SO2		10:8		//[R/W] Seleciona sa�da 2
`define 	SO3		14:12		//[R/W] Seleciona sa�da 3
`define 	SO4		18:16		//[R/W] Seleciona sa�da 4
`define 	SO5		22:20		//[R/W] Seleciona sa�da 5
`define 	SO6		26:24		//[R/W] Seleciona sa�da 6
`define 	SO7		30:28		//[R/W] Seleciona sa�da 7


/***********************************************************************************************************************
/*********** Registo SRSEL 
/***********************************************************************************************************************/
input [31:0] SRSEL;	//selecao dos ISR
output [7:0] SR;		//array ISR

/*defines de mapeamento*/
`define SSR0	31:28
`define SSR1	27:24
`define SSR2	23:20
`define SSR3	19:16
`define SSR4	15:12
`define SSR5	11:8
`define SSR6	7:4
`define SSR7	3:0

/*defines dos varios sinais IR(Service Request, array IRS)*/
`define SR_START_A	 4'b0000
`define SR_STOP_A	 	 4'b0001
`define SR_UP_DOWN_A	 4'b0010
`define SR_CLEAR_A 	 4'b0011
`define SR_RLCP0_A 	 4'b0100
`define SR_RLCP1_A 	 4'b0101
`define SR_OUV_T2A 	 4'b0110
`define SR_OUV_T2B 	 4'b0111
`define SR_START_B 	 4'b1000
`define SR_STOP_B 	 4'b1001
`define SR_RLCP0_B 	 4'b1010
`define SR_RLCP1_B 	 4'b1011
`define SR_SR00 		 4'b1100
`define SR_SR01 		 4'b1101
`define SR_SR10 		 4'b1110
`define SR_SR11 		 4'b1111


/***********************************************************************************************************************
/*********** Registo OUT
/***********************************************************************************************************************/
input [31:0] OUT;//Registo OUT
output reg [7:0] OUTX;//Outputs de overflow do timer

/*
Fornece informa��o sobre o estado da sa�da. E s� pode ser alterado atrav�s do sinal a ele associado.
Se CLROx e SETOx estiveram ativos OUTx n�o � afetado.
*/
`define 	O_OUT0		0		//[R] Cont�m o bit de estado da sa�da 0
`define 	O_OUT1		1		//[R] Cont�m o bit de estado da sa�da 1
`define 	O_OUT2		2		//[R] Cont�m o bit de estado da sa�da 2
`define 	O_OUT3		3		//[R] Cont�m o bit de estado da sa�da 3
`define 	O_OUT4		4		//[R] Cont�m o bit de estado da sa�da 4
`define 	O_OUT5		5		//[R] Cont�m o bit de estado da sa�da 5
`define 	O_OUT6		6		//[R] Cont�m o bit de estado da sa�da 6
`define 	O_OUT7		7		//[R] Cont�m o bit de estado da sa�da 7

/*
Ao escrever:
Limpa o bit de sa�da 'x':
1 - Limpa o bit de s�ida
0 - N�o tem efeito
Retorna sempre '0' quando � lido. 
Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
*/
`define 	O_CLRO0		7		//[W]Limpa o bit de sa�da out0
`define 	O_CLRO1		8		//[W]Limpa o bit de sa�da out1
`define 	O_CLRO2		10		//[W]Limpa o bit de sa�da out2
`define 	O_CLRO3		11		//[W]Limpa o bit de sa�da out3
`define 	O_CLRO4		12		//[W]Limpa o bit de sa�da out4
`define 	O_CLRO5		13		//[W]Limpa o bit de sa�da out5
`define 	O_CLRO6		14		//[W]Limpa o bit de sa�da out6
`define 	O_CLRO7		15		//[W]Limpa o bit de sa�da out7

/*
Ativa o bit de sa�da 'x':
Ao escrever:
1 - Ativa o bit de s�ida
0 - N�o tem efeito
Retorna sempre '0' quando � lido. 
Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
*/
`define 	O_SETO0		16		//[W]Ativa o bit de sa�da out0
`define 	O_SETO1		17		//[W]Ativa o bit de sa�da out1
`define 	O_SETO2		18		//[W]Ativa o bit de sa�da out2
`define 	O_SETO3		19		//[W]Ativa o bit de sa�da out3
`define 	O_SETO4		20		//[W]Ativa o bit de sa�da out4
`define 	O_SETO5		21		//[W]Ativa o bit de sa�da out5
`define 	O_SETO6		22		//[W]Ativa o bit de sa�da out6
`define 	O_SETO7		23		//[W]Ativa o bit de sa�da out7




/***********************************************************************************************************************
/*********** INPUTS
/***********************************************************************************************************************/
/********	Inputs para modo contador		****/
input IN0;
input IN1;

input clk;//clock geral

input [31:0] data_in;	//Barramento de dados dos timmers

/***********************************************************************************************************************
/*********** Overflow dos Timers
/***********************************************************************************************************************/
wire T0AOV; //Overflow do timer 0 A.
wire T0BOV; //Overflow do timer 0 B.
wire T0COV; //Overflow do timer 0 C.
wire T0DOV;	//Overflow do timer 0 D.
wire T1AOV; //Overflow do timer 1 A.
wire T1BOV;	//Overflow do timer 1 B.
wire T1COV; //Overflow do timer 1 C.
wire T1DOV; //Overflow do timer 1 D.

wire	OUV_T2A; //Overflow do timer 2 A.
wire	OUV_T2B; //Overflow do timer 2 B.



/***********************************************************************************************************************
/*********** Instancia��o do timmer 0
/***********************************************************************************************************************/
T0_T1_implementation T0(
	.TxAINS(T01IRS[`T0AINS]),							.TxBINS(T01IRS[`T0BINS]),							.TxCINS(T01IRS[`T0CINS]),							.TxDINS(T01IRS[`T0DINS]),
	.TxAREL(T01IRS[`T0AREL]),							.TxBREL(T01IRS[`T0BREL]),							.TxCREL(T01IRS[`T0CREL]),							.TxDREL(T01IRS[`T0DREL]),
	.TxINC(T01IRS[`T0INC]),
	.T01IN0(T01IRS[`T01IN0]),							.T01IN1(T01IRS[`T01IN1]),
	.IN0(IN0), 												.IN1(IN1), 												.OUV_T2A(OUV_T2A), 									.OUV_T2B(OUV_T2B),
	.TxARUN(T012RUN[`T0ARUN]), 						.TxBRUN(T012RUN[`T0BRUN]), 						.TxCRUN(T012RUN[`T0CRUN]), 						.TxDRUN(T012RUN[`T0DRUN]),
	.TxAOV(T0AOV), 										.TxBOV(T0BOV), 										.TxCOV(T0COV), 										.TxDOV(T0DOV),
	.RL_TxA(T1DOV), 										.TNxDOV(T1DOV),
	.TxASel(timer_write_sel[`T0ASel]), 				.TxBSel(timer_write_sel[`T0BSel]), 				.TxCSel(timer_write_sel[`T0CSel]), 				.TxDSel(timer_write_sel[`T0DSel]),
	.TxRASel(timer_reload_write_sel[`T0RASel]),	.TxRBSel(timer_reload_write_sel[`T0RBSel]),	.TxRCSel(timer_reload_write_sel[`T0RCSel]),	.TxRDSel(timer_reload_write_sel[`T0RDSel]),
	.data_in(data_in),									.clk(clk)
);

/***********************************************************************************************************************
/*********** Instancia��o do timmer 1
/***********************************************************************************************************************/
T0_T1_implementation T1(
	.TxAINS(T01IRS[`T1AINS]),							.TxBINS(T01IRS[`T1BINS]),							.TxCINS(T01IRS[`T1CINS]),							.TxDINS(T01IRS[`T1DINS]),
	.TxAREL(T01IRS[`T1AREL]),							.TxBREL(T01IRS[`T1BREL]),							.TxCREL(T01IRS[`T1CREL]),							.TxDREL(T01IRS[`T1DREL]),
	.TxINC(T01IRS[`T1INC]),
	.T01IN0(T01IRS[`T01IN0]),							.T01IN1(T01IRS[`T01IN1]),
	.IN0(IN0), 												.IN1(IN1), 												.OUV_T2A(OUV_T2A), 									.OUV_T2B(OUV_T2B),
	.TxARUN(T012RUN[`T1ARUN]), 						.TxBRUN(T012RUN[`T1BRUN]), 						.TxCRUN(T012RUN[`T1CRUN]), 						.TxDRUN(T012RUN[`T1DRUN]),
	.TxAOV(T1AOV), 										.TxBOV(T1BOV), 										.TxCOV(T1COV), 										.TxDOV(T1DOV),
	.RL_TxA(T0DOV), 										.TNxDOV(T0DOV),
	.TxASel(timer_write_sel[`T1ASel]), 				.TxBSel(timer_write_sel[`T1BSel]), 				.TxCSel(timer_write_sel[`T1CSel]), 				.TxDSel(timer_write_sel[`T1DSel]),
	.TxRASel(timer_reload_write_sel[`T1RASel]),	.TxRBSel(timer_reload_write_sel[`T1RBSel]),	.TxRCSel(timer_reload_write_sel[`T1RCSel]),	.TxRDSel(timer_reload_write_sel[`T1RDSel]),
	.data_in(data_in), 									.clk(clk)
);

/***********************************************************************************************************************
/*********** Implementa��o do registo T01OTS
/***********************************************************************************************************************/

/*Selec��o do output*/
wire OUT00;//Output Source 0 do timer 0
wire OUT01;//Output Source 1 do timer 0
wire OUT10;//Output Source 0 do timer 1
wire OUT11;//Output Source 1 do timer 1

assign OUT00 =	(T01OTS[`SOUT00] == `SOUT_TXA_OVF)	?	T0AOV	:
					(T01OTS[`SOUT00] == `SOUT_TXB_OVF)	?	T0BOV	:
					(T01OTS[`SOUT00] == `SOUT_TXC_OVF)	?	T0COV	:
					(T01OTS[`SOUT00] == `SOUT_TXD_OVF)	?	T0DOV	:
					1'b0;	
					
assign OUT01 =	(T01OTS[`SOUT01] == `SOUT_TXA_OVF)	?	T0AOV	:
					(T01OTS[`SOUT01] == `SOUT_TXB_OVF)	?	T0BOV	:
					(T01OTS[`SOUT01] == `SOUT_TXC_OVF)	?	T0COV	:
					(T01OTS[`SOUT01] == `SOUT_TXD_OVF)	?	T0DOV	:
					1'b0;	
					
assign OUT10 =	(T01OTS[`SOUT10] == `SOUT_TXA_OVF)	?	T1AOV	:
					(T01OTS[`SOUT10] == `SOUT_TXB_OVF)	?	T1BOV	:
					(T01OTS[`SOUT10] == `SOUT_TXC_OVF)	?	T1COV	:
					(T01OTS[`SOUT10] == `SOUT_TXD_OVF)	?	T1DOV	:
					1'b0;	
					
assign OUT11 =	(T01OTS[`SOUT11] == `SOUT_TXA_OVF)	?	T1AOV	:
					(T01OTS[`SOUT11] == `SOUT_TXB_OVF)	?	T1BOV	:
					(T01OTS[`SOUT11] == `SOUT_TXC_OVF)	?	T1COV	:
					(T01OTS[`SOUT11] == `SOUT_TXD_OVF)	?	T1DOV	:
					1'b0;

/*Selec��o do service request*/
wire SR00;//Service Request 0 do timer 0
wire SR01;//Service Request 1 do timer 0
wire SR10;//Service Request 0 do timer 1
wire SR11;//Service Request 1 do timer 1

assign SR00 =	(T01OTS[`SSR00] == `SSR_TXA_OVF)	?	T0AOV	:
					(T01OTS[`SSR00] == `SSR_TXB_OVF)	?	T0BOV	:
					(T01OTS[`SSR00] == `SSR_TXC_OVF)	?	T0COV	:
					(T01OTS[`SSR00] == `SSR_TXD_OVF)	?	T0DOV	:
					1'b0;	
					
assign SR01 =	(T01OTS[`SSR01] == `SSR_TXA_OVF)	?	T0AOV	:
					(T01OTS[`SSR01] == `SSR_TXB_OVF)	?	T0BOV	:
					(T01OTS[`SSR01] == `SSR_TXC_OVF)	?	T0COV	:
					(T01OTS[`SSR01] == `SSR_TXD_OVF)	?	T0DOV	:
					1'b0;	
					
assign SR10 =	(T01OTS[`SSR10] == `SSR_TXA_OVF)	?	T1AOV	:
					(T01OTS[`SSR10] == `SSR_TXB_OVF)	?	T1BOV	:
					(T01OTS[`SSR10] == `SSR_TXC_OVF)	?	T1COV	:
					(T01OTS[`SSR10] == `SSR_TXD_OVF)	?	T1DOV	:
					1'b0;	
					
assign SR11 =	(T01OTS[`SSR11] == `SSR_TXA_OVF)	?	T1AOV	:
					(T01OTS[`SSR11] == `SSR_TXB_OVF)	?	T1BOV	:
					(T01OTS[`SSR11] == `SSR_TXC_OVF)	?	T1COV	:
					(T01OTS[`SSR11] == `SSR_TXD_OVF)	?	T1DOV	:
					1'b0;			

/*Selec��o do trigger output*/
wire TRG00;//Trigger output 0 do timer 0
wire TRG01;//Trigger output 1 do timer 0
wire TRG10;//Trigger output 0 do timer 1
wire TRG11;//Trigger output 1 do timer 1

assign TRG00 =	(T01OTS[`STRG00] == `STRG_TXA_OVF)	?	T0AOV	:
					(T01OTS[`STRG00] == `STRG_TXB_OVF)	?	T0BOV	:
					(T01OTS[`STRG00] == `STRG_TXC_OVF)	?	T0COV	:
					(T01OTS[`STRG00] == `STRG_TXD_OVF)	?	T0DOV	:
					1'b0;	
					
assign TRG01 =	(T01OTS[`STRG01] == `STRG_TXA_OVF)	?	T0AOV	:
					(T01OTS[`STRG01] == `STRG_TXB_OVF)	?	T0BOV	:
					(T01OTS[`STRG01] == `STRG_TXC_OVF)	?	T0COV	:
					(T01OTS[`STRG01] == `STRG_TXD_OVF)	?	T0DOV	:
					1'b0;	
					
assign TRG10 =	(T01OTS[`STRG10] == `STRG_TXA_OVF)	?	T1AOV	:
					(T01OTS[`STRG10] == `STRG_TXB_OVF)	?	T1BOV	:
					(T01OTS[`STRG10] == `STRG_TXC_OVF)	?	T1COV	:
					(T01OTS[`STRG10] == `STRG_TXD_OVF)	?	T1DOV	:
					1'b0;	
					
assign TRG11 =	(T01OTS[`STRG11] == `STRG_TXA_OVF)	?	T1AOV	:
					(T01OTS[`STRG11] == `STRG_TXB_OVF)	?	T1BOV	:
					(T01OTS[`STRG11] == `STRG_TXC_OVF)	?	T1COV	:
					(T01OTS[`STRG11] == `STRG_TXD_OVF)	?	T1DOV	:
					1'b0;
					
/***********************************************************************************************************************
/*********** Implementa��o do registo OSEL
/***********************************************************************************************************************/	
wire [7:0] OUT_SELECTED;		//Wire de Outputs depois de seleccionado pelo registo OSEL
reg [7:0] OUT_SELECTED_PREV;	//Guarda o valor anterior do OUT_SELECTED,para verificar se ouve uma transi��o ascendente

assign OUT_SELECTED[0] = 	(OSEL[`SO0] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO0] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO0] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO0] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO0] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO0] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;
									
assign OUT_SELECTED[1] = 	(OSEL[`SO1] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO1] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO1] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO1] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO1] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO1] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;
									
assign OUT_SELECTED[2] = 	(OSEL[`SO2] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO2] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO2] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO2] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO2] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO2] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;
									
assign OUT_SELECTED[3] = 	(OSEL[`SO3] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO3] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO3] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO3] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO3] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO3] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;
									
assign OUT_SELECTED[4] = 	(OSEL[`SO4] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO4] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO4] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO4] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO4] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO4] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;
									
assign OUT_SELECTED[5] = 	(OSEL[`SO5] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO5] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO5] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO5] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO5] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO5] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;
									
assign OUT_SELECTED[6] = 	(OSEL[`SO6] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO6] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO6] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO6] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO6] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO6] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;
									
assign OUT_SELECTED[7] = 	(OSEL[`SO7] == `SO_OUT00)		? 	OUT00		:
									(OSEL[`SO7] == `SO_OUT01)		? 	OUT01		:
									(OSEL[`SO7] == `SO_OUT10)		? 	OUT10		:
									(OSEL[`SO7] == `SO_OUT11)		? 	OUT11		:
									(OSEL[`SO7] == `SO_OUV_T2A)	? 	OUV_T2A	:
									(OSEL[`SO7] == `SO_OUV_T2B)	? 	OUV_T2B	:
									1'b0;

/*Altera��o do sinal do output 0*/						
always @(OUT_SELECTED[0] or OUT[`O_CLRO0] or OUT[`O_SETO0])
begin
	if(OUT[`O_CLRO0] & ~OUT[`O_SETO0]) begin							//Efectuado um Clear por software
		OUTX[0] <= 1'b0;
	end else if(~OUT[`O_CLRO0] & OUT[`O_SETO0]) begin				//Efectuado um Set por software
		OUTX[0] <= 1'b1;
	end else if(OUT_SELECTED[0] & ~OUT_SELECTED_PREV[0]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[0] <= ~OUTX[0];													//Toggle ao output
	end
		OUT_SELECTED_PREV[0] <= OUT_SELECTED[0];						//Guardar o valor de OUT_SELECTED
end

/*Altera��o do sinal do output 1*/									
always @(OUT_SELECTED[1] or OUT[`O_CLRO1] or OUT[`O_SETO1])
begin
	if(OUT[`O_CLRO1] & ~OUT[`O_SETO1]) begin							//Efectuado um Clear por software
		OUTX[1] <= 1'b0;
	end else if(~OUT[`O_CLRO1] & OUT[`O_SETO1]) begin				//Efectuado um Set por software
		OUTX[1] <= 1'b1;
	end else if(OUT_SELECTED[1] & ~OUT_SELECTED_PREV[1]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[1] <= ~OUTX[1];													//Toggle ao output
	end
		OUT_SELECTED_PREV[1] <= OUT_SELECTED[1];						//Guardar o valor de OUT_SELECTED
end

/*Altera��o do sinal do output 2*/									
always @(OUT_SELECTED[2] or OUT[`O_CLRO2] or OUT[`O_SETO2])
begin
	if(OUT[`O_CLRO2] & ~OUT[`O_SETO2]) begin							//Efectuado um Clear por software
		OUTX[2] <= 1'b0;
	end else if(~OUT[`O_CLRO2] & OUT[`O_SETO2]) begin				//Efectuado um Set por software
		OUTX[2] <= 1'b1;
	end else if(OUT_SELECTED[2] & ~OUT_SELECTED_PREV[2]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[2] <= ~OUTX[2];													//Toggle ao output
	end
		OUT_SELECTED_PREV[2] <= OUT_SELECTED[2];						//Guardar o valor de OUT_SELECTED
end

/*Altera��o do sinal do output 3*/									
always @(OUT_SELECTED[3] or OUT[`O_CLRO3] or OUT[`O_SETO3])
begin
	if(OUT[`O_CLRO3] & ~OUT[`O_SETO3]) begin							//Efectuado um Clear por software
		OUTX[3] <= 1'b0;
	end else if(~OUT[`O_CLRO3] & OUT[`O_SETO3]) begin				//Efectuado um Set por software
		OUTX[3] <= 1'b1;
	end else if(OUT_SELECTED[3] & ~OUT_SELECTED_PREV[3]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[3] <= ~OUTX[3];													//Toggle ao output
	end
		OUT_SELECTED_PREV[3] <= OUT_SELECTED[3];						//Guardar o valor de OUT_SELECTED
end

/*Altera��o do sinal do output 4*/									
always @(OUT_SELECTED[4] or OUT[`O_CLRO4] or OUT[`O_SETO4])
begin
	if(OUT[`O_CLRO4] & ~OUT[`O_SETO4]) begin							//Efectuado um Clear por software
		OUTX[4] <= 1'b0;
	end else if(~OUT[`O_CLRO4] & OUT[`O_SETO4]) begin				//Efectuado um Set por software
		OUTX[4] <= 1'b1;
	end else if(OUT_SELECTED[4] & ~OUT_SELECTED_PREV[4]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[4] <= ~OUTX[4];													//Toggle ao output
	end
		OUT_SELECTED_PREV[4] <= OUT_SELECTED[4];						//Guardar o valor de OUT_SELECTED
end

/*Altera��o do sinal do output 5*/									
always @(OUT_SELECTED[5] or OUT[`O_CLRO5] or OUT[`O_SETO5])
begin
	if(OUT[`O_CLRO5] & ~OUT[`O_SETO5]) begin							//Efectuado um Clear por software
		OUTX[5] <= 1'b0;
	end else if(~OUT[`O_CLRO5] & OUT[`O_SETO5]) begin				//Efectuado um Set por software
		OUTX[5] <= 1'b1;
	end else if(OUT_SELECTED[5] & ~OUT_SELECTED_PREV[5]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[5] <= ~OUTX[5];													//Toggle ao output
	end
		OUT_SELECTED_PREV[5] <= OUT_SELECTED[5];						//Guardar o valor de OUT_SELECTED
end

/*Altera��o do sinal do output 6*/									
always @(OUT_SELECTED[6] or OUT[`O_CLRO6] or OUT[`O_SETO6])
begin
	if(OUT[`O_CLRO6] & ~OUT[`O_SETO6]) begin							//Efectuado um Clear por software
		OUTX[6] <= 1'b0;
	end else if(~OUT[`O_CLRO6] & OUT[`O_SETO6]) begin				//Efectuado um Set por software
		OUTX[6] <= 1'b1;
	end else if(OUT_SELECTED[6] & ~OUT_SELECTED_PREV[6]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[6] <= ~OUTX[6];													//Toggle ao output
	end
		OUT_SELECTED_PREV[6] <= OUT_SELECTED[6];						//Guardar o valor de OUT_SELECTED
end

/*Altera��o do sinal do output 7*/									
always @(OUT_SELECTED[7] or OUT[`O_CLRO7] or OUT[`O_SETO7])
begin
	if(OUT[`O_CLRO7] & ~OUT[`O_SETO7]) begin							//Efectuado um Clear por software
		OUTX[7] <= 1'b0;
	end else if(~OUT[`O_CLRO7] & OUT[`O_SETO7]) begin				//Efectuado um Set por software
		OUTX[7] <= 1'b1;
	end else if(OUT_SELECTED[7] & ~OUT_SELECTED_PREV[7]) begin	//Transi��o ascendente de OUT_SELECTED
		OUTX[7] <= ~OUTX[7];													//Toggle ao output
	end
		OUT_SELECTED_PREV[7] <= OUT_SELECTED[7];						//Guardar o valor de OUT_SELECTED
end

/***********************************************************************************************************************
/*********** Implementa��o do registo SRSEL
/***********************************************************************************************************************/	
/*	=================================================================
*		futuramente isto deixar� de estar definido. 
*		so esta de modo a nao dar erros, dado que o timer 2 ainda 
*		nao esta definido, logo estes sinais tambem nao estao definidos
*	==================================================================	*/
wire START_A;
wire STOP_A;
wire UP_DOWN_A;
wire CLEAR_A;
wire RLCP0_A;
wire RLCP1_A;
wire START_B;
wire STOP_B;
wire RLCP0_B;
wire RLCP1_B;


//Atribuicao dos sinais ir para isr
assign SR[0] = (SRSEL[`SSR0] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR0] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR0] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR0] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR0] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR0] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR0] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR0] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR0] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR0] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR0] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR0] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR0] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR0] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR0] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR0] == `SR_SR11)			? SR11			:
					1'b0;
				

assign SR[1] =	(SRSEL[`SSR1] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR1] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR1] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR1] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR1] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR1] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR1] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR1] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR1] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR1] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR1] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR1] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR1] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR1] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR1] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR1] == `SR_SR11)			? SR11			:
					1'b0;
					
assign SR[2] = (SRSEL[`SSR2] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR2] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR2] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR2] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR2] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR2] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR2] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR2] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR2] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR2] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR2] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR2] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR2] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR2] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR2] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR2] == `SR_SR11)			? SR11			:
					1'b0;

assign SR[3] = (SRSEL[`SSR3] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR3] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR3] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR3] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR3] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR3] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR3] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR3] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR3] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR3] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR3] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR3] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR3] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR3] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR3] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR3] == `SR_SR11)			? SR11			:
					1'b0;	

assign SR[4] = (SRSEL[`SSR4] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR4] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR4] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR4] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR4] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR4] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR4] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR4] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR4] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR4] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR4] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR4] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR4] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR4] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR4] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR4] == `SR_SR11)			? SR11			:
					1'b0;

assign SR[5] = (SRSEL[`SSR5] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR5] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR5] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR5] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR5] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR5] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR5] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR5] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR5] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR5] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR5] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR5] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR5] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR5] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR5] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR5] == `SR_SR11)			? SR11			:
					1'b0;
					
assign SR[6] = (SRSEL[`SSR6] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR6] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR6] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR6] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR6] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR6] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR6] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR6] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR6] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR6] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR6] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR6] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR6] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR6] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR6] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR6] == `SR_SR11)			? SR11			:
					1'b0;

assign SR[7] = (SRSEL[`SSR7] == `SR_START_A)		? START_A		:
					(SRSEL[`SSR7] == `SR_STOP_A)		? STOP_A			:
					(SRSEL[`SSR7] == `SR_UP_DOWN_A)	? UP_DOWN_A		:
					(SRSEL[`SSR7] == `SR_CLEAR_A)		? CLEAR_A		:
					(SRSEL[`SSR7] == `SR_RLCP0_A)		? RLCP0_A		:
					(SRSEL[`SSR7] == `SR_RLCP1_A)		? RLCP1_A		:
					(SRSEL[`SSR7] == `SR_OUV_T2A)		? OUV_T2A		:
					(SRSEL[`SSR7] == `SR_OUV_T2B)		? OUV_T2B		:
					(SRSEL[`SSR7] == `SR_START_B)		? START_B		:
					(SRSEL[`SSR7] == `SR_STOP_B)		? STOP_B			:
					(SRSEL[`SSR7] == `SR_RLCP0_B)		? RLCP0_B		:
					(SRSEL[`SSR7] == `SR_RLCP1_B)		? RLCP1_B		:
					(SRSEL[`SSR7] == `SR_SR00)			? SR00			:
					(SRSEL[`SSR7] == `SR_SR01)			? SR01			:
					(SRSEL[`SSR7] == `SR_SR10)			? SR10			:
					(SRSEL[`SSR7] == `SR_SR11)			? SR11			:
					1'b0;		


initial 
begin
	OUTX <= 0;
	OUT_SELECTED_PREV <= 0;
end
					
endmodule