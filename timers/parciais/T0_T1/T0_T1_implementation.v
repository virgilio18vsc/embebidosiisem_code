`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:33:19 04/20/2014 
// Design Name: 
// Module Name:    T0_T1_implementation 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module T0_T1_implementation(
	TxAINS,	TxBINS,	TxCINS,	TxDINS,
	TxAREL,	TxBREL,	TxCREL,	TxDREL,
	TxINC,
	T01IN0,	T01IN1,
	IN0, IN1, OUV_T2A, OUV_T2B,
	TxARUN, TxBRUN, TxCRUN, TxDRUN,
	TxAOV, TxBOV, 	TxCOV, TxDOV,
	RL_TxA, TNxDOV,
	TxASel, TxBSel, TxCSel, TxDSel,
	TxRASel, TxRBSel, TxRCSel, TxRDSel,
	TxDCBA, TxRDCBA,
	data_in, clk
	);


/***********************************************************************************************************************
/*********** Defines do m�dulo
/***********************************************************************************************************************/

/*Selec��o das entradas de clock*/
`define	TX_INS_CLK				2'b00		//Input do clk
`define	TX_INS_CNT0				2'b01		//Input do CNT0
`define	TX_INS_CNT1				2'b10		//Input do CNT1
`define	TX_INS_CCT				2'b11		//Entrada de carry (concatena��o)

/*Selec��o dos reloads*/
`define	TX_REL_RLD				1'b0		//Reload com overflow
`define	TX_REL_CCT				1'b1		//Concatena��o do overflow com o timer seguinte

/*Subtimers dos registos de 32-bits*/
`define 	TXA_BYTE					7:0
`define 	TXB_BYTE					15:8
`define 	TXC_BYTE					23:16
`define 	TXD_BYTE					31:24

/*Sele��o das entradas externas dos sinais de CNT0 e CNT1*/
`define 	TXD_EXT_CLK_OV_T2A	2'b00		//Overflow T2x
`define 	TXD_EXT_CLK_IN_POS	2'b01		//Transi��o positiva
`define 	TXD_EXT_CLK_IN_NEG	2'b10		//Transi��o negativa
`define 	TXD_EXT_CLK_IN_BOTH	2'b11		//Ambas as transi��es



/***********************************************************************************************************************
/*********** Inputs e outputs
/***********************************************************************************************************************/

/*****Controla a execu��o/Paragem dos timers*****/
input TxARUN; 								//[R] Controla a execu��o de T0A (0 - Para, 1 - Come�a)
input TxBRUN;								//[R] Controla a execu��o de T0B (0 - Para, 1 - Come�a)
input TxCRUN; 								//[R] Controla a execu��o de T0C (0 - Para, 1 - Come�a)
input TxDRUN;								//[R] Controla a execu��o de T0D (0 - Para, 1 - Come�a)


/*****Configura��o do timer*****/
input [1:0] TxAINS; 						//[R] Sele��o de entrada T0A
input [1:0] TxBINS;						//[R] Sele��o de entrada T0B
input [1:0] TxCINS;						//[R] Sele��o de entrada T0C
input [1:0] TxDINS;						//[R] Sele��o de entrada T0D


/*****Sele��o dos sinais externos para CNT0 e CNT1*****/
input [1:0] T01IN0; 						//[R] 00: Overflow/underflow do T2A (OUV_T2A), 
												//		01: Transi��o ascendente de IN0, 
												//		10: Transi��o descendente de IN0, 
												//		11: Transi��o ascendente de IN0
input [1:0] T01IN1; 						//[R] 00: Overflow/underflow do T2B (OUV_T2B), 
												//		01: Transi��o ascendente de IN1, 
												//		10: Transi��o descendente de IN1, 
												//		11: Transi��o ascendente de IN1


/*****Entradas de clock externo para CNT0 e CNT1*****/
input IN0;									// Entrada de clock externo.
input IN1;									// Entrada de clock externo.
input OUV_T2A;								// Entrada de clock externo. (overflow subtimer A do timer 2)
input OUV_T2B;								// Entrada de clock externo. (overflow subtimer B do timer 2)


/*****Configura��es de reload*****/
input TxAREL; 								//[R] 0: Reload, 1: Concatena��o com TB
input TxBREL; 								//[R] 0: Reload, 1: Concatena��o com TC
input TxCREL; 								//[R] 0: Reload, 1: Concatena��o com TD
input TxDREL; 								//[R] 0: Reload, 1: Concatena��o com TA


/*****Selec��o de concatena��o*****/
input TxINC;  								//[R] 0: Concatena com TD do pr�prio timer, 1: Concatena com TD do outro timer


/*****Overflow e sinais de retornos dos timers*****/

//Overflows dos subtimers do outro timer como inputs
input RL_TxA; 								//Sinal de reload do timer oposto que permite fazer reload de todos os subtimers juntos.
input TNxDOV;								//Sinal de overflow necess�rio para timers 64-bits.

//Overflows dos subtimers do pr�prio timer como output
output reg TxAOV;							//Overflow do timer x A.
output reg TxBOV;							//Overflow do timer x B.
output reg TxCOV;							//Overflow do timer x C.
output reg TxDOV;							//Overflow do timer x D.


/*****Registos para escrita dos registos de contagem e reload dos subtimers*****/
input TxASel;								//Escrita dispon�vel no registo TxDCBA, subtimer A.
input TxBSel;								//Escrita dispon�vel no registo TxDCBA, subtimer B.
input TxCSel;								//Escrita dispon�vel no registo TxDCBA, subtimer C.
input TxDSel;								//Escrita dispon�vel no registo TxDCBA, subtimer D.

input TxRASel;								//Escrita dispon�vel no registo TxRDCBA, subtimer A.
input TxRBSel;								//Escrita dispon�vel no registo TxRDCBA, subtimer B.
input TxRCSel;								//Escrita dispon�vel no registo TxRDCBA, subtimer C.
input TxRDSel;								//Escrita dispon�vel no registo TxRDCBA, subtimer D.


/*****Comunica��o de dados com o exterior*****/
input [31:0] data_in;					//Barramento de dados do timer.


/*****Sinal de clock geral*****/
input clk;


/***********************************************************************************************************************
/*********** Auxiliar para Debug
/***********************************************************************************************************************/
output [31:0] TxDCBA;					//Sa�das auxiliares para controlar registos de contagem.
output [31:0] TxRDCBA;					//Sa�das auxiliares para controlar registos de reload.


/******Registos para subtimers******/
reg [7:0] TxA, TxB, TxC, TxD;
reg [7:0] TxRA, TxRB, TxRC, TxRD;


/******Inicializa��o e assign dos registos de dados******/
assign TxDCBA = {TxD, TxC, TxB, TxA};
assign TxRDCBA = {TxRD, TxRC, TxRB, TxRA};

initial
begin
	TxA <= 0;
	TxB <= 0;
	TxC <= 0;
	TxD <= 0;
	
	TxRA <= 0;
	TxRB <= 0;
	TxRC <= 0;
	TxRD <= 0;
	
	TxAOV <= 0;
	TxBOV <= 0;
	TxCOV <= 0;
	TxDOV <= 0;
end


/*****Sinais externos para clock signals******/
reg CNT0;									//Registos com sinais externos como clock.
reg CNT1;									//Registos com sinais externos como clock.


/******Sempre que existe uma transi��o dos sinais de clock externo s�o atualizadas.******/
always @(IN0)
begin
	CNT0 <= 	(T01IN0 == `TXD_EXT_CLK_OV_T2A) 	? 	OUV_T2A 	:
				(T01IN0 == `TXD_EXT_CLK_IN_POS) 	? 	IN0 		:
				(T01IN0 == `TXD_EXT_CLK_IN_NEG) 	?	~IN0 		:
				(T01IN0 == `TXD_EXT_CLK_IN_BOTH) ? 	1			:
																0;
end

always @(IN1)
begin
	CNT1 <= 	(T01IN1 == `TXD_EXT_CLK_OV_T2A) 	? 	OUV_T2B	:
				(T01IN1 == `TXD_EXT_CLK_IN_POS) 	? 	IN1 		:
				(T01IN1 == `TXD_EXT_CLK_IN_NEG) 	? 	~IN1 		:
				(T01IN1 == `TXD_EXT_CLK_IN_BOTH)	?	1			:
																0;
end


/***********************************************************************************************************************
/*********** Clocks do m�dulo
/***********************************************************************************************************************/
/*****Clock de cada subtimer de 8-bit timer*****/
wire TxAclk;								//Sinal de clock do subtimer A de 8-bits.
wire TxBclk;								//Sinal de clock do subtimer B de 8-bits.
wire TxCclk;								//Sinal de clock do subtimer C de 8-bits.
wire TxDclk;								//Sinal de clock do subtimer D de 8-bits.

wire TxAclk_both;							//Sinal de clock do subtimer A de 8-bits quando ambas as transi��es s�o pretendidas.
wire TxBclk_both;							//Sinal de clock do subtimer B de 8-bits quando ambas as transi��es s�o pretendidas.
wire TxCclk_both;							//Sinal de clock do subtimer C de 8-bits quando ambas as transi��es s�o pretendidas.
wire TxDclk_both;							//Sinal de clock do subtimer D de 8-bits quando ambas as transi��es s�o pretendidas.


/*****Se ambas transi��es dos sinais externos IN0 ou IN1 s�o o clock do subtimer, � alterado o seu valor.******/
assign TxAclk_both = ( (T01IN0 == `TXD_EXT_CLK_IN_BOTH) && (TxAINS == `TX_INS_CNT0) ) ? IN0 :
							( (T01IN1 == `TXD_EXT_CLK_IN_BOTH) && (TxAINS == `TX_INS_CNT1) ) ? IN1 :
							0;
							
assign TxBclk_both = ( (T01IN0 == `TXD_EXT_CLK_IN_BOTH) && (TxBINS == `TX_INS_CNT0) ) ? IN0 :
							( (T01IN1 == `TXD_EXT_CLK_IN_BOTH) && (TxBINS == `TX_INS_CNT1) ) ? IN1 :
							0;
							
assign TxCclk_both = ( (T01IN0 == `TXD_EXT_CLK_IN_BOTH) && (TxCINS == `TX_INS_CNT0) ) ? IN0 :
							( (T01IN1 == `TXD_EXT_CLK_IN_BOTH) && (TxCINS == `TX_INS_CNT1) ) ? IN1 :
							0;
							
assign TxDclk_both = ( (T01IN0 == `TXD_EXT_CLK_IN_BOTH) && (TxDINS == `TX_INS_CNT0) ) ? IN0 :
							( (T01IN1 == `TXD_EXT_CLK_IN_BOTH) && (TxDINS == `TX_INS_CNT1) ) ? IN1 :
							0;


/*****Sele��o do clock de acordo com TxyINS*****/
assign TxAclk = (TxAINS == `TX_INS_CLK)									?	clk	: 		//Clock global.
					 (TxAINS == `TX_INS_CNT0)									?	CNT0	:		//Clock externo 0.
					 (TxAINS == `TX_INS_CNT1)									? 	CNT1	:		//Clock externo 1.
					 (TxAINS == `TX_INS_CCT && TxINC == `TX_REL_RLD )	? 	TxDOV	:		//Overflow do pr�ximo subtimer (controlado internamente).
					 (TxAINS == `TX_INS_CCT && TxINC == `TX_REL_CCT )	?	TNxDOV:		//Overflow do pr�ximo subtimer do outro timer. (caso particular do subtimer A).
																							0;


assign TxBclk = (TxBINS == `TX_INS_CLK)	?	clk	:
					 (TxBINS == `TX_INS_CNT0)	?	CNT0	:
					 (TxBINS == `TX_INS_CNT1)	? 	CNT1	:
					 (TxBINS == `TX_INS_CCT)	?	TxAOV :
															0;


assign TxCclk = (TxCINS == `TX_INS_CLK)	?	clk	:
					 (TxCINS == `TX_INS_CNT0)	?	CNT0	:
					 (TxCINS == `TX_INS_CNT1)	? 	CNT1	:
					 (TxCINS == `TX_INS_CCT)	?	TxBOV :
															0;


assign TxDclk = (TxDINS == `TX_INS_CLK)	?	clk	:
					 (TxDINS == `TX_INS_CNT0)	?	CNT0	:
					 (TxDINS == `TX_INS_CNT1)	? 	CNT1	:
					 (TxDINS == `TX_INS_CCT)	?	TxCOV	:
															0;


/***********************************************************************************************************************
/*********** Registos de reload
/***********************************************************************************************************************/
/*****Registos de reload atualizados sempre os registos de escrita (TxRySel) estiverem ativos*****/
always @(posedge clk)
begin
	if ( TxRASel )
		TxRA <= data_in[`TXA_BYTE];
		
	if ( TxRBSel )
		TxRB <= data_in[`TXB_BYTE];

	if ( TxRCSel )
		TxRC <= data_in[`TXC_BYTE];

	if ( TxRDSel )
		TxRD <= data_in[`TXD_BYTE];		
end


/***********************************************************************************************************************
/*********** Reload aquando do fim da contagem
/***********************************************************************************************************************/
/*****Registos de reload auxiliares para detetar quando terminou a contagem.*****/
reg 	ReloadA = 0;
reg	ReloadB = 0;
reg 	ReloadC = 0;
reg	ReloadD = 0;


/*****Registos auxiliares para verificar se a transi��o ocorrida foi ascendente ou descendente.*****/
reg	ReloadA_l = 0;
reg	ReloadB_l = 0;
reg	ReloadC_l = 0;
reg	ReloadD_l = 0;

reg	TxAOV_l;
reg	TxBOV_l;
reg	TxCOV_l;
reg	TxDOV_l;


/*****Wire para facilitar a verifica��o se est� a ser feita a concatena��o com o outro timer e se j� ocorreu o reload no outro timer.*****/
wire	ReloadTNx = TxINC && RL_TxA;


/*****Bloco always para que quando haja um overflow seja verificado se deve ser efetuado o reload de um ou v�rios subtimers.*****/
always @( RL_TxA or TxAOV or TxBOV or TxCOV or TxDOV )
begin

	//Para cada subtimer � verificado se ocorreu o overflow do subtimer mais significativo do contador. 
	ReloadA = ( TxAOV && !TxAOV_l && !TxAREL ) || ( TxBOV && !TxBOV_l && !TxBREL && TxAREL ) || ( TxCOV && !TxCOV_l && !TxCREL && TxBREL && TxAREL ) || ( TxDOV && !TxDOV_l && TxCREL && TxBREL && TxAREL );
	ReloadB = ( TxAOV && !TxAOV_l && TxAREL && TxDREL && TxCREL && TxBREL && ( TxBINS != `TX_INS_CCT ) ) || ( TxBOV && !TxBOV_l && !TxBREL ) || ( TxCOV && !TxCOV_l && !TxCREL && TxBREL ) || ( TxDOV && !TxDOV_l && TxCREL && TxBREL );
	ReloadC = ( TxAOV && !TxAOV_l && !TxAREL && TxDREL && TxCREL ) || ( TxBOV && !TxBOV_l && TxAREL && TxDREL && TxCREL && TxBREL && ( TxCINS != `TX_INS_CCT ) ) || ( TxCOV && !TxCOV_l && !TxCREL ) || ( TxDOV && !TxDOV_l && TxCREL );
	ReloadD = ( TxAOV && !TxAOV_l && !TxAREL && TxDREL ) || ( TxBOV && !TxBOV_l && !TxBREL && TxAREL && TxDREL ) || ( TxCOV && !TxCOV_l && !TxCREL && TxDREL && TxAREL && TxBREL && ( TxDINS != `TX_INS_CCT ) ) || ( TxDOV && !TxDOV_l && !TxDREL );
	
	
	//O caso do subtimer D � particular para verificar se existe concatena��o com o outro timer.. 
	if ( TxDOV && !TxDOV_l )
	begin
		if ( !TxINC )
			ReloadA = ReloadA && !TxDREL && ( TxAINS != `TX_INS_CCT );
			
		if ( TxINC && TxAREL )
			ReloadB = ReloadB && !TxDREL;
			
		if ( TxINC && TxAREL && TxBREL )
			ReloadC = ReloadC && !TxDREL;
	end


	//Caso esteja concatenado com o outro timer para al�m de ser necess�rio que o subtimer mais significativo tenha dado overflow tamb�m � necess�rio que o outro timer tenha dado overflow.. 
	if ( TxINC )
		ReloadA = ReloadA && ReloadTNx;
		
	if ( TxINC && TxAREL )
		ReloadB = ReloadB && ReloadTNx;
		
	if ( TxINC && TxAREL && TxBREL )
		ReloadC = ReloadC && ReloadTNx;
		
	if ( TxINC && TxAREL && TxBREL && TxCREL )
		ReloadD = ReloadD && ReloadTNx;


	//Registo com o estado atual do clock para poder verificar qual a transi��o ocorrida. 	
	TxAOV_l = TxAOV;
	TxBOV_l = TxBOV;
	TxCOV_l = TxCOV;
	TxDOV_l = TxDOV;
end


/*****A cada sinal de clock ou de reload o valor de contagem dos subtimers � atualizado.*****/
always @( TxAclk or TxAclk_both or ReloadA )
begin
	if ( TxARUN )
	begin
		//Se o bloco � executado por uma transi��o ascendente do sinal de reload, � feito o reload do registo de contagem.
		if ( ReloadA && !ReloadA_l )
			TxA <= TxRA;
		
		else if ( !( !ReloadA && ReloadA_l ) )
		begin
			//Enquanto o sinal de load estiver ativado, o valor da contagem � lido do barramento de dados.
			if ( TxASel )
				TxA <= data_in[`TXA_BYTE];
		
			//Se nem o load nem o reload � efetuado, ent�o existe o incremento do valor de contagem do subtimer.
			else if ( TxAclk || TxAclk_both )
				{TxAOV, TxA} <= TxA + 1;
		end
	end

	//Atualizado registo com o estado atual do sinal de reload para poder verificar qual a transi��o ocorrida. 	
	ReloadA_l <= ReloadA;
end


			
always @( TxBclk or TxBclk_both or ReloadB )
begin
	if ( TxBRUN )
	begin
		if ( ReloadB && !ReloadB_l )
			TxB <= TxRB;
		
		else if ( !( !ReloadB && ReloadB_l ) )
		begin
			if ( TxBSel )
				TxB <= data_in[`TXB_BYTE];
		
			else if ( TxBclk || TxBclk_both )
				{TxBOV, TxB} <= TxB + 1;
		end
	end

	ReloadB_l <= ReloadB;
end


always @( TxCclk or TxCclk_both or ReloadC )
begin
	if ( TxCRUN )
	begin
		if ( ReloadC && !ReloadC_l )
			TxC <= TxRC;
			
		else if ( !( !ReloadC && ReloadC_l ) )
		begin
			if ( TxCSel )
				TxC <= data_in[`TXC_BYTE];
		
			else if ( TxCclk || TxCclk_both )
				{TxCOV, TxC} <= TxC + 1;
		end
	end

	ReloadC_l <= ReloadC;
end


always @( TxDclk or TxDclk_both or ReloadD )
begin
	if ( TxDRUN )
	begin
		if ( ReloadD && !ReloadD_l )
			TxD <= TxRD;
		
		else if ( !( !ReloadD && ReloadD_l ) )
		begin
			if ( TxDSel )
				TxD <= data_in[`TXD_BYTE];
		
			else if ( TxDclk || TxDclk_both )
				{TxDOV, TxD} <= TxD + 1;
		end
	end

	ReloadD_l <= ReloadD;
end

endmodule
