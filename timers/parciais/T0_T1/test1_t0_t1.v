`timescale 1ns / 1ps

module test;

	// Inputs
	reg [1:0] TxAINS;
	reg [1:0] TxBINS;
	reg [1:0] TxCINS;
	reg [1:0] TxDINS;
	reg TxAREL;
	reg TxBREL;
	reg TxCREL;
	reg TxDREL;
	reg TxINC;
	reg [1:0] T01IN0;
	reg [1:0] T01IN1;
	reg IN0;
	reg IN1;
	reg OUV_T2A;
	reg OUV_T2B;
	reg TxARUN;
	reg TxBRUN;
	reg TxCRUN;
	reg TxDRUN;
	reg RL_TxA;
	reg TNxDOV;
	reg TxASel;
	reg TxBSel;
	reg TxCSel;
	reg TxDSel;
	reg TxRASel;
	reg TxRBSel;
	reg TxRCSel;
	reg TxRDSel;
	reg [31:0] data_in;
	reg clk;

	// Outputs
	wire TxAOV;
	wire TxBOV;
	wire TxCOV;
	wire TxDOV;
	wire [31:0] TxDCBA;
	wire [31:0] TxRDCBA;

	// Instantiate the Unit Under Test (UUT)
	T0_T1_implementation uut (
		.TxAINS(TxAINS), 
		.TxBINS(TxBINS), 
		.TxCINS(TxCINS), 
		.TxDINS(TxDINS), 
		.TxAREL(TxAREL), 
		.TxBREL(TxBREL), 
		.TxCREL(TxCREL), 
		.TxDREL(TxDREL), 
		.TxINC(TxINC), 
		.T01IN0(T01IN0), 
		.T01IN1(T01IN1), 
		.IN0(IN0), 
		.IN1(IN1), 
		.OUV_T2A(OUV_T2A), 
		.OUV_T2B(OUV_T2B), 
		.TxARUN(TxARUN), 
		.TxBRUN(TxBRUN), 
		.TxCRUN(TxCRUN), 
		.TxDRUN(TxDRUN), 
		.TxAOV(TxAOV), 
		.TxBOV(TxBOV), 
		.TxCOV(TxCOV), 
		.TxDOV(TxDOV), 
		.RL_TxA(RL_TxA), 
		.TNxDOV(TNxDOV), 
		.TxASel(TxASel), 
		.TxBSel(TxBSel), 
		.TxCSel(TxCSel), 
		.TxDSel(TxDSel), 
		.TxRASel(TxRASel), 
		.TxRBSel(TxRBSel), 
		.TxRCSel(TxRCSel), 
		.TxRDSel(TxRDSel), 
		.TxDCBA(TxDCBA), 
		.TxRDCBA(TxRDCBA), 
		.data_in(data_in), 
		.clk(clk)
	);

	initial begin
		// Initialize Inputs
		TxAINS = 0;
		TxBINS = 0;
		TxCINS = 0;
		TxDINS = 0;
		TxAREL = 0;
		TxBREL = 0;
		TxCREL = 0;
		TxDREL = 0;
		TxINC = 0;
		T01IN0 = 0;
		T01IN1 = 0;
		IN0 = 0;
		IN1 = 0;
		OUV_T2A = 0;
		OUV_T2B = 0;
		TxARUN = 0;
		TxBRUN = 0;
		TxCRUN = 0;
		TxDRUN = 0;
		RL_TxA = 0;
		TNxDOV = 0;
		TxASel = 0;
		TxBSel = 0;
		TxCSel = 0;
		TxDSel = 0;
		TxRASel = 0;
		TxRBSel = 0;
		TxRCSel = 0;
		TxRDSel = 0;
		data_in = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
		//Carregamento de TB e TC com 0xAA (se a correr...)
		data_in = (8'hAA) << 16;
		TxBSel = 1; 	TxCSel = 1;
		
		#5
		//Todos os timers a correr. TA e TD com o clock por defeito (global). TB e TC ir�o fazer load de 0xAA.
		TxARUN = 1;	TxBRUN = 1; TxCRUN = 1;	TxDRUN = 1;

		#5
		//Todos os timers parados.
		TxARUN = 0;	TxBRUN = 0; TxCRUN = 0;	TxDRUN = 0;

		#10
		//Clock de TB e TC � o overflow do timer anterior. Clock do TA � o posedge de CNT1 e clock de TD utiliza ambas as transi��es de CNT0.
		TxAINS = 2;
		TxBINS = 3;
		TxCINS = 3;
		TxDINS = 1;
		T01IN1 = 1;
		T01IN0 = 3;
		
		//Configurada concatena��o de TA com TB e TB com TC.
		TxAREL = 1;	TxBREL = 1;

		//Alterados registos de reload de TB e TC com (TB com 0xFA).
		data_in = (8'hFA) << 8;
		TxRBSel = 1; 	TxRCSel = 1;
	
		#5;
		//Todos os timers a correr com as novas configuracoes.
		TxARUN = 1;	TxBRUN = 1; TxCRUN = 1;	TxDRUN = 1;
	
		#50;
		//Apenas ap�s este ponto TB e TC come�am contagem porque apenas a partir daqui n�o est�o a fazer load.
		TxBSel = 0;	TxCSel = 0;
	end
      
	initial
	begin
		clk = 0;
		forever
			#1 clk = ~clk;
	end
	
	initial
	begin
		IN0 = 0;
		forever
			#1 IN0 = ~IN0;
	end
	
	initial
	begin
		IN1 = 0;
		forever
			#2 IN1 = ~IN1;
	end

endmodule
