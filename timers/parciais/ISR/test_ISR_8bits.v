`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:22:38 04/28/2014
// Design Name:   GPTU
// Module Name:   C:/prog_xilinx/Projecto/Registos/timer_tricore/test.v
// Project Name:  timer_tricore
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: GPTU
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test;

	/***********************************************************************************************************************
/*********** Registo T01IRS 
/***********************************************************************************************************************/
/*Selec��o das entradas de clock*/
`define	TO_INS_CLK	2'b00		//Input do clk
`define	TO_INS_CNT0	2'b01		//Input do CNT0
`define	TO_INS_CNT1	2'b10		//Input do CNT1
`define	TO_INS_CCT	2'b11		//Entrada de carry (concatena��o)

/*****Configura��o do timer***/
`define 	T0AINS 	1:0 	//[R/W] Sele��o de entrada T0A
`define 	T0BINS 	3:2	//[R/W] Sele��o de entrada T0B
`define	T0CINS 	5:4 	//[R/W] Sele��o de entrada T0C
`define	T0DINS 	7:6 	//[R/W] Sele��o de entrada T0D
`define	T1AINS 	9:8 	//[R/W] Sele��o de entrada T1A
`define	T1BINS 	11:10	//[R/W] Sele��o de entrada T1B
`define	T1CINS 	13:12	//[R/W] Sele��o de entrada T1C
`define	T1DINS 	15:14 //[R/W] Sele��o de entrada T1D


/*Selec��o doc reload*/
`define	TO_REL_RLD	1'b0	//Reload com overflow
`define	TO_REL_CCT	1'b1	//Concatena��o do overflow com o timer seguinte

/*****Configura��es de reload***/
`define 	T0AREL 	16 	//[R/W] 0: Reload, 1: Concatena��o com TB
`define	T0BREL 	17 	//[R/W] 0: Reload, 1: Concatena��o com TC
`define 	T0CREL	18 	//[R/W] 0: Reload, 1: Concatena��o com TD
`define	T0DREL 	19		//[R/W] 0: Reload, 1: Concatena��o com TA
`define 	T1AREL 	20 	//[R/W] 0: Reload, 1: Concatena��o com TB
`define	T1BREL 	21 	//[R/W] 0: Reload, 1: Concatena��o com TC
`define	T1CREL 	22 	//[R/W] 0: Reload, 1: Concatena��o com TD
`define	T1DREL 	23		//[R/W] 0: Reload, 1: Concatena��o com TA


/****Selec��o de concatena��o**/
`define 	T0INC 	24  	//[R/W] 0: O carry do T0A � o carry de sa�da do T0D, 
										//1: O carry do T0A � o carry de sa�da do T0D
`define	T1INC 	25 	//[R/W] 0: O carry do T1A � o carry de sa�da do T1D, 
										//S1: O carry do T1A � o carry de sa�da do T0D

/****Sele��o dos sinais externos para CNT0 e CNT1**/
`define 	T01IN0 	27:26	//[R/W] 00: Overflow/underflow do T2A (OUV_T2A), 
										//01: Transi��o ascendente de IN0, 
										//10: Transi��o descendente de IN0, 
										//11: Transi��o ascendente de IN0
`define 	T01IN1 	29:28	//[R/W] 00: Overflow/underflow do T2B (OUV_T2B), 
										//01: Transi��o ascendente de IN1, 
										//10: Transi��o descendente de IN1, 
										//11: Transi��o ascendente de IN1


/***********************************************************************************************************************
/*********** Sinal de controlo timer_write_sel
/***********************************************************************************************************************/
`define 	T0ASel 	4'b0000
`define 	T0BSel 	4'b0001
`define 	T0CSel  	4'b0010
`define	T0DSel 	4'b0011
`define 	T1ASel 	4'b0100
`define 	T1BSel 	4'b0101
`define 	T1CSel 	4'b0110
`define 	T1DSel 	4'b0111
`define 	T2ASel 	4'b1000
`define 	T2BSel 	4'b1001
`define 	T2CSel 	4'b1010
`define 	T2DSel 	4'b1011

/***********************************************************************************************************************
/*********** Sinal de controlo timer_reload_write_sel
/***********************************************************************************************************************/
`define 	T0RASel	4'b0000
`define	T0RBSel 	4'b0001
`define	T0RCSel 	4'b0010
`define	T0RDSel 	4'b0011
`define	T1RASel 	4'b0100
`define	T1RBSel 	4'b0101
`define	T1RCSel 	4'b0110
`define 	T1RDSel	4'b0111
`define	T2RASel 	4'b1000
`define 	T2RBSel	4'b1001
`define	T2RCSel 	4'b1010
`define	T2RDSel 	4'b1011

/***********************************************************************************************************************
/*********** Registo TO12RUN 
/***********************************************************************************************************************/
`define	T0ARUN	0 	//[RW] Controla a execu��o de T0A (0 - Para, 1 - Come�a)
`define	T0BRUN	1 	//[RW] Controla a execu��o de T0B (0 - Para, 1 - Come�a)
`define	T0CRUN 	2	//[RW] Controla a execu��o de T0C (0 - Para, 1 - Come�a)
`define	T0DRUN 	3 	//[RW] Controla a execu��o de T0C (0 - Para, 1 - Come�a)
`define	T1ARUN 	4 	//[RW] Controla a execu��o de T1A (0 - Para, 1 - Come�a)
`define	T1BRUN  	5 	//[RW] Controla a execu��o de T1B (0 - Para, 1 - Come�a)
`define	T1CRUN  	6 	//[RW] Controla a execu��o de T1C (0 - Para, 1 - Come�a)
`define	T1DRUN 	7 	//[RW] Controla a execu��o de T1D (0 - Para, 1 - Come�a)
`define	T2ARUN 	8	//[R] Cont�m o estado de execu��o de T2A (0 - Parado 1 - Em execu��o), s� alterado por Hardware
`define	T2ASETR 	9	//[W] Controla a execu��o de T2A (1 - Come�a, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
`define	T2ACLRR  10	//[W] Controla a paragem de T2A (1 - Para, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
//11 -> N�o usado
`define	T2BRUN  	12	//[R] Cont�m o estado de execu��o de T2B, (0 - Parado 1 - Em execu��o), s� alterado por Hardware
`define	T2BSETR  13	//[W] Controla a execu��o de T2B (1 - Come�a, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
`define	T2BCLRR  14	//[W] Controla a paragem de T2B (1 - Para, 0 - N�o tem efeito), retorna sempre '0' quando � lido. 
							//Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
//15 - 31 -> N�o usado

/***********************************************************************************************************************
/*********** Registo OSEL 
/***********************************************************************************************************************/
/*Escolha dos overflows que afectam a sa�da*/
`define	SO_OUT00		3'b000	//1� Overflow do timer 0
`define	SO_OUT01		3'b001	//2� Overflow do timer 0
`define	SO_OUT10		3'b010	//1� Overflow do timer 1
`define	SO_OUT11		3'b011	//2� Overflow do timer 1
`define	SO_OUV_T2A	3'b100	//Overflow do timer 2 A
`define	SO_OUV_T2B	3'b101	//Overflow do timer 2 B
//110 - Reservado
//111 - Reservado

/*Localiza��o dos bits de configura��o de cada sa�da*/
`define 	SO0		2:0		//[R/W] Seleciona sa�da 0
`define 	SO1		6:4		//[R/W] Seleciona sa�da 1
`define 	SO2		10:8		//[R/W] Seleciona sa�da 2
`define 	SO3		14:12		//[R/W] Seleciona sa�da 3
`define 	SO4		18:16		//[R/W] Seleciona sa�da 4
`define 	SO5		22:20		//[R/W] Seleciona sa�da 5
`define 	SO6		26:24		//[R/W] Seleciona sa�da 6
`define 	SO7		30:28		//[R/W] Seleciona sa�da 7


/***********************************************************************************************************************
/*********** Registo OUT
/***********************************************************************************************************************/
/*
Fornece informa��o sobre o estado da sa�da. E s� pode ser alterado atrav�s do sinal a ele associado.
Se CLROx e SETOx estiveram ativos OUTx n�o � afetado.
*/
`define 	O_OUT0		0		//[R] Cont�m o bit de estado da sa�da 0
`define 	O_OUT1		1		//[R] Cont�m o bit de estado da sa�da 1
`define 	O_OUT2		2		//[R] Cont�m o bit de estado da sa�da 2
`define 	O_OUT3		3		//[R] Cont�m o bit de estado da sa�da 3
`define 	O_OUT4		4		//[R] Cont�m o bit de estado da sa�da 4
`define 	O_OUT5		5		//[R] Cont�m o bit de estado da sa�da 5
`define 	O_OUT6		6		//[R] Cont�m o bit de estado da sa�da 6
`define 	O_OUT7		7		//[R] Cont�m o bit de estado da sa�da 7

/*
Ao escrever:
Limpa o bit de sa�da 'x':
1 - Limpa o bit de s�ida
0 - N�o tem efeito
Retorna sempre '0' quando � lido. 
Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
*/
`define 	O_CLRO0		7		//[W]Limpa o bit de sa�da out0
`define 	O_CLRO1		8		//[W]Limpa o bit de sa�da out1
`define 	O_CLRO2		10		//[W]Limpa o bit de sa�da out2
`define 	O_CLRO3		11		//[W]Limpa o bit de sa�da out3
`define 	O_CLRO4		12		//[W]Limpa o bit de sa�da out4
`define 	O_CLRO5		13		//[W]Limpa o bit de sa�da out5
`define 	O_CLRO6		14		//[W]Limpa o bit de sa�da out6
`define 	O_CLRO7		15		//[W]Limpa o bit de sa�da out7

/*
Ativa o bit de sa�da 'x':
Ao escrever:
1 - Ativa o bit de s�ida
0 - N�o tem efeito
Retorna sempre '0' quando � lido. 
Modifica��es por software s�o mais priorit�rias que modifica��es por hardware.
*/
`define 	O_SETO0		16		//[W]Ativa o bit de sa�da out0
`define 	O_SETO1		17		//[W]Ativa o bit de sa�da out1
`define 	O_SETO2		18		//[W]Ativa o bit de sa�da out2
`define 	O_SETO3		19		//[W]Ativa o bit de sa�da out3
`define 	O_SETO4		20		//[W]Ativa o bit de sa�da out4
`define 	O_SETO5		21		//[W]Ativa o bit de sa�da out5
`define 	O_SETO6		22		//[W]Ativa o bit de sa�da out6
`define 	O_SETO7		23		//[W]Ativa o bit de sa�da out7


/***********************************************************************************************************************
/*********** Registo T01OTS
/***********************************************************************************************************************/
/*Sele��o do pedido de servi�o SSRxy*/
`define 	SSR_TXA_OVF		2'b00	//Overflow timer A
`define 	SSR_TXB_OVF		2'b01	//Overflow timer B
`define 	SSR_TXC_OVF		2'b10	//Overflow timer C
`define 	SSR_TXD_OVF		2'b11	//Overflow timer D

/*Sele��o do sinal de sa�da STRGxy*/
`define 	STRG_TXA_OVF	2'b00	//Overflow timer A
`define 	STRG_TXB_OVF	2'b01	//Overflow timer B
`define 	STRG_TXC_OVF	2'b10	//Overflow timer C
`define 	STRG_TXD_OVF	2'b11	//Overflow timer D

/*Sele��o da sa�da SOUTxy*/
`define 	SOUT_TXA_OVF	2'b00	//Overflow timer A
`define 	SOUT_TXB_OVF	2'b01	//Overflow timer B
`define 	SOUT_TXC_OVF	2'b10	//Overflow timer C
`define 	SOUT_TXD_OVF	2'b11	//Overflow timer D

`define 	SOUT00	1:0		//[R/W]Seleciona da fonte da sa�da 0 de T0
`define 	SOUT01	3:2		//[R/W]Seleciona da fonte da sa�da 1 de T0
`define 	STRG00	5:4		//[R/W]Seleciona o trigger da fonte da sa�da 0 de T0
`define 	STRG01	7:6		//[R/W]Seleciona o trigger da fonte da sa�da 1 de T0
`define 	SSR00		9:8		//[R/W]Seleciona do pedido de servi�o 0 do T0
`define 	SSR01		11:10		//[R/W]Seleciona do pedido de servi�o 1 do T0
//[15:12] -> reservado(l�-se '0'; deve ser escrito com '0')
`define 	SOUT10	17:16		//[R/W]Seleciona da fonte da sa�da 0 de T1
`define 	SOUT11	19:18		//[R/W]Seleciona da fonte da sa�da 1 de T1
`define 	STRG10	21:20		//[R/W]Seleciona o trigger da fonte da sa�da 0 de T1
`define 	STRG11	23:22		//[R/W]Seleciona o trigger da fonte da sa�da 1 de T1
`define 	SSR10		25:24		//[R/W]Seleciona do pedido de servi�o 0 do T1
`define 	SSR11		27:26		//[R/W]Seleciona do pedido de servi�o 1 do T1
//[12] -> reservado(l�-se '0'; deve ser escrito com '0')



/*	====================================================
*	Mapeamento do SRSEL
*	===================================================*/
/*defines de mapeamento*/
`define SSR0	31:28
`define SSR1	27:24
`define SSR2	23:20
`define SSR3	19:16
`define SSR4	15:12
`define SSR5	11:8
`define SSR6	7:4
`define SSR7	3:0
/*	================================================== 
*	Confiracao dos sinais para serem ISR
*	=================================================*/
	/*defines dos varios sinais IR*/
`define SR_START_A	 4'b0000
`define SR_STOP_A	 	 4'b0001
`define SR_UP_DOWN_A	 4'b0010
`define SR_CLEAR_A 	 4'b0011
`define SR_RLCP0_A 	 4'b0100
`define SR_RLCP1_A 	 4'b0101
`define SR_OUV_T2A 	 4'b0110
`define SR_OUV_T2B 	 4'b0111
`define SR_START_B 	 4'b1000
`define SR_STOP_B 	 4'b1001
`define SR_RLCP0_B 	 4'b1010
`define SR_RLCP1_B 	 4'b1011
`define SR_SR00 		 4'b1100
`define SR_SR01 		 4'b1101
`define SR_SR10 		 4'b1110
`define SR_SR11 		 4'b1111

//	================================================
	// Inputs
	reg [31:0] T01IRS;
	reg [31:0] T01OTS;
	reg [4:0] timer_write_sel;
	reg [4:0] timer_reload_write_sel;
	reg IN0;
	reg IN1;
	reg [31:0] data_in;
	reg [31:0] T012RUN;
	reg [31:0] OSEL;
	reg [31:0] OUT;
	reg clk;
	reg [31:0] SRSEL;

	// Outputs
	wire [7:0] OUTX;
	wire [7:0] SR;

	// Instantiate the Unit Under Test (UUT)
	GPTU uut (
		.T01IRS(T01IRS), 
		.T01OTS(T01OTS), 
		.timer_write_sel(timer_write_sel), 
		.timer_reload_write_sel(timer_reload_write_sel), 
		.IN0(IN0), 
		.IN1(IN1), 
		.data_in(data_in),
		.T012RUN(T012RUN), 
		.OSEL(OSEL), 
		.OUT(OUT), 
		.OUTX(OUTX), 
		.clk(clk),
		.SRSEL(SRSEL),
		.SR(SR)	
	);

initial begin
		// Initialize Inputs
		T01IRS = 0;
		T01OTS = 0;
		timer_write_sel = 0;
		timer_reload_write_sel = 0;
		IN0 = 0;
		IN1 = 0;
		data_in = 0;
		T012RUN = 0;
		OSEL = 0;
		OUT = 0;
		clk = 0;
		SRSEL = 0;

		// Wait 100 ns for global reset to finish
		#100;
		//=======================Configuracao do timer 0_A=============================
		//sera incrementado a cada subida do pulso do clock interno
		T01IRS[`T0AINS] = `TO_INS_CLK;
		//ira fazer reload quando ha overflow
		T01IRS[`T0AREL] = `TO_REL_RLD;
		//para recarregar o subtimer quando h� overflow, com valor a 0x00
		timer_reload_write_sel[`T0RASel] = 1;
		//==============configuracao do ISR apenas para overflow do A===================
		//SR00 dependera do overflow do Timer0_A
		T01OTS[`SSR00] = `SSR_TXA_OVF;
		//SR0(LSB do array ISR) dependera do sinal do SR00
		SRSEL[`SSR0] = `SR_SR00;
		
		#5;
		//timer A running		
		T012RUN[`T0ARUN] = 1;        
	end

	initial
	begin
		clk = 0;
		forever
			#1 clk = ~clk;
	end
      
endmodule
