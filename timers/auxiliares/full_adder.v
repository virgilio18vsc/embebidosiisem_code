`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:40:29 04/20/2014 
// Design Name: 
// Module Name:    full_adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module full_adder(
		a,
		b,
		cin,
		s,
		cout
    );

input a;
input b;
input cin;
output s;
output cout;

assign s = a ^ b ^ cin;
assign cout = (a & b) | ( a & cin ) | ( b & cin );

endmodule
