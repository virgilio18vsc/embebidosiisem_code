`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:20:25 03/08/2014 
// Design Name: 
// Module Name:    ffd 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ffd(
		enable,
		d,
		clock,
		q,
		qneg
    );

input enable;
input [7:0] d;
input clock;
output reg [7:0] q;
output reg [7:0] qneg;

initial begin
	q = 0;
	qneg = 0;
end

always@(posedge clock && enable)
begin
	q = d;
	qneg = ~d;
end

endmodule
