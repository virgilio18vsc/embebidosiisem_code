`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:47:02 04/20/2014 
// Design Name: 
// Module Name:    full_adder_32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module full_adder_32(
		p,
		q,
		cin,
		r
    );

input [31:0] p, q;
input cin;
output [32:0] r;
wire [30:0] carry;

full_adder fa0(p[0], q[0], ci, r[0], carry[0]);
full_adder fa1(p[1], q[1], carry[0], r[1], carry[1]);
full_adder fa2(p[2], q[2], carry[1], r[2], carry[2]);
full_adder fa3(p[3], q[3], carry[2], r[3], carry[3]);
full_adder fa4(p[4], q[4], carry[3], r[4], carry[4]);
full_adder fa5(p[5], q[5], carry[4], r[5], carry[5]);
full_adder fa6(p[6], q[6], carry[5], r[6], carry[6]);
full_adder fa7(p[7], q[7], carry[6], r[7], carry[7]);
full_adder fa8(p[8], q[8], carry[7], r[8], carry[8]);
full_adder fa9(p[9], q[9], carry[8], r[9], carry[9]);
full_adder fa10(p[10], q[10], carry[9], r[10], carry[10]);
full_adder fa11(p[11], q[11], carry[10], r[11], carry[11]);
full_adder fa12(p[12], q[12], carry[11], r[12], carry[12]);
full_adder fa13(p[13], q[13], carry[12], r[13], carry[13]);
full_adder fa14(p[14], q[14], carry[13], r[14], carry[14]);
full_adder fa15(p[15], q[15], carry[14], r[15], carry[15]);
full_adder fa16(p[16], q[16], carry[15], r[16], carry[16]);
full_adder fa17(p[17], q[17], carry[16], r[17], carry[17]);
full_adder fa18(p[18], q[18], carry[17], r[18], carry[18]);
full_adder fa19(p[19], q[19], carry[18], r[19], carry[19]);
full_adder fa20(p[20], q[20], carry[19], r[20], carry[20]);
full_adder fa21(p[21], q[21], carry[20], r[21], carry[21]);
full_adder fa22(p[22], q[22], carry[21], r[22], carry[22]);
full_adder fa23(p[23], q[23], carry[22], r[23], carry[23]);
full_adder fa24(p[24], q[24], carry[23], r[24], carry[24]);
full_adder fa25(p[25], q[25], carry[24], r[25], carry[25]);
full_adder fa26(p[26], q[26], carry[25], r[26], carry[26]);
full_adder fa27(p[27], q[27], carry[26], r[27], carry[27]);
full_adder fa28(p[28], q[28], carry[27], r[28], carry[28]);
full_adder fa29(p[29], q[29], carry[28], r[29], carry[29]);
full_adder fa30(p[30], q[30], carry[29], r[30], carry[30]);
full_adder fa31(p[31], q[31], carry[30], r[31], r[32]);

endmodule
