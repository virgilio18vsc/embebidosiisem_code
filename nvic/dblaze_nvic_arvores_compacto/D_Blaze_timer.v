`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins   
// 
// Create Date:    14:18:57 03/17/2014 
// Design Name: 
// Module Name:    D_Blaze_timer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_timer(
    //inputs
	 input 			clock,
	 input 			reset,
	 input 			select,
	 input [31:0]	ctrl,
	 //outputs
	 output reg		ssr0,
	 output reg		ssr1,
	 output reg		ssr2,
	 output reg		ssr3,
	 output reg		ssr4,
	 output reg		ssr5,
	 output reg		ssr6,
	 output reg		ssr7
	 );

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [2:0] command;
wire [2:0] type;
wire [15:0] dec_output;

//////////////////////////////////////////////////////////////////////////////////
D_Blaze_ctrl_dec decoder(
   //inputs
	.ctrl(ctrl), 
	.select(select),
	//outputs 
	.command(command),
	.type(type),
	.dec_output(dec_output)
);

//////////////////////////////////////////////////////////////////////////////////
// Debug /////////////////////////////////////////////////////////////////////////

initial
begin
	ssr0 = 1;
	ssr1 = 1;
	ssr2 = 0;
	ssr3 = 0;	
	ssr4 = 0;
	ssr5 = 0;
	ssr6 = 0;
	ssr7 = 0;

	#15
	ssr0 = 0;
	ssr1 = 0;
	ssr2 = 0;

	#30
	ssr0 = 0;
	ssr1 = 0;
	ssr2 = 1;
	
	#10
	ssr0 = 0;
	ssr1 = 0;
	ssr2 = 0;
end

endmodule
