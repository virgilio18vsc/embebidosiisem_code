`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:00:50 03/28/2014 
// Design Name: 
// Module Name:    D_Blaze_multiplier 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_multiplier(
	input  [15:0]  multiplicand, 
   input  [15:0]  multiplier,
   output [31:0]  product
	);
   
assign product = (multiplicand * multiplier);

endmodule
