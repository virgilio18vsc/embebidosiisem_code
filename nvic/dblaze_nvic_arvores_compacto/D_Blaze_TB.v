`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:51:12 04/10/2014
// Design Name:   D_Blaze_SoC
// Module Name:   C:/ESRG/D_Blaze_SoC_V1.6/D_Blaze_SoC/D_Blaze_TB.v
// Project Name:  D_Blaze_SoC
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: D_Blaze_SoC
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module D_Blaze_TB;

	// Inputs
	reg clock;
	reg rst;
	reg [7:0] in_pins;

	// Outputs
	wire [7:0] out_pins;

	// Instantiate the Unit Under Test (UUT)
	D_Blaze_SoC uut (
		.clock(clock), 
		.rst(rst), 
		.in_pins(in_pins), 
		.out_pins(out_pins)
	);

initial
begin
	// Initialize Inputs
	clock = 0;
	rst = 0;
	in_pins = 0;

	// Wait 100 ns for global reset to finish
	#10;
	rst = 1; 
	// Add stimulus here
	in_pins=8'b00000011;
end
     
always #5 clock = ~clock;
	  
endmodule

