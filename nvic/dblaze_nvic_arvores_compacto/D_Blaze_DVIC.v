`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins   
// 
// Create Date:    14:24:49 03/17/2014 
// Design Name: 
// Module Name:    D_Blaze_DVIC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module D_Blaze_DVIC(
    //inputs
	 clock,															//Rel�gio do sistema
	 reset,															//Sinal de reset
	 ctrl,															//Sinal de controlo (tem que ser alterado para o barramento standard)
	 select,															//Sinal de controlo (tem que ser alterado para o barramento standard)
	 reti_signal,													//Sinal de RETI, enviado pelo processador quando � executado um RETI
	 interrupt_request,											//Vetor de interrup��es
	 interrupt_ack,												//Sinal de ack, enviado pelo processador quando � executada a interrup��o
	 
	 //outputs
	 interrupt_address,											//Endere�o para onde o micro tem que saltar quando h� uma interrup��o
	 interrupt_pending											//Indica ao processador que existe uma interrup��o a pending
	 );

parameter N_INTERRUPTS = 32; 									//Tamanho do Vector de interrup��es (Recomendado tamanho de base 2)
parameter N_BITS = 5; 											//Numero de bits minimo capaz de representar o n�mero de interrup��es (Ex: 32 Interrup��es => 5 Bits)
parameter COL_NUMBER = 4;										//Numero de colunas do registo IPR
parameter ROW_NUMBER = N_INTERRUPTS / COL_NUMBER;		//Numero de linhas do registo IPR

/***********************************************************************************************************************
/*********** Sinais de Input
/***********************************************************************************************************************/	 	 
input clock;
input reset;
input [31:0] ctrl;
input select;
input reti_signal;
input [N_INTERRUPTS - 1:0] interrupt_request;
input interrupt_ack;

/***********************************************************************************************************************
/*********** Sinais de Output
/***********************************************************************************************************************/	
output [11:0] interrupt_address;
output reg interrupt_pending;


wire [11:0] new_interrupt_address;		//Endere�o do vetor de interrup��es lido do vetor de interrup��es.
reg [5:0] vector_index;						//Indice do vetor de interrup��es com o endere�o da interrup��o.

//Vetor de interrup��es
vector_table interrupt_table(
 .clka(clock),
 .rsta(0),
 .ena(1'b1),
 .addra(vector_index),
 .douta(new_interrupt_address)
);


/***********************************************************************************************************************
/*********** Registo IPR(Interrupt Priority Registers)
/***********************************************************************************************************************/	 
reg [7:0] IPR [(N_INTERRUPTS/4) - 1:0][3:0];	//Guarda o valor das prioridade de grupo e sub-grupo por cada interrup��o
															//Prioridade de 0 a 31, sendo 0 a de maior prioridade e 31 a de menor (MSB).
															//Guarda os valores das flags de estado da interrup��o (ENABLED, PENDING, STACKED)

//Posicionamento dos registos de prioridade(Grupo e Sub-Grupo)
//PRI_<Tipo>_P<Numero de grupos de prioridade>_S<Numero de sub-grupos de prioridades>
//Tipo:
//->G	:	Grupos de prioridades
//->SG:	Subgrupos de prioridades

`define PRI_G_P32_S0		4:0

`define PRI_G_P16_S2		4:1
`define PRI_SG_P16_S2	0

`define PRI_G_P8_S4		4:2
`define PRI_SG_P8_S4		1:0

`define PRI_G_P4_S8		4:3
`define PRI_SG_P4_S8		2:0

`define PRI_G_P2_S16		4
`define PRI_SG_P2_S16	3:0

`define PRI_SG_P0_S32	4:0


//Posi��o das flags de estado das interrup��es
`define F_FLAGS			7:5

`define	F_ENABLE 		5
`define	F_PENDING 		6
`define	F_STACKED 		7


/***********************************************************************************************************************
/*********** Registo AIRCR(Application Interrupt and Reset Control Register)
/***********************************************************************************************************************/	
reg [31:0] AIRCR; 
`define PRIGROUP	2:0	//Cursor de divis�o dos registos PRI em prioridades e sub-prioridades

//Configura��es do cursor de divisao dos registos PRI em prioridades e sub-prioridades
`define PRIO_P32_S0 		3'b000		//32 Prioridades e 0 Sub-prioridades
`define PRIO_P16_S2 		3'b001		//16 Prioridades e 2 Sub-prioridades
`define PRIO_P8_S4		3'b010		//8 Prioridades e 4 Sub-prioridades
`define PRIO_P4_S8 		3'b011		//4 Prioridades e 8 Sub-prioridades
`define PRIO_P2_S16 		3'b100		//2 Prioridades e 16 Sub-prioridades
`define PRIO_P0_S32 		3'b101		//0 Prioridades e 32 Sub-prioridades


/***********************************************************************************************************************
/*********** Registo STIR (Software Trigger Interrupt Register)
/***********************************************************************************************************************/	 
reg [31:0] 	STIR;							//Gera interrup��es por software
reg			sw_interrupt_changed;	//Sinal ativado sempre que existe uma nova interrup��o pedida por sw.
						
`define INTID	N_BITS - 1:0			//INTID ID da interrup��o para despoletar (0-n_interrup��o) (W).
												//Por exemplo, escrever 0x03 vai gerar a IRQ3 por software.
							
//`define RESERVED_STIR	31:N_BITS	//[31:N_BITS] -> Reservado: l�-se '0' (R).


/***********************************************************************************************************************
/*********** Registo  CIS(Current Interrupt State)
/***********************************************************************************************************************/	
reg [31:0] CIS;	

`define IPB				0		//Indica se existe uma interrup��o pending.

`define VECTPENDING	6:1	//Indica o n�mero da interrup��o pendente de maior prioridade(R) -> Implementar no bloco always

`define ISB				7		//Indica se existe uma interrup��o stacked.

`define VECTSTACKED	12:8	//Indica o n�mero da interrup��o stacked de maior prioridade(R) -> Implementar no bloco always

`define AB				13		//Indica se existe uma interrup��o ativa.

`define ETMINTNUM		18:14	//Indica o n�mero da interrup��o em execu��o (0-31) (R) -> Implementar no bloco always

`define CURRPRI 		23:19 //Indica a prioridade da interrup��o em execu��o. Configurada nos registos IPR (R) -> Implementar no bloco always

`define RESERVED_CIS	31:24	//[7:0] -> Reservado: l�-se '0' (R).

									
/***********************************************************************************************************************
/*********** Selecc�o das Grupo/SubGrupo de prioridades
/***********************************************************************************************************************/	
wire [4:0] g_priority [N_INTERRUPTS - 1:0];	//Prioridades de grupo

wire [4:0] sg_priority [N_INTERRUPTS - 1:0];	//Prioridades de sub-grupo

/*Cria os multiplexers para selec��o do nivel de grupo e sub-grupo de prioridades
do registo IPR em fun��o da configura��o do campo PRIGROUP no registo AIRCR*/
generate
	genvar i_mux;						
	for (i_mux = 0; i_mux < N_INTERRUPTS; i_mux = i_mux + 1)
		begin: l_p_mux
			assign {g_priority[i_mux], sg_priority[i_mux]} = 
					(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P32_S0], 5'b00000																					}	:	
					(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P16_S2],	4'b0000,	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P16_S2]	}	:	
					(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P8_S4], 	3'b000, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P8_S4]	}	:
					(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P4_S8], 	2'b00, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P4_S8]	}	:
					(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P2_S16],	1'b0, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P2_S16]	}	:
					(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,																								IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P0_S32]	}	:
					10'b0000000000;
		end
endgenerate


/***********************************************************************************************************************
/*********** Arvore de decis�o de pending 
/***********************************************************************************************************************/	
wire [N_BITS-1:0] decision_t_pend [N_INTERRUPTS - 2:0]; //Wire para cria��o da arvore de decis�o de pending

/*Cria os toda a arvore de decis�o de pending*/
generate
	genvar i_dt_pend;
	assign decision_t_pend[0] = (	{~IPR[0][0][`F_ENABLE], ~IPR[0][0][`F_PENDING], g_priority[0], sg_priority[0]} <= 
								{~IPR[0][1][`F_ENABLE], ~IPR[0][1][`F_PENDING], g_priority[1], sg_priority[1]})	
								?	5'd0	:	5'd1;//Gera comparadores para comparar a 1� e 2� interrup��o
								
	for (i_dt_pend = 1; i_dt_pend < N_INTERRUPTS - 1; i_dt_pend = i_dt_pend + 1)//Gera comparadores para comparar todas as outras interrup��es
		begin: l_d_t_pend
			assign decision_t_pend[i_dt_pend] = (	{	~IPR[ decision_t_pend[i_dt_pend - 1][N_BITS-1:2] ][ decision_t_pend[i_dt_pend - 1][1:0] ][ `F_ENABLE ], 
																	~IPR[ decision_t_pend[i_dt_pend - 1][N_BITS-1:2] ][ decision_t_pend[i_dt_pend - 1][1:0] ][ `F_PENDING ], 
																	g_priority[ decision_t_pend[i_dt_pend - 1] ], 
																	sg_priority[ decision_t_pend[i_dt_pend - 1] ]
																} <= 
																{	~IPR[ (i_dt_pend + 1) >> 2 ][ (i_dt_pend + 1) & 2'b11 ][ `F_ENABLE ], 
																	~IPR[ (i_dt_pend + 1) >> 2 ][ (i_dt_pend + 1) & 2'b11 ][ `F_PENDING ], 
																	g_priority[ i_dt_pend + 1 ], 
																	sg_priority[ i_dt_pend + 1 ]
																})	
																?	decision_t_pend[i_dt_pend - 1]	:	i_dt_pend + 1;
		end
endgenerate


/***********************************************************************************************************************
/*********** Arvore de decis�o de stacked
/***********************************************************************************************************************/	
wire [N_BITS-1:0] decision_t_stack [N_INTERRUPTS - 2:0];	//Wire para cria��o da arvore de decis�o de Stacked

/*Cria os toda a arvore de decis�o de stacked*/
generate
	genvar i_dt_stack;
	assign decision_t_stack[0] = (	{~IPR[0][0][`F_ENABLE], ~IPR[0][0][`F_STACKED], g_priority[0], sg_priority[0]} <= 
												{~IPR[0][1][`F_ENABLE], ~IPR[0][1][`F_STACKED], g_priority[1], sg_priority[1]})	
												?	5'd0	:	5'd1;////Gera comparadores para comparar a 1� e 2� interrup��o
								
	for (i_dt_stack = 1; i_dt_stack < N_INTERRUPTS - 1; i_dt_stack = i_dt_stack + 1)//Gera comparadores para comparar todas as outras interrup��es
		begin: l_d_t_stack
			assign decision_t_stack[i_dt_stack] = (	{	~IPR[ decision_t_stack[i_dt_stack - 1][N_BITS-1:2] ][ decision_t_stack[i_dt_stack - 1][1:0] ][ `F_ENABLE ], 
																		~IPR[ decision_t_stack[i_dt_stack - 1][N_BITS-1:2] ][ decision_t_stack[i_dt_stack - 1][1:0] ][ `F_STACKED ], 
																		g_priority[ decision_t_stack[i_dt_stack - 1] ], 
																		sg_priority[ decision_t_stack[i_dt_stack - 1] ]
																	} <= 
																	{	~IPR[ (i_dt_stack + 1) >> 2 ][ (i_dt_stack + 1) & 2'b11 ][ `F_ENABLE ], 
																		~IPR[ (i_dt_stack + 1) >> 2 ][ (i_dt_stack + 1) & 2'b11 ][ `F_STACKED ], 
																		g_priority[ i_dt_stack + 1 ], 
																		sg_priority[ i_dt_stack + 1 ]
																	})	
																	?	decision_t_stack[i_dt_stack - 1]	:	i_dt_stack + 1;
		end
endgenerate


/***********************************************************************************************************************
/*********** Fim das �rvores de decis�o
/***********************************************************************************************************************/

reg reti_signal_last;						//Registo para verificar o posedge dos sinais de RETI.

//Output com endere�o da interrup��o sempre que existe um endere�o lido do vetor de interrup��es.
assign interrupt_address = vector_index ? new_interrupt_address : 0;


integer i_ipr_row, i_ipr_col;				//Variaveis de indexacao nos ciclos for para percorrer registo IPR.

always@(clock or interrupt_ack or reti_signal or reset)
begin
	//Sempre que � recebido um reset s�o restaurados os valores por defeito do NVIC.
	if ( reset )
	begin
		for (i_ipr_row = 0; i_ipr_row < ROW_NUMBER; i_ipr_row = i_ipr_row + 1)
		begin
			for (i_ipr_col = 0; i_ipr_col < COL_NUMBER; i_ipr_col = i_ipr_col + 1)
			begin
				IPR[i_ipr_row][i_ipr_col] = 8'h0;
			end
		end
		
		CIS[`RESERVED_CIS] = 0;
		CIS[`AB] = 0;
		CIS[`ETMINTNUM] = 0;
		CIS[`VECTPENDING] = 0;
		CIS[`VECTSTACKED] = 0;
		CIS[`CURRPRI] = 0;

		interrupt_pending <= 0;
		vector_index <= 0;

		reti_signal_last = 0;
		
///////////////////////////////////////////////////////////////////////		
/////////// TESTES ////////////////////////////////////////////////////		
		AIRCR = `PRIO_P32_S0;

		IPR[0][0][`F_ENABLE] = 1;
		IPR[0][0][4:0] = 2;

		IPR[0][1][`F_ENABLE] = 1;
		IPR[0][1][4:0] = 3;
	
		IPR[0][2][`F_ENABLE] = 1;
		IPR[0][2][4:0] = 1;

		IPR[0][3][`F_ENABLE] = 0;
		IPR[0][3][4:0] = 0;

		IPR[1][1][`F_ENABLE] = 1;
		IPR[1][1][4:0] = 4;
		
		STIR[`INTID] = 5;

		sw_interrupt_changed = 1;
///////////////////////////////////////////////////////////////////////

	end
	
	else
	begin
		//Sempre que existe um novo pedido de interrup��o por parte de um perif�rico, o sinal de pendente
		//	dessa interrup��o � assinalada.
		
		for (i_ipr_row = 0; i_ipr_row < ROW_NUMBER; i_ipr_row = i_ipr_row + 1)
		begin
			for (i_ipr_col = 0; i_ipr_col < COL_NUMBER; i_ipr_col = i_ipr_col + 1)
			begin
				IPR[i_ipr_row][i_ipr_col][`F_PENDING] = IPR[i_ipr_row][i_ipr_col][`F_PENDING]
												| interrupt_request[(i_ipr_row*4)+i_ipr_col]
												| (sw_interrupt_changed & STIR[`INTID] == (i_ipr_row*4)+i_ipr_col);
			end
		end

		//Se a interrup��o por sw foi atendida � limpo o registo que despoletou a interrup��o. 
		if ( sw_interrupt_changed )
			sw_interrupt_changed = 0;

		//As interrup��es pending e stacke de maior prioridade (obtidas pelas �rvores de decis�o) s�o
		//	guardadas no registo CIS. Os bits PB e SB indicam se estas s�o efetivamente enable e pending/stacked.
		CIS[`VECTPENDING] = decision_t_pend[30];
		CIS[`IPB] = IPR[decision_t_pend[30] / (N_BITS-1)][decision_t_pend[30] % (N_BITS-1)][`F_ENABLE] & IPR[decision_t_pend[30] / (N_BITS-1)][decision_t_pend[30] % (N_BITS-1)][`F_PENDING];
		CIS[`VECTSTACKED] = decision_t_stack[30];
		CIS[`ISB] = IPR[decision_t_stack[30] / (N_BITS-1)][decision_t_stack[30] % (N_BITS-1)][`F_ENABLE] & IPR[decision_t_stack[30] / (N_BITS-1)][decision_t_stack[30] % (N_BITS-1)][`F_STACKED];
		
		//Quando um sinal de RETI � recebido (interrup��o terminada) o sinal de ativo � limpo.
		if ( reti_signal & ~reti_signal_last )
		begin
			CIS[`AB] = 0;
		end
		
		reti_signal_last = reti_signal;
		
		//Se uma interrup��o terminar e uma stacked for a mais priorit�ria das que aguardam entrar em
		//	execu��o...
		if ( reti_signal & CIS[`ISB] & ( ~CIS[`IPB] | (CIS[`IPB] & ( g_priority[CIS[`VECTSTACKED]] <= g_priority[CIS[`VECTPENDING]]) ) ) )
		begin
			//Se a interrup��o foi recebida pelo CPU, esta � passada para ativa.
			if ( interrupt_ack & interrupt_pending )
			begin
				//Sinal de stacked da interrup��o que passou para ativa � limpo.
				for (i_ipr_row = 0; i_ipr_row < ROW_NUMBER; i_ipr_row = i_ipr_row + 1)
				begin
					for (i_ipr_col = 0; i_ipr_col < COL_NUMBER; i_ipr_col = i_ipr_col + 1)
					begin
						IPR[i_ipr_row][i_ipr_col][`F_STACKED] = IPR[i_ipr_row][i_ipr_col][`F_STACKED] & ~( CIS[`ETMINTNUM] == (i_ipr_row*4)+i_ipr_col );
					end
				end
				
				//Informa��o da interrup��o ativa (n�mero e prioridade) no registo CIS.				
				CIS[`AB] = 1;
				CIS[`ETMINTNUM] = CIS[`VECTSTACKED];
				CIS[`CURRPRI] = ( g_priority[CIS[`ETMINTNUM]] );
				
				interrupt_pending <= 0;
				vector_index <= 0;
			end

			//Se CPU ainda n�o recebeu pedido de nova interrup��o...
			else
			begin	
				//� feito pedido de interrup��o ao CPU, sem envio do endere�o da interrup��o, uma vez que
				//	RETI recupera o fluxo de execu��o no ponto onde a interrup��o passou para stacked.
				interrupt_pending <= 1;
				vector_index <= 0;
			end
		end
		
		//Se a interrup��o pending tem prioridade superior � ativa (se existente) e �s stacked (se
		//	existentes)...
		if ( CIS[`IPB] & ( ( CIS[`AB] & ( g_priority[CIS[`VECTPENDING]] < CIS[`CURRPRI] ) ) | ( ~CIS[`AB] & ( ~CIS[`ISB] | CIS[`ISB] & ( g_priority[CIS[`VECTPENDING]] < g_priority[CIS[`VECTSTACKED]] ) ) ) ) )
		begin
			//Se uma interrup��o pending foi recebida pelo CPU, esta � passada para ativa.
			if ( interrupt_ack & interrupt_pending )
			begin
				//Se uma interrup��o ativa � interrompida por outra pending de maior prioridade esta �
				//	passada para stacked.
				if ( CIS[`AB] )
				begin
					for (i_ipr_row = 0; i_ipr_row < ROW_NUMBER; i_ipr_row = i_ipr_row + 1)
					begin
						for (i_ipr_col = 0; i_ipr_col < COL_NUMBER; i_ipr_col = i_ipr_col + 1)
						begin
							IPR[i_ipr_row][i_ipr_col][`F_STACKED] = IPR[i_ipr_row][i_ipr_col][`F_STACKED] | ( CIS[`ETMINTNUM] == (i_ipr_row*4)+i_ipr_col );
						end
					end
				end
				
								
				//Informa��o da interrup��o ativa (n�mero e prioridade) no registo CIS.
				CIS[`AB] = 1;
				CIS[`ETMINTNUM] = CIS[`VECTPENDING];
				CIS[`CURRPRI] = ( g_priority[CIS[`ETMINTNUM]] );
				
				//Limpo o sinal de pending da interrup��o que passou para ativa.
				for (i_ipr_row = 0; i_ipr_row < ROW_NUMBER; i_ipr_row = i_ipr_row + 1)
				begin
					for (i_ipr_col = 0; i_ipr_col < COL_NUMBER; i_ipr_col = i_ipr_col + 1)
					begin
						IPR[i_ipr_row][i_ipr_col][`F_PENDING] = IPR[i_ipr_row][i_ipr_col][`F_PENDING] & ( CIS[`VECTPENDING] != ((i_ipr_row*4)+i_ipr_col) );
					end
				end
	
				interrupt_pending <= 0;
				vector_index <= 0;
			end

			//Se pedido ao CPU de interrup��o para ainda n�o foi efetuado...
			else
			begin
				//Indice do vetor de interrup��es atualizado com o n�mero da interrup��o que passou de
				// pending para ativa (deixado o indice zero para situa��o particular das interrup��es
				//	stacked).
				vector_index <= CIS[`VECTPENDING] + 1;
			
				//Se o endere�o da interrup��o foi lido do vetor de interrup��es � indicado a CPU que deve
				//	alterar o fluxo de execu��o para a nova interrup��o.
				if ( new_interrupt_address )
				begin
					interrupt_pending <= 1;
				end	
			end	
		end
	end
end
endmodule
