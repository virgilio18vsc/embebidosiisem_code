`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    21:14:06 01/14/2014 
// Design Name: 
// Module Name:    D_Blaze_buf2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_buf2(
    input clock,
	 input reset,
	 input [149:0] in,
	 output reg [149:0] out
	 );

always@(posedge clock)
begin
	if(reset)
		out <= 0;
	else
		out <= in;
end

endmodule
