`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    14:23:49 10/23/2013 
// Design Name: 
// Module Name:    D-Blaze_SoC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_SoC(
   //inputs
	input 			clock,
	input 			rst,
	input 	[7:0] in_pins,
	input		[4:0] buttons,
	//outputs
	output 	[7:0] out_pins
);

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [15:0] select;									// Select Peripheric
wire [26:0] peracc;									// Peripheric Access
wire [31:0] ctrl;										// Control word from control bus
wire reset;												// Reset signal for CPU
wire reti_signal;										// Foi feito um fetch de um RETI
wire interrupt_pending;								// Interrup��o � espera de ser atendida
wire [11:0] interrupt_address;					// Endere�o da ISR
wire [31:0] interrupt_request;					// Barramento de pedidos de interrup��es pelos perif�ricos

assign reset = !rst;									// Virtex 5 reset is always 1

//Wires n�o ligados agora... T�m que ser ligados aos perif�ricos
assign interrupt_request[31] = 0;
assign interrupt_request[30] = 0;
assign interrupt_request[29] = 0;
assign interrupt_request[28] = 0;
assign interrupt_request[27] = 0;
assign interrupt_request[26] = 0;
assign interrupt_request[25] = 0;
assign interrupt_request[24] = 0;
assign interrupt_request[23] = 0;
assign interrupt_request[22] = 0;
assign interrupt_request[21] = 0;
assign interrupt_request[20] = 0;
assign interrupt_request[19] = 0;
assign interrupt_request[18] = 0;
assign interrupt_request[17] = 0;
assign interrupt_request[16] = 1;
assign interrupt_request[15] = 1;
assign interrupt_request[14] = 0;
assign interrupt_request[13] = 0;
assign interrupt_request[12] = 0;
assign interrupt_request[11] = 0;
assign interrupt_request[10] = 0;
assign interrupt_request[9] = 0;
assign interrupt_request[8] = 0;


//////////////////////////////////////////////////////////////////////////////////
// CPU ///////////////////////////////////////////////////////////////////////////
D_Blaze_CPU CPU(
	//inputs
	.clock(clock),
	.reset(reset),
	.in_pins(in_pins),
	.interrupt_pending(interrupt_pending),
	.interrupt_address(interrupt_address),
	//outputs
	.peracc(peracc),
	.reti_signal(reti_signal),
	.interrupt_ack(interrupt_ack)
);

//////////////////////////////////////////////////////////////////////////////////
// Control bus ///////////////////////////////////////////////////////////////////
D_Blaze_ctrl_enc control_enc(
    //inputs
	 .clock(clock), 
	 .reset(reset),
	 .peracc(peracc),
	 //outputs	 
	 .ctrl(ctrl), 
	 .select(select)
);

//////////////////////////////////////////////////////////////////////////////////
// Pario /////////////////////////////////////////////////////////////////////////
D_Blaze_pario pario(
   //inputs
	.clock(clock), 
	.reset(reset),
	.select(select[0]),
	.ctrl(ctrl),
	//outputs
	.out_pins(out_pins)
);

//////////////////////////////////////////////////////////////////////////////////
// Timers ////////////////////////////////////////////////////////////////////////
D_Blaze_timer timer(
	//inputs
	.clock(clock), 
	.reset(reset),
	.select(select[1]),
	//outputs
	.ssr0(interrupt_request[0]),
	.ssr1(interrupt_request[1]),
	.ssr2(interrupt_request[2]),
	.ssr3(interrupt_request[3]),
	.ssr4(interrupt_request[4]),
	.ssr5(interrupt_request[5]),
	.ssr6(interrupt_request[6]),
	.ssr7(interrupt_request[7])
);
	 
//////////////////////////////////////////////////////////////////////////////////
// DVIC //////////////////////////////////////////////////////////////////////////
D_Blaze_DVIC DVIC(
	//inputs
	.reset(reset),
	.clock(clock), 
	.ctrl(ctrl),
	.select(select[2]),
	.reti_signal(reti_signal),
	.interrupt_request(interrupt_request),
	.interrupt_ack(interrupt_ack),
	//outputs
	.interrupt_address(interrupt_address),
	.interrupt_pending(interrupt_pending)
);


endmodule
