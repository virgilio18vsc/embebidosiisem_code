`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    14:21:36 10/30/2013 
// Design Name: 
// Module Name:    D_Blaze_alu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_alu(
	input 			[31:0] In0,
	input 			[31:0] In1,
	input 			[2:0]  Op,
	output 			[31:0] Out
   );

assign Out =	(Op == `ALU_ADD)			?	In0 + In1:					//ADD
					(Op == `ALU_SUB)			?	In0 - In1:					//SUB		
					(Op == `ALU_LOGIC_OR)	?	In0 | In1:					//OR
					(Op == `ALU_LOGIC_AND)	?	In0 & In1:					//AND
					(Op == `ALU_LOGIC_XOR)	?	In0 ^ In1:					//XOR     	
					(Op == `ALU_LOGIC_NOT)	?	~(In0):						//NOT
													   32'b0;

endmodule

