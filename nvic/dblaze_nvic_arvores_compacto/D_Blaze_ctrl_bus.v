`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    10:59:31 03/11/2014 
// Design Name: 
// Module Name:    D_Blaze_ctrl_bus 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_ctrl_enc(
    //inputs
	 input 			clock,
	 input 			reset,
	 input [26:0]	peracc,
	 //outputs
	 output [31:0] ctrl,
	 output [15:0] select
);
 
//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [3:0] peripheric;									// Peripheric 
wire 		  peripheric_access;							// Access to a peripheric 
//////////////////////////////////////////////////////////////////////////////////
assign peripheric_access = peracc[26];
assign peripheric = peracc[25:22];

assign select[0] = (peripheric_access & peripheric == 0);		
assign select[1] = (peripheric_access & peripheric == 1);
assign select[2] = (peripheric_access & peripheric == 2);
assign select[3] = (peripheric_access & peripheric == 3);
assign select[4] = (peripheric_access & peripheric == 4);
assign select[5] = (peripheric_access & peripheric == 5);
assign select[6] = (peripheric_access & peripheric == 6);
assign select[7] = (peripheric_access & peripheric == 7);
assign select[8] = (peripheric_access & peripheric == 8);		
assign select[9] = (peripheric_access & peripheric == 9);
assign select[10] = (peripheric_access & peripheric == 10);
assign select[11] = (peripheric_access & peripheric == 11);
assign select[12] = (peripheric_access & peripheric == 12);
assign select[13] = (peripheric_access & peripheric == 13);
assign select[14] = (peripheric_access & peripheric == 14);
assign select[15] = (peripheric_access & peripheric == 15);

assign ctrl={6'b0, peracc[21:19] , peracc[18:16], peracc[15:0]};

endmodule

module D_Blaze_ctrl_dec(
    //inputs
	 input [31:0]	ctrl,
	 input			select,
	 //outputs
	 output [2:0] 	command,
	 output [2:0] 	type,
	 output [15:0] dec_output
);

assign command = ctrl[21:19];
assign type = ctrl[18:16];
assign dec_output = ctrl[15:0];


endmodule

