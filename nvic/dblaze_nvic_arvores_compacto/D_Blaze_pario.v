`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    11:14:39 04/02/2014 
// Design Name: 
// Module Name:    D_Blaze_pario 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_pario(
    //inputs
	 input clock,
	 input reset,
	 input select,
	 input [31:0]ctrl,
	 //outputs
	 output reg [7:0]out_pins
	 );

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [2:0] command;
wire [2:0] type;
wire [15:0] dec_output;
wire [7:0]out_pins_aux;

//////////////////////////////////////////////////////////////////////////////////
D_Blaze_ctrl_dec decoder(
   //inputs
	.ctrl(ctrl), 
	.select(select),
	//outputs 
	.command(command),
	.type(type),
	.dec_output(dec_output)
);


assign out_pins_aux =	(reset)															? 8'b0:
								(select & command == 3'd0 & type == 3'd0)				? dec_output[7:0]:
																									  out_pins;

always @(posedge clock)
begin
	if(reset)
		out_pins <= 8'b0;
	else
		out_pins <= out_pins_aux;
end

endmodule
