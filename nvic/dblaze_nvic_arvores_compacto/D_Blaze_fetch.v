`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins 
// 
// Create Date:    13:59:13 10/27/2013 
// Design Name: 
// Module Name:    D_Blaze_fetch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_fetch(
   //Inputs
	input 			clock,
	input 			reset,
	input	[1:0]		jump_signal,
	input [31:0]  	jump_value,
	input [11:0]	pc_restore,
	input 			interrupt_pending,
	input [11:0]	interrupt_address,
	input	[2:0]		interrupt_delay,
	input				bubble,
	//Outputs
	output [31:0]	instruction,
	output [`INST_MEM-1:0] pc_value,
	output			interrupt_ack
);

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
reg  [`INST_MEM-1:0] PC;				// Program counter
wire [31:0] IR;							// Fetched instruction

//////////////////////////////////////////////////////////////////////////////////
// Instruction memory ////////////////////////////////////////////////////////////
assign instruction = IR;
assign pc_value = PC;

//Indica que a interrup��o foi recebida pelo CPU. 
assign interrupt_ack = ((interrupt_pending != 0) & ( (PC == interrupt_address) | (interrupt_address == 0) ) );
	
Imem inst_mem(
	.clka(clock),
	.rsta(reset),
	.ena(1'b1),
	.addra(PC),
	.douta(IR)
);
	 
//////////////////////////////////////////////////////////////////////////////////
// PC update /////////////////////////////////////////////////////////////////////
always@(posedge clock)
	if(reset) 
		begin																		// Reset condition
			PC <= 32'b0;	
		end
	else if(jump_signal == 2'b01 | jump_signal == 2'b11 ) 
	begin																			// BR, BEQ, BNE, BGE, BGT, BLE, BLT 
		PC <= ((PC + jump_value) - 2'b10);								// BEQI, BNEI, BGEI, BGTI, BLEI, BLTI
	end
	else if(jump_signal == 2'b10) 
	begin																			// BRA, BRAI, CALL
		PC <= jump_value;
	end
	else if(pc_restore != 0) 
	begin																			// RET, RETI
		PC <= pc_restore;
	end
	else if((interrupt_pending != 0) & ( interrupt_address != 0 ) & ( interrupt_delay <= 1 )) 
	begin																			// Nova interrup��o pending recebida e possivel de ser ativa.
		PC <= interrupt_address;
	end
	else 
	begin																			// Normal execution
		PC <= PC + 1'b1;			
	end
endmodule
