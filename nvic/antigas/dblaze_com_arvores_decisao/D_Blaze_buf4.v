`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    21:18:46 01/14/2014 
// Design Name: 
// Module Name:    D_Blaze_buf4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_buf4(
    input clock,
	 input reset,
	 input [52:0] in,
	 output reg [52:0] out
	 );

always@(posedge clock)
begin
	if(reset)
		out <= 0;
	else
		out <= in;
end

endmodule
