`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins 
// 
// Create Date:    13:59:44 10/27/2013 
// Design Name: 
// Module Name:    D_Blaze_execute 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_execute(
	//Inputs
	input  						clock,
	input  						reset,
	input [5:0]					opcode,
	input [`REG_SIZE-1:0]	RF_out_d,
	input [`REG_SIZE-1:0]	RF_out_a,
	input [`REG_SIZE-1:0]	RF_out_b,	
	input [31:0]				Imm,
	input							barrel_dir,
	input [9:0]					Forward_ctrl,
	input [4:0]					Rd,
	//Outputs
	output [31:0]				execute_output,
	output [31:0]				forward_execute_output,
	output						load_signal,
	output [31:0]  			data_forward_exec,
	output [1:0]				CMP_output,
	output						ret_signal
 	);	

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [31:0] input0;										// ALU input0
wire [31:0] input1;										// ALU input1
wire [2:0]  operation;									// ALU operation
wire [31:0]	ALU_output;									//	Output from ALU

wire [31:0] input2;										// CMP input2
wire [31:0] input3;										// CMP input3

wire [15:0] multiplier;									// MUL input
wire [15:0] multiplicand;								// MUL input
wire [31:0]	MUL_output;									//	Output from MUL

wire [15:0] dividend;									// DIV input
wire [15:0] divider;										// DIV input
wire [31:0]	DIV_output;									//	Output from DIV

wire        start;
wire [15:0] quotient;
wire [15:0] remainder;
wire  		ready;

wire [31:0] barrel_input;								// Barrel Shifter input
wire [4:0]  barrel_rot;									// Barrel Shifter rotation
wire [31:0] barrel_shifter_out;						// Barrel Shifter data output
//////////////////////////////////////////////////////////////////////////////////
// Load auxiliary ////////////////////////////////////////////////////////////////
assign load_signal = (opcode == `LW | opcode == `LWI | opcode == `LB | opcode == `LWBI) 					? 1'b1 : 1'b0;

assign ret_signal = (opcode == `RET | opcode == `RETI | opcode == `POP) 										? 1'b1 : 1'b0;

assign forward_execute_output = (opcode == `LW | opcode == `LWI | opcode == `LB | opcode == `LWBI) 	? execute_output:
										  (opcode == `RET | opcode == `RETI)												? execute_output:
										  (opcode == `POP)																		? execute_output:
																																		  32'b0;
																
//////////////////////////////////////////////////////////////////////////////////
// ALU inputs ////////////////////////////////////////////////////////////////////
assign input0 = (opcode == `BR | opcode == `BEQ | opcode == `BS | opcode == `BRI)												? 32'b0:	
					 (opcode == `MOV | opcode == `MOVI)																							? 32'b0:
					 (opcode == `NOT)																													? RF_out_d:
					 (opcode == `CMP | opcode == `MUL)																							? 32'b0:
																																							  RF_out_a;
											  
assign input1 = (opcode == `BR | opcode == `BEQ | opcode == `BS | opcode == `BRI)												? 32'b0:
					 (opcode == `NOT)																													? 32'b0:
					 (opcode == `ADDI | opcode == `SUBI | opcode == `ANDI | opcode == `ORI | opcode == `XORI)					? Imm:
					 (opcode == `SWI | opcode == `LWI | opcode == `LWBI | opcode == `SWBI)											? Imm:
					 (opcode == `MOV)																													? RF_out_a:
					 (opcode == `MOVI)																												? Imm:
					 (opcode == `CMP | opcode == `MUL)																							? 32'b0:
					 (opcode == `INPUT)																												? 32'b0:
																																							  RF_out_b;

assign operation = (opcode == `ADD | opcode == `ADDI)							? `ALU_ADD:
						 (opcode == `SW | opcode == `LW)								? `ALU_ADD:
						 (opcode == `LWBI | opcode == `SWBI)						? `ALU_SUB:
						 (opcode == `SB | opcode == `LB)								? `ALU_ADD:
						 (opcode == `INPUT)												? `ALU_ADD:
						 (opcode == `SWI | opcode == `LWI)							? `ALU_ADD:
						 (opcode == `MOVI | opcode == `MOV)							? `ALU_ADD:
						 (opcode == `CALL | opcode == `RET | opcode == `RETI)	? `ALU_ADD:	
						 (opcode == `PUSH | opcode == `POP)							? `ALU_ADD:	
						 (opcode == `SUB | opcode == `SUBI)							? `ALU_SUB:		
						 (opcode == `OR | opcode == `ORI)							? `ALU_LOGIC_OR:		
						 (opcode == `AND | opcode == `ANDI)							? `ALU_LOGIC_AND:		
						 (opcode == `XOR | opcode == `XORI)							? `ALU_LOGIC_XOR:		
						 (opcode == `NOT)													? `ALU_LOGIC_NOT:										 
																								  `ALU_NOP;
																								  
//////////////////////////////////////////////////////////////////////////////////
// Hazards /////////////////////////////////////////////////////////////////////// 
assign data_forward_exec = ((Forward_ctrl[5:4] == 2'b01) | (Forward_ctrl[3:2] == 2'b01) | (Forward_ctrl[1:0] == 2'b01)) ? execute_output: 
																																															32'b0;

//////////////////////////////////////////////////////////////////////////////////
// ALU ///////////////////////////////////////////////////////////////////////////
D_Blaze_alu ALU(
   .In0(input0),
	.In1(input1),
	.Op(operation),
	.Out(ALU_output));

//////////////////////////////////////////////////////////////////////////////////
// Multiplier inputs /////////////////////////////////////////////////////////////
assign multiplier = (opcode == `MUL)		? RF_out_a[15:0]:
														  32'b0;
																	  
assign multiplicand = (opcode == `MUL)		? RF_out_b[15:0]:
														  32'b0;

//////////////////////////////////////////////////////////////////////////////////
// Multiplier ////////////////////////////////////////////////////////////////////
D_Blaze_multiplier MUL(
   .multiplier(multiplier),
	.multiplicand(multiplicand),
	.product(MUL_output)
);	

//////////////////////////////////////////////////////////////////////////////////
// Multiplier inputs /////////////////////////////////////////////////////////////
assign dividend = (opcode == `DIV)		? RF_out_a[15:0]:
													  32'b0;
																	  
assign divider = (opcode == `DIV)		? RF_out_b[15:0]:
													  32'b0;

assign start = (reset) ? 1'b0: 
					(opcode == `DIV) ? 1'b1:
					1'b0;
//////////////////////////////////////////////////////////////////////////////////
// Divider ////////////////////////////////////////////////////////////////////
/*D_Blaze_divider DIV(
   .clock(clock),
	.start(start),
	.dividend(dividend),
	.divider(divider),
	.quotient(quotient),
	.remainder(remainder),
	.ready(ready)
);	*/

//////////////////////////////////////////////////////////////////////////////////
// Comparator inputs /////////////////////////////////////////////////////////////
assign input2 = (opcode == `CMP)		? RF_out_a:
												  32'b0;
																	  
assign input3 = (opcode == `CMP)		? RF_out_b:
												  32'b0;

//////////////////////////////////////////////////////////////////////////////////
// Comparator ////////////////////////////////////////////////////////////////////
D_Blaze_comparator CMP(
   .In2(input2),
	.In3(input3),
	.Out(CMP_output)
);	
	
//////////////////////////////////////////////////////////////////////////////////
// Barrel shifter inputs /////////////////////////////////////////////////////////
assign barrel_input = (opcode == `BS ) 						? RF_out_a:
																			  32'b0;

assign barrel_rot = (opcode == `BS) 							? RF_out_b[4:0]:
																			  5'b0;
													 
//////////////////////////////////////////////////////////////////////////////////
// Barrel shifter ////////////////////////////////////////////////////////////////
D_Blaze_barrel_shifter shifter(
	.in(barrel_input),
	.rot(barrel_rot),
	.dir(barrel_dir),
	.out(barrel_shifter_out)
);
//////////////////////////////////////////////////////////////////////////////////
assign execute_output = (opcode == `BS)						? barrel_shifter_out:
								(opcode == `CMP)						? CMP_output:
								(opcode == `MUL)						? MUL_output:
								//(opcode == `DIV)						? DIV_output:
																			  ALU_output;

endmodule
