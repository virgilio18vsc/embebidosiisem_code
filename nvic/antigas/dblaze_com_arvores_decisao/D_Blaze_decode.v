`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins
// 
// Create Date:    15:18:14 01/07/2014 
// Design Name: 
// Module Name:    D_Blaze_decode 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_decode(
   //Inputs
	input  						clock,
	input  						reset,
   input  [31:0]				instruction,
	input  [`REG_SIZE-1:0]	R_data,			
   input  [4:0]				R_addr,
	input  [9:0]				Forward_ctrl,
	input  [31:0] 				data_forward_exec,
	input  [31:0]				data_forward_mem,
	input							load_signal,
	input  [`INST_MEM-1:0] 	pc_value,
	input  [7:0]				in_pins,
	//Outputs
	output [5:0]				opcode,
	output [`REG_SIZE-1:0]	RF_out_a,
	output [`REG_SIZE-1:0]	RF_out_b,
	output [`REG_SIZE-1:0]	RF_out_d,
	output [31:0]				Imm,
	output [4:0]				Rd,
	output [4:0]				Ra,
	output [4:0]				Rb,
	output [1:0]				jump_signal,
	output [31:0]  			jump_value,
	output						barrel_dir,
	output [26:0]				peracc,
	output						reti_signal,
	output [2:0]				interrupt_delay,
	output						bubble
	);


	
//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [31:0]				RF_dina;										// Register-File data_in
wire [4:0]				RF_address;									// Register-File read/write address
wire						RF_we;										// Register-File write enable

reg  [2:0]				ignore;										// Auxiliary variable for branches/jumps
reg  [15:0]				imm_temp;									// Auxiliary varibale for IMM instructions 
reg						imm_signal;									// Auxiliary varibale for IMM instructions

wire [`REG_SIZE-1:0] RF_d;											// Register-File - Aux Register D
wire [`REG_SIZE-1:0] RF_a;											// Register-File - Aux Register A
wire [`REG_SIZE-1:0] RF_b;											// Register-File - Aux Register B
wire jump_side;														// For BRI backwards jumps

wire [11:0]				Stack_value;								// Stack Pointer value
wire 						Stack_update;								// Stack Pointer signal for updating

wire [5:0] 				opcode_aux;									// Help with hazrds
reg 						executed_imm_inst;

//////////////////////////////////////////////////////////////////////////////////
// Hazards ///////////////////////////////////////////////////////////////////////	
assign bubble = (Forward_ctrl[7:6] == 2'b01 | Forward_ctrl[7:6] == 2'b11) ? 1'b1 : 1'b0;
assign opcode = (bubble) ? 6'b0 : opcode_aux;	

//////////////////////////////////////////////////////////////////////////////////
// Decoding //////////////////////////////////////////////////////////////////////
assign opcode_aux = (ignore) 				? 6'b0:
												     instruction[31:26];
	
assign Rd	=	(ignore) 					? 5'b0:
													  instruction[25:21];

assign Ra	=	(ignore) 																													? 5'b0:
					(opcode == `CALL | opcode == `RET | opcode == `RETI | opcode == `PUSH | opcode == `POP)			? `STACK_POINTER:
																																						instruction[20:16];
												
assign Rb	=	(ignore) 					? 5'b0:
													  instruction[15:11];
		
assign Imm =	((opcode == `ADDI | opcode == `SUBI | opcode == `ANDI | opcode == `ORI | opcode == `XORI | opcode == `BRI | opcode == `MOVI) & !imm_signal)		? {16'b0,instruction[15:0]}:		// Not preceded by an IMM
					((opcode == `LWI | opcode == `SWI | opcode == `LWBI | opcode == `SWBI) & !imm_signal)																				? {16'b0,instruction[15:0]}:		// Not preceded by an IMM
					((opcode == `ADDI | opcode == `SUBI | opcode == `ANDI | opcode == `ORI | opcode == `XORI | opcode == `BRI | opcode == `MOVI) & imm_signal)		? {imm_temp,instruction[15:0]}:	// Preceded by an IMM
					((opcode == `LWI | opcode == `SWI | opcode == `LWBI | opcode == `SWBI) & imm_signal)																				? {imm_temp,instruction[15:0]}:	// Preceded by an IMM
																																																					  32'b0;
assign barrel_dir = ((opcode == `BS) & (instruction[10] == 1'b1)) ? 1'b1 : 1'b0;	// 0 - Right  1 - Left	
	
//////////////////////////////////////////////////////////////////////////////////																		  
/// Branches /////////////////////////////////////////////////////////////////////
assign jump_signal = ((opcode == `BR | opcode == `BRI) & Ra == 5'b00000)														? 2'b01:	// BR, BRI
							((opcode == `BR | opcode == `BRI) & Ra == 5'b01000)														? 2'b10:	// BRA, BRAI
							((opcode == `BEQ) & (Rd == 5'b00000) & (RF_out_a == 2'b00)) 											? 2'b11: // BEQ
							((opcode == `BEQ) & (Rd == 5'b00001) & ((RF_out_a == 2'b10) | (RF_out_a == 2'b01))) 			? 2'b11: // BNE
							((opcode == `BEQ) & (Rd == 5'b00101) & ((RF_out_a == 2'b10) | (RF_out_a == 2'b00))) 			? 2'b11: // BGE
							((opcode == `BEQ) & (Rd == 5'b00100) & (RF_out_a == 2'b10))  											? 2'b11: // BGT
							((opcode == `BEQ) & (Rd == 5'b00011) & ((RF_out_a == 2'b00) | (RF_out_a == 2'b01))) 			? 2'b11: // BLE
							((opcode == `BEQ) & (Rd == 5'b00010) & (RF_out_a == 2'b01))  											? 2'b11: // BLT
							(Forward_ctrl[7:6] == 2'b01 | Forward_ctrl[7:6] == 2'b11) 												? 2'b01:	//	Stall because of hazards involving loads
							(opcode == `CALL)																										? 2'b10: // Call's
							((Forward_ctrl[9:8] == 2'b01 | Forward_ctrl[9:8] == 2'b10)) 											? 2'b01: // Stack Pointer hazards
																																						  2'b00;

assign jump_value = (opcode == `BR | opcode == `BEQ)		? RF_out_b: 						// BR, BRA, BEQ, BNE, BGE, BGT, BLE, BLT
						  (opcode == `BRI & !jump_side)			? Imm:								// BRI, BRAI
						  (opcode == `BRI & jump_side)			? (~Imm + 1'b1):					// BRI, BRAI backwards jumps
						  (opcode == `CALL)							? {6'b0, instruction[25:0]}: 	// Call's
						  (Forward_ctrl[9:8]) 						? 32'b1:						  
																			  32'b0;

assign jump_side = ((opcode == `BRI) & (Rd[4] == 1'b1)) ? 1'b1 : 1'b0;													// Auxiliary for jumps backwards
									  
//////////////////////////////////////////////////////////////////////////////////																		  
/// Register-File variables //////////////////////////////////////////////////////
assign RF_dina = R_data;

assign RF_address = R_addr; 
																	
assign RF_we = 1'b1;


assign RF_out_d = (Forward_ctrl[5:4] == 2'b01)													? data_forward_exec:
						(Forward_ctrl[5:4] == 2'b10) 													? data_forward_mem:
						(Forward_ctrl[5:4] == 2'b11) 													? RF_dina: 	
						(opcode == `CALL)																	? {20'b0, pc_value}:
						(opcode == `RET | opcode == `RETI)											? 32'b0:	
																												  RF_d;
														  
assign RF_out_a = (Forward_ctrl[3:2] == 2'b01)													? data_forward_exec:
						(Forward_ctrl[3:2] == 2'b10) 													? data_forward_mem:
						(Forward_ctrl[3:2] == 2'b11) 													? RF_dina:
						(opcode == `POP)																	? (RF_a + 1'b1):
						(opcode == `RET | opcode == `RETI)											? (RF_a + 1'b1):
						(opcode == `INPUT)																? {25'b0, in_pins}:						
																												  RF_a;

assign RF_out_b = (Forward_ctrl[1:0] == 2'b01)													? data_forward_exec:
						(Forward_ctrl[1:0] == 2'b10) 													? data_forward_mem:
						(Forward_ctrl[1:0] == 2'b11) 													? RF_dina:
						(opcode == `CALL | opcode == `PUSH)											? 32'b0:
						(opcode == `RET | opcode == `RETI | opcode == `POP)					? 32'b0:
																												  RF_b;
//////////////////////////////////////////////////////////////////////////////////
///// Stack Pointer //////////////////////////////////////////////////////////////
assign Stack_value = 	(reset)																														? 12'hFFF:
								(opcode == `CALL | opcode == `PUSH)																					? (RF_a - 1'b1):
								(opcode == `RET | opcode == `RETI | opcode == `POP)															? (RF_a + 1'b1):
																																									Stack_value;
																					  
assign Stack_update = 	(opcode == `CALL | opcode == `RET | opcode == `RETI | opcode == `PUSH | opcode == `POP)			? 1'b1:
																																									1'b0;

//////////////////////////////////////////////////////////////////////////////////
///// Peripherals ////////////////////////////////////////////////////////////////
assign peracc = (opcode == `PERACC)	? {1'b1, instruction[25:0]}: 27'b0;

//////////////////////////////////////////////////////////////////////////////////
///// NVIC ///////////////////////////////////////////////////////////////////////
assign reti_signal = (opcode == `RETI);			//Sinaliza que foi feito o decode de um RETI.
assign interrupt_delay = ignore;						//Instru��o ignorada, logo interrup��o n�o pode ser iniciada.
																																  
//////////////////////////////////////////////////////////////////////////////////
///// Register-File //////////////////////////////////////////////////////////////
D_Blaze_register_file REGISTER_FILE(
    .clock(clock),
	 .reset(reset),
	 .wr_en(RF_we),			
	 .data_ina(RF_dina),
	 .addra(Ra),
	 .addrb(Rb),
	 .addrd(Rd),
	 .data_in_stack(Stack_value),
	 .stack_signal(Stack_update),
	 .addr_write(RF_address),
	 .data_outa(RF_a),
	 .data_outb(RF_b),
	 .data_outd(RF_d)
);	


//////////////////////////////////////////////////////////////////////////////////
// Branches auiliary /////////////////////////////////////////////////////////////
always@(posedge clock)
begin	
	if(reset) 
		begin																		// Reset 
			ignore <= 3'b000;
		end
	else if (jump_signal)
		begin																		// Ignore instructions if there is a jump
			ignore <= 3'b010;
		end
	else if (opcode == `RET | opcode == `RETI)
		begin																		// Ignora instru��es se ocorrer um RET ou RETI
			ignore <= 3'b100;
		end
	else if(ignore != 0)
		begin																																
			ignore <= ignore - 1'b1;			
		end
end
//////////////////////////////////////////////////////////////////////////////////
// IMM auiliary //////////////////////////////////////////////////////////////////		
always@(clock)
begin
	if(reset) 
		begin																		// Reset 
			imm_temp <= 16'b0;
			imm_signal <= 1'b0;
			executed_imm_inst <= 1'b0;
		end
	else if(opcode == `IMM)		
		begin																		// IMM instruction 
			imm_temp <= instruction[15:0];
			imm_signal <= 1'b1;
		end	
	else if(executed_imm_inst | (opcode != `ADDI & opcode != `SUBI & opcode != `ANDI & opcode != `ORI & opcode != `XORI & opcode != `BRI & opcode != `SWI & opcode != `LWI & opcode != `LWBI & opcode != `SWBI & opcode != `MOVI))	
		begin																		// Not a Immediate instruction
			imm_temp <= 16'b0;
			imm_signal <= 1'b0;
			executed_imm_inst <= 1'b0;
		end
	else if(opcode == `ADDI | opcode == `SUBI | opcode == `ANDI | opcode == `ORI | opcode == `XORI | opcode == `BRI | opcode == `SWI | opcode == `LWI | opcode == `LWBI | opcode == `SWBI | opcode == `MOVI)		
		begin																		// IMM instruction executed
			executed_imm_inst <= 1'b1;
		end
end

endmodule

