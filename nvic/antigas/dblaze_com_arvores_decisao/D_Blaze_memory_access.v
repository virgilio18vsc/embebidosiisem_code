`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins 
// 
// Create Date:    13:59:57 10/27/2013 
// Design Name: 
// Module Name:    D_Blaze_memory_access 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_memory_access(
	//Inputs
	input 						clock,
	input 						reset,
	input	 [5:0]				opcode,
	input	 [31:0]  			execute_output,
	input  [4:0]				Rd,
	input  [`REG_SIZE-1:0]	RF_out_d,
	input	 [31:0]  			forward_execute_output,
	input 						load_signal,
	input  [9:0]				Forward_ctrl,
	input 						ret_signal,
	input  [1:0]				CMP_output,
	//Outputs
	output [`REG_SIZE-1:0]	RF_data,							// Register file data input
	output [4:0]				RF_addr,							//	Register file write address
	output [31:0]				data_forward_mem,
	output [11:0]				pc_restore
   );
//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire  [3:0]  			wea_data_mem;						// Write enable for data memory
wire [`DATA_MEM-1:0] addr_data_mem;						// Write/Load address for data memory
wire [31:0] 			dina_data_in;						// Data input for data memory
wire [31:0] 			data_mem_out;						// Data output from data memory

//////////////////////////////////////////////////////////////////////////////////
// Register-File values //////////////////////////////////////////////////////////
assign RF_addr = 	(opcode == `ADD| opcode == `SUB | opcode == `CMP | opcode == `MUL | opcode == `DIV | opcode == `OR | opcode == `AND | opcode == `XOR | opcode == `NOT)			? Rd:
						(opcode == `ADDI | opcode == `SUBI | opcode == `ANDI | opcode == `ORI | opcode == `XORI)																								? Rd:
						(opcode == `LW | opcode == `LWI | opcode == `LB | opcode == `LWBI)																															? Rd:	
						(opcode == `BS)																																																? Rd:
						(opcode == `MOV)																																																? Rd:	
						(opcode == `MOVI)																																																? Rd:
						(opcode == `POP)																																																? Rd:
						(opcode == `INPUT)																																															? Rd:
																																																											  32'b0;
																																																											  
assign RF_data = 	(opcode == `ADD | opcode == `SUB | opcode == `MUL | opcode == `DIV | opcode == `OR | opcode == `AND | opcode == `XOR | opcode == `NOT)									? execute_output:
						(opcode == `ADDI | opcode == `SUBI | opcode == `ANDI | opcode == `ORI | opcode == `XORI)																								? execute_output:
						(opcode == `MOV)																																																? execute_output:	
						(opcode == `MOVI)																																																? execute_output:
						(opcode == `LW | opcode == `LWI | opcode == `LWBI)																																					? data_mem_out:
						(opcode == `LB)																																																? {23'b0, data_mem_out[7:0]}:
						(opcode == `BS)																																																? execute_output:
						(opcode == `CMP)																																																? {30'b0, CMP_output}:
						(opcode == `POP)																																																? data_mem_out:
						(opcode == `INPUT)																																															? execute_output:						
																																																											  32'b0;

//////////////////////////////////////////////////////////////////////////////////
// Hazards /////////////////////////////////////////////////////////////////////// 
assign data_forward_mem = (Forward_ctrl[5:4] == 2'b10 | Forward_ctrl[3:2] == 2'b10 | Forward_ctrl[1:0] == 2'b10) & (Forward_ctrl[7:6] != 2'b10) 	? RF_data:
								  (Forward_ctrl[7:6] == 2'b10)																									 						? RF_data:
								  (Forward_ctrl[7:6] == 2'b01 & Forward_ctrl[5:4] == 2'b01)																						? RF_data:
																																																	  32'b0;
																																																											  
//////////////////////////////////////////////////////////////////////////////////
// Data memory variables /////////////////////////////////////////////////////////																				 
assign   wea_data_mem = 	(opcode == `SW | opcode == `SWI | opcode == `SB | opcode == `SWBI)		? 4'b1111:
									(opcode == `CALL | opcode == `PUSH)													? 4'b1111:
																																	  4'b0;

assign	addr_data_mem =	(opcode == `SW | opcode == `SWI | opcode == `SB | opcode == `SWI)			? execute_output[11:0]:
									(load_signal)																				? forward_execute_output[11:0]:
									(opcode == `CALL | opcode == `PUSH)													? execute_output[11:0]:
									(ret_signal)																				? (forward_execute_output):
																																	  5'b0;

assign 	dina_data_in =		(opcode == `SW | opcode == `SWI | opcode == `SWI)								? RF_out_d:
									(opcode == `SB)																			? {23'b0, RF_out_d[7:0]}:
									(opcode == `CALL | opcode == `PUSH)													? RF_out_d:
																																	  32'b0;

assign   pc_restore = 		(opcode == `RET | opcode == `RETI) 													? data_mem_out[11:0]: 
																																	  12'b0;
//////////////////////////////////////////////////////////////////////////////////
// Data memory ///////////////////////////////////////////////////////////////////

  Dmem data_mem(
    .clka(clock),
	 .rsta(reset),
	 .wea(wea_data_mem),
	 .addra(addr_data_mem),
	 .dina(dina_data_in),
	 .douta(data_mem_out));

endmodule
