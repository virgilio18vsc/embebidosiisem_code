//`define SIMULATION 
`define INST_MEM	12				// Number of bits for addressing instruction memory
`define DATA_MEM	12				// Number of bits for addressing data memory
`define REG_SIZE	32				// 32-bit registers
//////////////////////////////////////////////////////////////////////////////////
///// Opcodes ///////////////////////////////////////////////////////////////////
`define NOP			6'b000000

`define ADD			6'b000001
`define ADDI		6'b001000

`define SUB			6'b000010
`define SUBI		6'b001001

`define CMP			6'b000101

`define MUL			6'b000011

`define DIV			6'b000100

`define BS			6'b010001

`define PUSH		6'b010010
`define POP			6'b010011
`define CALL		6'b010100
`define RET			6'b010101
`define RETI		6'b010110


`define MOV			6'b100100
`define MOVI		6'b101011

`define IMM 		6'b101100

`define OR			6'b100000
`define ORI			6'b101000
`define AND			6'b100001
`define ANDI		6'b101001
`define XOR			6'b100010
`define XORI		6'b101010
`define NOT			6'b100011

`define BR			6'b100110	//BR, BRA
`define BRI			6'b101110	//BRI, BRA
`define BEQ			6'b100111	//BEQ, BNE, BGE, BGT, BLE, BLT

`define LB			6'b110000
`define LW			6'b110010
`define LWI			6'b111010
`define LWBI		6'b111011

`define SB			6'b110100
`define SW			6'b110110
`define SWI			6'b111110
`define SWBI		6'b111101

`define PERACC		6'b011111

`define INPUT		6'b111111	
//////////////////////////////////////////////////////////////////////////////////
// ALU Functions /////////////////////////////////////////////////////////////////
`define ALU_NOP				3'd0
`define ALU_ADD				3'd1
`define ALU_SUB				3'd2
`define ALU_LOGIC_OR			3'd3
`define ALU_LOGIC_AND		3'd4
`define ALU_LOGIC_XOR		3'd5
`define ALU_LOGIC_NOT		3'd6


//////////////////////////////////////////////////////////////////////////////////
// Registers /////////////////////////////////////////////////////////////////////
`define STACK_POINTER		5'd31

