`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins 
// 
// Create Date:    20:04:23 03/06/2014 
// Design Name: 
// Module Name:    D_Blaze_comparator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_comparator(
    input [31:0] 	In2,
	 input [31:0] 	In3,
	 output [1:0]	Out
);

assign Out = 			((In2 > In3) & (In2[31] == 1'b0))		? 2'b10:	// In2 > In3
							(In2 == In3) 									? 2'b00:	// In2 = In3
							(In2 < In3)										? 2'b01: // In2 < In3
							((In2 > In3) & (In2[31] == 1'b1))		? 2'b01:	// In2 < In3 negative numbers
																				  2'b11; 

endmodule
