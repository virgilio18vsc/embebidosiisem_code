`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins   
// 
// Create Date:    14:24:49 03/17/2014 
// Design Name: 
// Module Name:    D_Blaze_DVIC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_DVIC(
    //inputs
	 input 			clock,
	 input 			reset,
	 input [31:0]	ctrl,
	 input 			select,
	 input			reti_signal,
	 input [31:0]	interrupt_request,
	 input			interrupt_ack,
	 //outputs
	 output [11:0] interrupt_address,
	 output reg		interrupt_pending
	 );

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [2:0] 	command;			// Decoded command
wire [2:0] 	type;				// Decoded type
wire [15:0] dec_output;		// Decoded output


wire [11:0] new_interrupt_address;		//Endere�o do vetor de interrup��es lido do vetor de interrup��es.
reg [5:0] vector_index;						//Indice do vetor de interrup��es com o endere�o da interrup��o.


//Vetor de interrup��es
vector_table interrupt_table(
 .clka(clock),
 .rsta(0),
 .ena(1'b1),
 .addra(vector_index),
 .douta(new_interrupt_address)
);



/***********************************************************************************************************************
/*********** Registo IPR(Interrupt Priority Registers)
/***********************************************************************************************************************/	 
reg [31:0] IPR [7:0];	//Guarda o valor das prioridade de grupo e sub-grupo por cada interrup��o
								//Prioridade de 0 a 31, sendo 0 a de maior prioridade e 31 a de menor (MSB).
								//Guarda os valores das flags de estado da interrup��o (ENABLED, PENDING, STACKED)


//Posicionamento dos registos de prioridade(Grupo e Sub-Grupo)
//PRI_COL<Numero da coluna da matriz_G_P<Numero de grupos de prioridade>_S<Numero de sub-grupos de prioridades>
//->G	:	Grupos de prioridades
//->SG:	Subgrupos de prioridades

//Coluna 0
`define PRI_COL0_G_P32_S0		4:0

`define PRI_COL0_G_P16_S2		4:1
`define PRI_COL0_SG_P16_S2		0

`define PRI_COL0_G_P8_S4		4:2
`define PRI_COL0_SG_P8_S4		1:0

`define PRI_COL0_G_P4_S8		4:3
`define PRI_COL0_SG_P4_S8		2:0

`define PRI_COL0_G_P2_S16		4
`define PRI_COL0_SG_P2_S16		3:0

`define PRI_COL0_SG_P0_S32		4:0

//Coluna 1
`define PRI_COL1_G_P32_S0		12:8

`define PRI_COL1_G_P16_S2		12:9
`define PRI_COL1_SG_P16_S2		8

`define PRI_COL1_G_P8_S4		12:10
`define PRI_COL1_SG_P8_S4		9:8

`define PRI_COL1_G_P4_S8		12:11
`define PRI_COL1_SG_P4_S8		10:8

`define PRI_COL1_G_P2_S16		12
`define PRI_COL1_SG_P2_S16		11:8

`define PRI_COL1_SG_P0_S32		12:8

//Coluna 2
`define PRI_COL2_G_P32_S0		20:16

`define PRI_COL2_G_P16_S2		20:17
`define PRI_COL2_SG_P16_S2		16

`define PRI_COL2_G_P8_S4		20:18
`define PRI_COL2_SG_P8_S4		17:16

`define PRI_COL2_G_P4_S8		20:19
`define PRI_COL2_SG_P4_S8		18:16

`define PRI_COL2_G_P2_S16		20
`define PRI_COL2_SG_P2_S16		19:16

`define PRI_COL2_SG_P0_S32		20:16

//Coluna 3
`define PRI_COL3_G_P32_S0		28:24

`define PRI_COL3_G_P16_S2		28:25
`define PRI_COL3_SG_P16_S2		24

`define PRI_COL3_G_P8_S4		28:26
`define PRI_COL3_SG_P8_S4		25:24

`define PRI_COL3_G_P4_S8		28:27
`define PRI_COL3_SG_P4_S8		26:24

`define PRI_COL3_G_P2_S16		28
`define PRI_COL3_SG_P2_S16		27:24

`define PRI_COL3_SG_P0_S32		28:24

//Posi��o das flags de estado das interrup��es
//F_FLAGS_COL<Numero da coluna>
`define F_FLAGS_COL0		7:5
`define F_FLAGS_COL1		15:13
`define F_FLAGS_COL2		23:21
`define F_FLAGS_COL3 	31:29

//Linhas da matriz onde estam armazenadas informa�oes sobre a interrup��o
//PRI_ROW_<Numero da interrup��o inferior>_<Numero da nterrup��o superior>
`define PRI_ROW_0_3		0
`define PRI_ROW_4_7		1
`define PRI_ROW_8_11		2
`define PRI_ROW_12_15	3
`define PRI_ROW_16_19	4
`define PRI_ROW_20_23	5
`define PRI_ROW_24_27	6
`define PRI_ROW_28_31	7


/***********************************************************************************************************************
/*********** Registo AIRCR(Application Interrupt and Reset Control Register)
/***********************************************************************************************************************/	
reg [31:0] AIRCR; 
`define PRIGROUP	2:0	//Cursor de divis�o dos registos PRI em prioridades e sub-prioridades

//Configura��es do cursor de divisao dos registos PRI em prioridades e sub-prioridades
`define PRIO_P32_S0 		3'b000		//32 Prioridades e 0 Sub-prioridades
`define PRIO_P16_S2 		3'b001		//16 Prioridades e 2 Sub-prioridades
`define PRIO_P8_S4		3'b010		//8 Prioridades e 4 Sub-prioridades
`define PRIO_P4_S8 		3'b011		//4 Prioridades e 8 Sub-prioridades
`define PRIO_P2_S16 		3'b100		//2 Prioridades e 16 Sub-prioridades
`define PRIO_P0_S32 		3'b101		//0 Prioridades e 32 Sub-prioridades


/***********************************************************************************************************************
/*********** Registo STIR (Software Trigger Interrupt Register) Nota: lido pelo bloco always
/***********************************************************************************************************************/	 
wire [31:0] STIR;							//Gera interrup��es por software
reg 			sw_interrupt_changed;		//Pending change in STIR

						
`define INTID	4:0				//INTID	[4:0] ID da interrup��o para despoletar (0-31) (W).
										//Por exemplo, escrever 0x03 vai gerar a IRQ3 por software.

//RESERVED_STIR	31:5			//[31:5] -> Reservado: l�-se '0' (R).


/***********************************************************************************************************************
/*********** Registo  CIS(Current Interrupt State)
/***********************************************************************************************************************/	
reg [31:0] CIS;	

`define PB				0		//Indica se existe uma interrup��o pending.

`define VECTPENDING	6:1	//Indica o n�mero da interrup��o pendente de maior prioridade(R) -> Implementar no bloco always

`define SB				7		//Indica se existe uma interrup��o stacked.

`define VECTSTACKED	12:8	//Indica o n�mero da interrup��o stacked de maior prioridade(R) -> Implementar no bloco always

`define AB				13		//Indica se existe uma interrup��o ativa.

`define ETMINTNUM		18:14	//Indica o n�mero da interrup��o em execu��o (0-31) (R) -> Implementar no bloco always

`define CURRPRI 		23:19 //Indica a prioridade da interrup��o em execu��o. Configurada nos registos IPR (R) -> Implementar no bloco always

`define RESERVED_CIS	31:24	//[7:0] -> Reservado: l�-se '0' (R).


/***********************************************************************************************************************
/*********** Implementa��o do Registo IPR(Interrupt Priority Registers)
/***********************************************************************************************************************/	
wire [4:0] g_priority [31:0];		//Prioridades de grupo

wire [4:0] sg_priority [31:0];	//Prioridades de sub-grupo

assign {g_priority[0], sg_priority[0]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL0_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL0_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL0_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL0_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL0_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[1], sg_priority[1]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL1_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL1_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL1_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL1_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[2], sg_priority[2]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL2_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL2_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL2_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[3], sg_priority[3]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL3_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL3_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL3_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;
					
					

assign {g_priority[4], sg_priority[4]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL0_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL0_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL0_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL0_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL0_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[5], sg_priority[5]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL1_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL1_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL1_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL1_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[6], sg_priority[6]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL2_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL2_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL2_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[7], sg_priority[7]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL3_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL3_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL3_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;


assign {g_priority[8], sg_priority[8]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL0_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL0_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL0_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[9], sg_priority[9]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL1_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL1_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[10], sg_priority[10]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[11], sg_priority[11]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;
														

assign {g_priority[12], sg_priority[12]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[13], sg_priority[13]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P0_S32]																	}	:
														10'b0000000000;

assign {g_priority[14], sg_priority[14]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[15], sg_priority[15]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;
														

assign {g_priority[16], sg_priority[16]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[17], sg_priority[17]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[18], sg_priority[18]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[19], sg_priority[19]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;		


assign {g_priority[20], sg_priority[20]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P0_S32]																	}	:
														10'b0000000000;
														
assign {g_priority[21], sg_priority[21]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[22], sg_priority[22]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[23], sg_priority[23]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;	


assign {g_priority[24], sg_priority[24]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[25], sg_priority[25]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P0_S32]																	}	:
														10'b0000000000;

assign {g_priority[26], sg_priority[26]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[27], sg_priority[27]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;


assign {g_priority[28], sg_priority[28]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[29], sg_priority[29]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;				

assign {g_priority[30], sg_priority[30]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[31], sg_priority[31]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;														


//Flags do estado das prioridades ([0]->ENABLE, [1]->PENDING, [2]->STACKED)
wire [2:0] priority_flags [31:0];
`define	F_ENABLE 	0
`define	F_PENDING 	1
`define	F_STACKED 	2


assign priority_flags[0] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL0];
assign priority_flags[1] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL1];
assign priority_flags[2] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL2];
assign priority_flags[3] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL3];

assign priority_flags[4] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL0];
assign priority_flags[5] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL1];
assign priority_flags[6] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL2];
assign priority_flags[7] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL3];

assign priority_flags[8] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL0];
assign priority_flags[9] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL1];
assign priority_flags[10] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL2];
assign priority_flags[11] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL3];

assign priority_flags[12] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL0];
assign priority_flags[13] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL1];
assign priority_flags[14] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL2];
assign priority_flags[15] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL3];

assign priority_flags[16] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL0];
assign priority_flags[17] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL1];
assign priority_flags[18] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL2];
assign priority_flags[19] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL3];

assign priority_flags[20] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL0];
assign priority_flags[21] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL1];
assign priority_flags[22] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL2];
assign priority_flags[23] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL3];

assign priority_flags[24] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL0];
assign priority_flags[25] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL1];
assign priority_flags[26] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL2];
assign priority_flags[27] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL3];

assign priority_flags[28] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL0];
assign priority_flags[29] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL1];
assign priority_flags[30] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL2];
assign priority_flags[31] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL3];


/***********************************************************************************************************************
/*********** Arvore de decis�o PENDING
/***********************************************************************************************************************/
//Nivel 1
wire [4:0] pending_level_1 [15:0];


assign pending_level_1[0] =	({~priority_flags[0][`F_ENABLE], ~priority_flags[0][`F_PENDING], g_priority[0], sg_priority[0]} <= 
										{~priority_flags[1][`F_ENABLE], ~priority_flags[1][`F_PENDING], g_priority[1], sg_priority[1]})	
										?	5'd0	:	5'd1;
										
assign pending_level_1[1] =	({~priority_flags[2][`F_ENABLE], ~priority_flags[2][`F_PENDING], g_priority[2], sg_priority[2]} <= 
										{~priority_flags[3][`F_ENABLE], ~priority_flags[3][`F_PENDING], g_priority[3], sg_priority[3]})	
										?	5'd2	:	5'd3;


assign pending_level_1[2] =	({~priority_flags[4][`F_ENABLE], ~priority_flags[4][`F_PENDING], g_priority[4], sg_priority[4]} <= 
										{~priority_flags[5][`F_ENABLE], ~priority_flags[5][`F_PENDING], g_priority[5], sg_priority[5]})	
										?	5'd4	:	5'd5;
										
assign pending_level_1[3] =	({~priority_flags[6][`F_ENABLE], ~priority_flags[6][`F_PENDING], g_priority[6], sg_priority[6]} <= 
										{~priority_flags[7][`F_ENABLE], ~priority_flags[7][`F_PENDING], g_priority[7], sg_priority[7]})	
										?	5'd6	:	5'd7;


assign pending_level_1[4] =	({~priority_flags[8][`F_ENABLE], ~priority_flags[8][`F_PENDING], g_priority[8], sg_priority[8]} <= 
										{~priority_flags[9][`F_ENABLE], ~priority_flags[9][`F_PENDING], g_priority[9], sg_priority[9]})	
										?	5'd8	:	5'd9;
										
assign pending_level_1[5] =	({~priority_flags[10][`F_ENABLE], ~priority_flags[10][`F_PENDING], g_priority[10], sg_priority[10]} <= 
										{~priority_flags[11][`F_ENABLE], ~priority_flags[11][`F_PENDING], g_priority[11], sg_priority[11]})	
										?	5'd10	:	5'd11;
										

assign pending_level_1[6] =	({~priority_flags[12][`F_ENABLE], ~priority_flags[12][`F_PENDING], g_priority[12], sg_priority[12]} <= 
										{~priority_flags[13][`F_ENABLE], ~priority_flags[13][`F_PENDING], g_priority[13], sg_priority[13]})	
										?	5'd12	:	5'd13;
										
assign pending_level_1[7] =	({~priority_flags[14][`F_ENABLE], ~priority_flags[14][`F_PENDING], g_priority[14], sg_priority[14]} <= 
										{~priority_flags[15][`F_ENABLE], ~priority_flags[15][`F_PENDING], g_priority[15], sg_priority[15]})	
										?	5'd14	:	5'd15;
										
										
assign pending_level_1[8] =	({~priority_flags[16][`F_ENABLE], ~priority_flags[16][`F_PENDING], g_priority[16], sg_priority[16]} <= 
										{~priority_flags[17][`F_ENABLE], ~priority_flags[17][`F_PENDING], g_priority[17], sg_priority[17]})	
										?	5'd16	:	5'd17;
										
assign pending_level_1[9] =	({~priority_flags[18][`F_ENABLE], ~priority_flags[18][`F_PENDING], g_priority[18], sg_priority[18]} <= 
										{~priority_flags[19][`F_ENABLE], ~priority_flags[19][`F_PENDING], g_priority[19], sg_priority[19]})	
										?	5'd18	:	5'd19;


assign pending_level_1[10] =	({~priority_flags[20][`F_ENABLE], ~priority_flags[20][`F_PENDING], g_priority[20], sg_priority[20]} <= 
										{~priority_flags[21][`F_ENABLE], ~priority_flags[21][`F_PENDING], g_priority[21], sg_priority[21]})	
										?	5'd20	:	5'd21;
										
assign pending_level_1[11] =	({~priority_flags[22][`F_ENABLE], ~priority_flags[22][`F_PENDING], g_priority[22], sg_priority[22]} <= 
										{~priority_flags[23][`F_ENABLE], ~priority_flags[23][`F_PENDING], g_priority[23], sg_priority[23]})	
										?	5'd22	:	5'd23;


assign pending_level_1[12] =	({~priority_flags[24][`F_ENABLE], ~priority_flags[24][`F_PENDING], g_priority[24], sg_priority[24]} <= 
										{~priority_flags[25][`F_ENABLE], ~priority_flags[25][`F_PENDING], g_priority[25], sg_priority[25]})	
										?	5'd24	:	5'd25;
										
assign pending_level_1[13] =	({~priority_flags[26][`F_ENABLE], ~priority_flags[26][`F_PENDING], g_priority[26], sg_priority[26]} <= 
										{~priority_flags[27][`F_ENABLE], ~priority_flags[27][`F_PENDING], g_priority[27], sg_priority[27]})	
										?	5'd26	:	5'd27;	
										

assign pending_level_1[14] =	({~priority_flags[28][`F_ENABLE], ~priority_flags[28][`F_PENDING], g_priority[28], sg_priority[28]} <= 
										{~priority_flags[29][`F_ENABLE], ~priority_flags[29][`F_PENDING], g_priority[29], sg_priority[29]})	
										?	5'd28	:	5'd29;
										
assign pending_level_1[15] =	({~priority_flags[30][`F_ENABLE], ~priority_flags[30][`F_PENDING], g_priority[30], sg_priority[30]} <= 
										{~priority_flags[31][`F_ENABLE], ~priority_flags[31][`F_PENDING], g_priority[31], sg_priority[31]})	
										?	5'd30	:	5'd31;	
	
	
//Nivel 2
wire [4:0] pending_level_2 [7:0];


assign pending_level_2[0] =	({~priority_flags[pending_level_1[0]][`F_ENABLE], 	~priority_flags[pending_level_1[0]][`F_PENDING], g_priority[pending_level_1[0]], sg_priority[pending_level_1[0]]} <= 
										{~priority_flags[pending_level_1[1]][`F_ENABLE], 	~priority_flags[pending_level_1[1]][`F_PENDING], g_priority[pending_level_1[1]], sg_priority[pending_level_1[1]]})	
										?	pending_level_1[0]	:	pending_level_1[1];							

assign pending_level_2[1] =	({~priority_flags[pending_level_1[2]][`F_ENABLE], 	~priority_flags[pending_level_1[2]][`F_PENDING], g_priority[pending_level_1[2]], sg_priority[pending_level_1[2]]} <= 
										{~priority_flags[pending_level_1[3]][`F_ENABLE], 	~priority_flags[pending_level_1[3]][`F_PENDING], g_priority[pending_level_1[3]], sg_priority[pending_level_1[3]]})	
										?	pending_level_1[2]	:	pending_level_1[3];
										
assign pending_level_2[2] =	({~priority_flags[pending_level_1[4]][`F_ENABLE], 	~priority_flags[pending_level_1[4]][`F_PENDING], g_priority[pending_level_1[4]], sg_priority[pending_level_1[4]]} <= 
										{~priority_flags[pending_level_1[5]][`F_ENABLE], 	~priority_flags[pending_level_1[5]][`F_PENDING], g_priority[pending_level_1[5]], sg_priority[pending_level_1[5]]})	
										?	pending_level_1[4]	:	pending_level_1[5];									

assign pending_level_2[3] =	({~priority_flags[pending_level_1[6]][`F_ENABLE], 	~priority_flags[pending_level_1[6]][`F_PENDING], g_priority[pending_level_1[6]], sg_priority[pending_level_1[6]]} <= 
										{~priority_flags[pending_level_1[7]][`F_ENABLE], 	~priority_flags[pending_level_1[7]][`F_PENDING], g_priority[pending_level_1[7]], sg_priority[pending_level_1[7]]})	
										?	pending_level_1[6]	:	pending_level_1[7];
										
assign pending_level_2[4] =	({~priority_flags[pending_level_1[8]][`F_ENABLE], 	~priority_flags[pending_level_1[8]][`F_PENDING], g_priority[pending_level_1[8]], sg_priority[pending_level_1[8]]} <= 
										{~priority_flags[pending_level_1[9]][`F_ENABLE], 	~priority_flags[pending_level_1[9]][`F_PENDING], g_priority[pending_level_1[9]], sg_priority[pending_level_1[9]]})	
										?	pending_level_1[8]	:	pending_level_1[9];							

assign pending_level_2[5] =	({~priority_flags[pending_level_1[10]][`F_ENABLE], ~priority_flags[pending_level_1[10]][`F_PENDING], g_priority[pending_level_1[10]], sg_priority[pending_level_1[10]]} <= 
										{~priority_flags[pending_level_1[11]][`F_ENABLE], 	~priority_flags[pending_level_1[11]][`F_PENDING], g_priority[pending_level_1[11]], sg_priority[pending_level_1[11]]})	
										?	pending_level_1[10]	:	pending_level_1[11];
										
assign pending_level_2[6] =	({~priority_flags[pending_level_1[12]][`F_ENABLE], ~priority_flags[pending_level_1[12]][`F_PENDING], g_priority[pending_level_1[12]], sg_priority[pending_level_1[12]]} <= 
										{~priority_flags[pending_level_1[13]][`F_ENABLE], 	~priority_flags[pending_level_1[13]][`F_PENDING], g_priority[pending_level_1[13]], sg_priority[pending_level_1[13]]})	
										?	pending_level_1[12]	:	pending_level_1[13];									

assign pending_level_2[7] =	({~priority_flags[pending_level_1[14]][`F_ENABLE], ~priority_flags[pending_level_1[14]][`F_PENDING], g_priority[pending_level_1[14]], sg_priority[pending_level_1[14]]} <= 
										{~priority_flags[pending_level_1[15]][`F_ENABLE], 	~priority_flags[pending_level_1[15]][`F_PENDING], g_priority[pending_level_1[15]], sg_priority[pending_level_1[15]]})	
										?	pending_level_1[14]	:	pending_level_1[15];


//Nivel 3T
wire [4:0] pending_level_3 [3:0];


assign pending_level_3[0] =	({~priority_flags[pending_level_2[0]][`F_ENABLE], 	~priority_flags[pending_level_2[0]][`F_PENDING], g_priority[pending_level_2[0]], sg_priority[pending_level_2[0]]} <= 
										{~priority_flags[pending_level_2[1]][`F_ENABLE], 	~priority_flags[pending_level_2[1]][`F_PENDING], g_priority[pending_level_2[1]], sg_priority[pending_level_2[1]]})	
										?	pending_level_2[0]	:	pending_level_2[1];							

assign pending_level_3[1] =	({~priority_flags[pending_level_2[2]][`F_ENABLE], 	~priority_flags[pending_level_2[2]][`F_PENDING], g_priority[pending_level_2[2]], sg_priority[pending_level_2[2]]} <= 
										{~priority_flags[pending_level_2[3]][`F_ENABLE], 	~priority_flags[pending_level_2[3]][`F_PENDING], g_priority[pending_level_2[3]], sg_priority[pending_level_2[3]]})	
										?	pending_level_2[2]	:	pending_level_2[3];
										
assign pending_level_3[2] =	({~priority_flags[pending_level_2[4]][`F_ENABLE], 	~priority_flags[pending_level_2[4]][`F_PENDING], g_priority[pending_level_2[4]], sg_priority[pending_level_2[4]]} <= 
										{~priority_flags[pending_level_2[5]][`F_ENABLE], 	~priority_flags[pending_level_2[5]][`F_PENDING], g_priority[pending_level_2[5]], sg_priority[pending_level_2[5]]})	
										?	pending_level_2[4]	:	pending_level_2[5];									

assign pending_level_3[3] =	({~priority_flags[pending_level_2[6]][`F_ENABLE], 	~priority_flags[pending_level_2[6]][`F_PENDING], g_priority[pending_level_2[6]], sg_priority[pending_level_2[6]]} <= 
										{~priority_flags[pending_level_2[7]][`F_ENABLE], 	~priority_flags[pending_level_2[7]][`F_PENDING], g_priority[pending_level_2[7]], sg_priority[pending_level_2[7]]})	
										?	pending_level_2[6]	:	pending_level_2[7];
										


//Nivel 4
wire [4:0] pending_level_4 [1:0];


assign pending_level_4[0] =	({~priority_flags[pending_level_3[0]][`F_ENABLE], 	~priority_flags[pending_level_3[0]][`F_PENDING], g_priority[pending_level_3[0]], sg_priority[pending_level_3[0]]} <= 
										{~priority_flags[pending_level_3[1]][`F_ENABLE], 	~priority_flags[pending_level_3[1]][`F_PENDING], g_priority[pending_level_3[1]], sg_priority[pending_level_3[1]]})	
										?	pending_level_3[0]	:	pending_level_3[1];							

assign pending_level_4[1] =	({~priority_flags[pending_level_3[2]][`F_ENABLE], 	~priority_flags[pending_level_3[2]][`F_PENDING], g_priority[pending_level_3[2]], sg_priority[pending_level_3[2]]} <= 
										{~priority_flags[pending_level_3[3]][`F_ENABLE], 	~priority_flags[pending_level_3[3]][`F_PENDING], g_priority[pending_level_3[3]], sg_priority[pending_level_3[3]]})	
										?	pending_level_3[2]	:	pending_level_3[3];

										
//Nivel 4
wire [4:0] pending_level_5;


assign pending_level_5 =	({~priority_flags[pending_level_4[0]][`F_ENABLE], 	~priority_flags[pending_level_4[0]][`F_PENDING], g_priority[pending_level_4[0]], sg_priority[pending_level_4[0]]} <= 
									{~priority_flags[pending_level_4[1]][`F_ENABLE], 	~priority_flags[pending_level_4[1]][`F_PENDING], g_priority[pending_level_4[1]], sg_priority[pending_level_4[1]]})	
									?	pending_level_4[0]	:	pending_level_4[1];							


/***********************************************************************************************************************
/*********** Arvore de decis�o STACKED
/***********************************************************************************************************************/
//Nivel 1
wire [4:0] stacked_level_1 [15:0];


assign stacked_level_1[0] =	({~priority_flags[0][`F_ENABLE], ~priority_flags[0][`F_STACKED], g_priority[0], sg_priority[0]} <= 
										{~priority_flags[1][`F_ENABLE], ~priority_flags[1][`F_STACKED], g_priority[1], sg_priority[1]})	
										?	5'd0	:	5'd1;
										
assign stacked_level_1[1] =	({~priority_flags[2][`F_ENABLE], ~priority_flags[2][`F_STACKED], g_priority[2], sg_priority[2]} <= 
										{~priority_flags[3][`F_ENABLE], ~priority_flags[3][`F_STACKED], g_priority[3], sg_priority[3]})	
										?	5'd2	:	5'd3;


assign stacked_level_1[2] =	({~priority_flags[4][`F_ENABLE], ~priority_flags[4][`F_STACKED], g_priority[4], sg_priority[4]} <= 
										{~priority_flags[5][`F_ENABLE], ~priority_flags[5][`F_STACKED], g_priority[5], sg_priority[5]})	
										?	5'd4	:	5'd5;
										
assign stacked_level_1[3] =	({~priority_flags[6][`F_ENABLE], ~priority_flags[6][`F_STACKED], g_priority[6], sg_priority[6]} <= 
										{~priority_flags[7][`F_ENABLE], ~priority_flags[7][`F_STACKED], g_priority[7], sg_priority[7]})	
										?	5'd6	:	5'd7;


assign stacked_level_1[4] =	({~priority_flags[8][`F_ENABLE], ~priority_flags[8][`F_STACKED], g_priority[8], sg_priority[8]} <= 
										{~priority_flags[9][`F_ENABLE], ~priority_flags[9][`F_STACKED], g_priority[9], sg_priority[9]})	
										?	5'd8	:	5'd9;
										
assign stacked_level_1[5] =	({~priority_flags[10][`F_ENABLE], ~priority_flags[10][`F_STACKED], g_priority[10], sg_priority[10]} <= 
										{~priority_flags[11][`F_ENABLE], ~priority_flags[11][`F_STACKED], g_priority[11], sg_priority[11]})	
										?	5'd10	:	5'd11;
										

assign stacked_level_1[6] =	({~priority_flags[12][`F_ENABLE], ~priority_flags[12][`F_STACKED], g_priority[12], sg_priority[12]} <= 
										{~priority_flags[13][`F_ENABLE], ~priority_flags[13][`F_STACKED], g_priority[13], sg_priority[13]})	
										?	5'd12	:	5'd13;
										
assign stacked_level_1[7] =	({~priority_flags[14][`F_ENABLE], ~priority_flags[14][`F_STACKED], g_priority[14], sg_priority[14]} <= 
										{~priority_flags[15][`F_ENABLE], ~priority_flags[15][`F_STACKED], g_priority[15], sg_priority[15]})	
										?	5'd14	:	5'd15;
										
										
assign stacked_level_1[8] =	({~priority_flags[16][`F_ENABLE], ~priority_flags[16][`F_STACKED], g_priority[16], sg_priority[16]} <= 
										{~priority_flags[17][`F_ENABLE], ~priority_flags[17][`F_STACKED], g_priority[17], sg_priority[17]})	
										?	5'd16	:	5'd17;
										
assign stacked_level_1[9] =	({~priority_flags[18][`F_ENABLE], ~priority_flags[18][`F_STACKED], g_priority[18], sg_priority[18]} <= 
										{~priority_flags[19][`F_ENABLE], ~priority_flags[19][`F_STACKED], g_priority[19], sg_priority[19]})	
										?	5'd18	:	5'd19;


assign stacked_level_1[10] =	({~priority_flags[20][`F_ENABLE], ~priority_flags[20][`F_STACKED], g_priority[20], sg_priority[20]} <= 
										{~priority_flags[21][`F_ENABLE], ~priority_flags[21][`F_STACKED], g_priority[21], sg_priority[21]})	
										?	5'd20	:	5'd21;
										
assign stacked_level_1[11] =	({~priority_flags[22][`F_ENABLE], ~priority_flags[22][`F_STACKED], g_priority[22], sg_priority[22]} <= 
										{~priority_flags[23][`F_ENABLE], ~priority_flags[23][`F_STACKED], g_priority[23], sg_priority[23]})	
										?	5'd22	:	5'd23;


assign stacked_level_1[12] =	({~priority_flags[24][`F_ENABLE], ~priority_flags[24][`F_STACKED], g_priority[24], sg_priority[24]} <= 
										{~priority_flags[25][`F_ENABLE], ~priority_flags[25][`F_STACKED], g_priority[25], sg_priority[25]})	
										?	5'd24	:	5'd25;
										
assign stacked_level_1[13] =	({~priority_flags[26][`F_ENABLE], ~priority_flags[26][`F_STACKED], g_priority[26], sg_priority[26]} <= 
										{~priority_flags[27][`F_ENABLE], ~priority_flags[27][`F_STACKED], g_priority[27], sg_priority[27]})	
										?	5'd26	:	5'd27;	
										

assign stacked_level_1[14] =	({~priority_flags[28][`F_ENABLE], ~priority_flags[28][`F_STACKED], g_priority[28], sg_priority[28]} <= 
										{~priority_flags[29][`F_ENABLE], ~priority_flags[29][`F_STACKED], g_priority[29], sg_priority[29]})	
										?	5'd28	:	5'd29;
										
assign stacked_level_1[15] =	({~priority_flags[30][`F_ENABLE], ~priority_flags[30][`F_STACKED], g_priority[30], sg_priority[30]} <= 
										{~priority_flags[31][`F_ENABLE], ~priority_flags[31][`F_STACKED], g_priority[31], sg_priority[31]})	
										?	5'd30	:	5'd31;	
	
	
//Nivel 2
wire [4:0] stacked_level_2 [7:0];

assign stacked_level_2[0] =	({~priority_flags[stacked_level_1[0]][`F_ENABLE], 	~priority_flags[stacked_level_1[0]][`F_STACKED], g_priority[stacked_level_1[0]], sg_priority[stacked_level_1[0]]} <= 
										{~priority_flags[stacked_level_1[1]][`F_ENABLE], 	~priority_flags[stacked_level_1[1]][`F_STACKED], g_priority[stacked_level_1[1]], sg_priority[stacked_level_1[1]]})	
										?	stacked_level_1[0]	:	stacked_level_1[1];							

assign stacked_level_2[1] =	({~priority_flags[stacked_level_1[2]][`F_ENABLE], 	~priority_flags[stacked_level_1[2]][`F_STACKED], g_priority[stacked_level_1[2]], sg_priority[stacked_level_1[2]]} <= 
										{~priority_flags[stacked_level_1[3]][`F_ENABLE], 	~priority_flags[stacked_level_1[3]][`F_STACKED], g_priority[stacked_level_1[3]], sg_priority[stacked_level_1[3]]})	
										?	stacked_level_1[2]	:	stacked_level_1[3];
										
assign stacked_level_2[2] =	({~priority_flags[stacked_level_1[4]][`F_ENABLE], 	~priority_flags[stacked_level_1[4]][`F_STACKED], g_priority[stacked_level_1[4]], sg_priority[stacked_level_1[4]]} <= 
										{~priority_flags[stacked_level_1[5]][`F_ENABLE], 	~priority_flags[stacked_level_1[5]][`F_STACKED], g_priority[stacked_level_1[5]], sg_priority[stacked_level_1[5]]})	
										?	stacked_level_1[4]	:	stacked_level_1[5];									

assign stacked_level_2[3] =	({~priority_flags[stacked_level_1[6]][`F_ENABLE], 	~priority_flags[stacked_level_1[6]][`F_STACKED], g_priority[stacked_level_1[6]], sg_priority[stacked_level_1[6]]} <= 
										{~priority_flags[stacked_level_1[7]][`F_ENABLE], 	~priority_flags[stacked_level_1[7]][`F_STACKED], g_priority[stacked_level_1[7]], sg_priority[stacked_level_1[7]]})	
										?	stacked_level_1[6]	:	stacked_level_1[7];
										
assign stacked_level_2[4] =	({~priority_flags[stacked_level_1[8]][`F_ENABLE], 	~priority_flags[stacked_level_1[8]][`F_STACKED], g_priority[stacked_level_1[8]], sg_priority[stacked_level_1[8]]} <= 
										{~priority_flags[stacked_level_1[9]][`F_ENABLE], 	~priority_flags[stacked_level_1[9]][`F_STACKED], g_priority[stacked_level_1[9]], sg_priority[stacked_level_1[9]]})	
										?	stacked_level_1[8]	:	stacked_level_1[9];							

assign stacked_level_2[5] =	({~priority_flags[stacked_level_1[10]][`F_ENABLE], ~priority_flags[stacked_level_1[10]][`F_STACKED], g_priority[stacked_level_1[10]], sg_priority[stacked_level_1[10]]} <= 
										{~priority_flags[stacked_level_1[11]][`F_ENABLE], 	~priority_flags[stacked_level_1[11]][`F_STACKED], g_priority[stacked_level_1[11]], sg_priority[stacked_level_1[11]]})	
										?	stacked_level_1[10]	:	stacked_level_1[11];
										
assign stacked_level_2[6] =	({~priority_flags[stacked_level_1[12]][`F_ENABLE], ~priority_flags[stacked_level_1[12]][`F_STACKED], g_priority[stacked_level_1[12]], sg_priority[stacked_level_1[12]]} <= 
										{~priority_flags[stacked_level_1[13]][`F_ENABLE], 	~priority_flags[stacked_level_1[13]][`F_STACKED], g_priority[stacked_level_1[13]], sg_priority[stacked_level_1[13]]})	
										?	stacked_level_1[12]	:	stacked_level_1[13];									

assign stacked_level_2[7] =	({~priority_flags[stacked_level_1[14]][`F_ENABLE], ~priority_flags[stacked_level_1[14]][`F_STACKED], g_priority[stacked_level_1[14]], sg_priority[stacked_level_1[14]]} <= 
										{~priority_flags[stacked_level_1[15]][`F_ENABLE], 	~priority_flags[stacked_level_1[15]][`F_STACKED], g_priority[stacked_level_1[15]], sg_priority[stacked_level_1[15]]})	
										?	stacked_level_1[14]	:	stacked_level_1[15];


//Nivel 3
wire [4:0] stacked_level_3 [3:0];


assign stacked_level_3[0] =	({~priority_flags[stacked_level_2[0]][`F_ENABLE], 	~priority_flags[stacked_level_2[0]][`F_STACKED],g_priority[stacked_level_2[0]], sg_priority[stacked_level_2[0]]} <= 
										{~priority_flags[stacked_level_2[1]][`F_ENABLE], 	~priority_flags[stacked_level_2[1]][`F_STACKED], g_priority[stacked_level_2[1]], sg_priority[stacked_level_2[1]]})	
										?	stacked_level_2[0]	:	stacked_level_2[1];							

assign stacked_level_3[1] =	({~priority_flags[stacked_level_2[2]][`F_ENABLE], 	~priority_flags[stacked_level_2[2]][`F_STACKED], g_priority[stacked_level_2[2]], sg_priority[stacked_level_2[2]]} <= 
										{~priority_flags[stacked_level_2[3]][`F_ENABLE], 	~priority_flags[stacked_level_2[3]][`F_STACKED], g_priority[stacked_level_2[3]], sg_priority[stacked_level_2[3]]})	
										?	stacked_level_2[2]	:	stacked_level_2[3];
										
assign stacked_level_3[2] =	({~priority_flags[stacked_level_2[4]][`F_ENABLE], 	~priority_flags[stacked_level_2[4]][`F_STACKED], g_priority[stacked_level_2[4]], sg_priority[stacked_level_2[4]]} <= 
										{~priority_flags[stacked_level_2[5]][`F_ENABLE], 	~priority_flags[stacked_level_2[5]][`F_STACKED], g_priority[stacked_level_2[5]], sg_priority[stacked_level_2[5]]})	
										?	stacked_level_2[4]	:	stacked_level_2[5];									

assign stacked_level_3[3] =	({~priority_flags[stacked_level_2[6]][`F_ENABLE], 	~priority_flags[stacked_level_2[6]][`F_STACKED], g_priority[stacked_level_2[6]], sg_priority[stacked_level_2[6]]} <= 
										{~priority_flags[stacked_level_2[7]][`F_ENABLE], 	~priority_flags[stacked_level_2[7]][`F_STACKED], g_priority[stacked_level_2[7]], sg_priority[stacked_level_2[7]]})	
										?	stacked_level_2[6]	:	stacked_level_2[7];
										


//Nivel 4
wire [4:0] stacked_level_4 [1:0];


assign stacked_level_4[0] =	({~priority_flags[stacked_level_3[0]][`F_ENABLE], 	~priority_flags[stacked_level_3[0]][`F_STACKED], g_priority[stacked_level_3[0]], sg_priority[stacked_level_3[0]]} <= 
										{~priority_flags[stacked_level_3[1]][`F_ENABLE], 	~priority_flags[stacked_level_3[1]][`F_STACKED], g_priority[stacked_level_3[1]], sg_priority[stacked_level_3[1]]})	
										?	stacked_level_3[0]	:	stacked_level_3[1];							

assign stacked_level_4[1] =	({~priority_flags[stacked_level_3[2]][`F_ENABLE], 	~priority_flags[stacked_level_3[2]][`F_STACKED], g_priority[stacked_level_3[2]], sg_priority[stacked_level_3[2]]} <= 
										{~priority_flags[stacked_level_3[3]][`F_ENABLE], 	~priority_flags[stacked_level_3[3]][`F_STACKED], g_priority[stacked_level_3[3]], sg_priority[stacked_level_3[3]]})	
										?	stacked_level_3[2]	:	stacked_level_3[3];

										
//Nivel 4
wire [4:0] stacked_level_5;


assign stacked_level_5 =	({~priority_flags[stacked_level_4[0]][`F_ENABLE], 	~priority_flags[stacked_level_4[0]][`F_STACKED], g_priority[stacked_level_4[0]], sg_priority[stacked_level_4[0]]} <= 
									{~priority_flags[stacked_level_4[1]][`F_ENABLE], 	~priority_flags[stacked_level_4[1]][`F_STACKED], g_priority[stacked_level_4[1]], sg_priority[stacked_level_4[1]]})	
									?	stacked_level_4[0]	:	stacked_level_4[1];


/***********************************************************************************************************************
/*********** Fim das �rvores de decis�o
/***********************************************************************************************************************/


//Indice de inicio das flags de cada coluna do registo IPR.
`define FLAGS_COL0_INIT		5
`define FLAGS_COL1_INIT		13
`define FLAGS_COL2_INIT		21
`define FLAGS_COL3_INIT 	29


//Registos para verificar o posedge dos sinais de RETI e de pedido de interrup��o por parte
//	dos perif�ricos. 
reg reti_signal_last;


//Output com endere�o da interrup��o sempre que existe um endere�o lido do vetor de interrup��es.
assign interrupt_address = vector_index ? new_interrupt_address : 0;

//assign STIR[`INTID] = 16;

always@(clock or interrupt_ack or reti_signal or reset)
begin
	//Sempre que � recebido um reset s�o restaurados os valores por defeito do NVIC.
	if ( reset )
	begin
		IPR[`PRI_ROW_0_3] = 32'h0;
		IPR[`PRI_ROW_4_7] = 32'h0;
		IPR[`PRI_ROW_8_11] = 32'h0;
		IPR[`PRI_ROW_12_15] = 32'h0;
		IPR[`PRI_ROW_16_19] = 32'h0;
		IPR[`PRI_ROW_20_23] = 32'h0;
		IPR[`PRI_ROW_24_27] = 32'h0;
		IPR[`PRI_ROW_28_31] = 32'h0;

		sw_interrupt_changed = 0;
		
		CIS[`RESERVED_CIS] = 0;
		CIS[`AB] = 0;
		CIS[`ETMINTNUM] = 0;
		CIS[`VECTPENDING] = 0;
		CIS[`VECTSTACKED] = 0;
		CIS[`CURRPRI] = 0;

		interrupt_pending <= 0;
		vector_index <= 0;
		AIRCR <= {29'b00000000000000000000000000000, `PRIO_P16_S2};
		
		reti_signal_last = 0;
	end
	
	else 
	begin
		//Sempre que existe um novo pedido de interrup��o por parte de um perif�rico, o sinal de pendente
		//	dessa interrup��o � assinalada.
	
		IPR[`PRI_ROW_28_31][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_28_31][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[31]
																				| (sw_interrupt_changed & STIR[`INTID] == 31);

		IPR[`PRI_ROW_28_31][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_28_31][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[30]
																				| (sw_interrupt_changed & STIR[`INTID] == 30);

		IPR[`PRI_ROW_28_31][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_28_31][`FLAGS_COL1_INIT + `F_PENDING]
																				| interrupt_request[29]
																				| (sw_interrupt_changed & STIR[`INTID] == 29);

		IPR[`PRI_ROW_28_31][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_28_31][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[28]
																				| (sw_interrupt_changed & STIR[`INTID] == 28);
		
		IPR[`PRI_ROW_24_27][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_24_27][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[27]
																				| (sw_interrupt_changed & STIR[`INTID] == 27);
		
		IPR[`PRI_ROW_24_27][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_24_27][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[26]
																				| (sw_interrupt_changed & STIR[`INTID] == 26);

		IPR[`PRI_ROW_24_27][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_24_27][`FLAGS_COL1_INIT + `F_PENDING] 
																				| interrupt_request[25]
																				| (sw_interrupt_changed & STIR[`INTID] == 25);
		
		IPR[`PRI_ROW_24_27][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_24_27][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[24]
																				| (sw_interrupt_changed & STIR[`INTID] == 24);
		
		IPR[`PRI_ROW_20_23][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_20_23][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[23]
																				| (sw_interrupt_changed & STIR[`INTID] == 23);
		
		IPR[`PRI_ROW_20_23][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_20_23][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[22]
																				| (sw_interrupt_changed & STIR[`INTID] == 22);

		IPR[`PRI_ROW_20_23][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_20_23][`FLAGS_COL1_INIT + `F_PENDING]
																				| interrupt_request[21]
																				| (sw_interrupt_changed & STIR[`INTID] == 21);

		IPR[`PRI_ROW_20_23][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_20_23][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[20]
																				| (sw_interrupt_changed & STIR[`INTID] == 20);
		
		IPR[`PRI_ROW_16_19][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_16_19][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[19]
																				| (sw_interrupt_changed & STIR[`INTID] == 19);
		
		IPR[`PRI_ROW_16_19][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_16_19][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[18]
																				| (sw_interrupt_changed & STIR[`INTID] == 18);
		
		IPR[`PRI_ROW_16_19][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_16_19][`FLAGS_COL1_INIT + `F_PENDING]
																				| interrupt_request[17]
																				| (sw_interrupt_changed & STIR[`INTID] == 17);
		
		IPR[`PRI_ROW_16_19][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_16_19][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[16]
																				| (sw_interrupt_changed & STIR[`INTID] == 16);
		
		IPR[`PRI_ROW_12_15][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_12_15][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[15]
																				| (sw_interrupt_changed & STIR[`INTID] == 15);
																				
		IPR[`PRI_ROW_12_15][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_12_15][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[14]
																				| (sw_interrupt_changed & STIR[`INTID] == 14);

		IPR[`PRI_ROW_12_15][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_12_15][`FLAGS_COL1_INIT + `F_PENDING]
																				| interrupt_request[13]
																				| (sw_interrupt_changed & STIR[`INTID] == 13);
		
		IPR[`PRI_ROW_12_15][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_12_15][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[12]
																				| (sw_interrupt_changed & STIR[`INTID] == 12);
		
		IPR[`PRI_ROW_8_11][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_8_11][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[11]
																				| (sw_interrupt_changed & STIR[`INTID] == 11);

		IPR[`PRI_ROW_8_11][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_8_11][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[10]
																				| (sw_interrupt_changed & STIR[`INTID] == 10);

		IPR[`PRI_ROW_8_11][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_8_11][`FLAGS_COL1_INIT + `F_PENDING]
																				| interrupt_request[9]
																				| (sw_interrupt_changed & STIR[`INTID] == 9);

		IPR[`PRI_ROW_8_11][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_8_11][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[8]
																				| (sw_interrupt_changed & STIR[`INTID] == 8);
		
		IPR[`PRI_ROW_4_7][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_4_7][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[7]
																				| (sw_interrupt_changed & STIR[`INTID] == 7);
			
		IPR[`PRI_ROW_4_7][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_4_7][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[6]
																				| (sw_interrupt_changed & STIR[`INTID] == 6);

		IPR[`PRI_ROW_4_7][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_4_7][`FLAGS_COL1_INIT + `F_PENDING]
																				| interrupt_request[5]
																				| (sw_interrupt_changed & STIR[`INTID] == 5);

		IPR[`PRI_ROW_4_7][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_4_7][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[4]
																				| (sw_interrupt_changed & STIR[`INTID] == 4);
		
		IPR[`PRI_ROW_0_3][`FLAGS_COL3_INIT + `F_PENDING] = IPR[`PRI_ROW_0_3][`FLAGS_COL3_INIT + `F_PENDING]
																				| interrupt_request[3]
																				| (sw_interrupt_changed & STIR[`INTID] == 3);

		IPR[`PRI_ROW_0_3][`FLAGS_COL2_INIT + `F_PENDING] = IPR[`PRI_ROW_0_3][`FLAGS_COL2_INIT + `F_PENDING]
																				| interrupt_request[2]
																				| (sw_interrupt_changed & STIR[`INTID] == 2);
		
		IPR[`PRI_ROW_0_3][`FLAGS_COL1_INIT + `F_PENDING] = IPR[`PRI_ROW_0_3][`FLAGS_COL1_INIT + `F_PENDING]
																				| interrupt_request[1]
																				| (sw_interrupt_changed & STIR[`INTID] == 1);

		IPR[`PRI_ROW_0_3][`FLAGS_COL0_INIT + `F_PENDING] = IPR[`PRI_ROW_0_3][`FLAGS_COL0_INIT + `F_PENDING]
																				| interrupt_request[0]
																				| (sw_interrupt_changed & STIR[`INTID] == 0);


		//Se a interrup��o por sw foi atendida � limpo o registo que despoletou a interrup��o. 
		if ( sw_interrupt_changed )
			sw_interrupt_changed = 0;

		//As interrup��es pending e stacke de maior prioridade (obtidas pelas �rvores de decis�o) s�o
		//	guardadas no registo CIS. Os bits PB e SB indicam se estas s�o efetivamente enable e pending/stacked.
		CIS[`VECTPENDING] = pending_level_5;
		CIS[`PB] = priority_flags[pending_level_5][`F_ENABLE] & priority_flags[pending_level_5][`F_PENDING];
		CIS[`VECTSTACKED] = stacked_level_5;
		CIS[`SB] = priority_flags[stacked_level_5][`F_ENABLE] & priority_flags[stacked_level_5][`F_STACKED];


		//Quando um sinal de RETI � recebido (interrup��o terminada) o sinal de ativo � limpo.
		if ( reti_signal & ~reti_signal_last )
		begin
			CIS[`AB] = 0;
		end
		
		reti_signal_last = reti_signal;
		
		//Se a interrup��o pending tem prioridade superior � ativa (se existente) e �s stacked (se
		//	existentes)...
		if ( CIS[`PB] & ( ( CIS[`AB] & ( g_priority[CIS[`VECTPENDING]] < CIS[`CURRPRI] ) ) | ( ~CIS[`AB] & ( ~CIS[`SB] | CIS[`SB] & ( g_priority[CIS[`VECTPENDING]] < g_priority[CIS[`VECTSTACKED]] ) ) ) ) )
		begin
		
			//Se uma interrup��o pending foi recebida pelo CPU, esta � passada para ativa.
			if ( interrupt_ack & interrupt_pending )
			begin
				//Se uma interrup��o ativa � interrompida por outra pending de maior prioridade esta �
				//	passada para stacked.
				if ( CIS[`AB] )
				begin
					IPR[0][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 0 ) ? 1 : IPR[0][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[0][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 1 ) ? 1 : IPR[0][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[0][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 2 ) ? 1 : IPR[0][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[0][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 3 ) ? 1 : IPR[0][`FLAGS_COL3_INIT + `F_STACKED];

					IPR[1][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 4 ) ? 1 : IPR[1][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[1][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 5 ) ? 1 : IPR[1][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[1][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 6 ) ? 1 : IPR[1][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[1][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 7 ) ? 1 : IPR[1][`FLAGS_COL3_INIT + `F_STACKED];

					IPR[2][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 8 ) ? 1 : IPR[2][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[2][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 9 ) ? 1 : IPR[2][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[2][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 10 ) ? 1 : IPR[2][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[2][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 11 ) ? 1 : IPR[2][`FLAGS_COL3_INIT + `F_STACKED];
								 
					IPR[3][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 12 ) ? 1 : IPR[3][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[3][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 13 ) ? 1 : IPR[3][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[3][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 14 ) ? 1 : IPR[3][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[3][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 15 ) ? 1 : IPR[3][`FLAGS_COL3_INIT + `F_STACKED];
								 
					IPR[4][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 16 ) ? 1 : IPR[4][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[4][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 17 ) ? 1 : IPR[4][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[4][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 18 ) ? 1 : IPR[4][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[4][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 19 ) ? 1 : IPR[4][`FLAGS_COL3_INIT + `F_STACKED];

					IPR[5][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 20 ) ? 1 : IPR[5][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[5][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 21 ) ? 1 : IPR[5][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[5][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 22 ) ? 1 : IPR[5][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[5][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 23 ) ? 1 : IPR[5][`FLAGS_COL3_INIT + `F_STACKED];
								 
					IPR[6][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 24 ) ? 1 : IPR[6][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[6][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 25 ) ? 1 : IPR[6][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[6][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 26 ) ? 1 : IPR[6][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[6][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 27 ) ? 1 : IPR[6][`FLAGS_COL3_INIT + `F_STACKED];
								 
					IPR[7][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 28 ) ? 1 : IPR[7][`FLAGS_COL0_INIT + `F_STACKED];
					IPR[7][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 29 ) ? 1 : IPR[7][`FLAGS_COL1_INIT + `F_STACKED];
					IPR[7][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 30 ) ? 1 : IPR[7][`FLAGS_COL2_INIT + `F_STACKED];
					IPR[7][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 31 ) ? 1 : IPR[7][`FLAGS_COL3_INIT + `F_STACKED];
				end
				
				//Informa��o da interrup��o ativa (n�mero e prioridade) no registo CIS.
				CIS[`AB] = 1;
				CIS[`ETMINTNUM] = CIS[`VECTPENDING];
				CIS[`CURRPRI] = ( g_priority[CIS[`ETMINTNUM]] );
				
				//Limpo o sinal de pending da interrup��o que passou para ativa.
				IPR[0][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 0 ) ? 0 : IPR[0][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[0][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 1 ) ? 0 : IPR[0][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[0][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 2 ) ? 0 : IPR[0][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[0][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 3 ) ? 0 : IPR[0][`FLAGS_COL3_INIT + `F_PENDING];

				IPR[1][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 4 ) ? 0 : IPR[1][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[1][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 5 ) ? 0 : IPR[1][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[1][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 6 ) ? 0 : IPR[1][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[1][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 7 ) ? 0 : IPR[1][`FLAGS_COL3_INIT + `F_PENDING];

				IPR[2][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 8 ) ? 0 : IPR[2][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[2][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 9 ) ? 0 : IPR[2][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[2][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 10 ) ? 0 : IPR[2][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[2][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 11 ) ? 0 : IPR[2][`FLAGS_COL3_INIT + `F_PENDING];
							 
				IPR[3][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 12 ) ? 0 : IPR[3][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[3][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 13 ) ? 0 : IPR[3][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[3][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 14 ) ? 0 : IPR[3][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[3][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 15 ) ? 0 : IPR[3][`FLAGS_COL3_INIT + `F_PENDING];
							 
				IPR[4][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 16 ) ? 0 : IPR[4][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[4][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 17 ) ? 0 : IPR[4][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[4][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 18 ) ? 0 : IPR[4][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[4][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 19 ) ? 0 : IPR[4][`FLAGS_COL3_INIT + `F_PENDING];

				IPR[5][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 20 ) ? 0 : IPR[5][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[5][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 21 ) ? 0 : IPR[5][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[5][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 22 ) ? 0 : IPR[5][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[5][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 23 ) ? 0 : IPR[5][`FLAGS_COL3_INIT + `F_PENDING];
							 
				IPR[6][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 24 ) ? 0 : IPR[6][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[6][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 25 ) ? 0 : IPR[6][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[6][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 26 ) ? 0 : IPR[6][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[6][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 27 ) ? 0 : IPR[6][`FLAGS_COL3_INIT + `F_PENDING];
							 
				IPR[7][`FLAGS_COL0_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 28 ) ? 0 : IPR[7][`FLAGS_COL0_INIT + `F_PENDING];
				IPR[7][`FLAGS_COL1_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 29 ) ? 0 : IPR[7][`FLAGS_COL1_INIT + `F_PENDING];
				IPR[7][`FLAGS_COL2_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 30 ) ? 0 : IPR[7][`FLAGS_COL2_INIT + `F_PENDING];
				IPR[7][`FLAGS_COL3_INIT + `F_PENDING] = ( CIS[`VECTPENDING] == 31 ) ? 0 : IPR[7][`FLAGS_COL3_INIT + `F_PENDING];
			
				interrupt_pending <= 0;
				vector_index <= 0;
			end

			//Se pedido ao CPU de interrup��o para ainda n�o foi efetuado...
			else
			begin
				//Indice do vetor de interrup��es atualizado com o n�mero da interrup��o que passou de
				// pending para ativa (deixado o indice zero para situa��o particular das interrup��es
				//	stacked).
				vector_index <= CIS[`VECTPENDING] + 1;
			
				//Se o endere�o da interrup��o foi lido do vetor de interrup��es � indicado a CPU que deve
				//	alterar o fluxo de execu��o para a nova interrup��o.
				if ( new_interrupt_address )
				begin
					interrupt_pending <= 1;
				end	
			end	
		end

		//Se uma interrup��o terminar e uma stacked for a mais priorit�ria das que aguardam entrar em
		//	execu��o...
		if ( reti_signal & CIS[`SB] & ( ~CIS[`PB] | (CIS[`PB] & ( g_priority[CIS[`VECTSTACKED]] <= g_priority[CIS[`VECTPENDING]]) ) ) )
		begin
			//Se a interrup��o foi recebida pelo CPU, esta � passada para ativa.
			if ( interrupt_ack & interrupt_pending )
			begin
				//Sinal de stacked da interrup��o que passou para ativa � limpo.
				IPR[0][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 0 ) ? 0 : IPR[0][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[0][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 1 ) ? 0 : IPR[0][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[0][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 2 ) ? 0 : IPR[0][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[0][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 3 ) ? 0 : IPR[0][`FLAGS_COL3_INIT + `F_STACKED];

				IPR[1][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 4 ) ? 0 : IPR[1][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[1][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 5 ) ? 0 : IPR[1][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[1][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 6 ) ? 0 : IPR[1][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[1][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 7 ) ? 0 : IPR[1][`FLAGS_COL3_INIT + `F_STACKED];

				IPR[2][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 8 ) ? 0 : IPR[2][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[2][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 9 ) ? 0 : IPR[2][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[2][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 10 ) ? 0 : IPR[2][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[2][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 11 ) ? 0 : IPR[2][`FLAGS_COL3_INIT + `F_STACKED];
							 
				IPR[3][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 12 ) ? 0 : IPR[3][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[3][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 13 ) ? 0 : IPR[3][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[3][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 14 ) ? 0 : IPR[3][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[3][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 15 ) ? 0 : IPR[3][`FLAGS_COL3_INIT + `F_STACKED];
							 
				IPR[4][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 16 ) ? 0 : IPR[4][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[4][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 17 ) ? 0 : IPR[4][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[4][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 18 ) ? 0 : IPR[4][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[4][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 19 ) ? 0 : IPR[4][`FLAGS_COL3_INIT + `F_STACKED];

				IPR[5][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 20 ) ? 0 : IPR[5][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[5][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 21 ) ? 0 : IPR[5][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[5][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 22 ) ? 0 : IPR[5][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[5][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 23 ) ? 0 : IPR[5][`FLAGS_COL3_INIT + `F_STACKED];
							 
				IPR[6][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 24 ) ? 0 : IPR[6][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[6][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 25 ) ? 0 : IPR[6][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[6][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 26 ) ? 0 : IPR[6][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[6][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 27 ) ? 0 : IPR[6][`FLAGS_COL3_INIT + `F_STACKED];
							 
				IPR[7][`FLAGS_COL0_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 28 ) ? 0 : IPR[7][`FLAGS_COL0_INIT + `F_STACKED];
				IPR[7][`FLAGS_COL1_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 29 ) ? 0 : IPR[7][`FLAGS_COL1_INIT + `F_STACKED];
				IPR[7][`FLAGS_COL2_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 30 ) ? 0 : IPR[7][`FLAGS_COL2_INIT + `F_STACKED];
				IPR[7][`FLAGS_COL3_INIT + `F_STACKED] = ( CIS[`ETMINTNUM] == 31 ) ? 0 : IPR[7][`FLAGS_COL3_INIT + `F_STACKED];

				//Informa��o da interrup��o ativa (n�mero e prioridade) no registo CIS.				
				CIS[`AB] = 1;
				CIS[`ETMINTNUM] = CIS[`VECTSTACKED];
				CIS[`CURRPRI] = ( g_priority[CIS[`ETMINTNUM]] );
				
				interrupt_pending <= 0;
				vector_index <= 0;
			end

			//Se CPU ainda n�o recebeu pedido de nova interrup��o...
			else
			begin	
				//� feito pedido de interrup��o ao CPU, sem envio do endere�o da interrup��o, uma vez que
				//	RETI recupera o fluxo de execu��o no ponto onde a interrup��o passou para stacked.
				interrupt_pending <= 1;
				vector_index <= 0;
			end
		end
	end
end
endmodule
