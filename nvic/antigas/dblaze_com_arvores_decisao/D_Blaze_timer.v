`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins   
// 
// Create Date:    14:18:57 03/17/2014 
// Design Name: 
// Module Name:    D_Blaze_timer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_timer(
    //inputs
	 input 			clock,
	 input 			reset,
	 input 			select,
	 input [31:0]	ctrl,
	 //outputs
	 output			ssr0,
	 output			ssr1,
	 output			ssr2,
	 output			ssr3,
	 output			ssr4,
	 output			ssr5,
	 output			ssr6,
	 output			ssr7
	 );

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [2:0] command;
wire [2:0] type;
wire [15:0] dec_output;

//////////////////////////////////////////////////////////////////////////////////
D_Blaze_ctrl_dec decoder(
   //inputs
	.ctrl(ctrl), 
	.select(select),
	//outputs 
	.command(command),
	.type(type),
	.dec_output(dec_output)
);

//////////////////////////////////////////////////////////////////////////////////
// Debug /////////////////////////////////////////////////////////////////////////
assign ssr0 = 0;
assign ssr1 = 0;
assign ssr2 = 0;
assign ssr3 = 0;
assign ssr4 = 0;
assign ssr5 = 0;
assign ssr6 = 0;
assign ssr7 = 0;

endmodule
