`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    14:35:48 02/14/2014 
// Design Name: 
// Module Name:    D_Blaze_hazard_unit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_hazard_unit(
	input  [20:0]  Stage2,
	input  [20:0] 	Stage3,
	input  [20:0]	Stage4,
	input  [20:0]	Stage5,
	output [9:0]	Forward_ctrl
    );

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [5:0] 	decode_opcode;									//
wire [4:0] 	decode_Rd;										//
wire [4:0] 	decode_Ra;										//
wire [4:0] 	decode_Rb;										//

wire [5:0] 	execute_opcode;								//
wire [4:0] 	execute_Rd;										//
wire [4:0] 	execute_Ra;										//
wire [4:0] 	execute_Rb;										//

wire [5:0] 	memory_opcode;									//
wire [4:0] 	memory_Rd;										//
wire [4:0] 	memory_Ra;										//
wire [4:0] 	memory_Rb;										//

wire [5:0] 	wb_opcode;										//
wire [4:0] 	wb_Rd;											//
wire [4:0] 	wb_Ra;											//
wire [4:0] 	wb_Rb;											//

wire 		 	Rd_eq_Rd_exec;									// Ra = Rd from stage execute
wire 		 	Rd_eq_Rd_mem;									// Ra = Rd from stage memory
wire 		 	Rd_eq_Rd_wb;									// Ra = Rd from stage write-back

wire 		 	Ra_eq_Rd_exec;									// Ra = Rd from stage execute
wire 		 	Ra_eq_Rd_mem;									// Ra = Rd from stage memory
wire 		 	Ra_eq_Rd_wb;									// Ra = Rd from stage write-back

wire 		 	Rb_eq_Rd_exec;									// Ra = Rd from stage execute
wire 		 	Rb_eq_Rd_mem;									// Ra = Rd from stage memory
wire 		 	Rb_eq_Rd_wb;									// Ra = Rd from stage write-back

wire [1:0]	Forward_ctrl_d;								// Auxiliary variable for final hazard signal
wire [1:0]	Forward_ctrl_a;								// Auxiliary variable for final hazard signal
wire [1:0]	Forward_ctrl_b;								// Auxiliary variable for final hazard signal

wire 			forward_exec_d;								// Auxiliary variable for final hazard signal
wire 			forward_mem_d;									// Auxiliary variable for final hazard signal
wire 			forward_wb_d;									// Auxiliary variable for final hazard signal

wire 			forward_exec_a;								// Auxiliary variable for final hazard signal
wire 			forward_mem_a;									// Auxiliary variable for final hazard signal
wire 			forward_wb_a;									// Auxiliary variable for final hazard signal

wire 			forward_exec_b;								// Auxiliary variable for final hazard signal
wire 			forward_mem_b;									// Auxiliary variable for final hazard signal
wire 			forward_wb_b;									// Auxiliary variable for final hazard signal


wire [1:0]	Hazard_load;									// 
wire [1:0]	Hazard_SP;										// 
//////////////////////////////////////////////////////////////////////////////////
// Decoding //////////////////////////////////////////////////////////////////////
assign decode_opcode = Stage2[20:15];

assign decode_Rd = Stage2[14:10];

assign decode_Ra = (decode_opcode  == `MOVI) ? 5'b0:
															  Stage2[9:5];
assign decode_Rb = Stage2[4:0];


assign execute_opcode = Stage3[20:15];

assign execute_Rd = Stage3[14:10];

assign execute_Ra = (execute_opcode  == `MOVI) ? 5'b0:
															    Stage3[9:5];

assign execute_Rb = Stage3[4:0];


assign memory_opcode = Stage4[20:15];

assign memory_Rd = Stage4[14:10];

assign memory_Ra = (memory_opcode  == `MOVI) ? 5'b0:
															  Stage4[9:5];

assign memory_Rb = Stage4[4:0];


assign execute_Rb = Stage3[4:0];


assign wb_opcode = Stage5[20:15];

assign wb_Rd = Stage5[14:10];

assign wb_Ra = (memory_opcode  == `MOVI) ? 5'b0:
														 Stage5[9:5];

assign wb_Rb = Stage5[4:0];

//////////////////////////////////////////////////////////////////////////////////
// Hazard signals ////////////////////////////////////////////////////////////////

// Stack Pointer hazards /////////////////////////////////////////////////////////
assign Hazard_SP = ((decode_opcode == `POP | decode_opcode == `PUSH | decode_opcode == `CALL | decode_opcode == `RET | decode_opcode == `RETI) & (execute_opcode == `MOV & execute_Rd == 5'h1f))	? 2'b01:
						 ((decode_opcode == `POP | decode_opcode == `PUSH | decode_opcode == `CALL | decode_opcode == `RET | decode_opcode == `RETI) & (memory_opcode == `MOV & memory_Rd == 5'h1f)) 		? 2'b10:
						 ((decode_opcode == `POP | decode_opcode == `PUSH | decode_opcode == `CALL | decode_opcode == `RET | decode_opcode == `RETI) & (wb_opcode == `MOV & wb_Rd == 5'h1f))					? 2'b11:
																																																																		2'b00;
 
// Write hazards /////////////////////////////////////////////////////////////////
assign Rd_eq_Rd_exec = ((execute_opcode != 0 & (decode_opcode == `SW | decode_opcode == `SWI | decode_opcode == `SWBI | decode_opcode == `NOT | decode_opcode == `PUSH | decode_opcode == `INPUT))	 & (execute_Rd == decode_Rd ) & execute_Rd !=0)		? 1'b1:
																																																																																		  1'b0;
assign Rd_eq_Rd_mem = ((memory_opcode != 0 & (decode_opcode == `SW | decode_opcode == `SWI | decode_opcode == `SWBI | decode_opcode == `NOT | decode_opcode == `PUSH | decode_opcode == `INPUT)) & (memory_Rd == decode_Rd ) & memory_Rd !=0)			? 1'b1:
																																																																																		  1'b0;
assign Rd_eq_Rd_wb = ((wb_opcode != 0 & (decode_opcode == `SW | decode_opcode == `SWI | decode_opcode == `SWBI | decode_opcode == `NOT | decode_opcode == `PUSH | decode_opcode == `INPUT)) & (wb_Rd == decode_Rd ) & wb_Rd !=0)   						 	? 1'b1:
																																																																																		  1'b0;	
// Load hazards //////////////////////////////////////////////////////////////////
assign Hazard_load = ((execute_opcode == `LB | execute_opcode == `LW | execute_opcode == `LWI | execute_opcode == `LWBI | execute_opcode == `POP) & (Rd_eq_Rd_exec | Ra_eq_Rd_exec | Rb_eq_Rd_exec))																									? 2'b01:
							((memory_opcode == `LB | memory_opcode == `LW | memory_opcode == `LWI | memory_opcode == `LWBI | memory_opcode == `POP) & (Rd_eq_Rd_mem | Ra_eq_Rd_mem | Rb_eq_Rd_mem)) 	  																										? 2'b10:
							((decode_opcode == `LWBI | decode_opcode == `LW | decode_opcode == `LB | decode_opcode == `LWI | decode_opcode == `POP) & (execute_opcode == `PUSH | execute_opcode == `SW | execute_opcode == `SWI | execute_opcode == `SB | execute_opcode == `SWBI)) 	? 2'b11:
																																																																																										  2'b00;
																																
//////////////////////////////////////////////////////////////////////////////////
																																 
assign Ra_eq_Rd_exec = ((decode_opcode != 0 & execute_opcode != 0 & (execute_opcode != `SW & execute_opcode != `SWI & execute_opcode != `SWBI & execute_opcode != `PUSH)) & (execute_Rd == decode_Ra ) & execute_Rd !=0) 		? 1'b1:
																																																																										  1'b0;																																					 																											  
assign Ra_eq_Rd_mem = ((decode_opcode != 0 & memory_opcode != 0 & (memory_opcode != `SW & memory_opcode != `SWI & memory_opcode != `SWBI & memory_opcode != `PUSH)) & (memory_Rd == decode_Ra ) & memory_Rd !=0) 					? 1'b1:
																																																																										  1'b0;
assign Ra_eq_Rd_wb = ((decode_opcode != 0 & wb_opcode != 0 & (wb_opcode != `SW & wb_opcode != `SWI & wb_opcode != `SWBI & wb_opcode != `PUSH)) & (wb_Rd == decode_Ra ) & wb_Rd !=0) 														? 1'b1:
																																																																										  1'b0;
																																															  
assign Rb_eq_Rd_exec = ((decode_opcode != 0 & execute_opcode != 0 & (execute_opcode != `BEQ & execute_opcode != `SW & execute_opcode != `SWI & execute_opcode != `SWBI & execute_opcode != `PUSH)) & (execute_Rd == decode_Rb ) & execute_Rd !=0) 		? 1'b1:
																																																																																		  1'b0;																																						
assign Rb_eq_Rd_mem = ((decode_opcode != 0 & memory_opcode != 0 & (memory_opcode != `SW & memory_opcode != `SWI & memory_opcode != `SWBI & memory_opcode != `PUSH)) & (memory_Rd == decode_Rb ) & memory_Rd !=0) 													? 1'b1:
																																																																																		  1'b0;																											  																																					 
assign Rb_eq_Rd_wb = ((decode_opcode != 0 & wb_opcode != 0 & (wb_opcode != `SW & wb_opcode != `SWI & wb_opcode != `SWBI & wb_opcode != `PUSH)) & (wb_Rd == decode_Rb ) & wb_Rd !=0) 																						? 1'b1:
																																																																																		  1'b0;
																																								 
//////////////////////////////////////////////////////////////////////////////////
assign forward_exec_d = (Rd_eq_Rd_exec);
assign forward_mem_d = (Rd_eq_Rd_mem);
assign forward_wb_d = (Rd_eq_Rd_wb);

assign forward_exec_a = (Ra_eq_Rd_exec);
assign forward_mem_a = (Ra_eq_Rd_mem);
assign forward_wb_a = (Ra_eq_Rd_wb);
	
assign forward_exec_b = (Rb_eq_Rd_exec);
assign forward_mem_b = (Rb_eq_Rd_mem);
assign forward_wb_b = (Rb_eq_Rd_wb);

//////////////////////////////////////////////////////////////////////////////////
assign Forward_ctrl_d = (forward_exec_d) 										? 2'b01:
								(forward_mem_d) 										? 2'b10:	
								(forward_wb_d)											? 2'b11:
																							  2'b00;
																							  
assign Forward_ctrl_a = (forward_exec_a) 										? 2'b01:
								(forward_mem_a) 										? 2'b10:	
								(forward_wb_a)											? 2'b11:
																							  2'b00;
																							  
assign Forward_ctrl_b = (forward_exec_b) 										? 2'b01:
								(forward_mem_b) 										? 2'b10:	
								(forward_wb_b)											? 2'b11:
																							  2'b00;

//////////////////////////////////////////////////////////////////////////////////
// Final hazard signal ///////////////////////////////////////////////////////////																							  
assign Forward_ctrl = {Hazard_SP, Hazard_load, Forward_ctrl_d, Forward_ctrl_a, Forward_ctrl_b};																  

//////////////////////////////////////////////////////////////////////////////////
endmodule
