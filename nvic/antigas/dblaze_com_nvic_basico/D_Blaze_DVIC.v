`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins   
// 
// Create Date:    14:24:49 03/17/2014 
// Design Name: 
// Module Name:    D_Blaze_DVIC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_DVIC(
    //inputs
	 input 			clock,
	 input 			reset,
	 input [31:0]	ctrl,
	 input 			select,
	 input			reti_signal,
	 input [31:0]	interrupt_request,
	 input			interrupt_ack,
	 //outputs
	 output [11:0] interrupt_address,
	 output reg		interrupt_enable
	 );

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [2:0] 	command;
wire [2:0] 	type;
wire [15:0] dec_output;

reg  [4:0] 	vector_index;					// 
wire [4:0]	decision_tree;					//Resultado da �rvore de decis�o.

vector_table interrupt_table(
 .clka(clock),
 .rsta(reset),
 .ena(1'b1),
 .addra(vector_index),						//Depois ser� o sinal interrupt que indicar� qual a interrup��o despoletada.
 .douta(interrupt_address)
);

//Assigns com �rvore de decis�o.
assign decision_tree = 4;

//
always@(clock)
begin
	vector_index = decision_tree;
	interrupt_enable = 1;
	
	if ( interrupt_enable && interrupt_ack )
		interrupt_enable = 0;
end

//////////////////////////////////////////////////////////////////////////////////
D_Blaze_ctrl_dec decoder(
   //inputs
	.ctrl(ctrl), 
	.select(select),
	//outputs 
	.command(command),
	.type(type),
	.dec_output(dec_output)
);

endmodule
