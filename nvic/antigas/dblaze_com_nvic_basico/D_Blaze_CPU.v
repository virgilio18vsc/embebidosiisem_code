`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins
// 
// Create Date:    14:24:05 10/23/2013 
// Design Name: 
// Module Name:    D-Blaze_CPU 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module D_Blaze_CPU(
	//inputs
	input 				clock,
	input 				reset,
	input 	[7:0] 	in_pins,
	input 				interrupt_enable,
	input 	[31:0]	interrupt_address,
	//outputs
	output [26:0]	peracc,
	output reti_signal,
	output interrupt_ack
    );

//////////////////////////////////////////////////////////////////////////////////
// Variables /////////////////////////////////////////////////////////////////////
wire [31:0]  			 instruction;									// Current instruction
wire [`INST_MEM-1:0]  pc_value;										// Current PC value


wire [5:0]				 opcode;											// Current opcode
wire [`REG_SIZE-1:0]  RF_out_a;										// Register-File - Register A
wire [`REG_SIZE-1:0]  RF_out_b;										// Register-File - Register B
wire [`REG_SIZE-1:0]  RF_out_d;										// Register-File - Register D
wire [31:0]  			 Imm;												// Immediate
wire [4:0]	 			 Rd;												// Register for hazard unit
wire [4:0]	 			 Ra;												// Register for hazard unit											
wire [4:0]	 			 Rb;												//	Register for hazard unit


wire [4:0]	 			 RF_addr;										// Register-File write address
wire [31:0]	 			 RF_data;										// Register-File write data
wire [31:0]	 			 execute_output;								//	Output from execute stage
wire [31:0]  		 	 data_mem_out;									// Output from Data Memory

	
wire [43:0]  			 buf1;											// Buffer 1
wire [149:0] 			 buf2;											// Buffer 2
wire [86:0]  			 buf3;											// Buffer 3
wire [52:0]  			 buf4;											// Buffer 4

wire [1:0]				 CMP_output;									// Output from Comparator
wire [1:0]	 			 jump_signal;									// Auxiliary variable for branches/jumps
wire [31:0]  			 jump_value;									//	Value of the branches/jumps
	
wire [31:0]  			 forward_execute_output;					//	Auxiliary variable for Load 
wire 			 			 load_signal;									// Auxiliary varibale for Load

wire 						 barrel_dir;									// Barrel Shifter direction (0 - Right  1 - Left)

wire [9:0]				 Forward_ctrl;									// Control signal for data forwarding
wire [31:0]				 data_forward_exec; 							//	Data forwarded from execute stage
wire [31:0]				 data_forward_mem; 							// Data forwarded from memory stage

wire 			 			 ret_signal;									// Auxiliary varibale for RET
wire [11:0]				 pc_restore;									// Value for RET


//////////////////////////////////////////////////////////////////////////////////
// Fecth Stage ///////////////////////////////////////////////////////////////////

D_Blaze_fetch	FETCH ( 
	//inputs
	.clock(clock), 
	.reset(reset),
	.jump_signal(jump_signal),
	.jump_value(jump_value),
	.pc_restore(pc_restore),
	.interrupt_enable(interrupt_enable),
	.interrupt_address(interrupt_address),
	//outputs
	.instruction(instruction),
	.pc_value(pc_value),
	.interrupt_ack(interrupt_ack)
);

//////////////////////////////////////////////////////////////////////////////////
// Buffer 1 //////////////////////////////////////////////////////////////////////
D_Blaze_buf1 buffer1 (
	.clock(clock),
	.reset(reset),
	.in({pc_value, instruction}),
	.out(buf1)
);

//////////////////////////////////////////////////////////////////////////////////
// Decode Stage //////////////////////////////////////////////////////////////////
D_Blaze_decode	DECODE (
	//inputs
	.clock(clock), 
	.reset(reset),
	.instruction(buf1[31:0]),
	.R_addr(buf4[36:32]),
	.R_data(buf4[31:0]),
	.Forward_ctrl(Forward_ctrl),
	.data_forward_exec(data_forward_exec),
	.data_forward_mem(data_forward_mem),
	.load_signal(load_signal),
	.pc_value(buf1[43:32]),
	.in_pins(in_pins),
	//outputs
	.opcode(opcode),
	.RF_out_a(RF_out_a),
	.RF_out_b(RF_out_b),
	.RF_out_d(RF_out_d),
	.Imm(Imm),	
	.Rd(Rd),
	.Ra(Ra),
	.Rb(Rb),
	.jump_signal(jump_signal),
	.jump_value(jump_value),
	.barrel_dir(barrel_dir),
	.peracc(peracc),
	.reti_signal(reti_signal)
);

//////////////////////////////////////////////////////////////////////////////////
// Buffer 2 //////////////////////////////////////////////////////////////////////
D_Blaze_buf2 buffer2 (
	.clock(clock),
	.reset(reset),
	.in({Ra, Rb, barrel_dir, RF_out_d, opcode, RF_out_a, RF_out_b, Rd, Imm}),
	.out(buf2)
);

//////////////////////////////////////////////////////////////////////////////////
// Execute Stage /////////////////////////////////////////////////////////////////
D_Blaze_execute EXECUTE (
	//inputs
	.clock(clock),
	.reset(reset), 
	.opcode(buf2[106:101]),
	.RF_out_a(buf2[100:69]),
	.RF_out_b(buf2[68:37]),
	.Imm(buf2[31:0]),
	.barrel_dir(buf2[139]),
	.Forward_ctrl(Forward_ctrl),
	.RF_out_d(buf2[138:107]),
	.Rd(buf2[36:32]),
	//outputs
	.execute_output(execute_output),
	.forward_execute_output(forward_execute_output),
	.load_signal(load_signal),
	.data_forward_exec(data_forward_exec),
	.CMP_output(CMP_output),
	.ret_signal(ret_signal)
);

//////////////////////////////////////////////////////////////////////////////////
// Buffer 3 //////////////////////////////////////////////////////////////////////
D_Blaze_buf3 buffer3 (
	.clock(clock),
	.reset(reset),
	.in({CMP_output, buf2[149:145]/*Ra*/, buf2[144:140]/*Rb*/, buf2[138:107]/*RF_out_d*/, buf2[36:32]/*Rd*/, buf2[106:101]/*opcode*/, execute_output}),
	.out(buf3)
);

//////////////////////////////////////////////////////////////////////////////////
// Memory Acess Stage ////////////////////////////////////////////////////////////
D_Blaze_memory_access MEMORY_ACCESS (
	//inputs
	.clock(clock),
	.reset(reset), 
	.opcode(buf3[37:32]),
	.execute_output(buf3[31:0]),
	.Rd(buf3[42:38]),
	.RF_out_d(buf3[74:43]),
	.forward_execute_output(forward_execute_output),
	.load_signal(load_signal),
	.Forward_ctrl(Forward_ctrl),
	.ret_signal(ret_signal),
	.CMP_output(buf3[86:85]),
	//outputs
	.RF_data(RF_data),
	.RF_addr(RF_addr),
	.data_forward_mem(data_forward_mem),
	.pc_restore(pc_restore)
);

//////////////////////////////////////////////////////////////////////////////////
// Buffer 4 //////////////////////////////////////////////////////////////////////
D_Blaze_buf4 buffer4 (
	.clock(clock),
	.reset(reset),
	.in({buf3[37:32], buf3[84:80] , buf3[79:75], RF_addr, RF_data}),
	.out(buf4)
);

//////////////////////////////////////////////////////////////////////////////////
// Hazard unit ///////////////////////////////////////////////////////////////////
D_Blaze_hazard_unit hazard_unit(
	//inputs
	.Stage2({buf1[31:26], buf1[25:21], buf1[20:16], buf1[15:11]}),
	.Stage3({buf2[106:101], buf2[36:32], buf2[149:145], buf2[144:140]}),
	.Stage4({buf3[37:32], buf3[42:38], buf3[84:80], buf3[79:75]}),
	.Stage5({buf4[52:47], buf4[36:32], buf4[46:42], buf4[41:37]}),
	//outputs
	.Forward_ctrl(Forward_ctrl)
); 

endmodule
