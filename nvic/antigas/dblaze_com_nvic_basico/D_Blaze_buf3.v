`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    21:18:27 01/14/2014 
// Design Name: 
// Module Name:    D_Blaze_buf3 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_buf3(
    input clock,
	 input reset,
	 input [86:0] in,
	 output reg [86:0] out
	 );

always@(posedge clock)
begin
	if(reset)
		out <= 0;
	else
		out <= in;
end

endmodule
