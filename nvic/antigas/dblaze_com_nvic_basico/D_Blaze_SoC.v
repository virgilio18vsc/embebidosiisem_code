`timescale 1ns / 1ps
`include "defines.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    14:23:49 10/23/2013 
// Design Name: 
// Module Name:    D-Blaze_SoC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_SoC(
   //inputs
	input 			clock,
	input 			rst,
	input 	[7:0] in_pins,
	input		[4:0] buttons,
	//outputs
	output 	[7:0] out_pins
);

//////////////////////////////////////////////////////////////////////////////////
// Local variables ///////////////////////////////////////////////////////////////
wire [15:0] select;									// Select Peripheric
wire [26:0] peracc;									// Peripheric Access
wire [31:0] ctrl;										// Control word from control bus
wire reset;												// Reset signal for CPU
wire reti_signal;
wire interrupt_enable;
wire [31:0] interrupt_address;
wire [31:0] interrupt_request;

assign reset = !rst;									// Virtex 5 reset is always 1
//////////////////////////////////////////////////////////////////////////////////
// CPU ///////////////////////////////////////////////////////////////////////////
D_Blaze_CPU CPU(
	//inputs
	.clock(clock),
	.reset(reset),
	.in_pins(in_pins),
	.interrupt_enable(interrupt_enable),
	.interrupt_address(interrupt_address),
	//outputs
	.peracc(peracc),
	.reti_signal(reti_signal),
	.interrupt_ack(interrupt_ack)
);

//////////////////////////////////////////////////////////////////////////////////
// Control bus ///////////////////////////////////////////////////////////////////
D_Blaze_ctrl_enc control_enc(
    //inputs
	 .clock(clock), 
	 .reset(reset),
	 .peracc(peracc),
	 //outputs	 
	 .ctrl(ctrl), 
	 .select(select)
);

//////////////////////////////////////////////////////////////////////////////////
// Pario /////////////////////////////////////////////////////////////////////////
D_Blaze_pario pario(
   //inputs
	.clock(clock), 
	.reset(reset),
	.select(select[0]),
	.ctrl(ctrl),
	//outputs
	.out_pins(out_pins)
);

//////////////////////////////////////////////////////////////////////////////////
// Timers ////////////////////////////////////////////////////////////////////////
D_Blaze_timer timer(
	//inputs
	.clock(clock), 
	.reset(reset),
	.select(select[1]),
	//outputs
	.ssr0(interrupt_request[0]),
	.ssr1(interrupt_request[1]),
	.ssr2(interrupt_request[2]),
	.ssr3(interrupt_request[3]),
	.ssr4(interrupt_request[4]),
	.ssr5(interrupt_request[5]),
	.ssr6(interrupt_request[6]),
	.ssr7(interrupt_request[7])
);
	 
//////////////////////////////////////////////////////////////////////////////////
// DVIC //////////////////////////////////////////////////////////////////////////
D_Blaze_DVIC DVIC(
	//inputs
	.clock(clock), 
	.reset(reset),
	.ctrl(ctrl),
	.select(select[2]),
	.reti_signal(reti_signal),
	.interrupt_request(interrupt_request),
	.interrupt_ack(interrupt_ack),
	//outputs
	.interrupt_address(interrupt_address),
	.interrupt_enable(interrupt_enable)
);


endmodule
