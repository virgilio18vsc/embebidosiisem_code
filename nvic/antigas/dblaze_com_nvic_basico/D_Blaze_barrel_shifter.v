`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:40:40 02/10/2014 
// Design Name: 
// Module Name:    D_Blaze_barrel_shifter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_barrel_shifter(
	input [31:0]	in,
	input [4:0]		rot,
	input 			dir,
	output [31:0]	out
	);


assign out = (dir) ? (in << rot) : (in >> rot) ;
							

endmodule

