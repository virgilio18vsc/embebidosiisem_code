`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ESRG
// Engineer: Jo�o Martins  
// 
// Create Date:    14:22:04 10/30/2013 
// Design Name: 
// Module Name:    D_Blaze_register_file 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_Blaze_register_file(
   input 			clock,
	input 			reset,
	input				wr_en,
	input	 [31:0]	data_ina,
	input	 [11:0]	data_in_stack,
	input				stack_signal,
   input  [4:0]	addra,
	input  [4:0]	addrb,
	input  [4:0]	addrd,	
	input  [4:0]	addr_write,
	output [31:0]	data_outa,
	output [31:0]	data_outb,
	output [31:0]	data_outd
	);

reg [31:0]	register_file [31:0];
reg [4:0] 	i;


always@(posedge clock)
begin
	if(reset)
		begin
			for (i = 0; i < 31; i = i + 1)
			register_file[i] <= 0;						
			register_file[31] <= 32'hFFF;			// Top of stack in RAM						
		end
	if(wr_en)
		begin
			register_file[addr_write] <= data_ina;	// Write
		end
	if(stack_signal)
		begin
			register_file[31] <= data_in_stack;	// Write stack
		end
end	

assign data_outa = register_file[addra];
assign data_outb = register_file[addrb];
assign data_outd = register_file[addrd];

endmodule
