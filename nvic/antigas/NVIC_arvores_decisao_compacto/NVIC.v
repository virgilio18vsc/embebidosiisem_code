`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:15:40 05/13/2014 
// Design Name: 
// Module Name:    NVIC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module NVIC(
		clock,
		reset,
		ctrl,
		select,
		reti_signal,
		interrupt_request,
		interrupt_ack,
		interrupt_address,
		interrupt_enable
    );

parameter N_INTERRUPTS = 32; //Tamanho do Vector de interrup��es (Recomendado tamanho de base 2)
parameter N_BITS = 5; //Numero de bits minimo capaz de representar o n�mero de interrup��es (Ex: 32 Interrup��es => 5 Bits)

/***********************************************************************************************************************
/*********** Sinais de Input
/***********************************************************************************************************************/	 	 
input clock;
input reset;
input [31:0] ctrl;
input select;
input reti_signal;
input [N_INTERRUPTS - 1:0] interrupt_request;
input interrupt_ack;

/***********************************************************************************************************************
/*********** Sinais de Output
/***********************************************************************************************************************/	
output [11:0] interrupt_address;
output interrupt_enable;

/***********************************************************************************************************************
/*********** Registo IPR(Interrupt Priority Registers)
/***********************************************************************************************************************/	 
reg [7:0] IPR [(N_INTERRUPTS/4) - 1:0][3:0];	//Guarda o valor das prioridade de grupo e sub-grupo por cada interrup��o
															//Prioridade de 0 a 31, sendo 0 a de maior prioridade e 31 a de menor (MSB).
															//Guarda os valores das flags de estado da interrup��o (ENABLED, PENDING, STACKED)

//Posicionamento dos registos de prioridade(Grupo e Sub-Grupo)
//PRI_<Tipo>_P<Numero de grupos de prioridade>_S<Numero de sub-grupos de prioridades>
//Tipo:
//->G	:	Grupos de prioridades
//->SG:	Subgrupos de prioridades

`define PRI_G_P32_S0		4:0

`define PRI_G_P16_S2		4:1
`define PRI_SG_P16_S2	0

`define PRI_G_P8_S4		4:2
`define PRI_SG_P8_S4		1:0

`define PRI_G_P4_S8		4:3
`define PRI_SG_P4_S8		2:0

`define PRI_G_P2_S16		4
`define PRI_SG_P2_S16	3:0

`define PRI_SG_P0_S32	4:0


//Posi��o das flags de estado das interrup��es
`define F_FLAGS			7:5

`define	F_ENABLE 		5
`define	F_PENDING 		6
`define	F_STACKED 		7


/***********************************************************************************************************************
/*********** Registo AIRCR(Application Interrupt and Reset Control Register)
/***********************************************************************************************************************/	
reg [31:0] AIRCR; 
`define PRIGROUP	2:0	//Cursor de divis�o dos registos PRI em prioridades e sub-prioridades

//Configura��es do cursor de divisao dos registos PRI em prioridades e sub-prioridades
`define PRIO_P32_S0 		3'b000		//32 Prioridades e 0 Sub-prioridades
`define PRIO_P16_S2 		3'b001		//16 Prioridades e 2 Sub-prioridades
`define PRIO_P8_S4		3'b010		//8 Prioridades e 4 Sub-prioridades
`define PRIO_P4_S8 		3'b011		//4 Prioridades e 8 Sub-prioridades
`define PRIO_P2_S16 		3'b100		//2 Prioridades e 16 Sub-prioridades
`define PRIO_P0_S32 		3'b101		//0 Prioridades e 32 Sub-prioridades


/***********************************************************************************************************************
/*********** Registo STIR (Software Trigger Interrupt Register)
/***********************************************************************************************************************/	 
wire [31:0] STIR;		//Gera interrup��es por software
						
`define INTID	N_BITS - 1:0	//INTID ID da interrup��o para despoletar (0-n_interrup��o) (W).
										//Por exemplo, escrever 0x03 vai gerar a IRQ3 por software.
							
//[31:5]Reservado: l�-se '0'; deve ser escrito com '0' (R).


						
/***********************************************************************************************************************
/*********** Registo  CIS(Current Interrupt State)
/***********************************************************************************************************************/	
reg [31:0] CIS;	

`define ETMINTSTAT	1:0	//Indica o estado da interrup��o no ciclo atual (R) -> Implementar no bloco always
									//00 - Sem estado
									//01 - Entrada na interrup��o
									//10 - Sa�da da interrup��o
									//11 - Fazer o stacking e o fetching do vetor de interrup��o. 
									//O ETMINTSTAT entrada/retorno � atribu�do no 1� ciclo do novo contexto da interrup��o.

`define RESERVED_CIS	7:2	//[6:2] -> Reservado

`define PB				8		//Indica se existe uma interrup��o pending.

`define VECTPENDING	13:9	//Indica o n�mero da interrup��o pendente de maior prioridade(R) -> Implementar no bloco always

`define SB				14		//Indica se existe uma interrup��o stacked.

`define VECTSTACKED	19:15	//Indica o n�mero da interrup��o stacked de maior prioridade(R) -> Implementar no bloco always

`define AB				20		//Indica se existe uma interrup��o ativa.

`define ETMINTNUM		25:21	//Indica o n�mero da interrup��o em execu��o (0-31) (R) -> Implementar no bloco always

`define CURRPRI 		31:26 //Indica a prioridade da interrup��o em execu��o. Configurada nos registos IPR (R) -> Implementar no bloco always


/***********************************************************************************************************************
/*********** Selecc�o das Grupo/SubGrupo de prioridades
/***********************************************************************************************************************/	
wire [4:0] g_priority [N_INTERRUPTS - 1:0];	//Prioridades de grupo

wire [4:0] sg_priority [N_INTERRUPTS - 1:0];	//Prioridades de sub-grupo

/*Cria os multiplexers para selec��o do nivel de grupo e sub-grupo de prioridades
do registo IPR em fun��o da configura��o do campo PRIGROUP no registo AIRCR*/
generate
	genvar i_mux;						
	for (i_mux = 0; i_mux < N_INTERRUPTS; i_mux = i_mux + 1)
		begin: l_p_mux
			assign {g_priority[i_mux], sg_priority[i_mux]} = 
					(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P32_S0], 5'b00000																					}	:	
					(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P16_S2],	4'b0000,	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P16_S2]	}	:	
					(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P8_S4], 	3'b000, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P8_S4]	}	:
					(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P4_S8], 	2'b00, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P4_S8]	}	:
					(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_G_P2_S16],	1'b0, 	IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P2_S16]	}	:
					(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,																								IPR[ (i_mux>>2) & 3'b111 ][ i_mux & 2'b11 ][`PRI_SG_P0_S32]	}	:
					10'b0000000000;
		end
endgenerate


/***********************************************************************************************************************
/*********** Arvore de decis�o de pending 
/***********************************************************************************************************************/	
wire [N_BITS-1:0] decision_t_pend [N_INTERRUPTS - 2:0]; //Wire para cria��o da arvore de decis�o de pending

/*Cria os toda a arvore de decis�o de pending*/
generate
	genvar i_dt_pend;
	assign decision_t_pend[0] = (	{~IPR[0][0][`F_ENABLE], ~IPR[0][0][`F_PENDING], g_priority[0], sg_priority[0]} <= 
								{~IPR[0][1][`F_ENABLE], ~IPR[0][1][`F_PENDING], g_priority[1], sg_priority[1]})	
								?	5'd0	:	5'd1;//Gera comparadores para comparar a 1� e 2� interrup��o
								
	for (i_dt_pend = 1; i_dt_pend < N_INTERRUPTS - 1; i_dt_pend = i_dt_pend + 1)//Gera comparadores para comparar todas as outras interrup��es
		begin: l_d_t_pend
			assign decision_t_pend[i_dt_pend] = (	{	~IPR[ decision_t_pend[i_dt_pend - 1][N_BITS-1:2] ][ decision_t_pend[i_dt_pend - 1][1:0] ][ `F_ENABLE ], 
																	~IPR[ decision_t_pend[i_dt_pend - 1][N_BITS-1:2] ][ decision_t_pend[i_dt_pend - 1][1:0] ][ `F_PENDING ], 
																	g_priority[ decision_t_pend[i_dt_pend - 1] ], 
																	sg_priority[ decision_t_pend[i_dt_pend - 1] ]
																} <= 
																{	~IPR[ (i_dt_pend + 1) >> 2 ][ (i_dt_pend + 1) & 2'b11 ][ `F_ENABLE ], 
																	~IPR[ (i_dt_pend + 1) >> 2 ][ (i_dt_pend + 1) & 2'b11 ][ `F_PENDING ], 
																	g_priority[ i_dt_pend + 1 ], 
																	sg_priority[ i_dt_pend + 1 ]
																})	
																?	decision_t_pend[i_dt_pend - 1]	:	i_dt_pend + 1;
		end
endgenerate


/***********************************************************************************************************************
/*********** Arvore de decis�o de stacked
/***********************************************************************************************************************/	
wire [N_BITS-1:0] decision_t_stack [N_INTERRUPTS - 2:0];	//Wire para cria��o da arvore de decis�o de Stacked

/*Cria os toda a arvore de decis�o de stacked*/
generate
	genvar i_dt_stack;
	assign decision_t_stack[0] = (	{~IPR[0][0][`F_ENABLE], ~IPR[0][0][`F_STACKED], g_priority[0], sg_priority[0]} <= 
												{~IPR[0][1][`F_ENABLE], ~IPR[0][1][`F_STACKED], g_priority[1], sg_priority[1]})	
												?	5'd0	:	5'd1;////Gera comparadores para comparar a 1� e 2� interrup��o
								
	for (i_dt_stack = 1; i_dt_stack < N_INTERRUPTS - 1; i_dt_stack = i_dt_stack + 1)//Gera comparadores para comparar todas as outras interrup��es
		begin: l_d_t_stack
			assign decision_t_stack[i_dt_stack] = (	{	~IPR[ decision_t_stack[i_dt_stack - 1][N_BITS-1:2] ][ decision_t_stack[i_dt_stack - 1][1:0] ][ `F_ENABLE ], 
																		~IPR[ decision_t_stack[i_dt_stack - 1][N_BITS-1:2] ][ decision_t_stack[i_dt_stack - 1][1:0] ][ `F_STACKED ], 
																		g_priority[ decision_t_stack[i_dt_stack - 1] ], 
																		sg_priority[ decision_t_stack[i_dt_stack - 1] ]
																	} <= 
																	{	~IPR[ (i_dt_stack + 1) >> 2 ][ (i_dt_stack + 1) & 2'b11 ][ `F_ENABLE ], 
																		~IPR[ (i_dt_stack + 1) >> 2 ][ (i_dt_stack + 1) & 2'b11 ][ `F_STACKED ], 
																		g_priority[ i_dt_stack + 1 ], 
																		sg_priority[ i_dt_stack + 1 ]
																	})	
																	?	decision_t_stack[i_dt_stack - 1]	:	i_dt_stack + 1;
		end
endgenerate


initial
 begin
	IPR[0][0] = 8'hff;
	IPR[0][1] = 8'hff;
	IPR[0][2] = 8'hff;
	IPR[0][3] = 8'hff;
	
	IPR[1][0] = 8'hff;
	IPR[1][1] = 8'hff;
	IPR[1][2] = 8'hff;
	IPR[1][3] = 8'hff;
	
	IPR[2][0] = 8'hff;
	IPR[2][1] = 8'hff;
	IPR[2][2] = 8'hff;
	IPR[2][3] = 8'hff;
	
	IPR[3][0] = 8'hff;
	IPR[3][1] = 8'hff;
	IPR[3][2] = 8'hff;
	IPR[3][3] = 8'hff;
	
	IPR[4][0] = 8'hff;
	IPR[4][1] = 8'hff;
	IPR[4][2] = 8'hff;
	IPR[4][3] = 8'hff;
	
	IPR[5][0] = 8'hff;
	IPR[5][1] = 8'hff;
	IPR[5][2] = 8'hff;
	IPR[5][3] = 8'hff;
	
	IPR[6][0] = 8'hff;
	IPR[6][1] = 8'hff;
	IPR[6][2] = 8'hff;
	IPR[6][3] = 8'hff;
	
	
	IPR[7][0] = 8'hff;
	IPR[7][1] = 8'hff;
	IPR[7][2] = 8'hff;
	IPR[7][3] = 8'hff;
	
	AIRCR = `PRIO_P32_S0;
end

		
endmodule
