`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:02:20 05/14/2014
// Design Name:   NVIC
// Module Name:   C:/prog/prog_xilinx/Projecto/NVIC/NVIC_V0/NVIC/NVIC_test.v
// Project Name:  NVIC
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: NVIC
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module NVIC_test;

	// Inputs
	reg clock;
	reg reset;
	reg [31:0] ctrl;
	reg select;
	reg reti_signal;
	reg [31:0] interrupt_request;
	reg interrupt_ack;

	// Outputs
	wire [11:0] interrupt_address;
	wire interrupt_enable;

	// Instantiate the Unit Under Test (UUT)
	NVIC uut (
		.clock(clock), 
		.reset(reset), 
		.ctrl(ctrl), 
		.select(select), 
		.reti_signal(reti_signal), 
		.interrupt_request(interrupt_request), 
		.interrupt_ack(interrupt_ack), 
		.interrupt_address(interrupt_address), 
		.interrupt_enable(interrupt_enable)
	);

	initial begin
		// Initialize Inputs
		clock = 0;
		reset = 0;
		ctrl = 0;
		select = 0;
		reti_signal = 0;
		interrupt_request = 0;
		interrupt_ack = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

