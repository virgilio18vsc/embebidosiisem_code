`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:15:40 05/13/2014 
// Design Name: 
// Module Name:    NVIC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module NVIC(
		clock,
		reset,
		ctrl,
		select,
		reti_signal,
		interrupt_request,
		interrupt_ack,
		interrupt_address,
		interrupt_enable
    );
	 
/***********************************************************************************************************************
/*********** Sinais de Input
/***********************************************************************************************************************/	 	 
input clock;
input reset;
input [31:0] ctrl;
input select;
input reti_signal;
input [31:0] interrupt_request;
input interrupt_ack;

/***********************************************************************************************************************
/*********** Sinais de Output
/***********************************************************************************************************************/	
output [11:0] interrupt_address;
output interrupt_enable;


/***********************************************************************************************************************
/*********** Registo ISR(Interrupt Set Enable Register) Nota: mapeado registo IPR
/***********************************************************************************************************************/	 
/*reg [31:0] ISR;	//Habilita determinadas interrup��es atrav�s do �ndice escolhido.
						//sub-registo [31:0] SETENA(R/W)
						//Bits de ativa��o de interrup��es (Um por cada interrup��o).
						//1 - Ativa interrup��o
						//0 - Desativa interrup��o
*/

/***********************************************************************************************************************
/*********** Registo ISPR(Interrupt Set Pending Register) Nota: mapeado registo IPR
/***********************************************************************************************************************/	 
/*reg [31:0] ISPR;	//For�a determinadas interrup��es a ficarem pendentes.
						//sub-registo [31:0] SETPEND(R/W)
						//Bits de controlo de pending.
						//1 - For�a a interrup��o a ficar pendente
						//0 - For�a a interrup��o a ficar inativa 
						//Escrever em SETPEND quando a interrup��o est� ativa ou stacked n�o tem efeito.
*/

/***********************************************************************************************************************
/*********** Registo IPR(Interrupt Priority Registers)
/***********************************************************************************************************************/	 
reg [31:0] IPR [7:0];	//Guarda o valor das prioridade de grupo e sub-grupo por cada interrup��o
								//Prioridade de 0 a 31, sendo 0 a de maior prioridade e 31 a de menor (MSB).
								//Guarda os valores das flags de estado da interrup��o (ENABLED, PENDING, STACKED)


//Posicionamento dos registos de prioridade(Grupo e Sub-Grupo)
//PRI_COL<Numero da coluna da matriz_G_P<Numero de grupos de prioridade>_S<Numero de sub-grupos de prioridades>
//->G	:	Grupos de prioridades
//->SG:	Subgrupos de prioridades

//Coluna 0
`define PRI_COL0_G_P32_S0		4:0

`define PRI_COL0_G_P16_S2		4:1
`define PRI_COL0_SG_P16_S2		0

`define PRI_COL0_G_P8_S4		4:2
`define PRI_COL0_SG_P8_S4		1:0

`define PRI_COL0_G_P4_S8		4:3
`define PRI_COL0_SG_P4_S8		2:0

`define PRI_COL0_G_P2_S16		4
`define PRI_COL0_SG_P2_S16		3:0

`define PRI_COL0_SG_P0_S32		4:0

//Coluna 1
`define PRI_COL1_G_P32_S0		12:8

`define PRI_COL1_G_P16_S2		12:9
`define PRI_COL1_SG_P16_S2		8

`define PRI_COL1_G_P8_S4		12:10
`define PRI_COL1_SG_P8_S4		9:8

`define PRI_COL1_G_P4_S8		12:11
`define PRI_COL1_SG_P4_S8		10:8

`define PRI_COL1_G_P2_S16		12
`define PRI_COL1_SG_P2_S16		11:8

`define PRI_COL1_SG_P0_S32		12:8

//Coluna 2
`define PRI_COL2_G_P32_S0		20:16

`define PRI_COL2_G_P16_S2		20:17
`define PRI_COL2_SG_P16_S2		16

`define PRI_COL2_G_P8_S4		20:18
`define PRI_COL2_SG_P8_S4		17:16

`define PRI_COL2_G_P4_S8		20:19
`define PRI_COL2_SG_P4_S8		18:16

`define PRI_COL2_G_P2_S16		20
`define PRI_COL2_SG_P2_S16		19:16

`define PRI_COL2_SG_P0_S32		20:16

//Coluna 3
`define PRI_COL3_G_P32_S0		28:24

`define PRI_COL3_G_P16_S2		28:25
`define PRI_COL3_SG_P16_S2		24

`define PRI_COL3_G_P8_S4		28:26
`define PRI_COL3_SG_P8_S4		25:24

`define PRI_COL3_G_P4_S8		28:27
`define PRI_COL3_SG_P4_S8		26:24

`define PRI_COL3_G_P2_S16		28
`define PRI_COL3_SG_P2_S16		27:24

`define PRI_COL3_SG_P0_S32		28:24

//Posi��o das flags de estado das interrup��es
//F_FLAGS_COL<Numero da coluna>
`define F_FLAGS_COL0		7:5
`define F_FLAGS_COL1		15:13
`define F_FLAGS_COL2		23:21
`define F_FLAGS_COL3 	31:29

//Linhas da matriz onde estam armazenadas informa�oes sobre a interrup��o
//PRI_ROW_<Numero da interrup��o inferior>_<Numero da nterrup��o superior>
`define PRI_ROW_0_3		0
`define PRI_ROW_4_7		1
`define PRI_ROW_8_11		2
`define PRI_ROW_12_15	3
`define PRI_ROW_16_19	4
`define PRI_ROW_20_23	5
`define PRI_ROW_24_27	6
`define PRI_ROW_28_31	7

/***********************************************************************************************************************
/*********** Registo AIRCR(Application Interrupt and Reset Control Register)
/***********************************************************************************************************************/	
reg [31:0] AIRCR; 
`define PRIGROUP	2:0	//Cursor de divis�o dos registos PRI em prioridades e sub-prioridades

//Configura��es do cursor de divisao dos registos PRI em prioridades e sub-prioridades
`define PRIO_P32_S0 		3'b000		//32 Prioridades e 0 Sub-prioridades
`define PRIO_P16_S2 		3'b001		//16 Prioridades e 2 Sub-prioridades
`define PRIO_P8_S4		3'b010		//8 Prioridades e 4 Sub-prioridades
`define PRIO_P4_S8 		3'b011		//4 Prioridades e 8 Sub-prioridades
`define PRIO_P2_S16 		3'b100		//2 Prioridades e 16 Sub-prioridades
`define PRIO_P0_S32 		3'b101		//0 Prioridades e 32 Sub-prioridades


/***********************************************************************************************************************
/*********** Registo ICSR(Interrupt Control State Register) Nota: N�o ser� feito agora
/***********************************************************************************************************************/	 
/*reg [31:0] ICSR;	//For�a a ficar pendente a Non-Maskable Interrupt (NMI);											X
						//Indica as exce��es pendentes;
						//Indica o n�mero do vetor da exce��o pendente com maior prioridade;
						//Indica o n�mero do vetor da exce��o ativa.
	

`define NMIPENDSET	31		//Altera o bit da NMI(W):																			X
									//1 - For�a a NMI a ficar pendente
									//0 - N�o for�a a ficar pendente
									//No caso de a NMI ser for�ada a ficar pendente vai ficar logo 
									//ativa porque � uma interrup��o com a maior prioridade.

//[30:23]Reservado: l�-se '0'; deve ser escrito com '0'(R).

`define ISRPENDING	22		//Indica se alguma interrup��o est� pendente (NMI exclusive)(R). 						X

`define VECTPENDING	21:12	//Indica o n�mero da interrup��o pendente de maior prioridade(R).						V

`define RETTOBASE		11		//Indica se a interrup��o atualmente em execu��o � a �nica interrup��o ativa(R). V

//[10:9]Reservado: l�-se '0'; deve ser escrito com '0'(R).

`define VECTACTIVE	8:0	//Indica o n�mero da exce��o que est� em execu��o (NMI inclusive). 					X
									//Para obter o n�mero da interrup��o basta subtrair 2 ao campo VECTACTIVE(R).
*/

/***********************************************************************************************************************
/*********** Registo STIR (Software Trigger Interrupt Register) Nota: lido pelo bloco always
/***********************************************************************************************************************/	 
wire [31:0] STIR;		//Gera interrup��es por software
						
`define INTID	4:0	//INTID	[4:0] ID da interrup��o para despoletar (0-31) (W).
							//Por exemplo, escrever 0x03 vai gerar a IRQ3 por software.
							
//[31:5]Reservado: l�-se '0'; deve ser escrito com '0' (R).


/***********************************************************************************************************************
/*********** Registo ABR (Active Bit Register) Nota: Mapeado registo IPR
/***********************************************************************************************************************/	
/*wire [31:0] ABR;	//Sub-registo ACTIVE	[31:0] Flags que representam se as interrup��es est�o ativas (R).
						//1 - Interrup��o ativa
						//0 - Interrup��o inativa
*/
						
/***********************************************************************************************************************
/*********** Registo SBR (Stacked Bit Register) Nota: Mapeado registo IPR
/***********************************************************************************************************************/	
/*wire [31:0] SBR;	//Sub-registo STACKED [31:0] Flags que representam se as interrup��es est�o stacked (R):
						//1 - Interrup��o stacked
						//0 - Interrup��o n�o stacked
*/
						
/***********************************************************************************************************************
/*********** Registo  CIS(Current Interrupt State)
/***********************************************************************************************************************/	
reg [31:0] CIS;	

`define ETMINTSTAT	1:0	//Indica o estado da interrup��o no ciclo atual (R) -> Implementar no bloco always
									//00 - Sem estado
									//01 - Entrada na interrup��o
									//10 - Sa�da da interrup��o
									//11 - Fazer o stacking e o fetching do vetor de interrup��o. 
									//O ETMINTSTAT entrada/retorno � atribu�do no 1� ciclo do novo contexto da interrup��o.
//[10:2] -> Reservado

`define ETMINTNUM		15:11	//Indica o n�mero da interrup��o em execu��o (0-31) (R) -> Implementar no bloco always


//[15:12] -> Reservado

`define CURRPRI 		21:16 //Indica a prioridade da interrup��o em execu��o. Configurada nos registos IPR (R) -> Implementar no bloco always

`define VECTPENDING	31:22	//Indica o n�mero da interrup��o pendente de maior prioridade(R) -> Implementar no bloco always


/***********************************************************************************************************************
/*********** Implementa��o do Registo IPR(Interrupt Priority Registers)
/***********************************************************************************************************************/	
wire [4:0] g_priority [31:0];		//Prioridades de grupo

wire [4:0] sg_priority [31:0];	//Prioridades de sub-grupo

assign {g_priority[0], sg_priority[0]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL0_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL0_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL0_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL0_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL0_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[1], sg_priority[1]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL1_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL1_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL1_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL1_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[2], sg_priority[2]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL2_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL2_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL2_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[3], sg_priority[3]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_0_3][`PRI_COL3_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_0_3][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_0_3][`PRI_COL3_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_0_3][`PRI_COL3_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_0_3][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_0_3][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;
					
					

assign {g_priority[4], sg_priority[4]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL0_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL0_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL0_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL0_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL0_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[5], sg_priority[5]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL1_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL1_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL1_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL1_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[6], sg_priority[6]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL2_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL2_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL2_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[7], sg_priority[7]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_4_7][`PRI_COL3_G_P32_S0], 	5'b00000														}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_4_7][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_4_7][`PRI_COL3_G_P8_S4], 		3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_4_7][`PRI_COL3_G_P4_S8], 		2'b00, 	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_4_7][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_4_7][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;


assign {g_priority[8], sg_priority[8]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL0_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL0_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL0_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[9], sg_priority[9]}	=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL1_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL1_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[10], sg_priority[10]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[11], sg_priority[11]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_8_11][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_8_11][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_8_11][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P8_S4]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_8_11][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P4_S8]		}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_8_11][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_8_11][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;
														

assign {g_priority[12], sg_priority[12]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[13], sg_priority[13]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL1_SG_P0_S32]																	}	:
														10'b0000000000;

assign {g_priority[14], sg_priority[14]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[15], sg_priority[15]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_12_15][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_12_15][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_12_15][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_12_15][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_12_15][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_12_15][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;
														

assign {g_priority[16], sg_priority[16]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[17], sg_priority[17]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[18], sg_priority[18]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[19], sg_priority[19]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_16_19][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_16_19][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_16_19][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_16_19][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_16_19][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_16_19][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;		


assign {g_priority[20], sg_priority[20]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL0_SG_P0_S32]																	}	:
														10'b0000000000;
														
assign {g_priority[21], sg_priority[21]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[22], sg_priority[22]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[23], sg_priority[23]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_20_23][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_20_23][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_20_23][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_20_23][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_20_23][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_20_23][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;	


assign {g_priority[24], sg_priority[24]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[25], sg_priority[25]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL1_SG_P0_S32]																	}	:
														10'b0000000000;

assign {g_priority[26], sg_priority[26]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[27], sg_priority[27]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_24_27][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_24_27][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_24_27][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_24_27][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_24_27][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_24_27][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;


assign {g_priority[28], sg_priority[28]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL0_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL0_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL0_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL0_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL0_G_P2_S16],	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL0_SG_P0_S32]																		}	:
														10'b0000000000;
														
assign {g_priority[29], sg_priority[29]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL1_G_P32_S0],	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL1_G_P16_S2],	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL1_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL1_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL1_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL1_SG_P0_S32]																		}	:
														10'b0000000000;				

assign {g_priority[30], sg_priority[30]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL2_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL2_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL2_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL2_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL2_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL2_SG_P0_S32]																		}	:
														10'b0000000000;

assign {g_priority[31], sg_priority[31]}=	(AIRCR[`PRIGROUP] == `PRIO_P32_S0)	?	{				IPR[`PRI_ROW_28_31][`PRI_COL3_G_P32_S0], 	5'b00000															}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P16_S2)	?	{1'b0, 		IPR[`PRI_ROW_28_31][`PRI_COL3_G_P16_S2], 	4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P16_S2]	}	:	
														(AIRCR[`PRIGROUP] == `PRIO_P8_S4)	?	{2'b00, 		IPR[`PRI_ROW_28_31][`PRI_COL3_G_P8_S4], 	3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P8_S4]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P4_S8)	?	{3'b000, 	IPR[`PRI_ROW_28_31][`PRI_COL3_G_P4_S8], 	2'b00, 	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P4_S8]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P2_S16)	?	{4'b0000,	IPR[`PRI_ROW_28_31][`PRI_COL3_G_P2_S16], 	1'b0, 	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P2_S16]	}	:
														(AIRCR[`PRIGROUP] == `PRIO_P0_S32)	?	{5'b00000,	IPR[`PRI_ROW_28_31][`PRI_COL3_SG_P0_S32]																		}	:
														10'b0000000000;														



wire [2:0] priority_flags [31:0];//Flags do estado das prioridades ([0]->ENABLE, [1]->PENDING, [2]->STACKED)
`define	F_ENABLE 	0
`define	F_PENDING 	1
`define	F_STACKED 	2

assign priority_flags[0] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL0];
assign priority_flags[1] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL1];
assign priority_flags[2] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL2];
assign priority_flags[3] 	= IPR[`PRI_ROW_0_3][`F_FLAGS_COL3];

assign priority_flags[4] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL0];
assign priority_flags[5] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL1];
assign priority_flags[6] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL2];
assign priority_flags[7] 	= IPR[`PRI_ROW_4_7][`F_FLAGS_COL3];

assign priority_flags[8] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL0];
assign priority_flags[9] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL1];
assign priority_flags[10] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL2];
assign priority_flags[11] 	= IPR[`PRI_ROW_8_11][`F_FLAGS_COL3];

assign priority_flags[12] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL0];
assign priority_flags[13] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL1];
assign priority_flags[14] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL2];
assign priority_flags[15] 	= IPR[`PRI_ROW_12_15][`F_FLAGS_COL3];

assign priority_flags[16] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL0];
assign priority_flags[17] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL1];
assign priority_flags[18] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL2];
assign priority_flags[19] 	= IPR[`PRI_ROW_16_19][`F_FLAGS_COL3];

assign priority_flags[20] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL0];
assign priority_flags[21] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL1];
assign priority_flags[22] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL2];
assign priority_flags[23] 	= IPR[`PRI_ROW_20_23][`F_FLAGS_COL3];

assign priority_flags[24] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL0];
assign priority_flags[25] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL1];
assign priority_flags[26] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL2];
assign priority_flags[27] 	= IPR[`PRI_ROW_24_27][`F_FLAGS_COL3];

assign priority_flags[28] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL0];
assign priority_flags[29] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL1];
assign priority_flags[30] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL2];
assign priority_flags[31] 	= IPR[`PRI_ROW_28_31][`F_FLAGS_COL3];

/***********************************************************************************************************************
/*********** Arvore de decis�o PENDING
/***********************************************************************************************************************/
//Nivel 1
wire [4:0] pending_level_1 [15:0];


assign pending_level_1[0] =	({~priority_flags[0][`F_ENABLE], ~priority_flags[0][`F_PENDING], g_priority[0], sg_priority[0]} <= 
										{~priority_flags[1][`F_ENABLE], ~priority_flags[1][`F_PENDING], g_priority[1], sg_priority[1]})	
										?	5'd0	:	5'd1;
										
assign pending_level_1[1] =	({~priority_flags[2][`F_ENABLE], ~priority_flags[2][`F_PENDING], g_priority[2], sg_priority[2]} <= 
										{~priority_flags[3][`F_ENABLE], ~priority_flags[3][`F_PENDING], g_priority[3], sg_priority[3]})	
										?	5'd2	:	5'd3;


assign pending_level_1[2] =	({~priority_flags[4][`F_ENABLE], ~priority_flags[4][`F_PENDING], g_priority[4], sg_priority[4]} <= 
										{~priority_flags[5][`F_ENABLE], ~priority_flags[5][`F_PENDING], g_priority[5], sg_priority[5]})	
										?	5'd4	:	5'd5;
										
assign pending_level_1[3] =	({~priority_flags[6][`F_ENABLE], ~priority_flags[6][`F_PENDING], g_priority[6], sg_priority[6]} <= 
										{~priority_flags[7][`F_ENABLE], ~priority_flags[7][`F_PENDING], g_priority[7], sg_priority[7]})	
										?	5'd6	:	5'd7;


assign pending_level_1[4] =	({~priority_flags[8][`F_ENABLE], ~priority_flags[8][`F_PENDING], g_priority[8], sg_priority[8]} <= 
										{~priority_flags[9][`F_ENABLE], ~priority_flags[9][`F_PENDING], g_priority[9], sg_priority[9]})	
										?	5'd8	:	5'd9;
										
assign pending_level_1[5] =	({~priority_flags[10][`F_ENABLE], ~priority_flags[10][`F_PENDING], g_priority[10], sg_priority[10]} <= 
										{~priority_flags[11][`F_ENABLE], ~priority_flags[11][`F_PENDING], g_priority[11], sg_priority[11]})	
										?	5'd10	:	5'd11;
										

assign pending_level_1[6] =	({~priority_flags[12][`F_ENABLE], ~priority_flags[12][`F_PENDING], g_priority[12], sg_priority[12]} <= 
										{~priority_flags[13][`F_ENABLE], ~priority_flags[13][`F_PENDING], g_priority[13], sg_priority[13]})	
										?	5'd12	:	5'd13;
										
assign pending_level_1[7] =	({~priority_flags[14][`F_ENABLE], ~priority_flags[14][`F_PENDING], g_priority[14], sg_priority[14]} <= 
										{~priority_flags[15][`F_ENABLE], ~priority_flags[15][`F_PENDING], g_priority[15], sg_priority[15]})	
										?	5'd14	:	5'd15;
										
										
assign pending_level_1[8] =	({~priority_flags[16][`F_ENABLE], ~priority_flags[16][`F_PENDING], g_priority[16], sg_priority[16]} <= 
										{~priority_flags[17][`F_ENABLE], ~priority_flags[17][`F_PENDING], g_priority[17], sg_priority[17]})	
										?	5'd16	:	5'd17;
										
assign pending_level_1[9] =	({~priority_flags[18][`F_ENABLE], ~priority_flags[18][`F_PENDING], g_priority[18], sg_priority[18]} <= 
										{~priority_flags[19][`F_ENABLE], ~priority_flags[19][`F_PENDING], g_priority[19], sg_priority[19]})	
										?	5'd18	:	5'd19;


assign pending_level_1[10] =	({~priority_flags[20][`F_ENABLE], ~priority_flags[20][`F_PENDING], g_priority[20], sg_priority[20]} <= 
										{~priority_flags[21][`F_ENABLE], ~priority_flags[21][`F_PENDING], g_priority[21], sg_priority[21]})	
										?	5'd20	:	5'd21;
										
assign pending_level_1[11] =	({~priority_flags[22][`F_ENABLE], ~priority_flags[22][`F_PENDING], g_priority[22], sg_priority[22]} <= 
										{~priority_flags[23][`F_ENABLE], ~priority_flags[23][`F_PENDING], g_priority[23], sg_priority[23]})	
										?	5'd22	:	5'd23;


assign pending_level_1[12] =	({~priority_flags[24][`F_ENABLE], ~priority_flags[24][`F_PENDING], g_priority[24], sg_priority[24]} <= 
										{~priority_flags[25][`F_ENABLE], ~priority_flags[25][`F_PENDING], g_priority[25], sg_priority[25]})	
										?	5'd24	:	5'd25;
										
assign pending_level_1[13] =	({~priority_flags[26][`F_ENABLE], ~priority_flags[26][`F_PENDING], g_priority[26], sg_priority[26]} <= 
										{~priority_flags[27][`F_ENABLE], ~priority_flags[27][`F_PENDING], g_priority[27], sg_priority[27]})	
										?	5'd26	:	5'd27;	
										

assign pending_level_1[14] =	({~priority_flags[28][`F_ENABLE], ~priority_flags[28][`F_PENDING], g_priority[28], sg_priority[28]} <= 
										{~priority_flags[29][`F_ENABLE], ~priority_flags[29][`F_PENDING], g_priority[29], sg_priority[29]})	
										?	5'd28	:	5'd29;
										
assign pending_level_1[15] =	({~priority_flags[30][`F_ENABLE], ~priority_flags[30][`F_PENDING], g_priority[30], sg_priority[30]} <= 
										{~priority_flags[31][`F_ENABLE], ~priority_flags[31][`F_PENDING], g_priority[31], sg_priority[31]})	
										?	5'd30	:	5'd31;	
	
	
//Nivel 2
wire [4:0] pending_level_2 [7:0];


assign pending_level_2[0] =	({~priority_flags[pending_level_1[0]][`F_ENABLE], 	~priority_flags[pending_level_1[0]][`F_PENDING], g_priority[pending_level_1[0]], sg_priority[pending_level_1[0]]} <= 
										{~priority_flags[pending_level_1[1]][`F_ENABLE], 	~priority_flags[pending_level_1[1]][`F_PENDING], g_priority[pending_level_1[1]], sg_priority[pending_level_1[1]]})	
										?	pending_level_1[0]	:	pending_level_1[1];							

assign pending_level_2[1] =	({~priority_flags[pending_level_1[2]][`F_ENABLE], 	~priority_flags[pending_level_1[2]][`F_PENDING], g_priority[pending_level_1[2]], sg_priority[pending_level_1[2]]} <= 
										{~priority_flags[pending_level_1[3]][`F_ENABLE], 	~priority_flags[pending_level_1[3]][`F_PENDING], g_priority[pending_level_1[3]], sg_priority[pending_level_1[3]]})	
										?	pending_level_1[2]	:	pending_level_1[3];
										
assign pending_level_2[2] =	({~priority_flags[pending_level_1[4]][`F_ENABLE], 	~priority_flags[pending_level_1[4]][`F_PENDING], g_priority[pending_level_1[4]], sg_priority[pending_level_1[4]]} <= 
										{~priority_flags[pending_level_1[5]][`F_ENABLE], 	~priority_flags[pending_level_1[5]][`F_PENDING], g_priority[pending_level_1[5]], sg_priority[pending_level_1[5]]})	
										?	pending_level_1[4]	:	pending_level_1[5];									

assign pending_level_2[3] =	({~priority_flags[pending_level_1[6]][`F_ENABLE], 	~priority_flags[pending_level_1[6]][`F_PENDING], g_priority[pending_level_1[6]], sg_priority[pending_level_1[6]]} <= 
										{~priority_flags[pending_level_1[7]][`F_ENABLE], 	~priority_flags[pending_level_1[7]][`F_PENDING], g_priority[pending_level_1[7]], sg_priority[pending_level_1[7]]})	
										?	pending_level_1[6]	:	pending_level_1[7];
										
assign pending_level_2[4] =	({~priority_flags[pending_level_1[8]][`F_ENABLE], 	~priority_flags[pending_level_1[8]][`F_PENDING], g_priority[pending_level_1[8]], sg_priority[pending_level_1[8]]} <= 
										{~priority_flags[pending_level_1[9]][`F_ENABLE], 	~priority_flags[pending_level_1[9]][`F_PENDING], g_priority[pending_level_1[9]], sg_priority[pending_level_1[9]]})	
										?	pending_level_1[8]	:	pending_level_1[9];							

assign pending_level_2[5] =	({~priority_flags[pending_level_1[10]][`F_ENABLE], ~priority_flags[pending_level_1[10]][`F_PENDING], g_priority[pending_level_1[10]], sg_priority[pending_level_1[10]]} <= 
										{~priority_flags[pending_level_1[11]][`F_ENABLE], 	~priority_flags[pending_level_1[11]][`F_PENDING], g_priority[pending_level_1[11]], sg_priority[pending_level_1[11]]})	
										?	pending_level_1[10]	:	pending_level_1[11];
										
assign pending_level_2[6] =	({~priority_flags[pending_level_1[12]][`F_ENABLE], ~priority_flags[pending_level_1[12]][`F_PENDING], g_priority[pending_level_1[12]], sg_priority[pending_level_1[12]]} <= 
										{~priority_flags[pending_level_1[13]][`F_ENABLE], 	~priority_flags[pending_level_1[13]][`F_PENDING], g_priority[pending_level_1[13]], sg_priority[pending_level_1[13]]})	
										?	pending_level_1[12]	:	pending_level_1[13];									

assign pending_level_2[7] =	({~priority_flags[pending_level_1[14]][`F_ENABLE], ~priority_flags[pending_level_1[14]][`F_PENDING], g_priority[pending_level_1[14]], sg_priority[pending_level_1[14]]} <= 
										{~priority_flags[pending_level_1[15]][`F_ENABLE], 	~priority_flags[pending_level_1[15]][`F_PENDING], g_priority[pending_level_1[15]], sg_priority[pending_level_1[15]]})	
										?	pending_level_1[14]	:	pending_level_1[15];


//Nivel 3T
wire [4:0] pending_level_3 [3:0];


assign pending_level_3[0] =	({~priority_flags[pending_level_2[0]][`F_ENABLE], 	~priority_flags[pending_level_2[0]][`F_PENDING], g_priority[pending_level_2[0]], sg_priority[pending_level_2[0]]} <= 
										{~priority_flags[pending_level_2[1]][`F_ENABLE], 	~priority_flags[pending_level_2[1]][`F_PENDING], g_priority[pending_level_2[1]], sg_priority[pending_level_2[1]]})	
										?	pending_level_2[0]	:	pending_level_2[1];							

assign pending_level_3[1] =	({~priority_flags[pending_level_2[2]][`F_ENABLE], 	~priority_flags[pending_level_2[2]][`F_PENDING], g_priority[pending_level_2[2]], sg_priority[pending_level_2[2]]} <= 
										{~priority_flags[pending_level_2[3]][`F_ENABLE], 	~priority_flags[pending_level_2[3]][`F_PENDING], g_priority[pending_level_2[3]], sg_priority[pending_level_2[3]]})	
										?	pending_level_2[2]	:	pending_level_2[3];
										
assign pending_level_3[2] =	({~priority_flags[pending_level_2[4]][`F_ENABLE], 	~priority_flags[pending_level_2[4]][`F_PENDING], g_priority[pending_level_2[4]], sg_priority[pending_level_2[4]]} <= 
										{~priority_flags[pending_level_2[5]][`F_ENABLE], 	~priority_flags[pending_level_2[5]][`F_PENDING], g_priority[pending_level_2[5]], sg_priority[pending_level_2[5]]})	
										?	pending_level_2[4]	:	pending_level_2[5];									

assign pending_level_3[3] =	({~priority_flags[pending_level_2[6]][`F_ENABLE], 	~priority_flags[pending_level_2[6]][`F_PENDING], g_priority[pending_level_2[6]], sg_priority[pending_level_2[6]]} <= 
										{~priority_flags[pending_level_2[7]][`F_ENABLE], 	~priority_flags[pending_level_2[7]][`F_PENDING], g_priority[pending_level_2[7]], sg_priority[pending_level_2[7]]})	
										?	pending_level_2[6]	:	pending_level_2[7];
										


//Nivel 4
wire [4:0] pending_level_4 [1:0];


assign pending_level_4[0] =	({~priority_flags[pending_level_3[0]][`F_ENABLE], 	~priority_flags[pending_level_3[0]][`F_PENDING], g_priority[pending_level_3[0]], sg_priority[pending_level_3[0]]} <= 
										{~priority_flags[pending_level_3[1]][`F_ENABLE], 	~priority_flags[pending_level_3[1]][`F_PENDING], g_priority[pending_level_3[1]], sg_priority[pending_level_3[1]]})	
										?	pending_level_3[0]	:	pending_level_3[1];							

assign pending_level_4[1] =	({~priority_flags[pending_level_3[2]][`F_ENABLE], 	~priority_flags[pending_level_3[2]][`F_PENDING], g_priority[pending_level_3[2]], sg_priority[pending_level_3[2]]} <= 
										{~priority_flags[pending_level_3[3]][`F_ENABLE], 	~priority_flags[pending_level_3[3]][`F_PENDING], g_priority[pending_level_3[3]], sg_priority[pending_level_3[3]]})	
										?	pending_level_3[2]	:	pending_level_3[3];

										
//Nivel 4
wire [4:0] pending_level_5;


assign pending_level_5 =	({~priority_flags[pending_level_4[0]][`F_ENABLE], 	~priority_flags[pending_level_4[0]][`F_PENDING], g_priority[pending_level_4[0]], sg_priority[pending_level_4[0]]} <= 
									{~priority_flags[pending_level_4[1]][`F_ENABLE], 	~priority_flags[pending_level_4[1]][`F_PENDING], g_priority[pending_level_4[1]], sg_priority[pending_level_4[1]]})	
									?	pending_level_4[0]	:	pending_level_4[1];							


/***********************************************************************************************************************
/*********** Arvore de decis�o STACKED
/***********************************************************************************************************************/
//Nivel 1
wire [4:0] stacked_level_1 [15:0];


assign stacked_level_1[0] =	({~priority_flags[0][`F_ENABLE], ~priority_flags[0][`F_STACKED], g_priority[0], sg_priority[0]} <= 
										{~priority_flags[1][`F_ENABLE], ~priority_flags[1][`F_STACKED], g_priority[1], sg_priority[1]})	
										?	5'd0	:	5'd1;
										
assign stacked_level_1[1] =	({~priority_flags[2][`F_ENABLE], ~priority_flags[2][`F_STACKED], g_priority[2], sg_priority[2]} <= 
										{~priority_flags[3][`F_ENABLE], ~priority_flags[3][`F_STACKED], g_priority[3], sg_priority[3]})	
										?	5'd2	:	5'd3;


assign stacked_level_1[2] =	({~priority_flags[4][`F_ENABLE], ~priority_flags[4][`F_STACKED], g_priority[4], sg_priority[4]} <= 
										{~priority_flags[5][`F_ENABLE], ~priority_flags[5][`F_STACKED], g_priority[5], sg_priority[5]})	
										?	5'd4	:	5'd5;
										
assign stacked_level_1[3] =	({~priority_flags[6][`F_ENABLE], ~priority_flags[6][`F_STACKED], g_priority[6], sg_priority[6]} <= 
										{~priority_flags[7][`F_ENABLE], ~priority_flags[7][`F_STACKED], g_priority[7], sg_priority[7]})	
										?	5'd6	:	5'd7;


assign stacked_level_1[4] =	({~priority_flags[8][`F_ENABLE], ~priority_flags[8][`F_STACKED], g_priority[8], sg_priority[8]} <= 
										{~priority_flags[9][`F_ENABLE], ~priority_flags[9][`F_STACKED], g_priority[9], sg_priority[9]})	
										?	5'd8	:	5'd9;
										
assign stacked_level_1[5] =	({~priority_flags[10][`F_ENABLE], ~priority_flags[10][`F_STACKED], g_priority[10], sg_priority[10]} <= 
										{~priority_flags[11][`F_ENABLE], ~priority_flags[11][`F_STACKED], g_priority[11], sg_priority[11]})	
										?	5'd10	:	5'd11;
										

assign stacked_level_1[6] =	({~priority_flags[12][`F_ENABLE], ~priority_flags[12][`F_STACKED], g_priority[12], sg_priority[12]} <= 
										{~priority_flags[13][`F_ENABLE], ~priority_flags[13][`F_STACKED], g_priority[13], sg_priority[13]})	
										?	5'd12	:	5'd13;
										
assign stacked_level_1[7] =	({~priority_flags[14][`F_ENABLE], ~priority_flags[14][`F_STACKED], g_priority[14], sg_priority[14]} <= 
										{~priority_flags[15][`F_ENABLE], ~priority_flags[15][`F_STACKED], g_priority[15], sg_priority[15]})	
										?	5'd14	:	5'd15;
										
										
assign stacked_level_1[8] =	({~priority_flags[16][`F_ENABLE], ~priority_flags[16][`F_STACKED], g_priority[16], sg_priority[16]} <= 
										{~priority_flags[17][`F_ENABLE], ~priority_flags[17][`F_STACKED], g_priority[17], sg_priority[17]})	
										?	5'd16	:	5'd17;
										
assign stacked_level_1[9] =	({~priority_flags[18][`F_ENABLE], ~priority_flags[18][`F_STACKED], g_priority[18], sg_priority[18]} <= 
										{~priority_flags[19][`F_ENABLE], ~priority_flags[19][`F_STACKED], g_priority[19], sg_priority[19]})	
										?	5'd18	:	5'd19;


assign stacked_level_1[10] =	({~priority_flags[20][`F_ENABLE], ~priority_flags[20][`F_STACKED], g_priority[20], sg_priority[20]} <= 
										{~priority_flags[21][`F_ENABLE], ~priority_flags[21][`F_STACKED], g_priority[21], sg_priority[21]})	
										?	5'd20	:	5'd21;
										
assign stacked_level_1[11] =	({~priority_flags[22][`F_ENABLE], ~priority_flags[22][`F_STACKED], g_priority[22], sg_priority[22]} <= 
										{~priority_flags[23][`F_ENABLE], ~priority_flags[23][`F_STACKED], g_priority[23], sg_priority[23]})	
										?	5'd22	:	5'd23;


assign stacked_level_1[12] =	({~priority_flags[24][`F_ENABLE], ~priority_flags[24][`F_STACKED], g_priority[24], sg_priority[24]} <= 
										{~priority_flags[25][`F_ENABLE], ~priority_flags[25][`F_STACKED], g_priority[25], sg_priority[25]})	
										?	5'd24	:	5'd25;
										
assign stacked_level_1[13] =	({~priority_flags[26][`F_ENABLE], ~priority_flags[26][`F_STACKED], g_priority[26], sg_priority[26]} <= 
										{~priority_flags[27][`F_ENABLE], ~priority_flags[27][`F_STACKED], g_priority[27], sg_priority[27]})	
										?	5'd26	:	5'd27;	
										

assign stacked_level_1[14] =	({~priority_flags[28][`F_ENABLE], ~priority_flags[28][`F_STACKED], g_priority[28], sg_priority[28]} <= 
										{~priority_flags[29][`F_ENABLE], ~priority_flags[29][`F_STACKED], g_priority[29], sg_priority[29]})	
										?	5'd28	:	5'd29;
										
assign stacked_level_1[15] =	({~priority_flags[30][`F_ENABLE], ~priority_flags[30][`F_STACKED], g_priority[30], sg_priority[30]} <= 
										{~priority_flags[31][`F_ENABLE], ~priority_flags[31][`F_STACKED], g_priority[31], sg_priority[31]})	
										?	5'd30	:	5'd31;	
	
	
//Nivel 2
wire [4:0] stacked_level_2 [7:0];

assign stacked_level_2[0] =	({~priority_flags[stacked_level_1[0]][`F_ENABLE], 	~priority_flags[stacked_level_1[0]][`F_STACKED], g_priority[stacked_level_1[0]], sg_priority[stacked_level_1[0]]} <= 
										{~priority_flags[stacked_level_1[1]][`F_ENABLE], 	~priority_flags[stacked_level_1[1]][`F_STACKED], g_priority[stacked_level_1[1]], sg_priority[stacked_level_1[1]]})	
										?	stacked_level_1[0]	:	stacked_level_1[1];							

assign stacked_level_2[1] =	({~priority_flags[stacked_level_1[2]][`F_ENABLE], 	~priority_flags[stacked_level_1[2]][`F_STACKED], g_priority[stacked_level_1[2]], sg_priority[stacked_level_1[2]]} <= 
										{~priority_flags[stacked_level_1[3]][`F_ENABLE], 	~priority_flags[stacked_level_1[3]][`F_STACKED], g_priority[stacked_level_1[3]], sg_priority[stacked_level_1[3]]})	
										?	stacked_level_1[2]	:	stacked_level_1[3];
										
assign stacked_level_2[2] =	({~priority_flags[stacked_level_1[4]][`F_ENABLE], 	~priority_flags[stacked_level_1[4]][`F_STACKED], g_priority[stacked_level_1[4]], sg_priority[stacked_level_1[4]]} <= 
										{~priority_flags[stacked_level_1[5]][`F_ENABLE], 	~priority_flags[stacked_level_1[5]][`F_STACKED], g_priority[stacked_level_1[5]], sg_priority[stacked_level_1[5]]})	
										?	stacked_level_1[4]	:	stacked_level_1[5];									

assign stacked_level_2[3] =	({~priority_flags[stacked_level_1[6]][`F_ENABLE], 	~priority_flags[stacked_level_1[6]][`F_STACKED], g_priority[stacked_level_1[6]], sg_priority[stacked_level_1[6]]} <= 
										{~priority_flags[stacked_level_1[7]][`F_ENABLE], 	~priority_flags[stacked_level_1[7]][`F_STACKED], g_priority[stacked_level_1[7]], sg_priority[stacked_level_1[7]]})	
										?	stacked_level_1[6]	:	stacked_level_1[7];
										
assign stacked_level_2[4] =	({~priority_flags[stacked_level_1[8]][`F_ENABLE], 	~priority_flags[stacked_level_1[8]][`F_STACKED], g_priority[stacked_level_1[8]], sg_priority[stacked_level_1[8]]} <= 
										{~priority_flags[stacked_level_1[9]][`F_ENABLE], 	~priority_flags[stacked_level_1[9]][`F_STACKED], g_priority[stacked_level_1[9]], sg_priority[stacked_level_1[9]]})	
										?	stacked_level_1[8]	:	stacked_level_1[9];							

assign stacked_level_2[5] =	({~priority_flags[stacked_level_1[10]][`F_ENABLE], ~priority_flags[stacked_level_1[10]][`F_STACKED], g_priority[stacked_level_1[10]], sg_priority[stacked_level_1[10]]} <= 
										{~priority_flags[stacked_level_1[11]][`F_ENABLE], 	~priority_flags[stacked_level_1[11]][`F_STACKED], g_priority[stacked_level_1[11]], sg_priority[stacked_level_1[11]]})	
										?	stacked_level_1[10]	:	stacked_level_1[11];
										
assign stacked_level_2[6] =	({~priority_flags[stacked_level_1[12]][`F_ENABLE], ~priority_flags[stacked_level_1[12]][`F_STACKED], g_priority[stacked_level_1[12]], sg_priority[stacked_level_1[12]]} <= 
										{~priority_flags[stacked_level_1[13]][`F_ENABLE], 	~priority_flags[stacked_level_1[13]][`F_STACKED], g_priority[stacked_level_1[13]], sg_priority[stacked_level_1[13]]})	
										?	stacked_level_1[12]	:	stacked_level_1[13];									

assign stacked_level_2[7] =	({~priority_flags[stacked_level_1[14]][`F_ENABLE], ~priority_flags[stacked_level_1[14]][`F_STACKED], g_priority[stacked_level_1[14]], sg_priority[stacked_level_1[14]]} <= 
										{~priority_flags[stacked_level_1[15]][`F_ENABLE], 	~priority_flags[stacked_level_1[15]][`F_STACKED], g_priority[stacked_level_1[15]], sg_priority[stacked_level_1[15]]})	
										?	stacked_level_1[14]	:	stacked_level_1[15];


//Nivel 3
wire [4:0] stacked_level_3 [3:0];


assign stacked_level_3[0] =	({~priority_flags[stacked_level_2[0]][`F_ENABLE], 	~priority_flags[stacked_level_2[0]][`F_STACKED],g_priority[stacked_level_2[0]], sg_priority[stacked_level_2[0]]} <= 
										{~priority_flags[stacked_level_2[1]][`F_ENABLE], 	~priority_flags[stacked_level_2[1]][`F_STACKED], g_priority[stacked_level_2[1]], sg_priority[stacked_level_2[1]]})	
										?	stacked_level_2[0]	:	stacked_level_2[1];							

assign stacked_level_3[1] =	({~priority_flags[stacked_level_2[2]][`F_ENABLE], 	~priority_flags[stacked_level_2[2]][`F_STACKED], g_priority[stacked_level_2[2]], sg_priority[stacked_level_2[2]]} <= 
										{~priority_flags[stacked_level_2[3]][`F_ENABLE], 	~priority_flags[stacked_level_2[3]][`F_STACKED], g_priority[stacked_level_2[3]], sg_priority[stacked_level_2[3]]})	
										?	stacked_level_2[2]	:	stacked_level_2[3];
										
assign stacked_level_3[2] =	({~priority_flags[stacked_level_2[4]][`F_ENABLE], 	~priority_flags[stacked_level_2[4]][`F_STACKED], g_priority[stacked_level_2[4]], sg_priority[stacked_level_2[4]]} <= 
										{~priority_flags[stacked_level_2[5]][`F_ENABLE], 	~priority_flags[stacked_level_2[5]][`F_STACKED], g_priority[stacked_level_2[5]], sg_priority[stacked_level_2[5]]})	
										?	stacked_level_2[4]	:	stacked_level_2[5];									

assign stacked_level_3[3] =	({~priority_flags[stacked_level_2[6]][`F_ENABLE], 	~priority_flags[stacked_level_2[6]][`F_STACKED], g_priority[stacked_level_2[6]], sg_priority[stacked_level_2[6]]} <= 
										{~priority_flags[stacked_level_2[7]][`F_ENABLE], 	~priority_flags[stacked_level_2[7]][`F_STACKED], g_priority[stacked_level_2[7]], sg_priority[stacked_level_2[7]]})	
										?	stacked_level_2[6]	:	stacked_level_2[7];
										


//Nivel 4
wire [4:0] stacked_level_4 [1:0];


assign stacked_level_4[0] =	({~priority_flags[stacked_level_3[0]][`F_ENABLE], 	~priority_flags[stacked_level_3[0]][`F_STACKED], g_priority[stacked_level_3[0]], sg_priority[stacked_level_3[0]]} <= 
										{~priority_flags[stacked_level_3[1]][`F_ENABLE], 	~priority_flags[stacked_level_3[1]][`F_STACKED], g_priority[stacked_level_3[1]], sg_priority[stacked_level_3[1]]})	
										?	stacked_level_3[0]	:	stacked_level_3[1];							

assign stacked_level_4[1] =	({~priority_flags[stacked_level_3[2]][`F_ENABLE], 	~priority_flags[stacked_level_3[2]][`F_STACKED], g_priority[stacked_level_3[2]], sg_priority[stacked_level_3[2]]} <= 
										{~priority_flags[stacked_level_3[3]][`F_ENABLE], 	~priority_flags[stacked_level_3[3]][`F_STACKED], g_priority[stacked_level_3[3]], sg_priority[stacked_level_3[3]]})	
										?	stacked_level_3[2]	:	stacked_level_3[3];

										
//Nivel 4
wire [4:0] stacked_level_5;


assign stacked_level_5 =	({~priority_flags[stacked_level_4[0]][`F_ENABLE], 	~priority_flags[stacked_level_4[0]][`F_STACKED], g_priority[stacked_level_4[0]], sg_priority[stacked_level_4[0]]} <= 
									{~priority_flags[stacked_level_4[1]][`F_ENABLE], 	~priority_flags[stacked_level_4[1]][`F_STACKED], g_priority[stacked_level_4[1]], sg_priority[stacked_level_4[1]]})	
									?	stacked_level_4[0]	:	stacked_level_4[1];	
										

initial
 begin
	IPR[0] <= 32'h00ff0000;
	IPR[1] <= 32'h00000020;
	IPR[2] <= 32'h00000000;
	IPR[3] <= 32'h00000000;
	IPR[4] <= 32'h00000000;
	IPR[5] <= 32'h00000000;
	IPR[6] <= 32'h00000000;
	IPR[7] <= 32'hBE000000;
	AIRCR <= {29'b00000000000000000000000000000, `PRIO_P2_S16};
 end
endmodule
